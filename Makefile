# Directories

BLD=_build
MOD=modules
FRM=formations

###############################################################################
#
# PANDOC
#
###############################################################################

#
# LOCAL_THEMES is a directory containing the themes
# by default it is themes/
#
ifeq ($(LOCAL_THEMES),)
	LOCAL_THEMES=../themes/
endif

# to get the themes:
# mkdir ../themes
# cd ../themes
# git clone https://github.com/hakimel/reveal.js.git
# git clone https://github.com/Wandmalfarbe/pandoc-latex-template.git

# Usually THEMES == LOCAL_THEMES, but this will change when we'll use docker
THEMES=$(LOCAL_THEMES)

# by default we use the local pandoc
PANDOC_BIN?=pandoc
PANDOC=$(PANDOC_BIN) --metadata=dlb:$(THEMES)

# if pandoc is not installed, let's use docker
ifeq (, $(shell which $(PANDOC)))
	DOCKER?=latest
endif

# even if pandoc is installed, we can use docker with `DOCKER=latest make`
ifneq ($(DOCKER),)
	DOCKER_THEMES=/root/themes
	# --privileged is necessary for people using SELinux
        # --rm removes the container after execution
	PANDOC=docker run --privileged --rm -it --volume `pwd`:/pandoc --volume $(LOCAL_THEME):$(DOCKER_THEME) dalibo/pandocker:$(DOCKER) --metadata=dlb:$(DOCKER_THEMES)
	THEMES=$(DOCKER_THEMES)
endif

#
# Pandoc Compilation Flags
#

MARKDOWN_FLAGS=-t markdown
REVEAL_FLAGS=-t revealjs --self-contained --standalone -V revealjs-url="$(THEMES)/reveal.js/" --template=templates/default.revealjs --variable transition="linear"
HTML_FLAGS=-t html5 --self-contained --standalone --toc --toc-depth=2 --template=templates/pandoc-uikit/uikit_template.html
PDF_FLAGS=--pdf-engine=xelatex --template=$(THEMES)/pandoc-latex-template/eisvogel.tex


#############################################################
#
# Modules
#
############################################################

#
# ADD NEW FORMAT HERE
# `FOO` will be replaced by the module id
#
MODULE_FORMATS = $(BLD)/$(MOD)/FOO/FOO.md
MODULE_FORMATS+= $(BLD)/$(MOD)/FOO/FOO.handout.html
MODULE_FORMATS+= $(BLD)/$(MOD)/FOO/FOO.handout.pdf
#MODULE_FORMATS+= $(BLD)/$(MOD)/FOO/FOO.handout.tex
MODULE_FORMATS+= $(BLD)/$(MOD)/FOO/FOO.slides.reveal.html

# /!\ beware of trailing whitespaces

CM1=cm1-decouvrir_postgresql
CM1_DST=$(BLD)/$(MOD)/cm1
CM1_SRC=$(MOD)/$(CM1)/header.md  $(shell cat $(MOD)/$(CM1)/SUMMARY.md)
cm1: $(subst FOO,cm1,$(MODULE_FORMATS))

TD1=td1
TD1_DST=$(BLD)/$(MOD)/td1
TD1_SRC=$(MOD)/$(TD1)/header.md  $(shell cat $(MOD)/$(TD1)/SUMMARY.md)
td1: $(subst FOO,td1,$(MODULE_FORMATS))

TP1=tp1
TP1_DST=$(BLD)/$(MOD)/tp1
TP1_SRC=$(MOD)/$(TP1)/header.md  $(shell cat $(MOD)/$(TP1)/SUMMARY.md)
tp1: $(subst FOO,tp1,$(MODULE_FORMATS))

CM2=cm2-transactions_acces_concurrents
CM2_DST=$(BLD)/$(MOD)/cm2
CM2_SRC=$(MOD)/$(CM2)/header.md  $(shell cat $(MOD)/$(CM2)/SUMMARY.md)
cm2: $(subst FOO,cm2,$(MODULE_FORMATS))

TD2=td2
TD2_DST=$(BLD)/$(MOD)/td2
TD2_SRC=$(MOD)/$(TD2)/header.md  $(shell cat $(MOD)/$(TD2)/SUMMARY.md)
td2: $(subst FOO,td2,$(MODULE_FORMATS))

TP2=tp2
TP2_DST=$(BLD)/$(MOD)/tp2
TP2_SRC=$(MOD)/$(TP2)/header.md  $(shell cat $(MOD)/$(TP2)/SUMMARY.md)
tp2: $(subst FOO,tp2,$(MODULE_FORMATS))

CM3=cm3-missions_du_DBA
CM3_DST=$(BLD)/$(MOD)/cm3
CM3_SRC=$(MOD)/$(CM3)/header.md  $(shell cat $(MOD)/$(CM3)/SUMMARY.md)
cm3: $(subst FOO,cm3,$(MODULE_FORMATS))

TD3=td3
TD3_DST=$(BLD)/$(MOD)/td3
TD3_SRC=$(MOD)/$(TD3)/header.md  $(shell cat $(MOD)/$(TD3)/SUMMARY.md)
td3: $(subst FOO,td3,$(MODULE_FORMATS))

TP3=tp3
TP3_DST=$(BLD)/$(MOD)/tp3
TP3_SRC=$(MOD)/$(TP3)/header.md  $(shell cat $(MOD)/$(TP3)/SUMMARY.md)
tp3: $(subst FOO,tp3,$(MODULE_FORMATS))

CM4=cm4-optimisation_et_index
CM4_DST=$(BLD)/$(MOD)/cm4
CM4_SRC=$(MOD)/$(CM4)/header.md  $(shell cat $(MOD)/$(CM4)/SUMMARY.md)
cm4: $(subst FOO,cm4,$(MODULE_FORMATS))

TD4=td4
TD4_DST=$(BLD)/$(MOD)/td4
TD4_SRC=$(MOD)/$(TD4)/header.md  $(shell cat $(MOD)/$(TD4)/SUMMARY.md)
td4: $(subst FOO,td4,$(MODULE_FORMATS))

TP4=tp4
TP4_DST=$(BLD)/$(MOD)/tp4
TP4_SRC=$(MOD)/$(TP4)/header.md  $(shell cat $(MOD)/$(TP4)/SUMMARY.md)
tp4: $(subst FOO,tp4,$(MODULE_FORMATS))

CM5=cm5-plpgsql-triggers
CM5_DST=$(BLD)/$(MOD)/cm5
CM5_SRC=$(MOD)/$(CM5)/header.md  $(shell cat $(MOD)/$(CM5)/SUMMARY.md)
cm5: $(subst FOO,cm5,$(MODULE_FORMATS))

TD5=td5
TD5_DST=$(BLD)/$(MOD)/td5
TD5_SRC=$(MOD)/$(TD5)/header.md  $(shell cat $(MOD)/$(TD5)/SUMMARY.md)
td5: $(subst FOO,td5,$(MODULE_FORMATS))

TP5=tp5
TP5_DST=$(BLD)/$(MOD)/tp5
TP5_SRC=$(MOD)/$(TP5)/header.md  $(shell cat $(MOD)/$(TP5)/SUMMARY.md)
tp5: $(subst FOO,tp5,$(MODULE_FORMATS))

# ADD NEW MODULE HERE
MODULES= cm1 td1 tp1 cm2 td2 tp2 cm3 td3 tp3 cm4 td4 tp4 cm5 td5 tp5


#############################################################
#
# Trainings / Formations
#
############################################################

#
# ADD NEW FORMAT HERE
# `BAR` will be replaced by the training id
#
TRAINING_FORMATS = $(BLD)/$(FRM)/BAR/BAR.md
#TRAINING_FORMATS+= $(BLD)/$(FRM)/BAR/BAR.handout.doc
# TRAINING_FORMATS+= $(BLD)/$(FRM)/BAR/BAR.handout.epub
TRAINING_FORMATS+= $(BLD)/$(FRM)/BAR/BAR.handout.html
#TRAINING_FORMATS+= $(BLD)/$(FRM)/BAR/BAR.handout.peecho.pdf
TRAINING_FORMATS+= $(BLD)/$(FRM)/BAR/BAR.handout.pdf
#TRAINING_FORMATS+= $(BLD)/$(FRM)/BAR/BAR.handout.tex
#TRAINING_FORMATS+= $(BLD)/$(FRM)/BAR/BAR.slides.beamer.tex
#TRAINING_FORMATS+= $(BLD)/$(FRM)/BAR/BAR.slides.beamer.pdf
TRAINING_FORMATS+= $(BLD)/$(FRM)/BAR/BAR.slides.reveal.html
#TRAINING_FORMATS+= $(BLD)/$(FRM)/BAR/BAR.slides.reveal_local.html

SEMAINE1_DST=$(BLD)/$(FRM)/semaine1
SEMAINE1_SRC=$(FRM)/semaine1/header.md  $(shell cat $(FRM)/semaine1/SUMMARY.md)
semaine1: $(subst BAR,semaine1,$(TRAINING_FORMATS))

SEMAINE2_DST=$(BLD)/$(FRM)/semaine2
SEMAINE2_SRC=$(FRM)/semaine2/header.md  $(shell cat $(FRM)/semaine2/SUMMARY.md)
semaine2: $(subst BAR,semaine2,$(TRAINING_FORMATS))

SEMAINE3_DST=$(BLD)/$(FRM)/semaine3
SEMAINE3_SRC=$(FRM)/semaine3/header.md  $(shell cat $(FRM)/semaine3/SUMMARY.md)
semaine3: $(subst BAR,semaine3,$(TRAINING_FORMATS))

SEMAINE4_DST=$(BLD)/$(FRM)/semaine4
SEMAINE4_SRC=$(FRM)/semaine4/header.md  $(shell cat $(FRM)/semaine4/SUMMARY.md)
semaine4: $(subst BAR,semaine4,$(TRAINING_FORMATS))

SEMAINE5_DST=$(BLD)/$(FRM)/semaine5
SEMAINE5_SRC=$(FRM)/semaine5/header.md  $(shell cat $(FRM)/semaine5/SUMMARY.md)
semaine5: $(subst BAR,semaine5,$(TRAINING_FORMATS))

COMPLET_DST=$(BLD)/$(FRM)/complet
COMPLET_SRC=$(FRM)/complet/header.md  $(shell cat $(FRM)/complet/SUMMARY.md)
complet: $(subst BAR,complet,$(TRAINING_FORMATS))


# ADD NEW TRAINING HERE
TRAININGS= semaine1 semaine2 semaine3 semaine4 semaine5 complet

#############################################################
#
# Markdown
#
# this export format is here to build intermediate files
# that will be used to export the other file formats
#
#############################################################

#
# MD_OBJ is the list of all the potential built markdown objects
# We'll use this list later to procuce the other lists of objects
#
MODULES_MD_OBJ = $(CM1_DST)/cm1.md $(TD1_DST)/td1.md  $(TP1_DST)/tp1.md
MODULES_MD_OBJ+= $(CM2_DST)/cm2.md $(TD2_DST)/td2.md  $(TP2_DST)/tp2.md
MODULES_MD_OBJ+= $(CM3_DST)/cm3.md $(TD3_DST)/td3.md  $(TP3_DST)/tp3.md
MODULES_MD_OBJ+= $(CM4_DST)/cm4.md $(TD4_DST)/td4.md  $(TP4_DST)/tp4.md
MODULES_MD_OBJ+= $(CM5_DST)/cm5.md $(TD5_DST)/td5.md  $(TP5_DST)/tp5.md
# ADD NEW MODULE HERE

TRAININGS_MD_OBJ = $(SEMAINE1_DST)/semaine1.md $(SEMAINE2_DST)/semaine2.md
TRAININGS_MD_OBJ+= $(SEMAINE3_DST)/semaine3.md $(SEMAINE4_DST)/semaine4.md
TRAININGS_MD_OBJ+= $(SEMAINE5_DST)/semaine5.md $(COMPLET_DST)/complet.md
# ADD NEW TRAINING HERE

MD_OBJ = $(MODULES_MD_OBJ)
MD_OBJ+= $(TRAININGS_MD_OBJ)

# Ensure that the intermediate markdown files are deleted automatically after the build
.INTERMEDIATE: $(MD_OBJ)

# modules
$(CM1_DST)/cm1.md: $(CM1_SRC)
$(TD1_DST)/td1.md: $(TD1_SRC)
$(TP1_DST)/tp1.md: $(TP1_SRC)
$(CM2_DST)/cm2.md: $(CM2_SRC)
$(TD2_DST)/td2.md: $(TD2_SRC)
$(TP2_DST)/tp2.md: $(TP2_SRC)
$(CM3_DST)/cm3.md: $(CM3_SRC)
$(TD3_DST)/td3.md: $(TD3_SRC)
$(TP3_DST)/tp3.md: $(TP3_SRC)
$(CM4_DST)/cm4.md: $(CM4_SRC)
$(TD4_DST)/td4.md: $(TD4_SRC)
$(TP4_DST)/tp4.md: $(TP4_SRC)
$(CM5_DST)/cm5.md: $(CM5_SRC)
$(TD5_DST)/td5.md: $(TD5_SRC)
$(TP5_DST)/tp5.md: $(TP5_SRC)
# ADD NEW MODULE HERE

# trainings
$(SEMAINE1_DST)/semaine1.md: $(SEMAINE1_SRC)
$(SEMAINE2_DST)/semaine2.md: $(SEMAINE2_SRC)
$(SEMAINE3_DST)/semaine3.md: $(SEMAINE3_SRC)
$(SEMAINE4_DST)/semaine4.md: $(SEMAINE4_SRC)
$(SEMAINE5_DST)/semaine5.md: $(SEMAINE5_SRC)
$(COMPLET_DST)/complet.md: $(COMPLET_SRC)
# ADD NEW TRAINING HERE

$(MD_OBJ):
	@mkdir -p $(@D)
	cat $^ > $@

md: $(MD_OBJ)

#############################################################
#
# DOC handout
#
##############################################################

# build
%.handout.doc: %.md
	$(PANDOC) $(DOC_FLAGS) $^ -o $@

# all
doc: $(MD_OBJ:.md=.handout.doc)


#############################################################
#
# HTML handout
#
##############################################################

%.handout.html: %.md
	$(PANDOC) $(HTML_FLAGS) $^ -o $@

# all
html: $(MD_OBJ:.md=.handout.html)


#############################################################
#
# PDF handout
#
#############################################################

%.handout.pdf: %.md
	$(PANDOC) $(PDF_FLAGS) $^ -o $@

# all
pdf: $(MD_OBJ:.md=.handout.pdf)


#############################################################
#
# TEX handout
#
# this is the itermedary format for PDF export
# it's useful to debug the PDF output
#
#############################################################

%.handout.tex: %.md
	$(PANDOC) $(PDF_FLAGS) $^ -o $@

# all
handout_tex: $(MD_OBJ:.md=.handout.tex)

#############################################################
#
# Reveal Slides
#
#############################################################

%.slides.reveal.html: %.md
	$(PANDOC) $(REVEAL_FLAGS) $^ -o $@

# all
reveal: $(MD_OBJ:.md=.slides.reveal.html)

#############################################################
#
# Global Targets
#
#############################################################

.PHONY: all

help:
	@echo "--------------------------------------------------------------"
	@echo "make list     : show all global targets"
	@echo "make modules  : export all modules in all suported formats"
	@echo "make pdf      : export all modules and trainings in PDF"
	@echo "make semaine1 : export module semaine1 in all supported formats"
	@echo "make cm2      : export training cm2 in all supported formats"
	@echo "make _build/formations/semaine2/semaine2.slides.reveal.html : export training semaine2 in reveal"
	@echo "make _build/modules/td1/td1.handout.pdf : export module td1 in pdf"
	@echo "--------------------------------------------------------------"
	@echo "Supported formats for handout : html, pdf"
	@echo "Supported formats for slides : reveal"
	@echo "--------------------------------------------------------------"
	@echo "make will use pandoc if it is installed else it will try with docker"
	@echo "you can force docker with : DOCKER=latest make ...."
	@echo "--------------------------------------------------------------"

all : reveal html pdf

clean:
	rm -fr $(BLD)

list:
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$' | xargs

modules: $(MODULES)

trainings: $(TRAININGS)

formations: $(TRAININGS)

meta:
	./meta.sh

documentation:
	$(PANDOC) `cat SUMMARY.md` -N --pdf-engine=xelatex -o documentation.pdf

guidelines: $(MODULES_MD_OBJ)
	echo $(MODULES_MD_OBJ)
	$(foreach m, $^ , $(PANDOC) --filter pandoc_dalibo_guidelines $(m); )

