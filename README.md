# Cours DBA PostgreSQL

Vous trouverez ici le matériel pédagogique que vous pourrez utiliser pour les
cours à l'université : Support de cours, Travaux Dirigés, Travaux Pratiques et
Corrigés.

Ces cours sont disponibles sous licence [Creative Commons BY-SA]. Vous êtes
libres d'utiliser, de modifier et de distribuer ces formations si vous
respecter les principes suivants :

* Créditer [Dalibo] comme la source de ces documents
* Redistribuer le contenu et les modifications sous les mêmes conditions

Toutes les contributions sont les bienvenues.

Voici quelque liens utiles pour utiliser ce matériel :

* Liste des [modules] disponibles
* [Installation] de la chaine de compilation
* [Comment Contribuer] et [Règles d'écriture]
* [Problèmes Connus]
* [Licence]


## Modules

Cette formation est composée de plusieurs modules. Chaque module est composé
de :

  * un Cours Magistral donné sur 1h15
  * une séance de Travaux Dirigé sur table, donnée en 1h30
  * une séance de Travaux Pratique sur PC, donnée en 2h15

Les modules finalisés actuellement sont :

  * cm1 : Découvrir PostgreSQL
  * cm2 : Transactions et accès concurrents
  * cm3 : Missions du DBA
  * cm4 : Optimisation et index
  * cm5 : PL/PgSQL et triggers

## Formations par Dalibo

Ces cours pour l'université sont issus des formations de [Dalibo]. Depuis plus
de 15 ans, Dalibo propose un catalogue de [formations PostgreSQL] riche,
complet et disponible sous licence [Creative Commons BY-SA].

<!--- Liens Externes -->

[Dalibo]: https://www.dalibo.com
[Creative Commons BY-SA]: https://creativecommons.org/licenses/by-sa/4.0/
[Formations PostgreSQL]: https://www.dalibo.com/formations

<!-- Liens Internes -->

[Installation]: INSTALL.md
[Comment Contribuer]: CONTRIBUTING.md
[Règles d'écriture]: SYNTAX.md
[Problèmes Connus]: KNOWN_ISSUES.md
[licence]: LICENSE.md
