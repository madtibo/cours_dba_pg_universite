# Outils d'édition


## Vim

Ci-dessous des options utiles à ajouter dans votre fichier ``~/.vim.rc``

```
au BufRead,BufNewFile *.md setlocal colorcolumn=80
au BufRead,BufNewFile *.md setlocal textwidth=80
au BufRead,BufNewFile *.md setlocal wrapmargin=2
au BufRead,BufNewFile *.md setlocal tabstop=2
```

Pour forcer le retour à la ligne lorsqu'une ligne dépasse 80 caractères, vous
pouvez sélectionner sur la ligne en question :

``
v
``

puis taper :

``
gq
``

Pour rewrapper toutes les lignes à partir de la ligne courante jusquà la fin du
fichier, vous pouvez lancer :

``
gqG
``

## Emacs


Pour rewrapper toutes les lignes d'un paragraphe, vous pouvez lancer :

``
M-x indent-region
``


## Téléchargement en bloc de tous les manuels déjà compilés

FIXME avec nouvelle URL.

``
wget -r --no-parent -nH -e robots=off --reject="index.*" --cut-dirs=4 --relative https://cloud.dalibo.com/p/exports/formation/manuels/
``



## GNU parallel

Si vous souhaitez compiler l'ensemble des formations, armez-vous de patience...
... ou installer GNU Parallel :) 

```
sudo apt-get install parallel
```

Si le script de compilation détecte la présence de GNU Parallel et que l'option
"-j <nb cpu>" est spécifié.  Il lancera plusieurs processus de
compilation. Attention en mettant un chiffre trop élevé le build peut échouter.
Les outils créent des fichiers temporaires dans `/tmp` et parfois ils se
marchent dessus!

