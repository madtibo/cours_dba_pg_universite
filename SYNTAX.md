# Rédaction des modules

La syntaxe markdown est assez vaste, par ailleurs il existe différentes 
syntaxes pour la même mise en forme.

Afin d'obtenir un contenu homogène il est primordial d'utiliser ces mêmes 
règles de syntaxe.


## Hiérarchie de l'information

Pour l'organisation des titres voici la syntaxe retenue : 


+--------+--------------+--------------------------------+
| Niveau | Syntaxe      | Description                    |
+========+==============+================================+
| H1     | `# Titre`    | le titre du module ou du TP    |
+--------+--------------+--------------------------------+
| H2     | `## Titre`   | le titre des parties           |
+--------+--------------+--------------------------------+
| H3     | `### Titre`  | le titre des slides “normales” |
+--------+--------------+--------------------------------+
| H4     | `#### Titre` | les sous-titres                |
+--------+--------------+--------------------------------+


## Règles d'écriture

* Chaque module commence avec une slide "Menu" qui reprend la liste de toutes
  le parties (H2)

* Chaque module commence avec une slide "Objectif" qui donne les grandes lignes
  du module

* Les titres H4 sont réservés aux handouts

* Pas plus de 8 items par liste ! Si la liste est trop longue faire une 2eme slide

* Pas plus de 2 niveaux de puces. 


## Typographie


\``xxxx`\` : pour les termes techniques ( `two-phase commit` ), les
paramètres, les commandes SQL ou shell

`**xxxx**`  : pour souligner un terme **important**

`_xxxx_` : pour indiquer un _néo-logisme_ ou un terme utilisé hors-contexte

`~~xxxx~~` : pour afficher du texte ~~barré~~

`> xxx ` pour les citations : 

> Ma citation




## Tableaux

C'est un vrai casse tête! Il existe beaucoup de syntaxes différentes.

Celle retenue est [grid_table](http://pandoc.org/MANUAL.html#extension-grid_tables), il est possible d'utiliser le plugin 
[vim-table-mode](https://github.com/dhruvasagar/vim-table-mode) pour vim. 
Avec les options suivantes :

```
let g:table_mode_corner_corner="+"
let g:table_mode_header_fillchar="="
```

Si vous voulez mettre plusieurs lignes dans une seule il faut terminer la 
ligne par un `\`.



## Séparation slide/handout

Pour séparer chaque slide il faut ajouter un séparateur `-----` suivi d'une 
ligne vide.

Pour les slides, il faut ajouter les balises suivantes autour du contenu :

```
<div class="slide-content>
Le contenu de ma slide
</div>
```

Pour le handout il faut ajouter les balises suivantes : 

```
<div class="notes">
Mon handout
</div>
```



## Listes

Il existe différents types de liste. Là ausi il existe plusieurs syntaxe 
markdown. Il est recommandé d'utiliser la syntaxe proposée dans la 
[documentation de pandoc](http://pandoc.org/MANUAL.html#lists)


### Liste Compacte 

* one
* two
* three

Code : 

```
* one
* two
* three
```

__Attention__ : Faire un saut de ligne avant une liste.


### Liste Espacée : 

* one

* two

* three

Code : 

```
* one

* two

* three
```

### Liste Avec paragraphe : 

  * First paragraph.

    Continued.

  * Second paragraph. With a code block, which must be indented
    eight spaces:

        { code }

Code : 

```
  * First paragraph.

    Continued.

  * Second paragraph. With a code block, which must be indented
    eight spaces:

        { code }
```

### Liste sur plusieurs niveaux (pas plus de 2 niveaux) : 

* fruits
    + apples: macintosh, red delicious
    + pears
    + peaches
* vegetables
    + broccoli
    + chard

Code : 

```
* fruits
    + apples: macintosh, red delicious
    + pears
    + peaches
* vegetables
    + broccoli
    + chard
```

### Liste énumérée

1.  one
2.  two
3.  three

Code : 

```
1.  one
2.  two
3.  three
```

### Liste de définitions

Term 1

:   Definition 1

Term 2 with *inline markup*

:   Definition 2

        { some code, part of Definition 2 }

    Third paragraph of definition 2.

Code : 

```
Term 1

:   Definition 1

Term 2 with *inline markup*

:   Definition 2

        { some code, part of Definition 2 }

    Third paragraph of definition 2.
```


## Images

Les fichiers doivent être placés dans le répertoire `medias` et préfixé par le
numéro du module ou par `common` s'ils sont communs à d'autres modules (schémas
par exemple). Il faut donc spécifier un chemin relatif au script de génération
des supports. Une image est un lien préfixé de `!` : 


![Ce texte s'affiche si l'image n'apparait pas, sinon c'est aussi une légende](medias/a1-croissance_pg3.png "Texte de survol")\ 


```
![Ce texte s'affiche si l'image n'apparait pas](medias/a1-croissance_pg3.png "Texte de survol")
```

### Positionnement

Si vous mettez un titre à l'image, latex va la traiter comme un
élément flottant et placera l'image "au mieux" dans le document ([plus d'info
ici](https://github.com/jgm/pandoc/issues/845) ). Pour forcer l'emplacement de 
l'image il est possible d'insérer un `\ ` (backslah + espace) pour garantir
qu'elle sera au meme endroit que dans la source markdown.

### Images et Cadres 

Le filtre `pandoc-latex-environment` affiche un cadre de couleurs lorsqu'il
tombe sur certains `<div>` dans la source markdown. Ce filtre plante quand il
tombe sur une image. Il n'y a pas à  court terme de piste concrète de
correction. Il y a cependant un contournement assez simple. 

La règle est la suivante:

* Ne pas mettre d'image dans les`<div>` qui sont taggués avec une des classes 
suivantes:  **slide-content**, **tip**, **important**, **warning**, **note**

* Les images sont acceptées dans les autres `<div>` en particulier le handout

Exemple:

```
### Jointure interne

<div class="slide-content">

  * Clause `INNER JOIN`
    * meilleure lisibilité
    * facilite le travail de l'optimiseur
  * Joint deux tables entre elles
    * Selon une condition de jointure

</div>

![Jointure interne](medias/s3-innerjoin.png)


<div class="notes">

Une jointure interne est considérée comme
```

Ici l'image sera quand même affiche dans la slide Reveal.

### Redondance

Si on appelle plusieurs fois une image il est aussi possible de la définir une
seule fois  : 


```markdown
Ici : ![Mon image]

La : ![Mon image]


[Mon image]: medias/a1-croissance_pg3.png
```


## Liens

Pour une doc complète, consultez la [section 'liens' du manuel pandoc](http://pandoc.org/MANUAL.html#links)

Concrètement vous pouvez faire 4 types de liens : automatiques, nommés, 
internes et récurrents

### Liens automatiques 

Le plus simple ! Vous mettez un URL encadré par '<' et '>'

**exemple :**  

```markdown
Ce lien sera "clickable" : <http://www.dalibo.com/>
```

**résultat** :

Ce lien sera "clickable" : <http://www.dalibo.com/> 

### Liens nommés                                                            
                                                                                
**exemple :**                                                                               

```markdown
syntaxe classique : [dalibo](http://www.dalibo.com/)

pour une image :

[![](http://blog.dalibo.com/assets/img/logo.png)](http://www.dalibo.com/)                           
```                                                                             

**résultat** :                                                                                

syntaxe classique : [dalibo](http://www.dalibo.com/)                            
                                                                                
pour une image :                                                                
                                                                                
[![](http://blog.dalibo.com/assets/img/logo.png)](http://www.dalibo.com/)   


### Liens internes

Vous pouvez aussi pointer sur un titre du document. Attention aux majuscules, 
aux espaces, aux accents, etc. ! Voir la liste exhaustives des 
[règles de transformation](http://pandoc.org/MANUAL.html#extension-auto_identifiers)
dans le manuel de pandoc.

**exemple :**  

```markdown
* [Relisez la section Tableaux du fichier SYNTAX](#tableaux)
* [Hiérarchie de l'information](#hiérarchie-de-linformation) 
```    

**résultat** :

* [Relisez la section Tableaux du fichier SYNTAX](#tableaux)
* [Hiérarchie de l'information](#hiérarchie-de-linformation) 

### Liens récurrents

Egalement appelés `shortcut_reference_links`. Ne vous fatiguez pas à redfinir 50
fois le même lien. Le fichier `common/links.md` contient déjà les liens courants.

**exemple :**  

```markdown
pas la peine de rédéfinir [PostgreSQL] à chaque fois !
```

**résultat** :

pas la peine de rédéfinir [PostgreSQL] à chaque fois !

[PostgreSQL]: http://www.postgresql.org/.





