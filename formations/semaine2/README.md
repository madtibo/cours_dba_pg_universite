# Bases de données 2 : transactions et accès concurrents
 
## Objectifs

Cette formation a pour objectif de transmettre à des étudiants
en L3 Informatique les connaissances et le savoir-faire technique
pour assurer les tâches suivantes :

  * le fonctionnement du moteur MVCC
  * les transactions dans PostgreSQL
  * le fonctionnement du _VACUUM_
  * la gestion des verrous

## Public concerné

Ce programme s'adresse aux étudiants de L3 Informatique.


## Pré-requis

   * Connaissances minimales en système d'exploitation et informatique ;
   * Notion de base de langage SQL.
