# Bases de données 2 : _PL/PgSQL_ et Triggers

## Objectifs

Cette formation a pour objectif de transmettre à des étudiants
en L3 Informatique les connaissances et le savoir-faire technique
pour assurer les tâches suivantes :

  * Connaissance de PL/PgSQL et des autres langages PL,
	* Écriture de fonctions en PL/PgSQL,
	* Connaissance du comportement des triggers,
	* Écriture de triggers.

## Public concerné

Ce programme s'adresse aux étudiants de L3 Informatique.


## Pré-requis

   * Connaissances minimales en système d'exploitation et informatique ;
   * Notion de base de langage SQL.
