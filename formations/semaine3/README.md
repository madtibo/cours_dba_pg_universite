# Bases de données 2 : missions du DBA
 
## Objectifs

Cette formation a pour objectif de transmettre à des étudiants
en L3 Informatique les connaissances et le savoir-faire technique
pour assurer les tâches suivantes :

    * sécurité et droits
    * sauvegardes et restaurations

## Public concerné

Ce programme s'adresse aux étudiants de L3 Informatique.


## Pré-requis

   * Connaissances minimales en système d'exploitation et informatique ;
   * Notion de base de langage SQL.
