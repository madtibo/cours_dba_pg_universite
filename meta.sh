#! /bin/bash

SRC=src

MODULES_DIR=modules
MODULES_BUILD=_build/modules
MODULES_SRC=`find $SRC/* -type d`


#
# MODULES
#

SLIDES_ALL=0
EXOS_ALL=0
PAGES_ALL=0

for p in $MODULES_SRC
do
  # p = src/a1-blablabla
  # m = a1-blablabla
  # t = a1
  m=`basename $p`	 
  t=`echo $m | sed s/-.*//`
  o=$MODULES_DIR/$m/META.md

  # Creation/Ecrasement du fichier
  echo "Generating $o ..."
  echo "---" > $o

  # Nombres de slides 
  #SLIDES=`grep '^-----$' $p/cours.md |wc -l`
  SLIDES=`grep 'div class="slide-content"' $p/cours.md |wc -l`
  SLIDES_ALL=$(( $SLIDES_ALL + $SLIDES ))
  echo "slides: $SLIDES" >> $o

  # Nombres de pages du manuel
  f=$MODULES_BUILD/$t/$t-pdf.pdf
  if [ -f $f ]; then
    PAGES=`pdfinfo $f | grep Pages | awk '{print $2}'`
    PAGES_ALL=$(( $PAGES_ALL + $PAGES ))
    echo "pages: $PAGES" >> $o
  else
    echo -e "\e[1;31m File $f does not exist. \e[0m"
  fi
 	
  # Nombres d'exercices
  f=$p/tp-enonce.md
  if [ -f $f ]; then
    EXOS=`grep '^\*\*.*\*\*$' $f |wc -l`
    EXOS_ALL=$(( $EXOS_ALL + $EXOS ))
    echo "exercices: $EXOS" >> $o
  else
    echo -e "\e[1;31m File $f does not exist. \e[0m"  
  fi

  # Fin
  echo "---" >> $o 

done


#
# META META
#
o=$MODULES_DIR/META.md

# Creation/Ecrasement du fichier
echo "Generating $o ..."
echo "---" > $o
echo "slides: $SLIDES_ALL" >> $o
echo "exercices: $EXOS_ALL" >> $o
echo "---" >> $o

