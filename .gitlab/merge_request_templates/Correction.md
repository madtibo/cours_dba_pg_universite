# Contexte

Description de ce que corrige cette Merge Request

# Règles

- [ ] J'ai respecté le [CONTRIBUTING.md](CONTRIBUTING.md) et le [SYNTAX.md](SYNTAX.md)
- [ ] Le ticket dans le dépôt formation a bien été renseigné
- [ ] Le build de la branche sur [jenkins](https://jenkins.dalibo.info/job/Manuels%20formation%20-%20multibranch/) est bien passé


