# Release trimestrielle

Numéro de la release :

# Règles

- [ ] J'ai lu le [RELEASE.md](RELEASE.md)
- [ ] La dernière version des thèmes a été ajoutée depuis plus de 2 semaines et le résultat vérifié
- [ ] La mise en forme de toutes les formations a été vérifiée pour éviter les problèmes flagrants
- [ ] common/globals.md contient le n° de la nouvelle release
- [ ] Un dernier build de la branche master sur [jenkins](https://jenkins.dalibo.info/job/Manuels%20formation%20-%20multibranch/) s'est bien passé
- [ ] J'ai préparé le mail d'annonce pour dlb-general

