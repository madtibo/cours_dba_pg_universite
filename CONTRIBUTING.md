# Comment Contribuer

## Création des modules

Un module de formation comprend un support de présentation (slide + handout) ainsi que des TD et des TP. L'ensemble du contenu est dans le répertoire "modules" avec un répertoire par module.

Le nom du répertoire doit être de la forme `<identifiant module>-<nom_module>`

Par exemple : 

```
modules
├── cm1-decouvrir_postgresql
│   ├── cours.md
│   ├── td-enonce.md
│   └── td-solution.md
│   ├── tp-enonce.md
│   └── tp-solution.md
```

Le répertoire doit contenir : 

* un fichier `cours.md` : comprend le contenu du module (hors tp).
* des fichiers `td-enonce/solution/md` : comprend le contenu des td
* des fichiers `tp-enonce/solution/md` : comprend le contenu des tp


### Présentation

Le support de présentation est rédigé dans le fichier `cours.md` pour chaque
module. Il est constitué de slides, chaque slide comprend une partie note
également appelée handout. La partie handout ne s'affiche pas dans les supports
de type présentation (beamer, reveal.js), en revanche elle s'affiche dans les
supports type texte (pdf, odt, epub, html).

La syntaxe pour séparer le slide des notes est abordée dans [Syntaxe
Markdown](#syntaxe-markdown)


### TD / TP

Les supports TD et TP sont constitués d'une partie énoncé et d'une partie
solution dans deux fichiers distincts `tp-enonce.md` et `tp-solution.md`. Cette
séparation permettra de générer séparément un support énoncé (dans modules) et
un support solution (dans formations).


## Création d'un support

Un `support` est un ensemble de contenus : la licence, un en-entête, un ou 
plusieurs modules, des TD et TP (énoncés et solutions), etc.

Il existe différents types de support : Module, Formation


### Support de type "Module"

Ce format est idéal pour générer les slides d'un module donné sans les
solutions.

Le sommaire d'un module comprend :

  * des fichiers globaux (licence, tags, liens)
  * un en-tête
  * le contenu d'un ou plusieurs modules
  * les énoncés des TD et le TP du module

__Exemple__ : [cm1-decouvrir_postgresql](modules/cm1/files.md)

__Commande__ : `make cm1`

### Support de type "Formation"

Cet export est utile pour produire un PDF complet de tous les modules avec les
solutions.

Le sommaire d'une formation comprend:

  * des fichiers globaux (licence, tags, liens)
  * un en-tête
  * le contenu d'un ou plusieurs modules
  * les énoncés des TD et le TP du module
  * les solutions des TD et le TP du module

__Exemple__ : [semaine1](formations/semaine1/files.md)

__Commande__ : `make semaine1`

### Fichiers globaux (global.md , links.md, licence.md)

Le dossier `common` contient des fichiers qui sont partagés entre tous les 
supports

* [licence.md](common/licence.md) : La licence d'utilisation
* [global.md](common/global.md)   : Des tags généraux (revision, auteur, lang)
* [links.md](common/links.md)     : Des liens usuels


### Fichier en-tête (header.md)

Ce fichier comprend des informations utiles à la génération du support : 

* Titre du support
* Liste de mots clés

Par exemple : 
```
---
title : 'Découvrir PostgreSQL'
keywords:
- postgres
- postgresql
- sql
- acid
---
```




## Workflow

### Présentation

Le projet possède plusieurs branches principales correspondant aux années universitaires.

La branche **master** correspond à la future version des supports. 
Les modifications se font donc sur cette branche en créant des "sous-branches".

Pour toute modification il faut créer une branche depuis **master**. Ces
modifications sont intégrées au fil du temps dans la branche **master**.

Lors du **release day**, la branche master est mergée dans une branche
correspondant à l'année universitaire.
  

### Faire une modification

On ne fait **jamais** de commit dans la branche **master**.

Tout modification passe par la création d'une branche depuis une branche existante.

Toutefois, il existe un seul cas où on s'autorise à faire un commit sur stable :

Dans le cas d'un **hot fix** pour corriger une erreur qui ne peut pas attendre
le prochain **release day**.

**Récupérer le dépôt**

Les supports sont sur un dépôt git, il faut donc cloner le dépôt : 

```
git clone git@gitlab.com:madtibo/cours_dba_pg_universite.git
Cloning into 'manuels'...
remote: Counting objects: 400, done.
remote: Compressing objects: 100% (24/24), done.
remote: Total 400 (delta 14), reused 0 (delta 0)
Receiving objects: 100% (400/400), 1.63 MiB | 0 bytes/s, done.
Resolving deltas: 100% (144/144), done.
Checking connectivity... done.
```

**Créer la branche recevant les modifications**

On ne fait **jamais** de commit dans la branche **master**. Si vous faites ça
vous serez en décalage avec le dépôt principal.

On crée donc une nouvelle branche : 

```
git checkout -b "ma-modification"
Switched to a new branch 'ma-modification'
```

Ensuite vous pouvez faire des modifications puis faire des add/commit.

```
emacs new.txt
git add new.txt
git commit .
[ma-modification 2658e44] J'ajoute un fichier new.txt
 1 file changed, 1 insertion(+)
 create mode 100644 new.txt
```

Actuellement vos modifications sont seulement locales. La seconde étape
consiste à __pusher__ la nouvelle branche sur le gitlab.

```
git push --set-upstream origin ma-modification
Counting objects: 3, done.
Delta compression using up to 8 threads.
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 294 bytes | 0 bytes/s, done.
Total 3 (delta 1), reused 0 (delta 0)
remote: 
remote: Create merge request for ma-modification:
remote:   https://gitlab.com/madtibo/cours_dba_pg_universite/merge_requests/new?merge_request%5Bsource_branch%5D=ma-modification
remote: 
To git@gitlab.com:madtibo/cours_dba_pg_universite.git
 * [new branch]      ma-modification -> ma-modification
Branch ma-modification set up to track remote branch ma-modification from origin.
```

Vous pouvez faire d'autres commit, il faudra faire des `git push` pour les
envoyer sur le dépôt distant. La génération des supports est déclenchée à
chaque commit.


### Demande d'approbation d'une modification

Dans gitlab un message **Merge Request** va apparaitre. Il faut créer cette MR.

### Supprimer une branche

Une fois la MR mergée vous pouvez la supprimer sur votre dépôt local :

```
# Local
git branch -D ma-modification
```

Si je veux supprimer ma branche locate et distance (et annuler les modifications) : 

``` bash
# Distant
git push origin --delete ma-modification

# Local
git branch -D ma-modification
```

### Resynchroniser son dépot personnel avec le dépot principal

Imaginons que j'ai forké le dépot `formation/manuel` sur mon espace perso dans `damien/manuel`.

Pour proposer des patchs correct, il faut repartir d'une base récente et donc resynchroniser régulièrement
la branche master de mon dépot personnel avec la branche master du dépot :

```
git checkout master
git remote add upstream git@gitlab.com:madtibo/cours_dba_pg_universite.git
git fetch upstream
git merge upstream/master
git push
```


## Annexe


Pour générer le CONTRIBUTING.pdf :

```
pandoc CONTRIBUTING.md --pdf-engine=xelatex -o CONTRIBUTING.pdf
```
