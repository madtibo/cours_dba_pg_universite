# Problèmes Habituels


En cas d'erreur de syntaxe, dalidoc vous retransmet directement les erreurs
LaTeX qui sont connues pour être complètement ésotériques. Voici une liste
d'erreurs "classiques" et la solution pour s'en débarasser :


Vérifier que tous les <div> sont fermés
-------------------------------------------------------------------------------

Erreur assez classique mais qui peut s'avérer assez dure à détecter.

Le message d'erreur Latex ressemble à ça :

```
! Dimension too large.
\fb@put@frame ...p \ifdim \dimen@ >\ht \@tempboxa 
                                                  \fb@putboxa #1\fb@afterfra...
l.6291 \end{frshaded}
```

Vous pouvez rapidement détecter un problème en compte les balises ouvrantes et
fermantes avec une commande comme celle-ci :

```
C=cours.md && cat $C | grep '<div' |wc -l && cat $C | grep '</div' |wc -l 
```



Images dans les DIV "slides-content"
-------------------------------------------------------------------------------

Si vous obtenez l'erreur suivante :

```
! LaTeX Error: Not in outer par mode.

See the LaTeX manual or LaTeX Companion for explanation.
Type  H <return>  for immediate help.
 ...                                              
                                                  
l.725 \centering
```

Vous avez probablement laissé une image dans un DIV "slide-content". 

Déplacez simplement l'image en dehors du DIV. Elle sera tout de même affichée
dans la slide.


_Bien :_

```
<div class="slide-content">                                                     
Bla Blaaaaah bla.                                                               
</div>                                                                                

![titre de mon schema](medias/mon_image.png)                                    
\
```


_Pas Bien :_

```
<div class="slide-content">                                                     
Bla Blaaaaah bla.

![titre de mon schema](medias/mon_image.png)
\
</div>
```
