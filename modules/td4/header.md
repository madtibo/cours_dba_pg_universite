---
subtitle : 'Bases de données 2'
title : 'TD 4 - Comprendre _EXPLAIN_'
keywords:
- postgres
- postgresql
- td
- EXPLAIN
- Optimisation
linkcolor:

#
# PDF Options
#

toc: false
url: https://blog.tribu-ml.fr/
disable-header-and-footer: true
listings-no-page-break: true

---

# Bases de données 2
