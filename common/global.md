---
revision: 23.10
date: octobre 2023

licence : "Creative Commons BY-SA"
include_licence : CC-BY-SA-4.0-FR

author: Thibaut MADELAINE

copyright: © 2005-2019 DALIBO SARL SCOP, © 2020-2023 Thibaut MADELAINE

trademarks: |
  Le logo éléphant de PostgreSQL ("Slonik") est une création sous copyright et
  le nom "PostgreSQL" est marque déposée par PostgreSQL Community Association
  of Canada.

url : https://dalibo.com/formations

about_the_author: |
  **À propos de Thibaut MADELAINE :**

  Thibaut MADELAINE est Développeur FullStack au sein de GitGuardian, une
  entreprise parisienne de cybersécurité spécialisée dans la détection des
  secrets.

merci: |
  **Remerciements :**
  Ce manuel de formation est issu des manuels de formations Dalibo. Nous
  remercions chaleureusement ici toutes les personnes qui ont contribué
  directement ou indirectement à cet ouvrage, notamment : Jean-Paul Argudo,
  Alexandre Anriot, Carole Arnaud, Alexandre Baron, Sharon Bonan, Damien
  Clochard, Christophe Courtois, Marc Cousin, Gilles Darold, Jehan-Guillaume de
  Rorthais, Ronan Dunklau, Vik Fearing, Stefan Fercot, Pierre Giraud, Nicolas
  Gollet, Dimitri Fontaine, Virginie Jourdan, Guillaume Lelarge, Jean-Louis
  Louër, Thibaut Madelaine, Adrien Nayrat, Flavie Perette, Thomas Reiss, Maël
  Rimbault, Julien Rouhaud, Stéphane Schildknecht, Julien Tachoires, Nicolas
  Thauvin, Cédric Villemain, Thibaud Walkowiak, David Bidoc.

abstract: |
  **Chers lectrices & lecteurs,**

  Cette formation PostgreSQL est issue des manuels Dalibo. Ils ont été repris
  par Thibaut MADELAINE pour rentrer dans le format universitaire avec Cours
  Magistraux, Travaux Dirigés (sans ordinateurs) et Travaux Pratiques (avec
  ordinateur).

  Au-delà du contenu technique en lui-même, l'intention des auteurs est de
  transmettre les valeurs qui animent et unissent les développeurs de PostgreSQL
  depuis toujours : partage, ouverture, transparence, créativité, dynamisme...
  Le but premier de cette formation est de vous aider à mieux exploiter toute la
  puissance de PostgreSQL mais nous espérons également qu'elles vous inciteront
  à devenir un membre actif de la communauté en partageant à votre tour le
  savoir-faire que vous aurez acquis avec nous.

  Nous mettons un point d'honneur à maintenir nos manuels à jour, avec des
  informations précises et des exemples détaillés. Toutefois malgré nos efforts
  et nos multiples relectures, il est probable que ce document contienne des
  oublis, des coquilles, des imprécisions ou des erreurs. Si vous constatez un
  souci, n'hésitez pas à le signaler sur le site gitlab
  <https://gitlab.com/madtibo/cours_dba_pg_universite/-/issues> !


#
# PDF Options
#

## Limiter la profondeur de la table des matières
toc-depth: 2

## Mettre les lien http en pieds de page
links-as-notes: true

## Police plus petite dans un bloc de code

code-blocks-fontsize: small

## eisvogel parameters
titlepage: true
titlepage-text-color: 3465A4

## Filtre : pandoc-latex-env = cadres de couleurs
## OBSOLETE voir pandoc-latex-admonition
latex-environment:
  importantframe: [important]
  warningframe: [warning]
  tipframe: [tip]
  noteframe: [note]
  frshaded: [slide-content]

## Filtre : pandoc-latex-admonition
pandoc-latex-admonition:
  - color: LightPink
    classes: [important]
    linewidth: 4
  - color: Khaki
    classes: [warning]
    linewidth: 4
  - color: DarkSeaGreen
    classes: [tip]
    linewidth: 4
  - color: Ivory
    classes: [note]
    linewidth: 4
  - color: DodgerBlue
    classes: [slide-content]
    linewidth: 4


#
# Reveal Options
#

# Taille affichage
width: 1200
height: 768

## beige/blood/moon/simple/solarized/black/league/night/serif/sky/white
theme: sky

## None - Fade - Slide - Convex - Concave - Zoom
transition: None

transition-speed: fast

# Barre de progression
progress: true

# Affiche N° de slide
slideNumber: true

# Le numero de slide apparait dans la barre d'adresse
history: true

# Defilement des slides avec la roulette
mouseWheel: true

# Annule la transformation uppercase de certains thèmes
title-transform : none

# Cache l'auteur sur la première slide
# Mettre en commentaire pour désactiver
hide_author_in_slide: true

---
