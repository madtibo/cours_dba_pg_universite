#!/usr/bin/env python
from __future__ import print_function
import sys, getopt

"""
Dalibo Pandoc filter.  It handles all special case transformations needed.
"""

from pandocfilters import toJSONFilter, RawBlock

debug = False

def dprint(msg):
    if (debug):
        print("%r" % msg, file=sys.stderr)

def latex(x):
    return RawBlock('latex', x)

def latex_box(contents,style="default"):
    if (style == 'notes'):
        return contents

    color = 'yellow'

    if (style == 'slide-content'):
        color = 'Orchid'
    return([latex(
        """
\\fcolorbox{black}{""" + color + """}{%
        \\begin{minipage}{0.9\\textwidth}
"""
    )] + contents +
        [latex(
"""
        \\end{minipage}%
}
"""
        )])

def dalifilter(key, value, format, meta):
    #dprint(meta)
    exclude_slide_content = False
    exclude_slide_delim = False

    if ('exclude-slide-content' in meta.keys()):
        exclude_slide_content = True
    if ('exclude-slide-delim' in meta.keys()):
        exclude_slide_delim = True

    if (key == 'HorizontalRule' and exclude_slide_delim):
        return([])
    elif key == 'Div':
        [[ident, classes, kvs], contents] = value
        dprint(", ".join(classes))
        if ((classes[0] == 'slide-content') and (exclude_slide_content)):
                return([])
        else:
                # XXX handle multi-class divs
                return(latex_box(contents, classes[0]))


def help(rc):
    print("""
Use pandoc's -M key=value or --metadaa=key:value to change behavior
Don't forget to "pip install pandocfilters"

  -h|--help                 display this message

Handled options:
  exclude-slide-content=1   ignore content of slides
  exclude-slide-delim=1     ignore slide delimiters
          """, file=sys.stderr)
    sys.exit(rc)

if __name__ == "__main__":
    try:
        opts, args = getopt.getopt(sys.argv[1:],"h", ["help"])
    except getopt.GetoptError:
        help(1)

    for opt, arg in opts:
        if (opt in ("-h", "--help")):
            help(0)

    toJSONFilter(dalifilter)
