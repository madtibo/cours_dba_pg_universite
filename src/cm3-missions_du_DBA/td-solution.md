
<div class="notes">

### Solutions du TD 3

#### Question de cours

  * Quel fichier permet de configurer les droits d'accès à l'instance ?
    * Le fichier de configuration _pg_hba.conf_ permet des contrôler
      l'authentification du client.
    * _HBA_ signifie « host-based authentication » : authentification fondée
      sur l'hôte.
  
  * Y a-t-il un lien entre utilisateur système et rôle PostgreSQL ?
    * Si le rôle n'est pas précisé dans la chaîne de connexion, le nom de
      l'utilisateur système est utilisé.
    * Lors d'une connexion en local par la méthode `peer`, l'accès est permis
      sans mot de passe pour la connexion d'un utilisateur à l'instance si un
      rôle du nom de l'utilisateur existe dans l'instance.

  * Quel ordre SQL permet de donner des droits sur une table ?
    * L'ordre SQL pour donner des droits sur une table est :
	  `GRANT (...) ON TABLE table_name TO role_specification`.
    * Les droits possibles sont :
	  `SELECT | INSERT | UPDATE | DELETE | TRUNCATE | REFERENCES | TRIGGER`

#### Outils PostgreSQL

```bash
admin$ psql -d ojm3 -h 192.168.0.5 -U adb1 -p 5454
```

Connexion à la base de données **ojm3** avec le rôle **adb1** sur l'instance
hébergée sur le serveur d'adresse IP 192.168.0.5 au port 5454.

```bash
postgres@nevrast$ psql -p 5433 -c 'SELECT pg_reload_conf();' -h /tmp
```

Exécution de la requête `SELECT pg_reload_conf();` qui recharge la
configuration de l'instance sur l'instance locale au port 5433 (fichier
`/tmp/.s.PGSQL.5433`). La connexion se fait à la base de données **postgres**
avec le rôle **postgres**.

```bash
postgres$ createdb -O app_user -h 10.51.32.40 -p 5446 my_app
```

Création de la base de données **my_app** sur l'instance hébergée sur le
serveur d'adresse IP 192.168.0.5 au port 5446. Le propriétaire de la base de
données sera **app_user**.

```bash
postgres@localhost$ export PGPORT=5436 && dropuser test
```

Destruction du rôle **test** de l'instance locale au port 5436 (sur un système
utilisant systemd, fichier `/var/run/postgresql/.s.PGSQL.5436`). La connexion
se fait à la base de données **postgres** avec le rôle **postgres**.



#### Sauvegardes

```bash
postgres@dbhost$ pg_dump -Fp -d miranda -f /tmp/miranda.sql -h /tmp/v9.6/
```

Sauvegarde en mode texte de la base de données miranda de l'instance hébergée
sur le serveur local au port 5432 (fichier `/tmp/v9.6/.s.PGSQL.5432`). La
connexion se fait à la base de données **postgres** avec le rôle
**postgres**. La sauvegarde est stockée dans le fichier **/tmp/miranda.sql**.

```bash
pgadmin@srv$ pg_dump -Fc -U thibaut -t decors -f /home/thibaut/db.dump main
```

Sauvegarde en mode custom (binaire) de la table **decors** de la base de
données **main** de l'instance hébergée sur le serveur local au port 5432
(fichier `/var/run/postgresql/.s.PGSQL.5432`). La connexion se fait à la base
de données **main** avec le rôle **thibaut**. La sauvegarde est stockée dans le
fichier **/tmp/db.dump**.

```bash
postgres@doriath$ /usr/pgsql-9.4/bin/pg_dump -Fd -d webapp -j 5 -f /tmp/ma_db
```

Sauvegarde en mode directory (binaire) de la base de données **webapp** de
l'instance hébergée sur le serveur local au port 5432 (fichier
`/var/run/postgresql/.s.PGSQL.5432`). La connexion se fait à la base de données
**webapp** avec le rôle **postgres**. La sauvegarde est stockée dans le
répertoire **/tmp/ma_db**. La sauvegarde est parallélisée en traitant 5 tables
à la fois.

```bash
jacques@calvin$ pg_dumpall -U tester -h 192.168.10.38 -f /tmp/ma_db.sql -d jon1
```

Tentative de sauvegarde en mode texte de l'instance hébergée sur le serveur
d'adresse IP 192.168.10.38 au port 5432. La connexion se ferait avec le rôle
**tester**. La sauvegarde serait stockée sur le serveur d'où la commande est
lancée, calvin, dans le fichier **/tmp/ma_db.sql**.  
Le problème vient du dernier paramètre **jon1**. Pour les outils _psql_ ou
_pg_dump_, le dernier paramètre précisera la base de données à laquelle se
connecter. _pg_dumpall_ n'accepte pas de tel paramètre. On pourra préciser avec
l'option `-l` la base de données à laquelle se connecter pour effectuer la
sauvegarde (par défaut _postgres_).

```bash
jacques@hobbes$ pg_dumpall -g > ~/pg_global.sql
```

Sauvegarde en mode texte des objets globaux (rôles et tablespaces) de
l'instance hébergée sur le serveur local au port 5432 (sur un système utilisant
systemd, fichier `/var/run/postgresql/.s.PGSQL.5432`). La connexion se fait à
la base de données **jacques** avec le rôle **jacques**. La sauvegarde est
redirigé de la sortie standard vers le fichier **/home/jacques/pg_global.sql**.

#### Restaurations

```sql
db=# \i /home/thibaut/oberon.sql
```

Nous sommes dans la console _psql_. On charge le fichier texte
**/home/thibaut/oberon.sql** dans la base de données **db**.  
Si l'accès à l'instance se fait de façon distante (_host_), le script chargé
est situé sur le disque du client. _psql_ ne donne en effet pas accès au
système de fichier du serveur hébergeant l'instance.

```bash
$ psql -U titania -h 11.56.99.77 -d puck -f /tmp/ma_db.sql
```

Chargement du fichier texte **/tmp/ma_db.sql** dans la base de données **puck**
de l'instance hébergée sur le serveur d'adresse IP 11.56.99.77 au port 5432. La
connexion se fait avec le rôle **titania**. Si le fichier contient des ordres
DDL sans précision sur le possesseur, tous les objets créés seront possédés par
le rôle **titania**.


```bash
lysandre@athenes$ pg_restore --clean --create -j3 -d hermia reve_de_minuit.dump
```

Restauration du contenu de l'archive **reve_de_minuit.dump** sur l'instance
hébergée sur le serveur local au port 5432 (fichier
`/var/run/postgresql/.s.PGSQL.5432`). La connexion se fait à la base de données
**hermia** avec le rôle **lysandre**. La base de données restaurée n'est pas
**hermia** qui est juste utilisée pour la connexion et les ordres `DROP
DATABASE` et `CREATE DATABASE`. La base de donnée restaurée figure dans le
fichier dump. Elle sera préalablement détruite si existante, puis recréée.

```bash
postgres@src$ pg_restore -U google -h 8.8.8.8 -l -f e3rsf.list e3rsf
```

Sauvegarde dans le fichier **e3rsf.list** de la liste du contenu de l'archive
**e3rsf**. Les options de l'adresse de l'hôte et du rôle de connexion ne sont
pas utilisées.

```bash
pgadmin@pghost$ pg_restore -L db.list -d sauvegarde -p 5434 ma_db.dump
```

Restauration des objets SQL contenus dans le fichier **ma_db.dump** et listés
dans le fichier **db.list** dans la base de données **sauvegarde** sur
l'instance hébergée sur le serveur local au port 5434 (fichier
`/var/run/postgresql/.s.PGSQL.5434`). La connexion se fait à la base de données
**sauvegarde** avec le rôle **pgadmin**.

#### Contrôle des accès

Ce fichier comporte plusieurs erreurs :

```
host    all       all         127.0.0.1/32    md5
```

autorise tous les utilisateurs, en IP, en local (127.0.0.1) à se connecter à
**toutes** les bases, ce qui est en contradiction avec :

```
local   sameuser  all                         peer
```

Le masque CIDR de :

```
host    all       all         89.192.0.3/8    md5
```

est incorrect, ce qui fait qu'au lieu d'autoriser _89.192.0.3_ à se connecter,
on autorise tout le réseau 89.*.

Les entrées :

```
hostssl   recherche      recherche   89.192.0.4/32   md5
hostssl   replication    repli       89.192.0.0/24   scram-sha-256
```

sont bonnes, mais inutiles, car masquées par la ligne précédente : toute ligne
correspondant à cette entrée correspondra aussi à la ligne précédente. Le
fichier étant lu séquentiellement, cette dernière entrée ne sert à rien.

Côté sécurité, le mode d'authentification _trust_ est à proscrire. Il est
conseillé de déclarer un mot de passe au rôle _web_ et de passer par le mode
d'authentification _md5_ ou _scram-sha-256_.

</div>

-----

\newpage
