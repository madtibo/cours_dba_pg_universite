
<div class="notes">

### Solutions du TP 3

#### Installation d'une nouvelle version majeure

Installer la version 16 de PostgreSQL à partir du PGDG :

```bash
root# sudo apt install curl ca-certificates gnupg
root# curl https://www.postgresql.org/media/keys/ACCC4CF8.asc \
           | gpg --dearmor \
           | sudo tee /etc/apt/trusted.gpg.d/apt.postgresql.org.gpg >/dev/null
root# apt update
root# apt install postgresql-16
```

Une instance _main_ sera créée automatiquement. Vérifier son port avec la
commande `pg_lsclusters`.

#### Sauvegardes logiques

Créer un répertoire `backups` dans le répertoire `home` de `postgres` pour y
déposer les fichiers d'export.

Se logguer avec l'utilisateur `postgres`, puis exécuter les commandes
suivantes :

```bash
postgres$ cd ~postgres
postgres$ mkdir backups
postgres$ chmod 700 backups
```

**Sauvegarde logique de toutes les bases**

Sauvegarder toutes les bases de données du cluster PostgreSQL à l'aide de
`pg_dumpall` dans le fichier : _~postgres/backups/base_all.sql.gz_

Se logguer avec l'utilisateur `postgres`, puis exécuter la commande suivante :

```bash
postgres$ pg_dumpall | gzip > ~postgres/backups/base_all.sql.gz
```

**Sauvegarde logique en mode directory d'une base**

Sauvegarder de façon parallélisée de la base de données `mondial` au format
_directory_ à l'aide de `pg_dump` dans le répertoire
_~postgres/backups/base_mondial_.

Se connecter avec l'utilisateur `postgres`, puis exécuter la commande suivante :

```bash
postgres$ pg_dump -Fd -j3 -f ~postgres/backups/base_mondial mondial
```

**Sauvegarde logique en mode custom d'une base**

Sauvegarder de la base de données `cave` au format _custom_ à l'aide de
`pg_dump` dans le répertoire _~postgres/backups/base_cave.dump_.

```bash
postgres$ pg_dump -Fc -f ~postgres/backups/base_cave.dump cave
```

**Export des objets globaux**

Exporter uniquement les objets globaux de l'instance (rôles et définitions de
tablespaces) à l'aide de `pg_dumpall` dans le fichier
_~postgres/backups/base_globals.sql_.

```bash
postgres$ pg_dumpall -g > ~postgres/backups/base_globals.sql
```

**Sauvegarde logique de tables**

Sauvegarder la table `city` dans le fichier _~postgres/backups/table_city.sql_ :

```bash
postgres$ pg_dump -t city mondial > ~postgres/backups/table_city.sql
```

#### Restaurations logiques

On supposera que le port de la nouvelle instance est le 5435.

**Restauration des données globales**

Restaurer les données globales de l'instance sauvegardée dans le fichier
_~postgres/backups/base_globals.sql_ :

```bash
postgres$ psql -p 5435 -f ~postgres/backups/base_globals.sql
```

**Restauration d'une base de données**

Restaurer le schéma de la base de données `mondial` dans une nouvelle base de
données nommée `mondial2` en utilisant le répertoire de sauvegarde
`base_mondial`. Le possesseur en sera le rôle _caviste_.

```bash
postgres$ createdb -p 5435 -O caviste mondial2
postgres$ pg_restore -p 5435 -d mondial2 -s -O --role caviste backups/base_mondial
```

**Restauration d'une table**

À partir du fichier de sauvegarde _~postgres/backups/table_city.sql_, restaurer
la table `city` dans la base de données `mondial2`.

La table city existe déjà, on peut au choix la supprimer ou éditer le fichier
de sauvegarde texte. Puis on peut recharger la sauvegarde :

```bash
postgres$ psql -p 5435 -f ~postgres/backups/table_city.sql mondial2
```

**Migration de données**

Copier les données de la base `mondial` dans une nouvelle base `mondial_test` sans passer par un fichier de sauvegarde :

```bash
postgres$ createdb -p 5435 -O caviste mondial_test
postgres$ pg_dump -Fc mondial | pg_restore -p 5435 -d mondial_test
```

**Restauration partielle**

Lister le contenu de l'archive dans un fichier:

```bash
postgres$ pg_restore -l ~postgres/backups/base_mondial > /tmp/contenu_archive.lst
```

Éditer _/tmp/contenu_archive.lst_, et supprimer ou commenter la ligne `TABLE
DATA` de la table `city`.

Créer la base de données `mondial3`, puis restaurer en utilisant ce fichier :

```bash
postgres$ createdb -p 5435 -O caviste mondial3
postgres$ pg_restore -p 5435 -L /tmp/contenu_archive -d mondial3 \
  ~postgres/backups/base_mondial.dump
```

**Restauration de la base de données cave**

À partir du fichier de sauvegarde _~postgres/backups/base_cave.dump_, restaurer
de façon parallélisée la base de données `cave`.

```bash
postgres$ pg_restore -p 5435 ~postgres/backups/base_cave.dump
```


#### Réalisation de sauvegardes physiques à chaud en local

* **Créer une sauvegarde physique avec l'outil `pg_basebackup`.**

* Créer, avec l'utilisateur PostgreSQL, le répertoire pour héberger les
  sauvegardes physiques :

```bash
postgres$ mkdir -p ~postgres/backups/16/main/backup
```

* Réalisez la sauvegarde

```bash
postgres$ pg_basebackup -D ~postgres/backups/16/main/backup/copie_1
```

* L'outil `pg_basebackup` ne sauvegarde pas les fichiers de configurations
  situés hors du répertoire `PGDATA`. Il ne faut pas oublier de copier ces
  fichiers :

```bash
postgres$ cp -rf /etc/postgresql/16/main/ ~postgres/backups/16/main/backup/copie_1/conf_files
```

**Créer une nouvelle instance avec le commande bash `pg_createcluster`**

Pour restaurer l'instance sauvegardée à la question précédente, nous devons
créer une nouvelle instance _copie_ :

```bash
postgres$ pg_createcluster 16 copie
```

**Restaurer la sauvegarde dans cette nouvelle instance et vérifier son bon
fonctionnement**

```bash
postgres$ rm -rf /var/lib/postgresql/16/copie/*
postgres$ cp -rf ~postgres/backups/16/main/backup/copie_1/* /var/lib/postgresql/16/copie/
```

Recopier les fichiers de configurations (en les adaptant si besoin, par exemple
le numéro de port) :

```bash
postgres$ cp -rf ~postgres/backups/16/main/backup/copie_1/conf_files/* etc/postgresql/16/copy/
postgres$ $ sed -i 's/^port =.*$/port = 5434/' /etc/postgresql/16/copie/postgresql.conf
```

Nous pouvons maintenant démarrer notre copie :

```bash
postgres$ sudo systemctl restart postgresql@16-copie.service
postgres$ pg_lsclusters 16 copie
Ver Cluster Port Status Owner    Data directory               Log file
16  copie   5435 online postgres /var/lib/postgresql/16/copie /var/log/pos(...)
```

#### Réalisation de sauvegardes PITR en local

* **Configurer l'instance PostgreSQL pour réaliser l'archivage des journaux de
transactions grâce au paramètre `archive_command`**

* Créer, avec l'utilisateur PostgreSQL, le répertoire pour héberger les
  archives des journaux de transactions :

```bash
postgres$ mkdir -p ~postgres/backups/16/main/wals
```

* mettre en place l'archivage des journaux de transactions en mettant à jour
  les paramètres `archive_mode` et `archive_command` :

```ini
archive_mode = on
archive_command = 'cp "%p" "/var/lib/postgresql/backups/16/main/wals/%f"'
```

Redémarrer l'instance PostgreSQL :

```bash
# systemctl restart postgresql@16-main.service
```

* **Vérifier le bon archivage des journaux de transactions avec la fonction
`pg_switch_wal`**

```bash
postgres$ psql -c 'SELECT pg_switch_wal()'
postgres$ ls -l ~postgres/backups/16/main/wals/
```

* **Créer une nouvelle sauvegarde physique avec l'outil `pg_basebackup`.**

```bash
postgres$ mkdir -p ~postgres/backups/16/main/backup
postgres$ pg_basebackup -p 5433 -D ~postgres/backups/16/main/backup/copie_1
```

* L'outil `pg_basebackup` ne sauvegarde pas les fichiers de configurations
  situés hors du répertoire `PGDATA`. Il ne faut pas oublier de copier ces
  fichiers :

```bash
postgres$ cp -rf /etc/postgresql/16/main/ ~postgres/backups/16/main/backup/copie_1/conf_files
```

* **Effectuer des actions dans l'instance d'origine. Forcez régulièrement
l'archivage des journaux de transactions.**

```bash
postgres$ createdb nouvelle
postgres$ psql -d nouvelle

# select pg_switch_wal();
# create table ma_table (i int);
# select pg_switch_wal();
# insert into ma_table (i) select generate_series(1, 10000000);
# select pg_switch_wal();
# delete from ma_table where i%4 = 0;
# select pg_switch_wal();
```

**Créer une nouvelle instance avec le commande bash `pg_createcluster`**

Pour restaurer l'instance sauvegardée à la question précédente, nous devons
créer une nouvelle instance _pitr_ :

```bash
postgres$ pg_createcluster 16 pitr
```

**Restaurer la sauvegarde dans cette nouvelle instance et vérifier son bon
fonctionnement**

Copie des fichiers :

```bash
postgres$ rm -rf /var/lib/postgresql/16/pitr/*
postgres$ cp -rf ~postgres/backups/16/main/backup/copie_1/* /var/lib/postgresql/16/pitr/
```

Recopier les fichiers de configurations (en les adaptant si besoin, par exemple
le numéro de port) :

```bash
postgres$ cp -rf ~postgres/backups/16/main/backup/copie_1/conf_files/* /etc/postgresql/16/pitr/
postgres$ $ sed -i 's/^port =.*$/port = 5435/' /etc/postgresql/16/pitr/postgresql.conf
```

Désactivation de l'archivage sur la nouvelle instance dans
`/etc/postgresql/16/pitr/postgresql.conf` :

```ini
archive_command = '/bin/true'
```

Configuration de la nouvelle instance pour rejouer les journaux de
transactions, édition du paramètre `restore_command` dans
`/etc/postgresql/16/pitr/postgresql.conf` :

```ini
restore_command = 'cp /var/lib/postgresql/backups/16/main/wals/%f "%p"'
```

Créer un fichier `recovery.signal` dans _PGDATA_ pour indiquer qu'on créé une
nouvelle instance (et non une instance secondaire en réplication) :

```bash
postgres$ touch /var/lib/postgresql/16/pitr/recovery.signal
```

**Restaurer la sauvegarde dans cette nouvelle instance et vérifier son bon
fonctionnement**

Copie des fichiers :

```bash
postgres$ rm -rf /var/lib/postgresql/16/copie/*
postgres$ cp -rf ~postgres/backups/16/main/backup/first_copie/* /var/lib/postgresql/16/copie/
```

Nous pouvons maintenant démarrer notre copie :

```bash
postgres$ sudo systemctl restart postgresql@16-copie.service
postgres$ pg_lsclusters 16 copie
Ver Cluster Port Status Owner    Data directory               Log file
16  copie   5435 online postgres /var/lib/postgresql/16/copie /var/log/pos(...)
```

#### Mise en place de pgBackRest

Réalisation de sauvegardes physiques à chaud avec le logiciel pgBackRest :

* **Installer le logiciel pgBackRest**

```bash
# apt install pgbackrest -y
```

* **Configurer le logiciel et l'instance PostgreSQL pour réaliser des sauvegardes
physique en ligne de commande**

Une sauvegarde PITR comprend 2 parties : une copie physique de l'instance,
ainsi que tous les fichiers WAL depuis le début de la copie de l'instance. Ces
fichiers WAL peuvent être archiver par PostgreSQL en lui précisant la ligne de
commande à exécuter pour chaque fichier WAL rempli.

**Nous allons créer une _stanza_ pgbackrest (la configuration nécessaire pour
sauvegarder notre instance).**

* Créer le répertoire de configuration de pgbackrest :

```bash
# mkdir -p /etc/pgbackrest
# chown postgres: /etc/pgbackrest
```

* Éditez les fichier `/etc/pgbackrest/pgbackrest.conf` avec l'utilisateur
  _postgres_ :

```
[global]
repo1-path=/var/lib/postgresql/backups/pgbackrest
repo1-retention-full=2
start-fast=y

[db]
pg1-path=/var/lib/postgresql/16/main
pg1-port=5434
```

* Initier la _stanza_:

```bash
postgres$ pgbackrest --stanza=db --log-level-console=info stanza-create
```

**Nous allons ensuite configurer PostgreSQL pour activer l'archivage des WAL.**

* Éditer le fichier de configuration de PostgreSQL :

```
archive_command = 'pgbackrest --stanza=db archive-push %p'
archive_mode = on
```

* Redémarrer l'instance

```bash
# systemctl restart postgresql@16-main.service
```

* Vérifions l'archivage des WAL :

```bash
postgres$ ls -l /var/lib/postgresql/backups/pgbackrest/archive/db/16-1/*
postgres$ psql -c 'CREATE DATABASE test'
postgres$ psql -c 'SELECT pg_switch_wal()'
postgres$ ls -l /var/lib/postgresql/backups/pgbackrest/archive/db/16-1/*
```

On doit avoir un répertoire vide lors du premier appel à _ls_ et avoir un
fichier du type
`00000001000000000000000E-fde16e3a71f0b78d42a486a2d4bad23b588022d5.gz` au
deuxième appel.

Si aucun fichier n'est présent, il faut vérifier dans les logs de PostgreSQL
pour trouver la cause du problème.

```bash
postgres$ less /var/log/postgresql/postgresql-16-main.log
```

On peut également vérifier la configuration de notre _stanza_ :

```bash
postgres$ pgbackrest --stanza=db --log-level-console=info check
```

* **Créer une sauvegarde physique.**

Exécutez les sauvegarde :

```bash
postgres$ pgbackrest --stanza=db --log-level-console=info backup
```

Vérifier le contenu :

```bash
postgres$ ls ~/backups/pgbackrest/backup/db/latest/pg_data/
backup_label.gz  pg_dynshmem   pg_replslot   pg_stat_tmp  PG_VERSION.gz
base		 pg_logical    pg_serial     pg_subtrans  pg_wal
global		 pg_multixact  pg_snapshots  pg_tblspc	  pg_xact
pg_commit_ts	 pg_notify     pg_stat	     pg_twophase  postgresql.auto.conf.gz
```

* **Restaurer une sauvegarde PITR dans une nouvelle instance.**

Création de la nouvelle instance :

```bash
# pg_createcluster 16 restoration
# rm -rf /var/lib/postgresql/16/restoration/*
```

Restauration de la sauvegarde dans la nouvelle instance:

``` bash
postgres$ pgbackrest --stanza=db \
    --pg1-path /var/lib/postgresql/16/restoration/ restore
```

Configuration de la nouvelle instance pour rejouer les journaux de transactions
en utilisant pgBackRest, édition du paramètre `restore_command` dans
`/etc/postgresql/16/restoration/postgresql.conf` :

```ini
restore_command = 'pgbackrest --stanza=db archive-get %f "%p"'
```

Créer un fichier `recovery.signal` dans _PGDATA_ pour indiquer qu'on créé une
nouvelle instance (et non une instance secondaire en réplication) :

```bash
postgres$ touch /var/lib/postgresql/16/restoration/recovery.signal
```

* Démarrer l'instance :

```bash
# systemctl restart postgresql@16-restoration.service
```

* Vérifier le bon fonctionnement en étudiant les logs et en se connectant sur
  l'instance.

```bash
postgresql$ psql -p 5435
psql (16.1 (Debian 16.1-1.pgdg120+1))
Saisissez « help » pour l'aide.

postgres=# \conninfo
Vous êtes connecté à la base de données « postgres » en tant qu'utilisateur « postgres » via le socket dans « /var/run/postgresql » via le port « 5435 ».
```

* **Mettre en place des sauvegardes et purges journalières (par _cron_ ou
  _systemd_).**

**Mettre en place des sauvegardes et purges journalières**

Ajout dans le crontab de l'utilisateur _postgres_ avec la commande `crontab
-e` :

```
30 00 * * * /usr/bin/pgbackrest --stanza=db --log-level-console=info backup
```

Avec `systemd`, création des deux fichiers suivants :

* `/etc/systemd/system/pitr_backrest.service` :

```
[Unit]
Description=Service de sauvegarde PITR de 16-main par pitr
After=local-fs.target

[Service]
Type=oneshot
RemainAfterExit=no
User=postgres
Group=postgres
ExecStart=/usr/bin/pgbackrest --stanza=db --log-level-console=info backup

[Install]
WantedBy=multi-user.target
```

* `/etc/systemd/system/pitr_backrest.timer` :

```
[Unit]
Description=lancement journalier de la sauvegarde PITR de 16-main

[Timer]
OnCalendar=00:30:00
Persistent=true

[Install]
WantedBy=timers.target
```

Puis activation des services :

```bash
# systemctl enable pitr_backrest.timer
# systemctl enable pitr_backrest.service
```

On pourra choisir une heure précise permettant de vérifier durant le TP du bon
fonctionnement de la sauvegarde.


</div>

------

\newpage
