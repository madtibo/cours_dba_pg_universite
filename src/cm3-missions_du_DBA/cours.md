# Missions du DBA

![PostgreSQL](medias/elephant-rock-valley-of-fire.jpg)
\

<div class="notes">
</div>

-----

## Programme de ce cours

<div class="slide-content">

  * Semaine 1 : découverte de PostgreSQL
  * Semaine 2 : transactions et accès concurrents
  * **Semaine 3** : missions du DBA
    * sécurité et droits
    * sauvegardes et restaurations
  * Semaine 4 : optimisation et indexation
  * Semaine 5 : _PL/PgSQL_ et triggers

</div>

<div class="notes">

</div>

-----

## La sécurité dans PostgreSQL

<div class="slide-content">

  * Sécurité physique d'une instance
  * Droits d'accès en PostgreSQL
  * Gestion des droits sur les objets

</div>


<div class="notes">
</div>

-----

## Sécurité physique

<div class="slide-content">

  * Données stockées dans des fichiers
  * Contrôle de l'accès au serveur
  * Application des mises à jours de sécurité

</div>


<div class="notes">

PostgreSQL stocke toutes les données dans des fichiers. Si un utilisateur
malintentionné accède à ces fichiers, il accède à toutes les données stockées.

PostgreSQL ne démarrera pas si les droits sur le répertoire de l'instance
(`data_directory`) est différent de `700` :

```
postgres$ ls -l /var/lib/postgresql/14
total 4
drwx------ 19 postgres postgres 4096 oct.  19 09:38 main
```

Sécuriser les droits sur ces fichiers est important. Sécuriser l'accès physique
ou par ssh à la machine l'est tout autant. Si vous donnez un mot de passe à
l'utilisateur _postgres_, utilisez un outil tel `apg` pour générer un mot de
passe aléatoire solide. Faites de même pour tout utilisateur pouvant se
connecter au serveur.

Des failles de sécurité peuvent être découvertes dans des logiciels tierces
installés sur votre serveur. Il est important d'appliquer les mises à jours de
sécurité régulièrement.

</div>

---------

### Chiffrements

<div class="slide-content">

  * Données disques
    * pas en natif
    * pgcrypto
  * Connexions
    * SSL
    * avec ou sans certificats serveur et/ou client

</div>

<div class="notes">

PostgreSQL ne chiffre pas les données sur disque. Si l'instance complète doit
être chiffrée, il est conseillé d'utiliser un système de fichiers qui
propose cette fonctionnalité. Attention au fait que cela ne vous protège que
contre la récupération des données sur un disque non monté. Quand le disque
est monté, les données sont lisibles suivant les règles d'accès d'Unix.

Néanmoins, il existe un module contrib appelé pgcrypto, permettant d'accéder
à des fonctions de chiffrement et de hachage. Cela permet de protéger les
informations provenant de colonnes spécifiques. Le chiffrement se fait du
côté serveur, ce qui sous-entend que l'information est envoyée en clair sur
le réseau. Le chiffrement SSL est donc obligatoire dans ce cas.

Par défaut, les sessions ne sont pas chiffrées. Les requêtes et les données
passent donc en clair sur le réseau. Il est possible de les chiffrer avec SSL,
ce qui aura une conséquence négative sur les performances. Il est aussi
possible d'utiliser les certificats (au niveau serveur et/ou client) pour
augmenter encore la sécurité des connexions.

</div>

-----

## Droits de connexion

<div class="slide-content">

  * Lors d'une connexion, indication :
    * de l'hôte (socket Unix ou alias/adresse IP)
    * du nom de la base de données
    * du nom du rôle
    * du mot de passe (parfois optionnel)
  * Suivant les trois premières informations
    * impose une méthode d'authentification

</div>


<div class="notes">

Lors d'une connexion, l'utilisateur fournit, explicitement ou non, plusieurs
informations. PostgreSQL va choisir une méthode d'authentification en se basant
sur les informations fournies et sur la configuration d'un fichier appelé
pg_hba.conf. _HBA_ est l'acronyme de Host Based Authentication.

</div>

-----

### `pg_hba.conf` et `pg_ident.conf`

<div class="slide-content">

  * Authentification multiple, suivant l’utilisateur, la base et la source de
la connexion.
    + `pg_hba.conf` (_Host Based Authentication_)
    + `pg_ident.conf`, si mécanisme externe d’authentification
    + paramètres `hba_file` et `ident_file`

</div>


<div class="notes">

L’authentification est paramétrée au moyen du fichier `pg_hba.conf`. Dans ce
fichier, pour une tentative de connexion à une base donnée, pour un
utilisateur donné, pour un transport (IP, IPV6, Socket Unix, SSL ou non), et
pour une source donnée, ce fichier permet de spécifier le mécanisme
d’authentification attendu.

Si le mécanisme d’authentification s’appuie sur un système externe (LDAP,
Kerberos, Radius…), des tables de correspondances entre utilisateur de la base
et utilisateur demandant la connexion peuvent être spécifiées dans
`pg_ident.conf`.

Ces noms de fichiers ne sont que les noms par défaut. Ils peuvent tout à fait
être remplacés en spécifiant de nouvelles valeurs de `hba_file` et
`ident_file` dans `postgresql.conf`.

</div>

-----

### Informations de connexion

<div class="slide-content">

  * Quatre informations :
    * socket Unix ou adresse/alias IP
    * numéro de port
    * nom de la base
    * nom du rôle
  * Fournies explicitement ou implicitement

</div>

<div class="notes">

Tous les outils fournis avec la distribution PostgreSQL (par exemple
`createuser `) acceptent des options en ligne de commande pour fournir les
informations en question :

  * `-h` pour la socket Unix ou l'adresse/alias IP
  * `-p` pour le numéro de port
  * `-d` pour le nom de la base
  * `-U` pour le nom du rôle

Si l'utilisateur ne passe pas ces informations, plusieurs variables
d'environnement sont vérifiées :

  * `PGHOST` pour la socket Unix ou l'adresse/alias IP
  * `PGPORT` pour le numéro de port
  * `PGDATABASE` pour le nom de la base
  * `PGUSER` pour le nom du rôle

Au cas où ces variables ne seraient pas configurées, des valeurs par défaut
sont utilisées :

  * la socket Unix
  * 5432 pour le numéro de port
  * suivant l'outil, la base postgres ou le nom de l'utilisateur PostgreSQL,
pour le nom de la base
  * le nom de l'utilisateur au niveau du système d'exploitation pour le nom du
rôle

Autrement dit, quelle que soit la situation, PostgreSQL remplacera les
informations non fournies explicitement par des informations provenant des
variables d'environnement, voire par des informations par défaut.

</div>

-----

### Configuration de l'authentification

<div class="slide-content">

  * PostgreSQL utilise les informations de connexion pour sélectionner la méthode
  * Fichier de configuration: `pg_hba.conf`
  * Se présente sous la forme d'un tableau
    * 4 colonnes d'informations
    * 1 colonne indiquant la méthode à appliquer
    * 1 colonne optionnelle d'options

</div>

<div class="notes">

Lorsque le serveur PostgreSQL récupère une demande de connexion, il connaît le
type de connexion utilisé par le client (socket Unix, connexion TCP SSL,
connexion TCP simple, etc). Il connaît aussi l'adresse IP du client (dans le
cas d'une connexion via une socket TCP), le nom de la base et celui de
l'utilisateur. Il va donc parcourir les lignes du tableau enregistré dans le
fichier `pg_hba.conf`. Il les parcourt dans l'ordre. La première qui correspond
aux informations fournies lui précise la méthode d'authentification. Il ne lui
reste plus qu'à appliquer cette méthode. Si elle fonctionne, la connexion est
autorisée et se poursuit. Si elle ne fonctionne pas, quelle qu'en soit la
raison, la connexion est refusée. Aucune autre ligne du fichier ne sera lue.

Il est donc essentiel de bien configurer ce fichier pour avoir une protection
maximale.

Le tableau se présente ainsi :

```
# local      DATABASE  USER  METHOD  [OPTIONS]
# host       DATABASE  USER  ADDRESS  METHOD  [OPTIONS]
# hostssl    DATABASE  USER  ADDRESS  METHOD  [OPTIONS]
# hostnossl  DATABASE  USER  ADDRESS  METHOD  [OPTIONS]
```

</div>

-----

### Colonne type

<div class="slide-content">

  * 4 valeurs possibles
    * `local`
    * `host`
    * `hostssl`
    * `hostnossl`
  * `hostssl` nécessite d'avoir activé `ssl` dans `postgresql.conf`
</div>

<div class="notes">

La colonne type peut contenir quatre valeurs différentes. La valeur `local`
concerne les connexions via la socket Unix. Toutes les autres valeurs concernent
les connexions via la socket TCP. La différence réside dans l'utilisation
forcée ou non du SSL :


  * `host`, connexion via la socket TCP, avec ou sans SSL ;
  * `hostssl`, connexion via la socket TCP, avec SSL ;
  * `hostnossl`, connexion via la socket TCP, sans SSL.

Il est à noter que l'option `hostssl` n'est utilisable que si le paramètre `
ssl` du fichier `postgresql.conf` est à `on`.

</div>

-----

### Colonne database

<div class="slide-content">

  * Nom de la base
  * Plusieurs bases (séparées par des virgules)
  * Nom d'un fichier contenant la liste des bases (précédé par une arobase)
  * Mais aussi
    * `all` (pour toutes les bases)
    * `sameuser`, `samerole` (pour la base de même nom que le rôle)
    * `replication` (pour les connexions de réplication physique)

</div>

<div class="notes">

La colonne peut recueillir le nom d'une base, le nom de plusieurs bases en les
séparant par des virgules, le nom d'un fichier contenant la liste des bases ou
quelques valeurs en dur. La valeur all indique toutes les bases. La valeur
replication est utilisée pour les connexions de réplication (il n'est pas
nécessaire d'avoir une base nommée replication). Enfin, la valeur `sameuser`
spécifie que l'enregistrement n'intercepte que si la base de données demandée
a le même nom que le rôle demandé, alors que la valeur `samerole` spécifie
que le rôle demandé doit être membre du rôle portant le même nom que la
base de données demandée.

</div>

-----

### Colonne user

<div class="slide-content">

  * Nom du rôle
  * Nom d'un groupe (précédé par un signe plus)
  * Plusieurs rôles (séparés par des virgules)
  * Nom d'un fichier contenant la liste des rôles (précédé par une arobase)
  * Mais aussi
    * `all` (pour tous les rôles)

</div>

<div class="notes">

La colonne peut recueillir le nom d'un rôle, le nom d'un groupe en le
précédant d'un signe plus, le nom de plusieurs rôles en les séparant par des
virgules, le nom d'un fichier contenant la liste des bases ou quelques valeurs
en dur. La valeur all indique tous les rôles.

</div>

-----

### Colonne adresse IP

<div class="slide-content">

  * Uniquement dans le cas d'une connexion `host`, `hostssl` et `hostnossl`
  * Soit l'adresse IP et le masque réseau
  * Soit l'adresse au format CIDR
  * Soit un nom DNS

</div>

<div class="notes">

La colonne de l'adresse IP permet d'indiquer une adresse IP ou un sous-réseau
IP. Il est donc possible de filtrer les connexions par rapport aux adresses IP,
ce qui est une excellente protection.

Voici deux exemples d'adresses IP au format adresse et masque de sous-réseau :

```
192.168.168.1 255.255.255.255
192.168.168.0 255.255.255.0
```

Et voici deux exemples d'adresses IP au format CIDR :

```
192.168.168.1/32
192.168.168.0/24
```

Il est possible d'utiliser un nom d'hôte ou un domaine DNS, au prix d'une
recherche DNS pour chaque hostname présent, pour chaque nouvelle connexion.

</div>

-----

### Colonne méthode

<div class="slide-content">

  * Précise la méthode d'authentification à utiliser
  * Deux types de méthodes
    * internes
    * externes
  * Possibilité d'ajouter des options dans une dernière colonne

</div>

<div class="notes">

La colonne de la méthode est la dernière colonne, voire l'avant-dernière si
vous voulez ajouter une option à la méthode d'authentification.

</div>

-----

### Colonne options

<div class="slide-content">

  * Dépend de la méthode d'authentification
  * Méthode externe : option `map`

</div>

<div class="notes">

Les options disponibles dépendent de la méthode d'authentification
sélectionnée. Toutes les méthodes externes permettent l'utilisation de
l'option map. Cette option a pour but d'indiquer la carte de correspondance à
sélectionner dans le fichier `pg_ident.conf`.

</div>

-----

### Méthodes internes

<div class="slide-content">

  * `trust`: à proscrire
  * `password` et `md5` : dépassées
  * `scram-sha-256`
  * `reject`

</div>

<div class="notes">

Le support de `scram-sha-256` est disponible à partir de la version 10. ELle
est devenue la méthode par défaut en version 14. Cette méthode est la plus
sûre.

La méthode `reject` est intéressante dans certains cas de figure. Par exemple,
on veut que le rôle u1 puisse se connecter à la base de données b1 mais pas aux
autres. Voici un moyen de le faire (pour une connexion via les sockets Unix) :

```
local  b1   u1  scram-sha-256
local  all  u1  reject
```

La méthode `trust` est certainement la pire. À partir du moment où le rôle
est reconnu, aucun mot de passe n'est demandé. Si le mot de passe est fourni
malgré tout, il n'est pas vérifié. Il est donc essentiel de proscrire cette
méthode d'authentification.

La méthode `password` force la saisie d'un mot de passe. Cependant, ce dernier
est envoyé en clair sur le réseau. Il n'est donc pas conseillé d'utiliser
cette méthode, surtout sur un réseau non sécurisé.

La méthode `md5` est la méthode conseillée avant la version 10. La saisie du
mot de passe est forcée. De plus, le mot de passe transite chiffré en md5.


</div>

-----

### Méthodes externes

<div class="slide-content">

  * `ldap`, `radius`, `cert`
  * `gss`, `sspi`
  * `ident`, **`peer`**, `pam`

</div>

<div class="notes">

Ces différentes méthodes permettent d'utiliser des annuaires d'entreprise
comme RADIUS, LDAP ou un ActiveDirectory. Certaines méthodes sont spécifiques
à Unix (comme `ident` et `peer`), voire à Linux (comme `pam`).

La méthode `LDAP` utilise un serveur LDAP pour authentifier l'utilisateur.

La méthode `GSSAPI` correspond au protocole du standard de l'industrie pour
l'authentification sécurisée définie dans RFC 2743. PostgreSQL supporte
`GSSAPI` avec l'authentification Kerberos suivant la RFC 1964 ce qui permet de
faire du « Single Sign-On ».

La méthode `Radius` permet d'utiliser un serveur RADIUS pour authentifier
l'utilisateur.

La méthode `ident` permet d'associer les noms des utilisateurs du système
d'exploitation aux noms des utilisateurs du système de gestion de bases de
données. Un démon fournissant le service ident est nécessaire.

La méthode `peer` permet d'associer les noms des utilisateurs du système
d'exploitation aux noms des utilisateurs du système de gestion de bases de
données. Ceci n'est possible qu'avec une connexion locale.

Quant à `pam`, il authentifie l'utilisateur en passant par les `Pluggable
Authentication Modules` (PAM) fournis par le système d'exploitation.

</div>

-----

### Le pg_hba.conf par défaut

<div class="slide-content">

Voici les entrées par défaut du fichier `pg_hba.conf`  
en version 14 sur Debian :

```
# TYPE  DATABASE        USER            ADDRESS                 METHOD
local   all             postgres                                peer
local   all             all                                     peer
host    all             all             127.0.0.1/32            scram-sha-256
host    all             all             ::1/128                 scram-sha-256
local   replication     all                                     peer
host    replication     all             127.0.0.1/32            scram-sha-256
host    replication     all             ::1/128                 scram-sha-256
```

</div>

<div class="notes">

</div>

-----

## Droits sur les objets

<div class="slide-content">

  * Droits sur les objets
  * Droits sur les méta-données
  * Héritage des droits

</div>

<div class="notes">

À l'installation de PostgreSQL, il est essentiel de s'assurer de la sécurité du
serveur : sécurité au niveau des accès, au niveau des objets, ainsi qu'au
niveau des données.

Ce chapitre va faire le point sur ce qu'un utilisateur peut faire par défaut et
sur ce qu'il ne peut pas faire. Nous verrons ensuite comment restreindre les
droits.

Pour bien comprendre l'intérêt des utilisateurs, il faut bien comprendre la
gestion des droits. Les droits sur les objets vont permettre aux utilisateurs de
créer des objets ou de les utiliser. Les commandes `GRANT` et `REVOKE` sont
essentielles pour cela. Modifier la définition d'un objet demande un autre type
de droit, que les commandes précédentes ne permettent pas d'obtenir.

Donner des droits à chaque utilisateur peut paraître long et difficile. C'est
pour cela qu'il est généralement préférable de donner des droits à une
entité spécifique dont certains utilisateurs hériteront.

</div>

-----

### Agir sur les droits

<div class="slide-content">

  * Donner un droit : `GRANT`
  * Retirer un droit : `REVOKE`
  * Droits spécifiques pour chaque type d'objets
  * Avoir le droit de donner le droit : `WITH GRANT OPTION`

</div>

<div class="notes">

Par défaut, seul le propriétaire a des droits sur son objet. Les
superutilisateurs n'ont pas de droit spécifique sur les objets mais étant donné
leur statut de superutilisateur, ils peuvent tout faire sur tous les objets.

Le propriétaire d'un objet peut décider de donner certains droits sur cet
objet à certains rôles. Il le fera avec la commande `GRANT` :

```sql
GRANT droits ON type_objet nom_objet TO role
```

Les droits
disponibles dépendent du type d'objet visé. Par exemple, il est possible de
donner le droit `SELECT` sur une table mais pas sur une fonction. Une fonction
ne se lit pas, elle s'exécute. Il est donc possible de donner le droit `EXECUTE
` sur une fonction. Il faut donner les droits aux différents objets
séparément. De plus, donner le droit `ALL` sur une base de données donne tous
les droits sur la base de données, autrement dit l'objet base de donnée, pas
sur les objets à l'intérieur de la base de données. `GRANT` n'est pas une
commande récursive. Prenons un exemple :

```
b1=# CREATE ROLE u20 LOGIN;
CREATE ROLE
b1=# CREATE ROLE u21 LOGIN;
CREATE ROLE
b1=# \c b1 u20
You are now connected to database "b1" as user "u20".
b1=> CREATE SCHEMA s1;
ERROR:  permission denied for database b1
b1=> \c b1 postgres
You are now connected to database "b1" as user "postgres".
b1=# GRANT CREATE ON DATABASE b1 TO u20;
GRANT
b1=# \c b1 u20
You are now connected to database "b1" as user "u20".
b1=> CREATE SCHEMA s1;
CREATE SCHEMA
b1=> CREATE TABLE s1.t1 (c1 integer);
CREATE TABLE
b1=> INSERT INTO s1.t1 VALUES (1), (2);
INSERT 0 2
b1=> SELECT * FROM s1.t1;
 c1
----
  1
  2
(2 rows)

b1=> \c b1 u21
You are now connected to database "b1" as user "u21".
b1=> SELECT * FROM s1.t1;
ERROR:  permission denied for schema s1
LINE 1: SELECT * FROM s1.t1;
                      ^
b1=> \c b1 u20
You are now connected to database "b1" as user "u20".
b1=> GRANT SELECT ON TABLE s1.t1 TO u21;
GRANT
b1=> \c b1 u21
You are now connected to database "b1" as user "u21".
b1=> SELECT * FROM s1.t1;
ERROR:  permission denied for schema s1
LINE 1: SELECT * FROM s1.t1;
                      ^
b1=> \c b1 u20
You are now connected to database "b1" as user "u20".
b1=> GRANT USAGE ON SCHEMA s1 TO u21;
GRANT
b1=> \c b1 u21
You are now connected to database "b1" as user "u21".
b1=> SELECT * FROM s1.t1;
 c1
----
  1
  2
(2 rows)

b1=> INSERT INTO s1.t1 VALUES (3);
ERROR:  permission denied for relation t1
```

Le problème de ce fonctionnement est qu'il faut indiquer les droits pour chaque
utilisateur, ce qui peut devenir difficile et long. Imaginez avoir à donner le
droit `SELECT` sur les 400 tables d'un schéma... long et fastidieux... Il est
néanmoins possible de donner les droits sur tous les objets d'un certain type
dans un schéma. Voici un exemple :

```sql
GRANT SELECT ON ALL TABLES IN SCHEMA s1 to u21;
```

Notez aussi que, lors de la création d'une base, PostgreSQL ajoute
automatiquement un schéma nommé public. Tous les droits sont donnés sur ce
schéma à un pseudo-rôle appelé public. Tous les rôles sont automatiquement
membres de ce pseudo-rôle. Si vous voulez gérer complètement les droits sur
ce schéma, il faut d'abord penser à enlever les droits pour ce pseudo-rôle.
Pour cela, il vous faut utiliser la commande `REVOKE` ainsi :

```sql
REVOKE ALL ON SCHEMA public FROM public;
```

Il est possible d'ajouter des droits pour des objets qui n'ont pas encore été
créés. En fait, la commande `ALTER DEFAULT PRIVILEGES` permet de donner des
droits par défaut à certains rôles. De cette façon, sur un schéma qui a
tendance à changer fréquemment, il n'est plus nécessaire de se préoccuper des
droits sur les objets.

Lorsqu'un droit est donné à un rôle, par défaut, ce rôle ne peut pas le
donner à un autre. Pour lui donner en plus le droit de donner ce droit à un
autre rôle, il faut utiliser la clause `WITH GRANT OPTION` comme le montre cet
exemple :

```
b1=# CREATE TABLE t2 (id integer);
CREATE TABLE
b1=# INSERT INTO t2 VALUES (1);
INSERT 0 1
b1=# SELECT * FROM t2;
 id
----
  1
(1 row)

b1=# \c b1 u1
You are now connected to database "b1" as user "u1".
b1=> SELECT * FROM t2;
ERROR:  permission denied for relation t2
b1=> \c b1 postgres
You are now connected to database "b1" as user "postgres".
b1=# GRANT SELECT ON TABLE t2 TO u1;
GRANT
b1=# \c b1 u1
You are now connected to database "b1" as user "u1".
b1=> SELECT * FROM t2;
 id
----
  1
(1 row)

b1=> \c b1 u2
You are now connected to database "b1" as user "u2".
b1=> SELECT * FROM t2;
ERROR:  permission denied for relation t2
b1=> \c b1 u1
You are now connected to database "b1" as user "u1".
b1=> GRANT SELECT ON TABLE t2 TO u2;
WARNING:  no privileges were granted for "t2"
GRANT
b1=> \c b1 postgres
You are now connected to database "b1" as user "postgres".
b1=# GRANT SELECT ON TABLE t2 TO u1 WITH GRANT OPTION;
GRANT
b1=# \c b1 u1
You are now connected to database "b1" as user "u1".
b1=> GRANT SELECT ON TABLE t2 TO u2;
GRANT
b1=> \c b1 u2
You are now connected to database "b1" as user "u2".
b1=> SELECT * FROM t2;
 id
----
  1
(1 row)
```

</div>

-----

### Droits sur les métadonnées

<div class="slide-content">

  * Seul le propriétaire peut changer la structure d'un objet
    * le renommer
    * le changer de schéma ou de tablespace
    * lui ajouter/retirer des colonnes
  * Un seul propriétaire
    * mais qui peut être un groupe

</div>

<div class="notes">

Les droits sur les objets ne concernent pas le changement des méta-données et
de la structure de l'objet. Seul le propriétaire (et les superutilisateurs)
peut le faire. S'il est nécessaire que plusieurs personnes puissent utiliser la
commande `ALTER` sur l'objet, il faut que ces différentes personnes aient un
rôle qui soit membre du rôle propriétaire de l'objet. Prenons un exemple :

```
b1=# \c b1 u21
You are now connected to database "b1" as user "u21".
b1=> ALTER TABLE s1.t1 ADD COLUMN c2 text;
ERROR:  must be owner of relation t1
b1=> \c b1 u20
You are now connected to database "b1" as user "u20".
b1=> GRANT u20 TO u21;
GRANT ROLE
b1=> \du
                                         List of roles
 Role name  |                         Attributes           |      Member of
------------+----------------------------------------------+--------------------
 admin      |                                              | {}
 caviste    |                                              | {}
 postgres   | Superuser, Create role, Create DB,          +| {}
            | Replication, Bypass RLS                      |
 stagiaire2 |                                              | {}
 u1         |                                              | {pg_signal_backend}
 u11        |                                              | {}
 u2         | Create DB                                    | {}
 u20        |                                              | {}
 u21        |                                              | {u20}
 u3         | Cannot login                                 | {}
 u4         | Create role, Cannot login                    | {}
 u5         |                                              | {u2,u6}
 u6         | Cannot login                                 | {}
 u7         | Create role                                  | {}
 u8         |                                              | {}
 u9         | Create DB                                    | {}

b1=> \c b1 u21
You are now connected to database "b1" as user "u21".
b1=> ALTER TABLE s1.t1 ADD COLUMN c2 text;
ALTER TABLE
```


Pour assigner un propriétaire différent aux objets ayant un certain
propriétaire, il est possible de faire appel à l'ordre `REASSIGN OWNED`. De
même, il est possible de supprimer tous les objets appartenant à un
utilisateur avec l'ordre `DROP OWNED`. Voici un exemple de ces deux commandes :

```
b1=# \d
         List of relations
 Schema |   Name   | Type  | Owner
--------+----------+-------+-------
 public | clients  | table | r31
 public | factures | table | r31
 public | t1       | table | r31
 public | t2       | table | r32
(4 rows)

b1=# REASSIGN OWNED BY r31 TO u1;
REASSIGN OWNED
b1=# \d
         List of relations
 Schema |   Name   | Type  | Owner
--------+----------+-------+-------
 public | clients  | table | u1
 public | factures | table | u1
 public | t1       | table | u1
 public | t2       | table | r32
(4 rows)

b1=# DROP OWNED BY u1;
DROP OWNED
b1=# \d
       List of relations
 Schema | Name | Type  | Owner
--------+------+-------+-------
 public | t2   | table | r32
(1 row)
```

</div>

-----

### Héritage des droits

<div class="slide-content">

  * Créer un rôle sans droit de connexion
  * Donner les droits à ce rôle
  * Placer les utilisateurs concernés comme membre de ce rôle

</div>

<div class="notes">

Plutôt que d'avoir à donner les droits sur chaque objet à chaque ajout d'un
rôle, il est beaucoup plus simple d'utiliser le système d'héritage des
droits.

Supposons qu'une nouvelle personne arrive dans le service de facturation. Elle
doit avoir accès à toutes les tables concernant ce service. Sans utiliser
l'héritage, il faudra récupérer les droits d'une autre personne du service pour
retrouver la liste des droits à donner à cette nouvelle personne. De plus, si
un nouvel objet est créé et que chaque personne du service doit pouvoir y
accéder, il faudra ajouter l'objet et ajouter les droits pour chaque personne
du service sur cet objet. C'est long et sujet à erreur. Il est préférable de
créer un rôle facturation, de donner les droits sur ce rôle, puis d'ajouter
chaque rôle du service facturation comme membre du rôle facturation. L'ajout et
la suppression d'un objet est très simple : il suffit d'ajouter ou de retirer
le droit sur le rôle facturation, et cela impactera tous les rôles membres.

Voici un exemple complet :

```
b1=# CREATE ROLE facturation;
CREATE ROLE
b1=# CREATE TABLE factures(id integer, dcreation date, libelle text,
montant numeric);
CREATE TABLE
b1=# GRANT ALL ON TABLE factures TO facturation;
GRANT
b1=# CREATE TABLE clients (id integer, nom text);
CREATE TABLE
b1=# GRANT ALL ON TABLE clients TO facturation;
GRANT
b1=# CREATE ROLE r1 LOGIN;
CREATE ROLE
b1=# GRANT facturation TO r1;
GRANT ROLE
b1=# \c b1 r1
You are now connected to database "b1" as user "r1".
b1=> SELECT * FROM factures;
 id | dcreation | libelle | montant
----+-----------+---------+---------
(0 rows)

b1=# CREATE ROLE r2 LOGIN;
CREATE ROLE
b1=# \c b1 r2
You are now connected to database "b1" as user "r2".
b1=> SELECT * FROM factures;
ERROR:  permission denied for relation factures
```

</div>

-----

### Droits par défaut

<div class="slide-content">

  * Un utilisateur standard peut
    * accéder à toutes les bases de données
    * créer des objets dans le schéma PUBLIC de toute base de données
    * créer des objets temporaires
    * modifier les paramètres de la session
    * créer des fonctions
    * exécuter des fonctions définies par d'autres dans le schéma PUBLIC
    * récupérer des informations sur l'instance
    * visualiser le source des vues et des fonctions

</div>

<div class="notes">

Par défaut, un utilisateur a beaucoup de droits.

Il peut accéder à toutes les bases de données. Il faut modifier le fichier `
pg_hba.conf` pour éviter cela. Il est aussi possible de supprimer ce droit avec
l'ordre suivant :

```sql
REVOKE CONNECT ON DATABASE nom_base FROM nom_utilisateur;
```

Il peut créer des objets dans le schéma disponible par
défaut (nommé `public`) sur chacune des bases de données où il peut se
connecter. Il est possible de supprimer ce droit avec l'ordre suivant :

```sql
REVOKE CREATE ON SCHEMA public FROM nom_utilisateur;
```

Il peut
créer des objets temporaires sur chacune des bases de données où il peut se
connecter. Il est possible de supprimer ce droit avec l'ordre suivant :

```sql
REVOKE TEMP ON DATABASE nom_base FROM nom_utilisateur;
```

Il peut modifier les paramètres de session (par exemple le paramètre `work_mem` pour
s'allouer beaucoup plus de mémoire). Il est impossible d'empêcher cela.

Il peut créer des fonctions, uniquement avec les langages de confiance,
uniquement dans les schémas où il a le droit de créer des objets. Il existe
deux solutions :

  * supprimer le droit d'utiliser un langage

```sql
REVOKE USAGE ON LANGUAGE nom_langage FROM nom_utilisateur;
```

  * supprimer le droit de créer des objets
dans un schéma

```sql
REVOKE CREATE ON SCHEMA nom_schema FROM nom_utilisateur;
```

Il peut exécuter toute fonction, y compris définie par d'autres,
à condition qu'elles soient créées dans des schémas où il a accès.
Il est possible d'empêcher cela en supprimant le droit d'exécution d'une
fonction:

```sql
REVOKE EXECUTE ON FUNCTION nom_fonction FROM nom_utilisateur;
```

Il peut récupérer des informations sur l'instance car il a le droit
de lire tous les catalogues systèmes. Par exemple, en lisant pg_class, il peut
connaitre la liste des tables, vues, séquences, etc. En parcourant pg_proc, il
dispose de la liste des fonctions. Il n'y a pas de contournement à cela : un
utilisateur doit pouvoir accéder aux catalogues systèmes pour travailler
normalement.

Enfin, il peut visualiser le source des vues et des fonctions. Il existe des
modules propriétaires de chiffrement (ou plutôt d'obfuscation) du code mais
rien de natif. Le plus simple est certainement de coder les fonctions sensibles
en C.

</div>

-----

### Par défaut (suite)

<div class="slide-content">

  * Un utilisateur standard ne peut pas
    * créer une base
    * créer un rôle
    * accéder au contenu des objets créés par d'autres
    * modifier le contenu d'objets créés par d'autres

</div>

<div class="notes">

Un utilisateur standard ne peut pas créer de bases et de rôles. Il a besoin
pour cela d'attributs particuliers (respectivement `CREATEDB` et `CREATEROLE`).

Il ne peut pas accéder au contenu (aux données) d'objets créés par d'autres
utilisateurs. Ces derniers doivent lui donner ce droit explicitement. De même,
il ne peut pas modifier le contenu et la définition d'objets créés par d'autres
utilisateurs. Là-aussi, ce droit doit être donné explicitement.

</div>

------

## La sauvegarde / restauration en base de données

<div class="slide-content">

  * Opération essentielle pour la sécurisation des données
  * PostgreSQL propose différentes solutions
    * de sauvegarde à froid ou à chaud, mais cohérentes
    * des méthodes de restauration partielle ou complète

</div>

<div class="notes">

La mise en place d'une solution de sauvegarde est une des opérations les plus
importantes après avoir installé un serveur PostgreSQL. En effet, nul n'est à
l'abri d'un bug logiciel, d'une panne matérielle, voire d'une erreur humaine.

Cette opération est néanmoins plus complexe qu'une sauvegarde standard car
elle doit pouvoir s'adapter aux besoins des utilisateurs. Quand le serveur ne
peut jamais être arrêté, la sauvegarde à froid des fichiers ne peut
convenir. Il faudra passer dans ce cas par un outil qui pourra sauvegarder les
données alors que les utilisateurs travaillent et qui devra respecter les
contraintes ACID pour fournir une sauvegarde cohérente des données.

PostgreSQL va donc proposer des méthodes de sauvegardes à froid (autrement dit
serveur arrêté) comme à chaud, mais de toute façon cohérente. Les
sauvegardes pourront être partielles ou complètes, suivant le besoin des
utilisateurs.

La méthode de sauvegarde dictera l'outil de restauration. Suivant l'outil, il
fonctionnera à froid ou à chaud, et permettra même dans certains cas de faire
une restauration partielle.

</div>

-----

### Plan

<div class="slide-content">

  * Politique de sauvegarde
  * Sauvegarde logique
  * Sauvegarde physique

</div>

<div class="notes">

</div>

-----

## Définir une politique de sauvegarde

<div class="slide-content">

  * Pourquoi établir une politique ?
  * Que sauvegarder ?
  * À quelle fréquence sauvegarder les données ?
  * Quels supports ?
  * Quels outils ?
  * Vérifier la restauration des sauvegardes

</div>

<div class="notes">

Afin d'assurer la sécurité des données, il est nécessaire de faire des
sauvegardes régulières.

Ces sauvegardes vont servir, en cas de problème, à restaurer les bases de
données dans un état le plus proche possible du moment où le problème est
survenu.

Cependant, le jour où une restauration sera nécessaire, il est possible que la
personne qui a mis en place les sauvegardes ne soit pas présente. C'est pour
cela qu'il est essentiel d'écrire et de maintenir un document qui indique la
mise en place de la sauvegarde et qui détaille comment restaurer une
sauvegarde.

En effet, suivant les besoins, les outils pour sauvegarder, le contenu de la
sauvegarde, sa fréquence ne seront pas les mêmes.

Par exemple, il n'est pas toujours nécessaire de tout sauvegarder. Une base de
données peut contenir des données de travail, temporaires et/ou faciles à
reconstruire, stockées dans des tables standards. Il est également possible
d'avoir une base dédiée pour stocker ce genre d'objets. Pour diminuer le temps
de sauvegarde (et du coup de restauration), il est possible de sauvegarder
partiellement son serveur pour ne conserver que les données importantes.

La fréquence peut aussi varier. Un utilisateur peut disposer d'un serveur
PostgreSQL pour un entrepôt de données, serveur qu'il n'alimente qu'une fois
par semaine. Dans ce cas, il est inutile de sauvegarder tous les jours. Une
sauvegarde après chaque alimentation (donc chaque semaine) est suffisante. En
fait, il faut déterminer la fréquence de sauvegarde des données selon :

  * le volume de données à sauvegarder ;
  * la criticité des données ;
  * la quantité de données qu'il est « acceptable » de perdre en cas de
problème.

Le support de sauvegarde est lui aussi très important. Il est possible de
sauvegarder les données sur un disque réseau (à travers Netbios ou NFS), sur
des disques locaux dédiés, sur des bandes ou tout autre support adapté. Dans
tous les cas, il est fortement déconseillé de stocker les sauvegardes sur les
disques utilisés par la base de données.

Ce document doit aussi indiquer comment effectuer la restauration. Si la
sauvegarde est composée de plusieurs fichiers, l'ordre de restauration des
fichiers peut être essentiel. De plus, savoir où se trouvent les sauvegardes
permet de gagner un temps important, qui évitera une immobilisation trop
longue.

De même, vérifier la restauration des sauvegardes de façon régulière est
une précaution très utile.

</div>

-----

### Objectifs

<div class="slide-content">

  * Sécuriser les données
  * Mettre à jour le moteur de données
  * Dupliquer une base de données de production
  * Archiver les données

</div>


<div class="notes">

L'objectif essentiel de la sauvegarde est la sécurisation des données.
Autrement dit, l'utilisateur cherche à se protéger d'une panne matérielle ou
d'une erreur humaine (un utilisateur qui supprimerait des données
essentielles). La sauvegarde permet de restaurer les données perdues. Mais ce n
'est pas le seul objectif d'une sauvegarde.

Une sauvegarde peut aussi servir à dupliquer une base de données sur un
serveur de test ou de préproduction. Elle permet aussi d'archiver des tables.
Cela se voit surtout dans le cadre des tables partitionnées où l'archivage de
la table la plus ancienne permet ensuite sa suppression de la base pour gagner
en espace disque.

Un autre cas d'utilisation de la sauvegarde est la mise à jour majeure de
versions PostgreSQL. Il s'agit de la solution historique de mise à jour (export
/import). Historique, mais pas obsolète.

</div>

-----

### RTO / RPO

<div class="slide-content">

  * RPO (_Recovery Point Objective_) : Perte de Données Maximale Admissible
  * RTO (_Recovery Time Objective_) : Durée Maximale d'Interruption Admissible
  * => Permettent de définir la politique de sauvegarde/restauration

</div>

![RTO et RPO](medias/common-rto-rpo.png)
\

<div class="notes">

Le RPO et RTO sont deux concepts déterminants dans le choix des politiques de
sauvegardes.

  * RPO faible : Le perte de données admissible est très faible voire nulle, il
faudra s'orienter vers des solutions de type :
    * Sauvegarde à chaud
    * PITR
    * Réplication (asynchrone/synchrone).
  * RPO important : On s'autorise une perte de données importante, on peut
utiliser des solutions de type :
    * Sauvegarde logique (dump)
    * Sauvegarde fichier à froid

  * RTO court : Durée d'interruption courte, le service doit vite remonter.
Nécessite des procédures avec le moins de manipulations possible et réduisant
le nombre d'acteurs :
    * Réplication
    * Solutions Haut Disponibilité
  * RTO long : La durée de reprise du service n'est
pas critique on peut utiliser des solutions simple comme :
    * Restauration fichier
    * Restauration sauvegarde logique (dump).

Plus le besoin en RTO/RPO sera court plus les solutions seront complexes à
mettre en œuvre. Inversement, pour des données non critiques, un RTO/RPO long
permet d'utiliser des solutions simples.

</div>

-----

### Différentes approches

<div class="slide-content">

  * Sauvegarde à froid des fichiers (ou physique)
  * Sauvegarde à chaud en SQL (ou logique)
  * Sauvegarde à chaud des fichiers (PITR)

</div>

<div class="notes">

À ces différents objectifs vont correspondre différentes approches de la
sauvegarde.

La sauvegarde au niveau système de fichiers permet de conserver une image
cohérente de l'intégralité des répertoires de données d'une instance
arrêtée. C'est la sauvegarde à froid. Cependant, l'utilisation d'outils de
snapshots pour effectuer les sauvegardes peut accélérer considérablement les
temps de sauvegarde des bases de données, et donc diminuer d'autant le temps
d'immobilisation du système.

La sauvegarde logique permet de créer un fichier texte de commandes SQL ou un
fichier binaire contenant le schéma et les données de la base de données.

La sauvegarde à chaud des fichiers est possible avec le _Point In Time Recovery_.

Suivant les prérequis et les limitations de chaque méthode, il est fort
possible qu'une seule de ces solutions soit utilisable. Par exemple, si le
serveur ne peut pas être arrêté la sauvegarde à froid est exclue d'office,
si la base de données est très volumineuse la sauvegarde logique devient très
longue, si l'espace disque est limité et que l'instance génère beaucoup de
journaux de transactions la sauvegarde _PITR_ sera difficile à mettre en place.

</div>

-----

### Sauvegardes logiques

<div class="slide-content">

  * À chaud
  * Cohérente
  * Locale ou à distance
  * 2 outils
    * `pg_dump`
    * `pg_dumpall`

</div>

<div class="notes">

La sauvegarde logique nécessite que le serveur soit en cours d'exécution. Un
outil se connecte à la base et récupère la déclaration des différents
objets ainsi que les données des tables.

Comme ce type d'outil n'a besoin que d'une connexion standard à la base de
données, il peut se connecter en local comme à distance. Cela implique qu'il
doive aussi respecter les autorisations de connexion configurées dans le
fichier `pg_hba.conf`.

Il existe deux outils de ce type pour la sauvegarde logique dans la distribution
officielle de PostgreSQL :

  * `pg_dump`, pour sauvegarder une base (complètement ou partiellement) ;
  * `pg_dumpall` pour sauvegarder toutes les bases ainsi que les objets globaux.

</div>

-----

### pg_dump

<div class="slide-content">

  * Sauvegarde une base de données
  * Sauvegarde complète ou partielle
  * Format de sortie :
    * texte SQL : `plain`
	* binaire : `tar`, `custom` ou `directory`

</div>

<div class="notes">

pg_dump est l'outil le plus utilisé pour sauvegarder une base de données
PostgreSQL. Une sauvegarde peut se faire de façon très simple. Par exemple :

```bash
postgres$ pg_dump b1 > b1.dump
```

sauvegardera la base b1 de l'instance locale sur le port 5432 dans un fichier
b1.dump.

Mais pg_dump permet d'aller bien plus loin que la sauvegarde d'une base de
données complète. Il existe pour cela de nombreuses options en ligne de
commande.

</div>

-----

### pg_dumpall

<div class="slide-content">

  * Sauvegarde d'une instance complète
    * Objets globaux
    * Bases de données
  * Toujours en format texte

</div>

<div class="notes">

`pg_dump` sauvegarde toute la structure et toutes les données locales à une
base de données. Cette commande ne sauvegarde pas la définition des objets
globaux, comme par exemple les utilisateurs et les tablespaces.

De plus, il peut être intéressant d'avoir une commande capable de sauvegarder
toutes les bases de l'instance. Reconstruire l'instance est beaucoup plus simple
car il suffit de rejouer ce seul fichier de sauvegarde.

</div>

-----

## Restauration d'une sauvegarde logique

<div class="slide-content">

  * Sauvegarde texte (option **p**) : _psql_
  * Sauvegarde binaire (options **t**, **c** ou **d**) : _pg_restore_

</div>

<div class="notes">

`pg_dump` permet de réaliser deux types de sauvegarde : une sauvegarde texte
(via le format plain) et une sauvegarde binaire (via les formats tar,
personnalisé et répertoire).

Chaque type de sauvegarde aura son outil :

  * `psql` pour les sauvegardes textes ;
  * `pg_restore` pour les sauvegardes binaires.

</div>

-----

### psql

<div class="slide-content">

  * client standard PostgreSQL
  * capable d'exécuter des requêtes
  * donc capable de restaurer une sauvegarde au format texte
  * très limité dans les options de restauration

</div>


<div class="notes">

`psql` est la console interactive de PostgreSQL. Elle permet de se connecter à
une base de données et d'y exécuter des requêtes, soit une par une, soit un
script complet. Or, la sauvegarde texte de `pg_dump` et de `pg_dumpall` fournit
un script SQL. Ce dernier est exécutable via `psql`.

</div>

-----

### pg_restore

<div class="slide-content">

  * restaure uniquement les sauvegardes au format binaire
    * donc tar, custom ou directory
    * format autodétecté (`-F` inutile, même si présent)
  * nombreuses options très intéressantes
  * restaure une base de données
    * complètement ou partiellement

</div>

<div class="notes">

`pg_restore` est un outil capable de restaurer les sauvegardes au format
binaire, quel qu'en soit le format. Il offre de nombreuses options très
intéressantes, la plus essentielle étant de permettre une restauration
partielle de façon aisée.

L'exemple typique d'utilisation de `pg_restore` est le suivant :

```bash
pg_restore -d b1 b1.dump
```

La base de données où la sauvegarde va être restaurée est indiquée avec
l'option `-d` et le nom du fichier de sauvegarde est le dernier argument dans
la ligne de commande.

</div>

-------

## Sauvegarde au niveau système de fichiers

<div class="slide-content">

  * À froid
  * Donc cohérente
  * Beaucoup d'outils
    * aucun spécifique à PostgreSQL
  * Attention à ne pas oublier les tablespaces

</div>

<div class="notes">

Toutes les données d'une instance PostgreSQL se trouvent dans des fichiers.
Donc sauvegarder les fichiers permet de sauvegarder une instance. Cependant,
cela ne peut pas se faire aussi simplement que ça. Lorsque PostgreSQL est en
cours d'exécution, il modifie certains fichiers du fait de l'activité des
utilisateurs ou des processus (interne ou non) de maintenances diverses.
Sauvegarder les fichiers de la base sans plus de manipulation ne peut donc se
faire qu'à froid. Il faut arrêter PostgreSQL pour disposer d'une sauvegarde
cohérente si la sauvegarde se fait au niveau du système de fichiers.

Le gros avantage de cette sauvegarde se trouve dans le fait que vous pouvez
utiliser tout outil de sauvegarde de fichier : `cp`, `scp`, `tar`, `ftp`,
`rsync`, etc.

Il est cependant essentiel d'être attentif aux données qui ne se trouvent pas
directement dans le répertoire des données. Notamment le répertoire des
journaux de transactions, qui est souvent placé dans un autre système de
fichiers pour gagner en performances. Si c'est le cas et que ce répertoire
n'est pas sauvegardé, la sauvegarde ne sera pas utilisable. De même, si des
tablespaces sont créés, il est essentiel d'intégrer ces autres répertoires dans
la sauvegarde de fichiers. Dans le cas contraire, une partie des données
manquera.

Voici un exemple de sauvegarde :

```bash
$ /etc/init.d/postgresql stop
$ tar cvfj data.tar.bz2 /var/lib/postgresql
$ /etc/init.d/postgresql start
```

Il est possible de réaliser les sauvegardes de
fichiers sans arrêter l'instance (_à chaud_), mais il s'agit d'une technique
avancée (dite _PITR_, ou _Point In Time Recovery_), qui nécessite la
compréhension de concepts non abordés dans le cadre de cette formation, comme
l'archivage des fichiers WAL.

</div>

-----

### Avantages

<div class="slide-content">

  * Rapide à la sauvegarde
  * Rapide à la restauration
  * Beaucoup d'outils disponibles

</div>

<div class="notes">

L'avantage de ce type de sauvegarde est sa rapidité. Cela se voit
essentiellement à la restauration où les fichiers ont seulement besoin d'être
créés. Les index ne sont pas recalculés par exemple, ce qui est certainement
le plus long dans la restauration d'une sauvegarde logique.

</div>

-----

### Inconvénients

<div class="slide-content">

  * Arrêt de la production
  * Sauvegarde de l'instance complète (donc aucune granularité)
  * Restauration de l'instance complète
  * Conservation de la fragmentation
  * Impossible de changer d'architecture

</div>

<div class="notes">

Il existe aussi de nombreux inconvénients à cette méthode.

Le plus important est certainement le fait qu'il faut arrêter la production.
L'instance PostgreSQL doit être arrêtée pour que la sauvegarde puisse être
effectuée.

Il ne sera pas possible de réaliser une sauvegarde ou une restauration
partielle, il n'y a pas de granularité. C'est forcément l'intégralité de
l'instance qui sera prise en compte.

Étant donné que les fichiers sont sauvegardés, toute la fragmentation des
tables et des index est conservée.

De plus, la structure interne des fichiers implique l'architecture où cette
sauvegarde sera restaurée. Donc une telle sauvegarde impose de conserver un
serveur 32 bits pour la restauration si la sauvegarde a été effectuée sur un
serveur 32 bits. De même, l'architecture LittleEndian/BigEndian doit être
respectée.

Tous ces inconvénients ne sont pas présents pour la sauvegarde logique.
Cependant, cette sauvegarde a aussi ses propres inconvénients, comme une
lenteur importante à la restauration.

</div>

-----

### Diminuer l'immobilisation

<div class="slide-content">

  * Utilisation de rsync
  * Une fois avant l'arrêt
  * Une fois après

</div>

<div class="notes">

Il est possible de diminuer l'immobilisation d'une sauvegarde de fichiers en
utilisant la commande `rsync`.

`rsync` permet de synchroniser des fichiers entre deux répertoires, en local ou
à distance. Il va comparer les fichiers pour ne transférer que ceux qui ont
été modifiés. Il est donc possible d'exécuter `rsync` avec PostgreSQL en cours
d'exécution pour récupérer un maximum de données, puis d'arrêter
PostgreSQL, de relancer `rsync` pour ne récupérer que les données modifiées
entre temps, et enfin de relancer PostgreSQL. Voici un exemple de ce cas
d'utilisation :

```bash
$ rsync /var/lib/postgresql /var/lib/postgresql2
$ /etc/init.d/postgresql stop
$ rsync -av /var/lib/postgresql /var/lib/postgresql2
$ /etc/init.d/postgresql start
```

</div>

-----

### Snapshot de partition

<div class="slide-content">

  * Avec certains systèmes de fichiers
  * Avec LVM
  * Avec la majorité des SAN

</div>

<div class="notes">

Certains systèmes de fichiers (principalement ZFS et le système de fichiers en
cours de développement BTRFS) ainsi que la majorité des SAN sont capables de
faire une sauvegarde d'un système de fichiers en instantané. En fait, ils
figent les blocs utiles à la sauvegarde. S'il est nécessaire de modifier un
bloc figé, ils utilisent un autre bloc pour stocker la nouvelle valeur. Cela
revient un peu au fonctionnement de PostgreSQL dans ses fichiers.

L'avantage est de pouvoir sauvegarder instantanément un système de fichiers.
L'inconvénient est que cela ne peut survenir que sur un seul système de
fichiers : impossible dans ce cas de déplacer les journaux de transactions sur
un autre système de fichiers pour gagner en performance ou d'utiliser des
tablespaces pour gagner en performance et faciliter la gestion de la volumétrie
des disques. De plus, comme PostgreSQL n'est pas arrêté au moment de la
sauvegarde, au démarrage de PostgreSQL sur la sauvegarde restaurée, ce dernier
devra rejouer les journaux de transactions.

Une baie SAN assez haut de gamme pourra disposer d'une fonctionnalité de
snapshot cohérent sur plusieurs volumes (« LUN »), ce qui permettra, si elle
est bien paramétrée, de réaliser un snapshot de tous les systèmes de
fichiers composant la base de façon cohérente.

Néanmoins, cela reste une méthode de sauvegarde très appréciable quand on
veut qu'elle ait le moins d'impact possible sur les utilisateurs.

</div>

-----

## PITR

<div class="slide-content">

  * Point In Time Recovery
  * À chaud
  * En continu
  * Cohérente

</div>

<div class="notes">

PITR est l'acronyme de _Point In Time Recovery_, autrement dit restauration à
un point dans le temps.

C'est une sauvegarde à chaud et surtout en continu. Là où une sauvegarde
logique du type `pg_dump` se fait au mieux une fois toutes les 24 h, la
sauvegarde PITR se fait en continue grâce à l'archivage des journaux de
transactions. De ce fait, ce type de sauvegarde diminue très fortement la
fenêtre de perte de données.

Bien qu'elle se fasse à chaud, la sauvegarde est cohérente.

</div>

-----

### Principes

<div class="slide-content">

  * Les journaux de transactions contiennent toutes les modifications
  * Il faut les archiver
  * ... et avoir une image des fichiers à un instant t
  * La restauration se fait en restaurant cette image
  * ... et en rejouant les journaux
    * entièrement
    * partiellement (_ie_ jusqu'à un certain moment)

</div>

<div class="notes">

Quand une transaction est validée, les données à écrire dans les fichiers de
données sont d'abord écrites dans un journal de transactions. Ces journaux
décrivent donc toutes les modifications survenant sur les fichiers de données,
que ce soit les objets utilisateurs comme les objets systèmes. Pour
reconstruire un système, il suffit donc d'avoir ces journaux et d'avoir un
état des fichiers du répertoire des données à un instant t. Toutes les
actions effectuées après cet instant t pourront être rejouées en demandant
à PostgreSQL d'appliquer les actions contenues dans les journaux. Les
opérations stockées dans les journaux correspondent à des modifications
physiques de fichiers, il faut donc partir d'une sauvegarde au niveau du
système de fichier, un export avec `pg_dump` n'est pas utilisable.

Il est donc nécessaire de conserver ces journaux de transactions. Or PostgreSQL
les recycle dès qu'il n'en a plus besoin. La solution est de demander au moteur
de les archiver ailleurs avant ce recyclage. On doit aussi disposer de
l'ensemble des fichiers qui composent le répertoire des données (incluant les
tablespaces si ces derniers sont utilisés).

La restauration a besoin des journaux de transactions archivés. Il ne sera pas
possible de restaurer et éventuellement revenir à un point donné avec la
sauvegarde seule. En revanche, une fois la sauvegarde des fichiers restaurée et
la configuration réalisée pour rejouer les journaux archivés, il sera
possible de les rejouer tous ou seulement une partie d'entre eux (en s'arrêtant
à un certain moment).

</div>

-----

### Avantages

<div class="slide-content">

  * Sauvegarde à chaud
  * Rejeu d'un grand nombre de journaux
  * Moins de perte de données

</div>

<div class="notes">

Tout le travail est réalisé à chaud, que ce soit l'archivage des journaux ou
la sauvegarde des fichiers de la base. En effet, il importe peu que les fichiers
de données soient modifiés pendant la sauvegarde car les journaux de
transactions archivés permettront de corriger tout incohérence par leur
application.

Il est possible de rejouer un très grand nombre de journaux (une journée, une
semaine, un mois, etc.). Évidemment, plus il y a de journaux à appliquer, plus
cela prendra du temps. Mais il n'y a pas de limite au nombre de journaux à
rejouer.

Dernier avantage, c'est le système de sauvegarde qui occasionnera le moins de
perte de données. Généralement, une sauvegarde `pg_dump` s'exécute toutes les
nuits, disons à 3 h du matin. Supposons qu'un gros problème survient à midi.
S'il faut restaurer la dernière sauvegarde, la perte de données sera de 9 h. Le
volume maximum de données perdu correspond à l'espacement des sauvegardes.
Avec l'archivage continu des journaux de transactions, la fenêtre de perte de
données va être fortement réduite. Plus l'activité est intense, plus la
fenêtre de temps sera petite : il faut changer de fichier de journal pour que
le journal précédent soit archivé et les fichiers de journaux sont de taille
fixe.

Pour les systèmes n'ayant pas une grosse activité, il est aussi possible de
forcer un changement de journal à intervalle régulier, ce qui a pour effet de
forcer son archivage, et donc dans les faits de pouvoir s'assurer une perte
maximale correspondant à cet intervalle.

</div>

-----

### Inconvénients

<div class="slide-content">

  * Sauvegarde de l'instance complète
  * Nécessite un grand espace de stockage (données + journaux)
  * Risque d'accumulation des journaux en cas d'échec d'archivage
  * Restauration de l'instance complète
  * Impossible de changer d'architecture
  * Plus complexe

</div>

<div class="notes">

Certains inconvénients viennent directement du fait qu'on copie les fichiers :
sauvegarde et restauration complète (impossible de ne restaurer qu'une seule
base ou que quelques tables), restauration sur la même architecture (32/64
bits, _little/big endian_), voire probablement le même système d'exploitation.

Elle nécessite en plus un plus grand espace de stockage car il faut sauvegarder
les fichiers (dont les index) ainsi que les journaux de transactions sur une
certaine période, ce qui peut être volumineux (en tout cas beaucoup plus que
des `pg_dump`).

En cas de problème dans l'archivage et selon la méthode choisie,
l'instance ne voudra pas effacer les journaux
non archivés. Il y a donc un risque d'accumulation de ceux-ci. Il faudra
surveiller la taille du `pg_wal`.

Enfin, cette méthode est plus complexe à mettre en place qu'une sauvegarde
`pg_dump`. Elle nécessite plus d'étapes, une réflexion sur l'archicture à
mettre en œuvre et une meilleure compréhension des mécanismes internes à
PostgreSQL pour en avoir la maîtrise.

</div>

-----

### Mise en place de la sauvegarde PITR

<div class="slide-content">

  * 2 étapes :
    * Archivage des journaux de transactions
    * Sauvegarde des fichiers de données

</div>

<div class="notes">

Même si la mise en place est plus complexe qu'un `pg_dump`, elle demande peu
d'étapes. La première chose à faire est de mettre en place l'archivage des
journaux de transactions. Un choix est à faire entre un archivage classique et
l'utilisation de l'outil `pg_receivewal`.

Lorsque cette étape est réalisée (et fonctionnelle), il est possible de passer
à la seconde : la sauvegarde des fichiers. Là-aussi, il y a différentes
possibilités : soit manuellement, soit `pg_basebackup`, soit son propre script.

</div>

-----

### Outils de sauvegarde PITR

<div class="slide-content">

  * Nombreux outils existants
  * Le plus avancé : pgBackRest

</div>


<div class="notes">

Les outils permettent de faciliter la mise en place de sauvegardes PITR. Ils
gèrent à la fois la sauvegarde et la restauration.

[pgBackRest](https://pgbackrest.org/) est écrit en C et perl, par David Steele
de Crunchy Data. Logiciel très complet, il permet de réaliser des sauvegardes
incrémentales avec une granularité au niveau du bloc. Il permet à la fois la
sauvegarde et la restauration.

Il s'agit d'un logiciel à part entière. Sa prise en main et sa mise en oeuvre
demande un certain investissement.

</div>

-----

## Matrice

<div class="slide-content">

+-----------------------+------------+---------+--------------+---------------+
|                       | Simplicité | Coupure | Restauration | Fragmentation |
+=======================+============+=========+==============+===============+
| copie à froid         | facile     | longue  | rapide       | conservée     |
+-----------------------+------------+---------+--------------+---------------+
| snapshot FS           | facile     | aucune  | rapide       | conservée     |
+-----------------------+------------+---------+--------------+---------------+
| pg_dump               | facile     | aucune  | lente        | perdue        |
+-----------------------+------------+---------+--------------+---------------+
| rsync + copie à froid | moyen      | courte  | rapide       | conservée     |
+-----------------------+------------+---------+--------------+---------------+
| _PITR_                | difficile  | aucune  | rapide       | conservée     |
+-----------------------+------------+---------+--------------+---------------+

</div>

<div class="notes">

Ce tableau indique les points importants de chaque type de sauvegarde. Il permet
de faciliter un choix entre les différentes méthodes.

</div>

-----

\newpage
