
## Travaux Dirigés 3

<div class="slide-content">

  * Les outils de sauvegardes et restaurations
    * _pg_dump_ et _pg_dumpall_
    * _psql_ et _pg_restore_

</div>

-----

### Questions de cours

<div class="slide-content">

  * Quel fichier permet de configurer les droits d'accès à l'instance ?
  * Y a-t-il un lien entre utilisateur système et rôle PostgreSQL ?
  * Quel ordre SQL permet de donner des droits sur une table ?

</div>

-----

### Options de connexions des outils

<div class="slide-content">

  * `-h` / `$PGHOST` / socket Unix
  * `-p` / `$PGPORT` / 5432
  * `-U` / `$PGUSER` / utilisateur du système
  * `$PGPASSWORD`
  * `.pgpass`

</div>

<div class="notes">

Les commandes `pg_dump` et `pg_dumpall` se connectent au serveur PostgreSQL comme
n'importe quel autre outil (psql, pgAdmin, etc.). Ils disposent donc des options
habituelles pour se connecter :

  * `-h` ou `--host` pour indiquer l'alias ou l'adresse IP du serveur ;
  * `-p` ou `--port` pour préciser le numéro de port ;
  * `-U` ou `--username` pour spécifier l'utilisateur ;
  * `-W` ne permet pas de saisir le mot de passe en ligne de commande. Il force
seulement psql à demander un mot de passe (en interactif donc).

À partir de la version 10, il est possible d'indiquer plusieurs hôtes et ports.
L'hôte sélectionné est le premier qui répond au paquet de démarrage. Si
l'authentication ne passe pas, la connexion sera en erreur. Il est aussi
possible de préciser si la connexion doit se faire sur un serveur en
lecture/écriture ou en lecture seule.

Par exemple on effectuera une sauvegarde depuis le premier serveur disponible
ainsi :

```
pg_dumpall -h esclave,maitre -p 5432,5433 -U postgres -f sauvegarde.sql
```

Si la connexion nécessite un mot de passe, ce dernier sera réclamé lors de la
connexion. Il faut donc faire attention avec `pg_dumpall` qui va se connecter à
chaque base de données, une par une. Dans tous les cas, il est préférable
d'utiliser un fichier `.pgpass` qui indique les mots de passe de connexion. Ce
fichier est créé à la racine du répertoire personnel de l'utilisateur qui
exécute la sauvegarde. Il contient les informations suivantes :

```
hote:port:base:utilisateur:mot de passe
```

Ce fichier est sécurisé dans le sens où seul l'utilisateur doit avoir le droit
de lire et écrire ce fichier. L'outil vérifiera cela avant d'accepter
d'utiliser les informations qui s'y trouvent.

</div>

-----

### pg_dump

<div class="slide-content">

L'outil de sauvegarde logique d'une base de données

</div>

------------

#### pg_dump - Format de sortie

<div class="slide-content">

  * `-F`
    * `p` : plain, SQL
    * `t` : tar
    * `c` : custom (spécifique PostgreSQL)
    * `d` : directory

</div>

<div class="notes">

`pg_dump` accepte d'enregistrer la sauvegarde suivant quatre formats :

  * un fichier SQL, donc un fichier texte dont l'encodage dépend de la base ;
  * un répertoire disposant du script SQL et des fichiers, compressés avec gzip,
contenant les données de chaque table ;
  * un fichier tar intégrant tous les fichiers décrits ci-dessus mais non
compressés ;
  * un fichier personnalisé, sorte de fichier tar compressé avec gzip.

</div>

-----

#### pg_dump - Fichier ou sortie standard

<div class="slide-content">

  * `-f` : fichier où est stockée la sauvegarde
  * sans `-f`, sur la sortie standard
</div>

<div class="notes">

Par défaut, et en dehors du format répertoire, toutes les données d'une
sauvegarde sont renvoyées sur la sortie standard de `pg_dump`. Il faut donc
utiliser une redirection pour renvoyer dans un fichier.

Cependant, il est aussi possible d'utiliser l'option `-f` pour spécifier le
fichier de la sauvegarde. L'utilisation de cette option est conseillée car elle
permet à `pg_restore` de trouver plus efficacement les objets à restaurer dans
le cadre d'une restauration partielle.

</div>

-----

#### pg_dump - Sélection de sections

<div class="slide-content">

  * `--section`
    * `pre-data`, la définition des objets (hors contraintes et index)
    * `data`, les données
    * `post-data`, la définition des contraintes et index

</div>

<div class="notes">

Historiquement, il est possible de sauvegarder que la structure avec l'option
`--schema-only` ou uniquement les données avec l'option
`--data-only`. Cependant, lors de la restauration, les index et contraintes
seront créées en même temps que le schéma. Lors de l'insertion des données, les
vérifications de contraintes et ajout de clé d'index seront faites à
l'insertion de chaque ligne.

Il est possible de sauvegarder une base par section. En fait, un fichier de
sauvegarde complet est composé de trois sections :

  * la définition des objets,
  * les données,
  * la définition des contraintes et index.
  
Le fait de séparer ainsi les sauvegardes permet de gagner en performance à la
vérification des contraintes et à la création d'index.

</div>

-----

#### pg_dump - Sélection d'objets

<div class="slide-content">

  * `-n <schema>` : uniquement ce schéma
  * `-N <schema>` : tous les schémas sauf celui-là
  * `-t <table>` : uniquement cette table
  * `-T <table>` : toutes les tables sauf celle-là
  * En option
    * possibilité d'en mettre plusieurs
    * exclure les données avec `--exclude-table-data=<table>`
    * avoir une erreur si l'objet est inconnu avec`--strict-names`

</div>

<div class="notes">

En dehors de la distinction structure/données, il est possible de demander de
ne sauvegarder qu'un objet. Les seuls objets sélectionnables au niveau de
`pg_dump` sont les tables et les schémas. L'option `-n` permet de sauvegarder
seulement le schéma cité après alors que l'option `-N` permet de sauvegarder
tous les schémas sauf celui cité après l'option. Le même système existe pour
les tables avec les options `-t` et `-T`. Il est possible de mettre ces options
plusieurs fois pour sauvegarder plusieurs tables spécifiques ou plusieurs
schémas.

Les équivalents longs de ces options sont: `--schema`, `--exclude-schema`,
`--table` et `--exclude-table`.

Par défaut, si certains objets sont trouvés et d'autres non, `pg_dump` ne dit
rien, et l'opération se termine avec succès. Ajouter l'option `--strict-names`
permet de s'assurer d'être averti avec une erreur sur le fait que `pg_dump` n'a
pas sauvegardé tous les objets souhaités. En voici un exemple (`t1`
existe, `t20` n'existe pas) :

```
$ pg_dump -t t1 -t t20 -f postgres.dump postgres
$ echo $?
0
$ pg_dump -t t1 -t t20 --strict-names -f postgres.dump postgres
pg_dump: no matching tables were found for pattern "t20"
$ echo $?
1
```

</div>

-----

#### pg_dump - Option divers

<div class="slide-content">

  * `-j <nombre_de_threads>` : en format _directory_
  * `-Z` : option de compression, de 0 à 9
  * `-O` : ignorer le propriétaire
  * `-x` : ignorer les droits
  * `-v` : pour voir la progression

</div>

<div class="notes">

Historiquement, `pg_dump` n'utilise qu'une seule connexion à la base de données
pour sauvegarder la définition des objets et les données. Cependant, une fois
que la première étape de récupération de la définition des objets est réalisée,
l'étape de sauvegarde des données peut être parallélisée pour profiter des
nombreux processeurs disponibles sur un serveur.  
L'option `-j` permet de préciser le nombre de connexions réalisées vers la base
de données. Chaque connexion est gérée dans `pg_dump` par un processus sous
Unix et par un thread sous Windows. Par contre, cette option est compatible
uniquement avec le format de sortie _directory_ (option `-F d`).  
Cela permet d'améliorer considérablement la vitesse de sauvegarde.

Il est possible de compresser la sauvegarde. La compression va de 0, pas de
compression, à 9, compression maximum.  
Par défaut, `pg_dump` utilise le niveau de compression par défaut de la libz
(`Z_DEFAULT_COMPRESSION`) qui correspond au meilleur compromis entre
compression et vitesse, équivalent au niveau 6.

Les options `--no-owner` et `--no-privileges` permettent de ne pas indiquer
respectivement le propriétaire et les droits de l'objet dans la sauvegarde.

Enfin, l'option `-v` (ou `--verbose`) permet de voir la progression de la
commande.

</div>

-----

### pg_dumpall

<div class="slide-content">

L'outil de sauvegarde logique d'une instance complète

</div>

------------


#### pg_dumpall - Format de sortie

<div class="slide-content">

  * `-F`
    * `p` : plain, SQL

</div>

<div class="notes">

Contrairement à `pg_dump`, `pg_dumpall` ne dispose que d'un format en sortie : le
fichier SQL. L'option est donc inutile.

</div>

-----

#### pg_dumpall - Fichier ou sortie standard

<div class="slide-content">

  * `-f` : fichier où est stockée la sauvegarde
  * sans `-f`, sur la sortie standard

</div>

<div class="notes">

La sauvegarde est automatiquement envoyée sur la sortie standard, sauf si la
ligne de commande précise l'option `-f` (ou `--file`) et le nom du fichier.

</div>

-----

#### pg_dumpall - Sélection des objets

<div class="slide-content">

  * `-g` : tous les objets globaux
  * `-r` : uniquement les rôles
  * `-t` : uniquement les tablespaces
  * `--no-role-passwords` : pour ne pas sauvegarder les mots de passe
    * permet de ne pas être superutilisateur

</div>

<div class="notes">

`pg_dumpall` étant créé pour sauvegarder l'instance complète, il disposera de
moins d'options de sélection d'objets. Néanmoins, il permet de ne sauvegarder
que la déclaration des objets globaux, ou des rôles, ou des tablespaces. Leur
versions longues sont respectivement: `--globals-only`, `--roles-only` et
`--tablespaces-only`.

</div>

-----

### Restauration d'une sauvegarde logique

<div class="slide-content">

  * Sauvegarde texte (option **p**) : _psql_
  * Sauvegarde binaire (options **t**, **c** ou **d**) : _pg_restore_

</div>

-----

#### psql - Options

<div class="slide-content">

  * `-f` pour indiquer le fichier contenant la sauvegarde
    * sans option `-f`, lit l'entrée standard
  * `-1` pour tout restaurer en une seule transaction
  * `-e` pour afficher les ordres SQL exécutés
  * `ON_ERROR_ROLLBACK`/`ON_ERROR_STOP`

</div>

<div class="notes">

Pour cela, il existe plusieurs moyens :

  * envoyer le script sur l'entrée standard de psql :

```bash
cat b1.dump | psql b1
```

  * utiliser l'option en ligne de
commande -f :

```bash
psql -f b1.dump b1
```

  * utiliser la méta-commande \i :

```
b1 =# \i b1.dump
```

Dans les deux premiers cas, la restauration peut se faire à distance alors que
dans le dernier cas, le fichier de la sauvegarde doit se trouver sur le serveur
de bases de données.

Le script est exécuté comme tout autre script SQL. Comme il n'y a pas
d'instruction `BEGIN` au début, l'échec d'une requête ne va pas empêcher
l'exécution de la suite du script, ce qui va généralement apporter un flot
d'erreurs. De plus, après une erreur, les requêtes précédentes sont toujours
validées. La base de données sera donc dans un état à moitié modifié, ce qui
peut poser un problème s'il ne s'agissait pas d'une base vierge. Il est donc
préférable parfois d'utiliser l'option en ligne de commande `-1` pour que le
script complet soit exécuté dans une seule transaction. Dans ce cas, si une
requête échoue, aucune modification n'aura réellement lieu sur la base, et il
sera possible de relancer la restauration après correction du problème.

Enfin, il est à noter qu'une restauration partielle de la sauvegarde est assez
complexe à faire. Deux solutions possibles, mais pénibles :

  * modifier le script SQL dans un éditeur de texte, ce qui peut être
impossible si ce fichier est suffisamment gros
  * utiliser des outils tels que grep et/ou sed pour extraire les portions
voulues, ce qui peut facilement devenir long et complexe

Deux variables psql peuvent être modifiées, ce qui permet d'affiner le
comportement de psql lors de l'exécution du script :

  * `ON_ERROR_ROLLBACK`: par défaut (valeur `off`), dans __une transaction__,
toute erreur entraîne le `ROLLBACK` de toute la transaction. Activer ce
paramètre permet que seule la commande en erreur soit annulée. psql effectue
des _savepoints_ avant chaque ordre, et y retourne en cas d'erreur, avant de
continuer le script. Ceci est utile surtout si vous avez utilisé l'option `-1`.
Il peut valoir `interactive` (ne s'arrêter dans le script qu'en mode
interactif, c'est-à-dire quand c'est une commande `\i` qui est lancée) ou `on`
dans quel cas il est actif en permanence.
  * `ON_ERROR_STOP`: par défaut, dans __un script__, une erreur n'arrête pas le
déroulement du script. On se retrouve donc souvent avec un ordre en erreur, et
beaucoup de mal pour le retrouver, puisqu'il est noyé dans la masse des
messages. Quand `ON_ERROR_STOP` est positionné (à `on`), le script est
interrompu dès qu'une erreur est détectée.

Les variables psql peuvent être modifiées :

  * par édition du `.psqlrc` (à déconseiller, cela va modifier le comportement
de psql pour toute personne utilisant le compte):

```
cat .psqlrc
\set ON_ERROR_ROLLBACK interactive
```

  * en option de ligne de commande de psql :

```
$ psql --set=ON_ERROR_ROLLBACK='on'
```

  * de façon interactive dans psql:

```
psql> \set ON_ERROR_ROLLBACK on
```

</div>

-----

#### pg_restore - Fichier ou entrée standard

<div class="slide-content">

  * Fichier à restaurer en dernier argument de la ligne de commande
  * **Attention à `-f`** : fichier en sortie

</div>

<div class="notes">

Le fichier à restaurer s'indique en dernier argument sur la ligne de commande.

L'option `-f` permet d'indiquer un fichier qui contiendra un script SQL
correspondant aux ordres à générer à la restauration. Il sera donc écrit. S
'il est utilisé pour indiquer le nom du fichier de sauvegarde à restaurer, ce
dernier se verra vidé pour contenir les erreurs éventuels de la restauration.
Autrement dit, il faut faire extrêmement attention lors de l'utilisation de
l'option `-f` avec l'outil  `pg_restore`.

**Utilisation de l'option `-f`**

Il est déconseillé d'utiliser l'option `-f` avec `pg_restore` ! Cette dernière
est contre intuitive : elle indique le journal d'activité de `pg_restore` et
**NON** le fichier à restaurer. Il est ainsi toujours recommandé d'utiliser
simplement la redirection standard qui ne portent jamais à confusion pour
récupérer les messages de `pg_restore`.

</div>

-----

#### pg_restore - Sélection de sections

<div class="slide-content">

  * `--section`
    * `pre-data`, la définition des objets (hors contraintes et index)
    * `data`, les données
    * `post-data`, la définition des contraintes et index

</div>

<div class="notes">

Il est possible de restaurer une base section par section. En fait, un fichier
de sauvegarde complet est composé de trois sections : 

  * la définition des objets,
  * les données,
  * la définition des contraintes et index.
  
Il est plus intéressant de restaurer par section que de restaurer schéma et
données séparément car cela permet d'avoir la partie contrainte et index dans
un script à part, ce qui accélère la restauration.

</div>

-----

#### pg_restore - Sélection d'objets

<div class="slide-content">

  * `-n <schema>` : uniquement ce schéma
  * `-N <schema>` : tous les schémas sauf ce schéma
  * `-t <table>` : cette relation
  * `-T <trigger>` : ce trigger
  * `-I <index>` : cet index
  * `-P <fonction>` : cette fonction
  * En option
    * possibilité d'en mettre plusieurs
    * `--strict-names`, pour avoir une erreur si l'objet est inconnu
</div>

<div class="notes">

`pg_restore` fournit quelques options supplémentaires pour sélectionner les
objets à restaurer. Il y a les options `-n` et `-t` qui ont la même
signification que pour `pg_dump`. `-N` n'existe que depuis la version 10 et a la
même signification que pour `pg_dump`. Par contre, `-T` a une signification
différente: `-T` précise un trigger dans `pg_restore`.

Il est à noter que l'option `-t` concerne toutes les relations : tables, vues,
vues matérialisées et séquences.

Il existe en plus les options `-I` et ` -P` (respectivement `--index` et
`--function`) pour restaurer respectivement un index et une procédure stockée
spécifique.

Là-aussi, il est possible de mettre plusieurs fois les options pour restaurer
plusieurs objets de même type ou de type différent.

Par défaut, si le nom de l'objet est inconnu, `pg_restore` ne dit rien, et
l'opération se termine avec succès. Ajouter l'option `--strict-names` permet de
s'assurer d'être averti avec une erreur sur le fait que `pg_restore` n'a pas
restauré l'objet souhaité.

</div>

-----

#### pg_restore - Sélection avancée

<div class="slide-content">

  * `-l` : récupération de la liste des objets
  * `-L <liste_objets>` : restauration uniquement des objets listés dans ce
    fichier

</div>

<div class="notes">

Les options précédentes sont intéressantes quand on a peu de sélection à
faire. Par exemple, cela convient quand on veut restaurer deux tables ou quatre
index. Quand il faut en restaurer beaucoup plus, cela devient plus difficile.
`pg_restore` fournit un moyen avancé pour sélectionner les objets.

L'option `-l` (`--list`) permet de connaître la liste des actions que réalisera
`pg_restore` avec un fichier particulier. Par exemple :

```
$ pg_restore -l b1.dump
;
; Archive created at Tue Dec 13 09:35:17 2011
;     dbname: b1
;     TOC Entries: 16
;     Compression: -1
;     Dump Version: 1.12-0
;     Format: CUSTOM
;     Integer: 4 bytes
;     Offset: 8 bytes
;     Dumped from database version: 9.1.3
;     Dumped by pg_dump version: 9.1.3
;
;
; Selected TOC Entries:
;
2752; 1262 37147 DATABASE - b1 guillaume
5; 2615 2200 SCHEMA - public guillaume
2753; 0 0 COMMENT - SCHEMA public guillaume
2754; 0 0 ACL - public guillaume
164; 3079 12528 EXTENSION - plpgsql
2755; 0 0 COMMENT - EXTENSION plpgsql
165; 1255 37161 FUNCTION public f1() guillaume
161; 1259 37148 TABLE public t1 guillaume
162; 1259 37151 TABLE public t2 guillaume
163; 1259 37157 VIEW public v1 guillaume
2748; 0 37148 TABLE DATA public t1 guillaume
2749; 0 37151 TABLE DATA public t2 guillaume
2747; 2606 37163 CONSTRAINT public t2_pkey guillaume
2745; 1259 37164 INDEX public t1_c1_idx guillaume
```

Toutes les lignes qui commencent avec un point-virgule sont des
commentaires. Le reste indique les objets à créer : un schéma public, le
langage plpgsql, la procédure stockée f1, les tables t1 et t2, la vue v1, la
clé primaire sur t2 et l'index sur t1.  Il indique aussi les données à
restaurer avec des lignes du type « TABLE DATA ». Donc, dans cette sauvegarde,
il y a les données pour les tables t1 et t2.

Il est possible de stocker cette information dans un fichier, de modifier le
fichier pour qu'il ne contienne que les objets que l'on souhaite restaurer, et
de demander à `pg_restore`, avec l'option `-L` (`--use-list`), de ne prendre en
compte que les actions contenues dans le fichier. Voici un exemple complet :

```
$ pg_restore -l b1.dump > liste_actions

$ cat liste_actions | \
  grep -v "f1" | \
  grep -v "TABLE DATA public t2" | \
  grep -v "INDEX public t1_c1_idx" \
  > liste_actions_modifiee

$ createdb b1_new

$ pg_restore -L liste_actions_modifiee -d b1_new -v b1.dump
pg_restore: connecting to database for restore
pg_restore: creating SCHEMA public
pg_restore: creating COMMENT SCHEMA public
pg_restore: creating EXTENSION plpgsql
pg_restore: creating COMMENT EXTENSION plpgsql
pg_restore: creating TABLE t1
pg_restore: creating TABLE t2
pg_restore: creating VIEW v1
pg_restore: restoring data for table "t1"
pg_restore: creating CONSTRAINT t2_pkey
pg_restore: setting owner and privileges for SCHEMA public
pg_restore: setting owner and privileges for COMMENT SCHEMA public
pg_restore: setting owner and privileges for ACL public
pg_restore: setting owner and privileges for EXTENSION plpgsql
pg_restore: setting owner and privileges for COMMENT EXTENSION plpgsql
pg_restore: setting owner and privileges for TABLE t1
pg_restore: setting owner and privileges for TABLE t2
pg_restore: setting owner and privileges for VIEW v1
pg_restore: setting owner and privileges for TABLE DATA t1
pg_restore: setting owner and privileges for CONSTRAINT t2_pkey
```

L'option `-v` de `pg_restore` permet de visualiser sa progression dans la
restauration. On remarque bien que la procédure stockée f1 ne fait pas partie
des objets restaurés. Tout comme l'index sur t1 et les données de la table t2.

</div>

-----

#### pg_restore - Option divers

<div class="slide-content">

  * `-j <nombre_de_threads>` : en format _custom_ ou _directory_
  * `-O` : ignorer le propriétaire
  * `-x` : ignorer les droits
  * `-1` pour tout restaurer en une seule transaction
  * `-c` : pour détruire un objet avant de le restaurer
  * `-C` : pour créer la base de donnée avant le rechargement
  
</div>


<div class="notes">

Historiquement, `pg_restore` n'utilise qu'une seule connexion à la base de
données pour y exécuter en série toutes les requêtes nécessaires pour restaurer
la base. Cependant, une fois que la première étape de création des objets est
réalisée, l'étape de copie des données et celle de création des index peuvent
être parallélisées pour profiter des nombreux processeurs disponibles sur un
serveur. L'option `-j` permet de préciser le nombre de connexions réalisées
vers la base de données. Chaque connexion est gérée dans `pg_restore` par un
processus sous Unix et par un thread sous Windows.

Les option `-O` et `-x` permettent de ne pas restaurer respectivement le
propriétaire et les droits des objets.

L'option `-1` permet d'exécuter `pg_restore` dans une seule transaction.
Attention, ce mode est incompatible avec le mode `-j` car on ne peut pas avoir
plusieurs sessions qui partagent la même transaction.

L'option `-c` permet d'exécuter des `DROP` des objets avant de les restaurer. Ce
qui évite les conflits à la restauration de tables par exemple: l'ancienne est
détruite avant de restaurer la nouvelle.  
L'option `-C` permet de créer la base de données avant de débuter la
restauration.  
Si les options `-c` et `-C` sont utilisées conjointement, la base de données
précisée avec l'option `-d` est uniquement utilisée pour lancer les commandes
`DROP DATABASE` et `CREATE DATABASE`.

Enfin, l'option `-v` permet de voir la progression de la commande.

</div>

-----

### Enoncé

<div class="notes">

### Les outils de sauvegardes et restaurations

Quel est le résultat de chacunes des commandes suivantes ?

Précisez toutes les informations en votre possession.

#### Outils PostgreSQL

```bash
admin$ psql -d ojm3 -h 192.168.0.5 -U adb1 -p 5454

postgres@nevrast$ psql -p 5433 -c 'SELECT pg_reload_conf();' -h /tmp

postgres$ createdb -O app_user -h 10.51.32.40 -p 5446 my_app

postgres@localhost$ export PGPORT=5436 && dropdb test
```

#### Sauvegardes

```bash
postgres@dbhost$ pg_dump -Fp -d miranda -f /tmp/miranda.sql -h /tmp/v9.6/

pgadmin@srv$ pg_dump -Fc -U thibaut -t decors -f /home/thibaut/db.dump main

postgres@doriath$ /usr/pgsql-9.4/bin/pg_dump -Fd -d webapp -j 5 -f /tmp/ma_db

jacques@calvin$ pg_dumpall -U tester -h 192.168.10.38 -f /tmp/ma_db.sql -d jon1

jacques@hobbes$ pg_dumpall -g > ~/pg_global.sql
```

#### Restaurations

```sql
db=# \i /home/thibaut/oberon.sql
```

```bash
$ psql -U titania -h 11.56.99.77 -d puck -f /tmp/ma_db.sql

lysandre@athenes$ pg_restore --clean --create -j3 -d hermia reve_de_minuit.dump

postgres@src$ pg_restore -U google -h 8.8.8.8 -l -f e3rsf.list e3rsf

pgadmin@pghost$ pg_restore -L db.list -d sauvegarde -p 5434 ma_db.dump
```

#### Contrôle des accès

Étudier le contenu du fichier pg_hba.conf suivant :

```
TYPE      DATABASE       USER        CIDR-ADDRESS    METHOD
local     all            postgres                    peer
local     web            web                         trust
local     sameuser       all                         peer
host      all            all         127.0.0.1/32    md5
host      all            all         89.192.0.3/8    md5
hostssl   recherche      recherche   89.192.0.4/32   passwd
local     replication    all                         peer
host      replication    all         127.0.0.1/32    md5
host      replication    all         ::1/128         md5
hostssl   replication    repli       89.192.0.0/24   scram-sha-256
```

</div>

-----
