
## Travaux Pratiques 3

<div class="slide-content">
  * Sauvegarde / restauration avec PostgreSQL
</div>

<div class="notes">

**Rappel**

Durant ces travaux pratiques, nous allons travailler sur notre serveur de base
de données PostgreSQL via la machine virtuelle du TP 1.

Effectuez les manipulations nécessaires pour réaliser les actions listées dans
la section _Énoncés_.

Vous pouvez vous aider des annexes de ce TP, des derniers TP, des sections
Administration via bash_ et _psql_ ainsi que de l'aide en ligne ou les pages de
manuels (`man`).


### Énoncés

#### Installation d'une nouvelle version majeure

* Installer la version 16 de PostgreSQL à partir du PGDG (aide :
<https://wiki.postgresql.org/wiki/Apt>).

#### Sauvegardes logiques

Créer un répertoire `backups` dans le répertoire `HOME` de `postgres` pour y
déposer les fichiers d'export.

Les sauvegardes seront à effectuer depuis l'instance en version 13 ou 15.

**Sauvegarde logique de toutes les bases**

Sauvegarder toutes les bases de données de l'instance PostgreSQL à l'aide de
`pg_dumpall` dans le fichier _~postgres/backups/base_all.sql.gz_.

La sauvegarde doit être compressée.

**Sauvegarde logique en mode directory d'une base**

Sauvegarder de façon parallélisée de la base de données `mondial` au format
_directory_ à l'aide de `pg_dump` dans le répertoire
_~postgres/backups/base_mondial_.

**Sauvegarde logique en mode custom d'une base**

Sauvegarder de la base de données `cave` au format _custom_ à l'aide de
`pg_dump` dans le répertoire _~postgres/backups/base_cave.dump_.

**Export des objets globaux**

Exporter uniquement les objets globaux de l'instance (rôles et définitions de
tablespaces) à l'aide de `pg_dumpall` dans le fichier
_~postgres/backups/base_globals.sql_.

**Sauvegarde logique de tables**

Sauvegarder la table `city` dans le fichier _~postgres/backups/table_city.sql_
au format *plain text*.

#### Restaurations logiques

**Toutes les restaurations sont à effectuer sur la nouvelle instance en version
16.**

**Restaurations logiques**

**Restauration des données globales**

Restaurer les données globales de l'instance sauvegardée dans le fichier
_~postgres/backups/base_globals.sql_.

**Restauration d'une base de données**

Restaurer le schéma de la base de données `mondial` (pas les données) dans une
nouvelle base de données nommée `mondial2` en utilisant le répertoire de
sauvegarde `base_mondial`. Le possesseur en sera le rôle _caviste_.

**Restauration d'une table**

À partir du fichier de sauvegarde _~postgres/backups/table_city.sql_, restaurer
la table `city` dans la base de données `mondial2`.

**Migration de données**

Copier le schéma et les données de la base `mondial` de l'ancienne instance
dans une nouvelle base `mondial_test` sans passer par un fichier de sauvegarde.

**Restauration partielle**

Restaurer dans une base `mondial3` tout le contenu de la sauvegarde de la base
`mondial`, sauf les données de la table `city`.

La définition de la table `city` et toutes les contraintes s'y rapportant
doivent être restaurées.

**Restauration de la base de données cave**

À partir du fichier de sauvegarde _~postgres/backups/base_cave.dump_, restaurer
de façon parallélisée la base de données `cave`.

#### Réalisation de sauvegardes physiques à chaud en local

  * Créer une sauvegarde physique de l'instance en version 16 avec l'outil
    `pg_basebackup`.
  * Créer une nouvelle instance `copie` avec le commande bash
    `pg_createcluster`.
  * Restaurer la sauvegarde dans cette nouvelle instance et vérifier son bon
    fonctionnement.

#### Réalisation de sauvegardes PITR en local

  * Configurer l'instance PostgreSQL en version 16 pour réaliser l'archivage
    des journaux de transactions grâce au paramètre `archive_command`.
  * Vérifier le bon archivage des journaux de transactions avec la fonction
    `pg_switch_wal`.
  * Créer une sauvegarde physique avec l'outil `pg_basebackup`.
  * Effectuer des actions dans l'instance d'origine (création d'une nouvelle
    base de données, de nouvelles tables, insertion et suppression de
    données...). Forcez régulièrement l'archivage des journaux de transactions.
  * Créer une nouvelle instance `pitr` avec le commande bash
    `pg_createcluster`.
  * Restaurer la sauvegarde dans cette nouvelle instance et appliquez tous les
    journaux de transactions archivés (paramètre `restore_command`). Vérifiez
    que tous les changements sont présents dans la nouvelle instance.

#### Mise en place de pgBackRest

Réalisation de sauvegardes physiques à chaud avec le logiciel pgBackRest :

  * Installer le logiciel pgBackRest.
  * Configurer le logiciel et l'instance PostgreSQL en version 16 pour réaliser
    des sauvegardes physique en ligne de commande.
  * Créer une sauvegarde physique.
  * Restaurer une des sauvegardes dans une nouvelle instance.
  * Mettre en place des sauvegardes et purges journalières (par _cron_ ou
    _systemd_).

</div>

-----
