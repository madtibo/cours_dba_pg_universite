<div class="notes">

### Solutions du TP 5

#### Exercice 1

Afin de visualiser avec quel rôle vous êtes connecté et sur quelle base, il est possible de modifier le prompt de l'outil `psql`. Pour cela, créer le fichier `$HOME/.psqlrc` avec le contenu suivant:
  `\set PROMPT1 '%n@%/%R%# '`


#### Exercice 2 : droits sur les objets

**Gestion des rôles**

Connexion avec l'utilisateur `caviste` (administrateur de la base `cave`) :

```bash
$ psql -U caviste cave
```

Création de la table `facture` :

```sql
caviste@cave=> CREATE TABLE facture (id int, objet text, creationts timestamp);
```



**Création d'un utilisateur et d'un groupe**

Création du rôle `secretariat` avec l'utilisateur `postgres` :

```sql
postgres@cave=> CREATE ROLE secretariat;
postgres@cave=> GRANT SELECT, INSERT, UPDATE, DELETE ON facture TO secretariat;
```

Création de l'utilisateur bob appartenant au rôle `secretariat` :

```sql
postgres@cave=> CREATE ROLE bob LOGIN IN ROLE SECRETARIAT;
```

Vérification de la création des deux rôles:

```sql
postgres@cave=> SELECT * FROM pg_roles;
```

Vous pouvez aussi utiliser `\du` pour obtenir la liste des utilisateurs, et `\dp`
pour connaître les droits des objets.

Connexion avec l'utilisateur bob :

```bash
$ psql -U bob cave
```

Insertion des factures:

```sql
bob@cave=> insert into facture values (1, 'Vin de Bordeaux', current_timestamp),
    (2, 'Vin de Bourgogne', current_timestamp);
```

Sélectionner les factures :

```sql
bob@cave=> select * from facture;
```

Modifier la deuxième facture avec la date et l'heure courante :

```sql
bob@cave=> update facture set creationts = now() where id = 2;
```

Supprimer la première facture :

```sql
bob@cave=> delete from facture where id = 1;
```

**Modification des permissions**

Retirer les droits `DELETE` sur la table facture pour le rôle `secretariat` avec l'utilisateur `postgres` :

```sql
postgres@cave=> REVOKE DELETE ON facture FROM secretariat;
```

Vérifier qu'il n'est pas possible de supprimer la deuxième facture avec l'utilisateur bob :

```sql
bob@cave=> delete from facture where id = 2;
```

Retirer tous les droits pour le groupe `secretariat` sur la table `appellation` :

```sql
postgres@cave=> REVOKE ALL ON appellation from secretariat;
```

Vérifier que l'utilisateur bob appartenant au groupe `secretariat` ne peut pas
sélectionner les appellations contenues dans la table `appellation` :

```sql
bob@cave=> select * from appellation;
```

Autoriser l'utilisateur bob à accéder à la table `appellation` en lecture :

```sql
postgres@cave=> GRANT SELECT ON appellation to bob;
```

Vérifier que l'utilisateur bob peut désormais accéder à la table `appellation` :

```sql
bob@cave=> select * from appellation;
```



**Héritage des droits au login**

Créer un utilisateur `tina` appartenant au rôle `secrétariat` :

```sql
postgres@cave=> CREATE ROLE tina LOGIN NOINHERIT;
```

Donner les droits du rôle `secretariat` à l'utilisateur `tina` :

```sql
postgres@cave=> GRANT secretariat to tina;
```

Se connecter avec l'utilisateur `tina` à la base de données cave et vérifier qu'il n'est pas possible d'accéder à la table `facture` :

```bash
$ psql -U tina cave
```



```sql
tina@cave=> select * from facture;
```

Positionner le rôle `secretariat` et sélectionner les données de la table `facture` :

```sql
tina@cave=> set role secretariat;
secretariat@cave=>  select * from facture;
```

L'utilisateur `tina` possède maintenant les droits du rôle `secretariat`.

**pg_hba.conf**

Par défaut, PostgreSQL n'écoute que sur les interfaces réseaux locales
(`127.0.0.1` et `::1`). Le paramètre de configuration à changer pour écouter
sur d'autres adresses est `listen_adresses` qui se trouve dans le fichier
`postgresql.conf` :

```
listen_adresses = '*'
```

Vous devez redémarrer votre instance pour que ces changements soient pris en
compte.

Pour autoriser vos voisins à accéder à la base, ajouter les lignes suivantes dans le fichier `pg_hba.conf` :

```
# pour accéder depuis votre machine hôte avec l'adresse IP 192.168.56.X/32
host       cave    bob     192.168.56.X/32           md5
# pour le voisin à votre gauche avec l'adresse IP 2001:660:7101:3:4::Y/128
host       cave    tina    2001:660:7101:3:4::Y/128  trust
# pour le voisin à votre droite avec l'adresse IP 10.130.196.Z/32
hostnossl  cave    bob     10.130.196.Z/32           md5
```

Vous devez recharger la configuration de  votre instance pour que ces
changements soient pris en compte.

#### Exercice 4 : coût des fonctions 

  * Écrire une fonction `hello` qui renvoie la chaîne de caractère « Hello World! »
  en SQL.

```sql
CREATE OR REPLACE FUNCTION hello()
RETURNS text
AS $BODY$
  SELECT 'hello world !'::text;
$BODY$
LANGUAGE SQL;
```


  * Écrire une fonction `hello_pl` qui renvoie la chaîne de caractère « Hello
  World! » en PL/PgSQL.

```sql
CREATE OR REPLACE FUNCTION hello_pl()
RETURNS text
AS $BODY$
  BEGIN
    RETURN 'hello world !';
  END
$BODY$
LANGUAGE plpgsql;
```

  * Comparer les coûts des deux plans d'exécutions de ces requêtes. Expliquer les
  coûts.

Requêtage :

```sql
cave=# explain SELECT hello();
                QUERY PLAN
------------------------------------------
 Result  (cost=0.00..0.01 rows=1 width=0)
(1 ligne)

cave=# explain SELECT hello_pl();
                QUERY PLAN
------------------------------------------
 Result  (cost=0.00..0.26 rows=1 width=0)
(1 ligne)
```

Par défaut, si on ne précise pas le coût (`COST`) d'une fonction, cette
dernière a un coût par défaut de 100. Ce coût est à multiplier par la valeur
de `cpu_operator_cost`, par défaut à 0.0025. Le coût total d'appel de la
fonction `hello_pl` est donc par défaut de :

`100*cpu_operator_cost + cpu_tuple_cost = 0.26`

  * Modifier la fonction `hello_pl` pour que son coût soit cohérent avec son temps
  d'exécution.

Le temps d'exécution de `hello_pl` est environ 4 fois plus long que celle de 
`hello`. Nous allons donc fixer son coût pour que :

```
nouveau_cout*cpu_operator_cost + cpu_tuple_cost = 0.04
nouveau_cout = (0.04 - 0.01) / 0.0025 = 12
```

Ce qui nous donne l'ordre suivant :

```sql
ALTER FUNCTION hello_pl() COST 12;
```

</div>

------

