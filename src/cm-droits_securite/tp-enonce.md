## Travaux Pratiques 5

<div class="slide-content">
  * Sécurité et Droits
</div>

<div class="notes">

**Rappel**

Durant ces travaux pratiques, nous allons utiliser la machine virtuelle du TP 3
pour héberger notre serveur de base de données PostgreSQL.

Effectuez les manipulations nécessaires pour réaliser les actions listées dans
la section _Énoncés_. 

Vous pouvez vous aider du cours, des derniers TP, ainsi que de l'aide en ligne ou
des pages de manuels (`man`).

### Énoncés

**MCD Base `cave`**

![Schéma de la base cave](medias/common-schema_cave.png)

#### Exercice 1

Modifier le prompt de l'outil `psql`. Pour cela, créer le fichier `$HOME/.psqlrc`.

Nous voulons afficher le nom de l'utilisateur, le nom de la base de données,
l'information sur l'état de l'entrée et enfin un symbole terminal différent si
l'utilisateur est un super-utilisateur ou non.

Vous devez obtenir pour l'utilisateur postgres sur la base postgres :
```sql
postgres@postgres=# 
```

Et pour l'utilisateur `caviste` sur la base de données `cave` :
```sql
caviste@cave=> 
```

Pour plus d'information à ce propos, consulter la page de manuel de `psql`, section *Advanced Features* > *Prompting*.

#### Exercice 2 : droits sur les objets


**Gestion des rôles**

Ajouter la table `facture (id int, objet text, creationts timestamp)` à la base de données `cave`.

**Création d'un utilisateur et d'un groupe**

À l'aide du super-utilisateur `postgres` :


  * créer un rôle `secretariat` sans droit de connexion, mais qui peut
  visualiser, ajouter, mettre à jour et supprimer des éléments de la table
  `facture` ;
  * créer un rôle `bob` qui peut se connecter et appartient au rôle
  `secretariat` ;
  * vérifier la création des deux rôles.

À l'aide du rôle `bob` :

  * insérer les factures ayant pour objet « Vin de Bordeaux » et « Vin de
  Bourgogne » avec la date et l'heure courante ;
  * sélectionner les factures de la table `factures` ;
  * mettre à jour la deuxième facture avec la date et l'heure courante ;
  * supprimer la première facture.

**Modification des permissions**

Retirer les droits `DELETE` sur la table `facture` pour le rôle `secretariat`. Vérifier qu'il n'est pas possible de supprimer la deuxième facture avec le rôle `bob`.

Retirer tous les droits pour le rôle `secretariat` sur la table `facture` et vérifier que le rôle `bob` ne peut pas sélectionner les appellations contenues dans la table `appellation`.

Autoriser `bob` à accéder à la table `appellation` en lecture. Vérifier que `bob` peut désormais accéder à la table `appellation`.

**Héritage des droits au login**

Redonner les droits au rôle `secretariat` de visualiser, ajouter, mettre à jour
et supprimer des éléments sur la table `facture`.

Créer un rôle `tina` appartenant au rôle `secrétariat`, avec l'attribut `LOGIN`, mais n'héritant pas des droits à la connexion.

Vérifier que `tina` ne peut pas accéder à la table `facture`.

Dans la session, activer le rôle `secretariat` (`SET ROLE`) et sélectionner les données de la table `facture`. Vérifier que `tina` possède maintenant les droits du rôle `secretariat`.

#### Exercice 3 : pg_hba.conf

Établissez la configuration suivante pour accéder à la base de données:

  * autorisez la connection à partir de votre machine hôte avec un mot de passe
  avec l'utilisateur `bob` ;
  * autorisez votre voisin de gauche à se connecter en IPv6 sans mot de passe 
  avec l'utilisateur `tina` ;
  * autorisez votre voisin de droite à se connecter avec un mot de passe
  (autorisation en IP sans ssl) avec l'utilisateur `bob`.

Etapes :

  * redirection du port 55432 de votre machine hôte vers le port 5432 de la
  machine virtuelle.
  * définition d'un mot de passe pour l'utilisateur `bob`
  * modification de votre fichier pg_hba.conf
  * rechargement de la configuration pour prendre en compte vos changements


#### Exercice 4 : coût des fonctions

Écrire une fonction `hello` qui renvoie la chaîne de caractère « Hello World! » en SQL.

Écrire une fonction `hello_pl` qui renvoie la chaîne de caractère « Hello World! » en PL/PgSQL.

Comparer les coûts des deux plans d'exécutions de ces requêtes. Expliquer les coûts.

Modifier la fonction `hello_pl` pour que son coût soit cohérent avec son temps d'exécution.

-----


### Machine Virtuelle

Si vous ne l'avez pas encore fait, la machine virtuelle du TP 3 doit-être
déployée.  
Se référer à ce TP pour la déployer.

Pour lancer votre machine virtuelle, lancer la commande suivante dans votre terminal :

```bash
vboxmanage startvm TP_bdd_2 --type headless
```

A la fin de votre TP, n'oubliez pas de l'éteindre, elle restera sinon allumée et
accessible à quiconque se connecte sur la machine que vous avez utilisé. Vous
pouvez éteindre votre machine virtuelle avec la commande :

```bash
vboxmanage controlvm TP_bdd_2 poweroff
```

Pour se connecter à votre machine virtuelle en ssh, utilisez la commande
suivante :

```bash
ssh <login>@192.168.56.102
```

Les mots de passe sont identiques aux logins : _tp_ et _root_.

Pour vous connecter au compte `postgres`, utilisez la commande à partir du
compte _root_ :

```bash
sudo -iu postgres
```


</div>

-----
