# Sécurité et Droits

![PostgreSQL](medias/w1-3574411866_717f8d2b91_z.jpg)
\

<div class="notes">
</div>

-----


## Introduction

<div class="slide-content">
  * Sécurité physique d'une instance
  * Droits d'accès en PostgreSQL
  * Gestion des droits sur les objets


</div>


<div class="notes">
</div>

-----

## Sécurité physique

<div class="slide-content">

  * Données stockées dans des fichiers
  * Contrôle de l'accès au serveur
  * Application des mises à jours de sécurité

</div>


<div class="notes">

PostgreSQL stocke toutes les données dans des fichiers. Si un utilisateur
malintentionné accède à ces fichiers, il accède à toutes les données stockées.

PostgreSQL ne démarrera pas si les droits sur le répertoire de l'instance
(`data_directory`) est différent de `700` :

```
postgres$ ls -l /var/lib/postgresql/10
total 4
drwx------ 19 postgres postgres 4096 oct.  19 09:38 main
```

Sécuriser les droits sur ces fichiers est important. Sécuriser l'accès physique ou
par ssh à la machine l'est tout autant. Si vous donnez un mot de passe à
l'utilisateur _postgres_, utilisez un outil tel `apg` pour générer un mot de passe
aléatoire solide. Faites de même pour tout utilisateur pouvant se connecter au
serveur.

Des failles de sécurité peuvent être découvertes dans des logiciels tierces
installés sur votre serveur. Il est important d'appliquer les mises à jours de
sécurité régulièrement.

</div>


-----




### Chiffrements

<div class="slide-content">

  * Données disques
    * pas en natif
    * pgcrypto
  * Connexions
    * SSL
    * avec ou sans certificats serveur et/ou client
</div>


<div class="notes">

PostgreSQL ne chiffre pas les données sur disque. Si l'instance complète doit
être chiffrée, il est conseillé d'utiliser un système de fichiers qui
propose cette fonctionnalité. Attention au fait que cela ne vous protège que
contre la récupération des données sur un disque non monté. Quand le disque
est monté, les données sont lisibles suivant les règles d'accès d'Unix.

Néanmoins, il existe un module contrib appelé pgcrypto, permettant d'accéder
à des fonctions de chiffrement et de hachage. Cela permet de protéger les
informations provenant de colonnes spécifiques. Le chiffrement se fait du
côté serveur, ce qui sous-entend que l'information est envoyée en clair sur
le réseau. Le chiffrement SSL est donc obligatoire dans ce cas.

Par défaut, les sessions ne sont pas chiffrées. Les requêtes et les données
passent donc en clair sur le réseau. Il est possible de les chiffrer avec SSL,
ce qui aura une conséquence négative sur les performances. Il est aussi
possible d'utiliser les certificats (au niveau serveur et/ou client) pour
augmenter encore la sécurité des connexions.

</div>

-----


## Droits de connexion

<div class="slide-content">

  * Lors d'une connexion, indication :
    * de l'hôte (socket Unix ou alias/adresse IP)
    * du nom de la base de données
    * du nom du rôle
    * du mot de passe (parfois optionnel)
  * Suivant les trois premières informations
    * impose une méthode d'authentification
</div>


<div class="notes">


Lors d'une connexion, l'utilisateur fournit, explicitement ou non, plusieurs
informations. PostgreSQL va choisir une méthode d'authentification en se basant
sur les informations fournies et sur la configuration d'un fichier appelé
pg_hba.conf. HBA est l'acronyme de Host Based Authentication.
</div>





-----


### `pg_hba.conf` et `pg_ident.conf`

<div class="slide-content">

  * Authentification multiple, suivant l’utilisateur, la base et la source de la
connexion.

    + `pg_hba.conf` (_Host Based Authentication_)
    + `pg_ident.conf`, si mécanisme externe d’authentification
    + paramètres `hba_file` et `ident_file`
</div>


<div class="notes">


L’authentification est paramétrée au moyen du fichier `pg_hba.conf`. Dans ce
fichier, pour une tentative de connexion à une base donnée, pour un
utilisateur donné, pour un transport (IP, IPV6, Socket Unix, SSL ou non), et
pour une source donnée, ce fichier permet de spécifier le mécanisme
d’authentification attendu.

Si le mécanisme d’authentification s’appuie sur un système externe (LDAP,
Kerberos, Radius…), des tables de correspondances entre utilisateur de la base
et utilisateur demandant la connexion peuvent être spécifiées dans
`pg_ident.conf`.

Ces noms de fichiers ne sont que les noms par défaut. Ils peuvent tout à fait
être remplacés en spécifiant de nouvelles valeurs de `hba_file` et
`ident_file` dans `postgresql.conf`.
</div>



-----


### Informations de connexion

<div class="slide-content">

  * Quatre informations :
    * socket Unix ou adresse/alias IP
    * numéro de port
    * nom de la base
    * nom du rôle
  * Fournies explicitement ou implicitement
</div>


<div class="notes">


Tous les outils fournis avec la distribution PostgreSQL (par exemple `createuser
`) acceptent des options en ligne de commande pour fournir les informations en
question :


  * `-h` pour la socket Unix ou l'adresse/alias IP
  * `-p` pour le numéro de port
  * `-d` pour le nom de la base
  * `-U` pour le nom du rôle

Si l'utilisateur ne passe pas ces informations, plusieurs variables d'
environnement sont vérifiées :


  * `PGHOST` pour la socket Unix ou l'adresse/alias IP
  * `PGPORT` pour le numéro de port
  * `PGDATABASE` pour le nom de la base
  * `PGUSER` pour le nom du rôle

Au cas où ces variables ne seraient pas configurées, des valeurs par défaut
sont utilisées :


  * la socket Unix
  * 5432 pour le numéro de port
  * suivant l'outil, la base postgres ou le nom de l'utilisateur PostgreSQL, pour le
nom de la base
  * le nom de l'utilisateur au niveau du système d'exploitation pour le nom du
rôle

Autrement dit, quelle que soit la situation, PostgreSQL remplacera les
informations non fournies explicitement par des informations provenant des
variables d'environnement, voire par des informations par défaut.
</div>



-----


### Configuration de l'authentification

<div class="slide-content">

  * PostgreSQL utilise les informations de connexion pour sélectionner la méthode
  * Fichier de configuration: `pg_hba.conf`
  * Se présente sous la forme d'un tableau
    * 4 colonnes d'informations
    * 1 colonne indiquant la méthode à appliquer
    * 1 colonne optionnelle d'options
</div>


<div class="notes">


Lorsque le serveur PostgreSQL récupère une demande de connexion, il connaît
le type de connexion utilisé par le client (socket Unix, connexion TCP SSL,
connexion TCP simple, etc). Il connaît aussi l'adresse IP du client (dans le
cas d'une connexion via une socket TCP), le nom de la base et celui de l'
utilisateur. Il va donc parcourir les lignes du tableau enregistré dans le
fichier `pg_hba.conf`. Il les parcourt dans l'ordre. La première qui correspond
aux informations fournies lui précise la méthode d'authentification. Il ne lui
reste plus qu'à appliquer cette méthode. Si elle fonctionne, la connexion est
autorisée et se poursuit. Si elle ne fonctionne pas, quelle qu'en soit la
raison, la connexion est refusée. Aucune autre ligne du fichier ne sera lue.

Il est donc essentiel de bien configurer ce fichier pour avoir une protection
maximale.

Le tableau se présente ainsi :

```
# local      DATABASE  USER  METHOD  [OPTIONS]
# host       DATABASE  USER  ADDRESS  METHOD  [OPTIONS]
# hostssl    DATABASE  USER  ADDRESS  METHOD  [OPTIONS]
# hostnossl  DATABASE  USER  ADDRESS  METHOD  [OPTIONS]
```


</div>



-----


### Colonne type

<div class="slide-content">

  * 4 valeurs possibles
    * `local`
    * `host`
    * `hostssl`
    * `hostnossl`
  * `hostssl` nécessite d'avoir activé `ssl` dans `postgresql.conf`
</div>


<div class="notes">


La colonne type peut contenir quatre valeurs différentes. La valeur `local`
concerne les connexions via la socket Unix. Toutes les autres valeurs concernent
les connexions via la socket TCP. La différence réside dans l'utilisation
forcée ou non du SSL :


  * `host`, connexion via la socket TCP, avec ou sans SSL ;
  * `hostssl`, connexion via la socket TCP, avec SSL ;
  * `hostnossl`, connexion via la socket TCP, sans SSL.

Il est à noter que l'option `hostssl` n'est utilisable que si le paramètre `
ssl` du fichier `postgresql.conf` est à `on`.
</div>



-----


### Colonne database

<div class="slide-content">

  * Nom de la base
  * Plusieurs bases (séparées par des virgules)
  * Nom d'un fichier contenant la liste des bases (précédé par une arobase)
  * Mais aussi
    * `all` (pour toutes les bases)
    * `sameuser`, `samerole` (pour la base de même nom que le rôle)
    * `replication` (pour les connexions de réplication physique)
</div>


<div class="notes">


La colonne peut recueillir le nom d'une base, le nom de plusieurs bases en les
séparant par des virgules, le nom d'un fichier contenant la liste des bases ou
quelques valeurs en dur. La valeur all indique toutes les bases. La valeur
replication est utilisée pour les connexions de réplication (il n'est pas
nécessaire d'avoir une base nommée replication). Enfin, la valeur `sameuser`
spécifie que l'enregistrement n'intercepte que si la base de données demandée
a le même nom que le rôle demandé, alors que la valeur `samerole` spécifie
que le rôle demandé doit être membre du rôle portant le même nom que la
base de données demandée.
</div>



-----


### Colonne user

<div class="slide-content">

  * Nom du rôle
  * Nom d'un groupe (précédé par un signe plus)
  * Plusieurs rôles (séparés par des virgules)
  * Nom d'un fichier contenant la liste des rôles (précédé par une arobase)
  * Mais aussi
    * `all` (pour tous les rôles)
</div>


<div class="notes">


La colonne peut recueillir le nom d'un rôle, le nom d'un groupe en le
précédant d'un signe plus, le nom de plusieurs rôles en les séparant par des
virgules, le nom d'un fichier contenant la liste des bases ou quelques valeurs
en dur. La valeur all indique tous les rôles.
</div>



-----


### Colonne adresse IP

<div class="slide-content">

  * Uniquement dans le cas d'une connexion `host`, `hostssl` et `hostnossl`
  * Soit l'adresse IP et le masque réseau
  * Soit l'adresse au format CIDR
  * Soit un nom DNS
</div>


<div class="notes">


La colonne de l'adresse IP permet d'indiquer une adresse IP ou un sous-réseau
IP. Il est donc possible de filtrer les connexions par rapport aux adresses IP,
ce qui est une excellente protection.

Voici deux exemples d'adresses IP au format adresse et masque de sous-réseau :

```
192.168.168.1 255.255.255.255
192.168.168.0 255.255.255.0
```


Et voici deux exemples d'adresses IP au format CIDR :

```
192.168.168.1/32
192.168.168.0/24
```

Il est possible d'utiliser un nom d'hôte ou un domaine DNS, au prix d'une
recherche DNS pour chaque hostname présent, pour chaque nouvelle connexion.
</div>



-----


### Colonne méthode

<div class="slide-content">

  * Précise la méthode d'authentification à utiliser
  * Deux types de méthodes
    * internes
    * externes
  * Possibilité d'ajouter des options dans une dernière colonne
</div>


<div class="notes">


La colonne de la méthode est la dernière colonne, voire l'avant-dernière si
vous voulez ajouter une option à la méthode d'authentification.
</div>



-----


### Colonne options

<div class="slide-content">

  * Dépend de la méthode d'authentification
  * Méthode externe : option `map`
</div>


<div class="notes">


Les options disponibles dépendent de la méthode d'authentification
sélectionnée. Toutes les méthodes externes permettent l'utilisation de
l'option map. Cette option a pour but d'indiquer la carte de correspondance à
sélectionner dans le fichier `pg_ident.conf`.
</div>



-----


### Méthodes internes

<div class="slide-content">

  * `trust`
  * `reject`
  * `md5`
  * `scram-sha-256`
  * `password` (à éviter)
</div>


<div class="notes">


La méthode `trust` est certainement la pire. À partir du moment où le rôle
est reconnu, aucun mot de passe n'est demandé. Si le mot de passe est fourni
malgré tout, il n'est pas vérifié. Il est donc essentiel de proscrire cette
méthode d'authentification.

La méthode `password` force la saisie d'un mot de passe. Cependant, ce dernier
est envoyé en clair sur le réseau. Il n'est donc pas conseillé d'utiliser
cette méthode, surtout sur un réseau non sécurisé.

La méthode `md5` est certainement la méthode la plus intéressante. La saisie
du mot de passe est forcée. De plus, le mot de passe transite chiffré en md5.

Le support de `scram-sha-256` est disponible à partir de la version 10. Cette
méthode est la plus sûre.

La méthode `reject` est intéressante dans certains cas de figure. Par exemple,
on veut que le rôle u1 puisse se connecter à la base de données b1 mais pas
aux autres. Voici un moyen de le faire (pour une connexion via les sockets Unix)
:

```
local  b1   u1  md5
local  all  u1  reject
```


</div>



-----


### Méthodes externes

<div class="slide-content">

  * `ldap`, `radius`, `cert`
  * `gss`, `sspi`
  * `ident`, `peer`, `pam`
  * `bsd`
</div>


<div class="notes">


Ces différentes méthodes permettent d'utiliser des annuaires d'entreprise
comme RADIUS, LDAP ou un ActiveDirectory. Certaines méthodes sont spécifiques
à Unix (comme `ident` et `peer`), voire à Linux (comme `pam`).

La méthode `LDAP` utilise un serveur LDAP pour authentifier l'utilisateur. Ceci
n'est disponible qu'à partir de la version 8.2.

La méthode `GSSAPI` correspond au protocole du standard de l'industrie pour l'
authentification sécurisée définie dans RFC 2743. PostgreSQL supporte `GSSAPI
` avec l'authentification Kerberos suivant la RFC 1964 ce qui permet de faire du
« Single Sign-On ». Attention, ce n'est disponible qu'à partir de la version
8.3.

La méthode `Radius` permet d'utiliser un serveur RADIUS pour authentifier l'
utilisateur, dès la version 9.0.

La méthode `ident` permet d'associer les noms des utilisateurs du système d'
exploitation aux noms des utilisateurs du système de gestion de bases de
données. Un démon fournissant le service ident est nécessaire.

La méthode `peer` permet d'associer les noms des utilisateurs du système d'
exploitation aux noms des utilisateurs du système de gestion de bases de
données. Ceci n'est possible qu'avec une connexion locale.

Quant à `pam`, il authentifie l'utilisateur en passant par les `Pluggable
Authentication Modules` (PAM) fournis par le système d'exploitation.

Avec la version 9.6 apparaît la méthode `bsd`. Cette méthode est similaire à
la méthode `password` mais utilise le système d'authentification BSD.
</div>



-----


### Méthodes obsolètes

<div class="slide-content">

  * `crypt` depuis la version 8.4
  * `krb5` depuis la version 9.3
</div>


<div class="notes">


La méthode `crypt` est jugée trop faible et ne fait plus partie des méthodes
utilisables à partir de la version 8.4.

La méthode `krb5` a été définitivement supprimée du code en version 9.4.
Elle n'était déjà plus utilisable depuis la version 9.3 et signalée
obsolète depuis la version 8.3. Il convient d'employer la méthode `GSSAPI`
pour utiliser une authentification à base de Kerberos.
</div>



-----


### Le pg_hba.conf par défaut

<div class="slide-content">

Voici les entrées par défaut du fichier `pg_hba.conf`  
en version 10 sur Debian :

```
# TYPE  DATABASE        USER            ADDRESS                 METHOD
local   all             postgres                                peer
local   all             all                                     peer
host    all             all             127.0.0.1/32            md5
host    all             all             ::1/128                 md5
local   replication     all                                     peer
host    replication     all             127.0.0.1/32            md5
host    replication     all             ::1/128                 md5
```

</div>


<div class="notes">

</div>

-----

## Gestion des droits sur les objets

<div class="slide-content">

  * Ce qu'un utilisateur standard peut faire
    * et ne peut pas faire
  * Restreindre les droits
</div>


<div class="notes">


À l'installation de PostgreSQL, il est essentiel de s'assurer de la sécurité
du serveur : sécurité au niveau des accès, au niveau des objets, ainsi qu'au
niveau des données.

Ce chapitre va faire le point sur ce qu'un utilisateur peut faire par défaut et
sur ce qu'il ne peut pas faire. Nous verrons ensuite comment restreindre les
droits. Enfin, nous verrons les possibilités de chiffrement.
</div>



-----


### Par défaut

<div class="slide-content">

  * Un utilisateur standard peut
    * accéder à toutes les bases de données
    * créer des objets dans le schéma PUBLIC de toute base de données
    * créer des objets temporaires
    * modifier les paramètres de la session
    * créer des fonctions
    * exécuter des fonctions définies par d'autres dans le schéma PUBLIC
    * récupérer des informations sur l'instance
    * visualiser le source des vues et des fonctions
</div>


<div class="notes">


Par défaut, un utilisateur a beaucoup de droits.

Il peut accéder à toutes les bases de données. Il faut modifier le fichier `
pg_hba.conf` pour éviter cela. Il est aussi possible de supprimer ce droit avec
l'ordre suivant :

```sql
REVOKE CONNECT ON DATABASE nom_base FROM nom_utilisateur;
```

Il peut créer des objets dans le schéma disponible par
défaut (nommé `public`) sur chacune des bases de données où il peut se
connecter. Il est possible de supprimer ce droit avec l'ordre suivant :

```sql
REVOKE CREATE ON SCHEMA public FROM nom_utilisateur;
```

Il peut
créer des objets temporaires sur chacune des bases de données où il peut se
connecter. Il est possible de supprimer ce droit avec l'ordre suivant :

```sql
REVOKE TEMP ON DATABASE nom_base FROM nom_utilisateur;
```

Il peut modifier les paramètres de session (par exemple le paramètre `work_mem` pour
s'allouer beaucoup plus de mémoire). Il est impossible d'empêcher cela.

Il peut créer des fonctions, uniquement avec les langages de confiance,
uniquement dans les schémas où il a le droit de créer des objets. Il existe
deux solutions :

  * supprimer le droit d'utiliser un langage

```sql
REVOKE USAGE ON LANGUAGE nom_langage FROM nom_utilisateur;
```

  * supprimer le droit de créer des objets
dans un schéma

```sql
REVOKE CREATE ON SCHEMA nom_schema FROM nom_utilisateur;
```

Il peut exécuter toute fonction, y compris définie par d'autres,
à condition qu'elles soient créées dans des schémas où il a accès.
Il est possible d'empêcher cela en supprimant le droit d'exécution d'une
fonction:

```sql
REVOKE EXECUTE ON FUNCTION nom_fonction FROM nom_utilisateur;
```

Il peut récupérer des informations sur l'instance car il a le droit
de lire tous les catalogues systèmes. Par exemple, en lisant pg_class, il peut
connaitre la liste des tables, vues, séquences, etc. En parcourant pg_proc, il
dispose de la liste des fonctions. Il n'y a pas de contournement à cela : un
utilisateur doit pouvoir accéder aux catalogues systèmes pour travailler
normalement.

Enfin, il peut visualiser le source des vues et des fonctions. Il existe des
modules propriétaires de chiffrement (ou plutôt d'obfuscation) du code mais
rien de natif. Le plus simple est certainement de coder les fonctions sensibles
en C.
</div>



-----


### Par défaut (suite)

<div class="slide-content">

  * Un utilisateur standard ne peut pas
    * créer une base
    * créer un rôle
    * accéder au contenu des objets créés par d'autres
    * modifier le contenu d'objets créés par d'autres
</div>


<div class="notes">


Un utilisateur standard ne peut pas créer de bases et de rôles. Il a besoin
pour cela d'attributs particuliers (respectivement `CREATEDB` et `CREATEROLE`).

Il ne peut pas accéder au contenu (aux données) d'objets créés par d'autres
utilisateurs. Ces derniers doivent lui donner ce droit explicitement. De même,
il ne peut pas modifier le contenu et la définition d'objets créés par d'
autres utilisateurs. Là-aussi, ce droit doit être donné explicitement.
</div>



-----


### Restreindre les droits

<div class="slide-content">

  * Sur les connexions
    * pg_hba.conf
  * Sur les objets
    * GRANT / REVOKE
    * SECURITY LABEL
  * Sur les fonctions
    * SECURITY DEFINER
    * LEAKPROOF
  * Sur les vues
    * security_barrier
    * WITH CHECK OPTION
</div>


<div class="notes">


Pour sécuriser plus fortement une instance, il est nécessaire de restreindre
les droits des utilisateurs.

Cela commence par la gestion des connexions. Les droits de connexion sont
généralement gérés via le fichier de configuration `pg_hba.conf`. Cette
configuration a déjà été abordé dans le chapitre Droits de connexion de ce
module de formation.

Cela passe ensuite par les droits sur les objets. On dispose pour cela des
instructions `GRANT` et `REVOKE`, qui ont été expliquées dans le chapitre
Droits sur les objets de ce module de formation. Il est possible d'aller plus
loin avec l'instruction `SECURITY LABEL` disponible depuis la version 9.1. Un
label de sécurité est un commentaire supplémentaire pris en compte par un
module de sécurité qui disposera de la politique de sécurité. Le seul module
de sécurité actuellement disponible est un module contrib appelé `se-pgsql`.

Certains objets disposent de droits particuliers. Par exemple, les fonctions
disposent des clauses `SECURITY DEFINER` et `LEAKPROOF`. La première permet
d' indiquer au système que la fonction doit s'exécuter avec les droits de son
propriétaire (et non pas avec ceux de l'utilisateur l'exécutant). Cela permet
d'éviter de donner des droits d'accès à certains objets. La seconde permet de
dire à PostgreSQL que cette fonction ne peut pas occasionner de fuites d'
informations. Ainsi, le planificateur de PostgreSQL sait qu'il peut optimiser
l'exécution des fonctions.

Quant aux vues, elles disposent d'une option appelée `security_barrier`.
Certains utilisateurs créent des vues pour filtrer des lignes, afin de
restreindre la visibilité sur certaines données. Or, cela peut se révéler
dangereux si un utilisateur malintentionné a la possibilité de créer une
fonction car il peut facilement contourner cette sécurité si cette option n'
est pas utilisée. Voici un exemple complet.

```
demo_9_2=# CREATE TABLE elements (id serial, contenu text, prive boolean);
NOTICE:  CREATE TABLE will create implicit sequence "elements_id_seq" for
         serial column "elements.id"
CREATE TABLE
demo_9_2=# INSERT INTO elements (contenu, prive)
VALUES ('a', false), ('b', false), ('c super prive', true), ('d', false),
       ('e prive aussi', true);
INSERT 0 5
demo_9_2=# SELECT * FROM elements;
 id |    contenu    | prive
----+---------------+-------
  1 | a             | f
  2 | b             | f
  3 | c super prive | t
  4 | d             | f
  5 | e prive aussi | t
(5 rows)
```

La table `elements` contient cinq
lignes, dont deux considérées comme privées. Nous allons donc créer une vue
ne permettant de voir que les lignes publiques.

```
demo_9_2=# CREATE OR REPLACE VIEW elements_public AS SELECT * FROM elements
WHERE CASE WHEN current_user='guillaume' THEN TRUE ELSE NOT prive END;
CREATE VIEW
demo_9_2=# SELECT * FROM elements_public;
 id |    contenu    | prive
----+---------------+-------
  1 | a             | f
  2 | b             | f
  3 | c super prive | t
  4 | d             | f
  5 | e prive aussi | t
(5 rows)

demo_9_2=# CREATE USER u1;
CREATE ROLE
demo_9_2=# GRANT SELECT ON elements_public TO u1;
GRANT
demo_9_2=# \c - u1
You are now connected to database "postgres" as user "u1".
demo_9_2=> SELECT * FROM elements;
ERROR:  permission denied for relation elements
demo_9_2=> SELECT * FROM elements_public ;
 id | contenu | prive
----+---------+-------
  1 | a       | f
  2 | b       | f
  4 | d       | f
(3 rows)
```

L'utilisateur `u1` n'a pas le
droit de lire directement la table `elements` mais a le droit d'y accéder via
la vue `elements_public`, uniquement pour les lignes dont le champ `prive` est
à `false`.

Avec une simple fonction, cela peut changer :

```
demo_9_2=> CREATE OR REPLACE FUNCTION abracadabra(integer, text, boolean)
RETURNS bool AS $$
BEGIN
RAISE NOTICE '% - % - %', $ 1, $ 2, $ 3;
RETURN true;
END$$
LANGUAGE plpgsql
COST 0.0000000000000000000001;
CREATE FUNCTION
demo_9_2=> SELECT * FROM elements_public WHERE abracadabra(id, contenu, prive);
NOTICE:  1 - a - f
NOTICE:  2 - b - f
NOTICE:  3 - c super prive - t
NOTICE:  4 - d - f
NOTICE:  5 - e prive aussi - t
 id | contenu | prive
----+---------+-------
  1 | a       | f
  2 | b       | f
  4 | d       | f
(3 rows)
```

Que s'est-il passé ? Pour
comprendre, il suffit de regarder l'EXPLAIN de cette requête :

```
demo_9_2=> EXPLAIN SELECT * FROM elements_public
WHERE abracadabra(id, contenu, prive);
                                  QUERY PLAN
--------------------------------------------------------------------------------
 Seq Scan on elements  (cost=0.00..28.15 rows=202 width=37)
   Filter: (abracadabra(id, contenu, prive) AND
           CASE WHEN ("current_user"() = 'u1'::name)
                THEN (NOT prive) ELSE true END)
(2 rows)
```

La fonction `
abracadrabra` a un coût si faible que PostgreSQL l'exécute avant le filtre de
la vue. Du coup, la fonction voit toutes les lignes de la table.

Seul moyen d'échapper à cette optimisation du planificateur, utiliser l'option
`security_barrier` en 9.2 :

```
demo_9_2=> \c - guillaume
You are now connected to database "postgres" as user "guillaume".
demo_9_2=# CREATE OR REPLACE VIEW elements_public WITH (security_barrier) AS
SELECT * FROM elements WHERE CASE WHEN current_user='guillaume'
THEN true ELSE NOT prive END;
CREATE VIEW
demo_9_2=# \c - u1
You are now connected to database "postgres" as user "u1".
demo_9_2=> SELECT * FROM elements_public WHERE abracadabra(id, contenu, prive);
NOTICE:  1 - a - f
NOTICE:  2 - b - f
NOTICE:  4 - d - f
 id | contenu | prive
----+---------+-------
  1 | a       | f
  2 | b       | f
  4 | d       | f
(3 rows)

demo_9_2=> EXPLAIN SELECT * FROM elements_public
WHERE abracadabra(id, contenu, prive);
                                        QUERY PLAN
--------------------------------------------------------------------------------
 Subquery Scan on elements_public  (cost=0.00..34.20 rows=202 width=37)
   Filter: abracadabra(elements_public.id, elements_public.contenu,
                       elements_public.prive)
   ->  Seq Scan on elements  (cost=0.00..28.15 rows=605 width=37)
         Filter: CASE WHEN ("current_user"() = 'u1'::name)
                      THEN (NOT prive) ELSE true END
(4 rows)
```

[Voir aussi cet article, par Robert Haas](http://rhaas.blogspot.com/2012/03/security-barrier-views.html).

Avec PostgreSQL 9.3, l'utilisation de l'option `security_barrier` empêche
également l'utilisateur de modifier les données de la table sous-jacente,
même s'il a les droits en écriture sur la vue :

```
postgres=# GRANT UPDATE ON elements_public TO u1;
GRANT

postgres=# \c - u1
You are now connected to database "postgres" as user "u1".

postgres=> update elements_public set prive = true where id = 2 ;
ERROR:  cannot update view "elements_public"
DETAIL:  Security-barrier views are not automatically updatable.
HINT:  To enable updating the view, provide an INSTEAD OF UPDATE trigger or an
       unconditional ON UPDATE DO INSTEAD rule.
```

À partir de la version 9.4 de
PostgreSQL, c'est désormais possible :

```
postgres=# GRANT UPDATE ON elements_public TO u1;
GRANT

postgres=# \c - u1
You are now connected to database "postgres" as user "u1".

postgres=# update elements_public set contenu = 'e' where id = 1 ;
UPDATE 1

postgres=# select * from elements_public ;
 id | contenu | prive
----+---------+-------
  2 | b       | f
  4 | d       | f
  1 | e       | f
(3 rows)
```

À noter que cela lui permet également
par défaut de modifier les lignes même si la nouvelle valeur les lui rend
inaccessibles :

```
postgres=> update elements_public set prive = true where id = 1 ;
UPDATE 1

postgres=> select * from elements_public ;
 id | contenu | prive
----+---------+-------
  2 | b       | f
  4 | d       | f
(2 rows)
```

La version 9.4 de PostgreSQL apporte donc également la
possibilité d'empêcher ce comportement, grâce à l'option `WITH CHECK OPTION`
de la syntaxe de création de la vue :

```
postgres=# CREATE OR REPLACE VIEW elements_public WITH (security_barrier) AS
SELECT * FROM elements WHERE CASE WHEN current_user='guillaume'
THEN true ELSE NOT prive END WITH CHECK OPTION ;

postgres=# \c - u1
You are now connected to database "postgres" as user "u1".

postgres=> update elements_public set prive = true where id = 2 ;
ERREUR:  new row violates WITH CHECK OPTION for view "elements_public"
DETAIL:  La ligne en échec contient (2, b, t)
```


</div>


-----


## Droits sur les objets

<div class="slide-content">

  * Droits sur les objets
  * Droits sur les méta-données
  * Héritage des droits
  * Changement de rôle
</div>


<div class="notes">


Pour bien comprendre l'intérêt des utilisateurs, il faut bien comprendre la
gestion des droits. Les droits sur les objets vont permettre aux utilisateurs de
créer des objets ou de les utiliser. Les commandes `GRANT` et `REVOKE` sont
essentielles pour cela. Modifier la définition d'un objet demande un autre type
de droit, que les commandes précédentes ne permettent pas d'obtenir.

Donner des droits à chaque utilisateur peut paraître long et difficile. C'est
pour cela qu'il est généralement préférable de donner des droits à une
entité spécifique dont certains utilisateurs hériteront.
</div>



-----


### Droits sur les objets

<div class="slide-content">

  * Donner un droit : `GRANT`
  * Retirer un droit : `REVOKE`
  * Droits spécifiques pour chaque type d'objets
  * Avoir le droit de donner le droit : `WITH GRANT OPTION`
</div>


<div class="notes">


Par défaut, seul le propriétaire a des droits sur son objet. Les
superutilisateurs n'ont pas de droit spécifique sur les objets mais étant
donné leur statut de superutilisateur, ils peuvent tout faire sur tous les
objets.

Le propriétaire d'un objet peut décider de donner certains droits sur cet
objet à certains rôles. Il le fera avec la commande `GRANT` :

```sql
GRANT droits ON type_objet nom_objet TO role
```

Les droits
disponibles dépendent du type d'objet visé. Par exemple, il est possible de
donner le droit `SELECT` sur une table mais pas sur une fonction. Une fonction
ne se lit pas, elle s'exécute. Il est donc possible de donner le droit `EXECUTE
` sur une fonction. Il faut donner les droits aux différents objets
séparément. De plus, donner le droit `ALL` sur une base de données donne tous
les droits sur la base de données, autrement dit l'objet base de donnée, pas
sur les objets à l'intérieur de la base de données. `GRANT` n'est pas une
commande récursive. Prenons un exemple :

```
b1=# CREATE ROLE u20 LOGIN;
CREATE ROLE
b1=# CREATE ROLE u21 LOGIN;
CREATE ROLE
b1=# \c b1 u20
You are now connected to database "b1" as user "u20".
b1=> CREATE SCHEMA s1;
ERROR:  permission denied for database b1
b1=> \c b1 postgres
You are now connected to database "b1" as user "postgres".
b1=# GRANT CREATE ON DATABASE b1 TO u20;
GRANT
b1=# \c b1 u20
You are now connected to database "b1" as user "u20".
b1=> CREATE SCHEMA s1;
CREATE SCHEMA
b1=> CREATE TABLE s1.t1 (c1 integer);
CREATE TABLE
b1=> INSERT INTO s1.t1 VALUES (1), (2);
INSERT 0 2
b1=> SELECT * FROM s1.t1;
 c1
----
  1
  2
(2 rows)

b1=> \c b1 u21
You are now connected to database "b1" as user "u21".
b1=> SELECT * FROM s1.t1;
ERROR:  permission denied for schema s1
LINE 1: SELECT * FROM s1.t1;
                      ^
b1=> \c b1 u20
You are now connected to database "b1" as user "u20".
b1=> GRANT SELECT ON TABLE s1.t1 TO u21;
GRANT
b1=> \c b1 u21
You are now connected to database "b1" as user "u21".
b1=> SELECT * FROM s1.t1;
ERROR:  permission denied for schema s1
LINE 1: SELECT * FROM s1.t1;
                      ^
b1=> \c b1 u20
You are now connected to database "b1" as user "u20".
b1=> GRANT USAGE ON SCHEMA s1 TO u21;
GRANT
b1=> \c b1 u21
You are now connected to database "b1" as user "u21".
b1=> SELECT * FROM s1.t1;
 c1
----
  1
  2
(2 rows)

b1=> INSERT INTO s1.t1 VALUES (3);
ERROR:  permission denied for relation t1
```

Le problème de ce fonctionnement est
qu'il faut indiquer les droits pour chaque utilisateur, ce qui peut devenir
difficile et long. Imaginez avoir à donner le droit `SELECT` sur les 400 tables
d'un schéma... long et fastidieux... En 9.0, il est néanmoins possible de
donner les droits sur tous les objets d'un certain type dans un schéma. Voici
un exemple :

```sql
GRANT SELECT ON ALL TABLES IN SCHEMA s1 to u21;
```

Notez aussi que, lors de la création d'une base, PostgreSQL ajoute
automatiquement un schéma nommé public. Tous les droits sont donnés sur ce
schéma à un pseudo-rôle appelé public. Tous les rôles sont automatiquement
membres de ce pseudo-rôle. Si vous voulez gérer complètement les droits sur
ce schéma, il faut d'abord penser à enlever les droits pour ce pseudo-rôle.
Pour cela, il vous faut utiliser la commande `REVOKE` ainsi :

```sql
REVOKE ALL ON SCHEMA public FROM public;
```

Enfin, toujours
avec la 9.0, il est possible d'ajouter des droits pour des objets qui n'ont pas
encore été créés. En fait, la commande `ALTER DEFAULT PRIVILEGES` permet de
donner des droits par défaut à certains rôles. De cette façon, sur un
schéma qui a tendance à changer fréquemment, il n'est plus nécessaire de se
préoccuper des droits sur les objets.

Lorsqu'un droit est donné à un rôle, par défaut, ce rôle ne peut pas le
donner à un autre. Pour lui donner en plus le droit de donner ce droit à un
autre rôle, il faut utiliser la clause `WITH GRANT OPTION` comme le montre cet
exemple :

```
b1=# CREATE TABLE t2 (id integer);
CREATE TABLE
b1=# INSERT INTO t2 VALUES (1);
INSERT 0 1
b1=# SELECT * FROM t2;
 id
----
  1
(1 row)

b1=# \c b1 u1
You are now connected to database "b1" as user "u1".
b1=> SELECT * FROM t2;
ERROR:  permission denied for relation t2
b1=> \c b1 postgres
You are now connected to database "b1" as user "postgres".
b1=# GRANT SELECT ON TABLE t2 TO u1;
GRANT
b1=# \c b1 u1
You are now connected to database "b1" as user "u1".
b1=> SELECT * FROM t2;
 id
----
  1
(1 row)

b1=> \c b1 u2
You are now connected to database "b1" as user "u2".
b1=> SELECT * FROM t2;
ERROR:  permission denied for relation t2
b1=> \c b1 u1
You are now connected to database "b1" as user "u1".
b1=> GRANT SELECT ON TABLE t2 TO u2;
WARNING:  no privileges were granted for "t2"
GRANT
b1=> \c b1 postgres
You are now connected to database "b1" as user "postgres".
b1=# GRANT SELECT ON TABLE t2 TO u1 WITH GRANT OPTION;
GRANT
b1=# \c b1 u1
You are now connected to database "b1" as user "u1".
b1=> GRANT SELECT ON TABLE t2 TO u2;
GRANT
b1=> \c b1 u2
You are now connected to database "b1" as user "u2".
b1=> SELECT * FROM t2;
 id
----
  1
(1 row)
```


</div>



-----


### Droits sur les métadonnées

<div class="slide-content">

  * Seul le propriétaire peut changer la structure d'un objet
    * le renommer
    * le changer de schéma ou de tablespace
    * lui ajouter/retirer des colonnes
  * Un seul propriétaire
    * mais qui peut être un groupe
</div>


<div class="notes">


Les droits sur les objets ne concernent pas le changement des méta-données et
de la structure de l'objet. Seul le propriétaire (et les superutilisateurs)
peut le faire. S'il est nécessaire que plusieurs personnes puissent utiliser la
commande `ALTER` sur l'objet, il faut que ces différentes personnes aient un
rôle qui soit membre du rôle propriétaire de l'objet. Prenons un exemple :

```
b1=# \c b1 u21
You are now connected to database "b1" as user "u21".
b1=> ALTER TABLE s1.t1 ADD COLUMN c2 text;
ERROR:  must be owner of relation t1
b1=> \c b1 u20
You are now connected to database "b1" as user "u20".
b1=> GRANT u20 TO u21;
GRANT ROLE
b1=> \du
                                         List of roles
 Role name  |                         Attributes           |      Member of
------------+----------------------------------------------+--------------------
 admin      |                                              | {}
 caviste    |                                              | {}
 postgres   | Superuser, Create role, Create DB,          +| {}
            | Replication, Bypass RLS                      |
 stagiaire2 |                                              | {}
 u1         |                                              | {pg_signal_backend}
 u11        |                                              | {}
 u2         | Create DB                                    | {}
 u20        |                                              | {}
 u21        |                                              | {u20}
 u3         | Cannot login                                 | {}
 u4         | Create role, Cannot login                    | {}
 u5         |                                              | {u2,u6}
 u6         | Cannot login                                 | {}
 u7         | Create role                                  | {}
 u8         |                                              | {}
 u9         | Create DB                                    | {}

b1=> \c b1 u21
You are now connected to database "b1" as user "u21".
b1=> ALTER TABLE s1.t1 ADD COLUMN c2 text;
ALTER TABLE
```


Pour assigner un propriétaire différent aux objets ayant un certain
propriétaire, il est possible de faire appel à l'ordre `REASSIGN OWNED`. De
même, il est possible de supprimer tous les objets appartenant à un
utilisateur avec l'ordre `DROP OWNED`. Voici un exemple de ces deux commandes :

```
b1=# \d
         List of relations
 Schema |   Name   | Type  | Owner
--------+----------+-------+-------
 public | clients  | table | r31
 public | factures | table | r31
 public | t1       | table | r31
 public | t2       | table | r32
(4 rows)

b1=# REASSIGN OWNED BY r31 TO u1;
REASSIGN OWNED
b1=# \d
         List of relations
 Schema |   Name   | Type  | Owner
--------+----------+-------+-------
 public | clients  | table | u1
 public | factures | table | u1
 public | t1       | table | u1
 public | t2       | table | r32
(4 rows)

b1=# DROP OWNED BY u1;
DROP OWNED
b1=# \d
       List of relations
 Schema | Name | Type  | Owner
--------+------+-------+-------
 public | t2   | table | r32
(1 row)
```


</div>



-----


### Droits plus globaux

<div class="slide-content">

  * Rôles systèmes d'administration
    * pg_signal_backend (9.6+)
  * Rôles systèmes de supervision
    * pg_read_all_stats (10+)
    * pg_read_all_settings (10+)
    * pg_stat_scan_tables (10+)
    * pg_monitor (10+)
</div>


<div class="notes">
Certaines fonctionnalités nécessitent l'attribut `SUPERUSER` alors qu'il
serait bon de pouvoir les effectuer sans avoir ce droit suprême. Cela concerne
principalement la sauvegarde et la supervision.

Après beaucoup de discussions, les développeurs de PostgreSQL ont décidé de
créer des rôles systèmes permettant d'avoir plus de droits. Le premier rôle
de ce type est `pg_signal_backend` qui donne le droit d'exécuter les fonctions
`pg_cancel_backend()` et `pg_terminate_backend()`, même en simple utilisateur
sur des requêtes autres que les siennes :

```
postgres=# \c - u1
You are now connected to database "postgres" as user "u1".
postgres=> SELECT usename, pid FROM pg_stat_activity WHERE usename IS NOT NULL;
 usename |  pid
---------+-------
 u2      | 23194
 u1      | 23195
(2 rows)

postgres=> SELECT pg_terminate_backend(23194);
ERROR:  must be a member of the role whose process is being terminated or member
        of pg_signal_backend
postgres=> \c - postgres
You are now connected to database "postgres" as user "postgres".
postgres=# GRANT pg_signal_backend TO u1;
GRANT ROLE
postgres=# \c - u1
You are now connected to database "postgres" as user "u1".
postgres=> SELECT pg_terminate_backend(23194);
 pg_terminate_backend
----------------------
 t
(1 row)

postgres=> SELECT usename, pid FROM pg_stat_activity WHERE usename IS NOT NULL;
 usename |  pid
---------+-------
 u1      | 23212
(1 row)
```

Par contre, les connexions des superutilisateurs ne sont pas concernées.

En version 10, quatre nouveaux rôles sont ajoutées. `pg_read_all_stats` permet
de lire les tables de statistiques d'activité. `pg_read_all_settings` permet
de lire la configuration de tous les paramètres. `pg_stat_scan_tables` permet
d'exécuter les procédures stockées de lecture des statistiques. `pg_monitor`
est le rôle typique pour de la supervision. Il combine les trois rôles
précédents. Leur utilisation est identique à `pg_signal_backend`.
</div>



-----


### Héritage des droits

<div class="slide-content">

  * Créer un rôle sans droit de connexion
  * Donner les droits à ce rôle
  * Placer les utilisateurs concernés comme membre de ce rôle
</div>


<div class="notes">


Plutôt que d'avoir à donner les droits sur chaque objet à chaque ajout d'un
rôle, il est beaucoup plus simple d'utiliser le système d'héritage des
droits.

Supposons qu'une nouvelle personne arrive dans le service de facturation. Elle
doit avoir accès à toutes les tables concernant ce service. Sans utiliser l'
héritage, il faudra récupérer les droits d'une autre personne du service pour
retrouver la liste des droits à donner à cette nouvelle personne. De plus, si
un nouvel objet est créé et que chaque personne du service doit pouvoir y
accéder, il faudra ajouter l'objet et ajouter les droits pour chaque personne
du service sur cet objet. C'est long et sujet à erreur. Il est préférable de
créer un rôle facturation, de donner les droits sur ce rôle, puis d'ajouter
chaque rôle du service facturation comme membre du rôle facturation. L'ajout
et la suppression d'un objet est très simple : il suffit d'ajouter ou de
retirer le droit sur le rôle facturation, et cela impactera tous les rôles
membres.

Voici un exemple complet :

```
b1=# CREATE ROLE facturation;
CREATE ROLE
b1=# CREATE TABLE factures(id integer, dcreation date, libelle text,
montant numeric);
CREATE TABLE
b1=# GRANT ALL ON TABLE factures TO facturation;
GRANT
b1=# CREATE TABLE clients (id integer, nom text);
CREATE TABLE
b1=# GRANT ALL ON TABLE clients TO facturation;
GRANT
b1=# CREATE ROLE r1 LOGIN;
CREATE ROLE
b1=# GRANT facturation TO r1;
GRANT ROLE
b1=# \c b1 r1
You are now connected to database "b1" as user "r1".
b1=> SELECT * FROM factures;
 id | dcreation | libelle | montant
----+-----------+---------+---------
(0 rows)

b1=# CREATE ROLE r2 LOGIN;
CREATE ROLE
b1=# \c b1 r2
You are now connected to database "b1" as user "r2".
b1=> SELECT * FROM factures;
ERROR:  permission denied for relation factures
```


</div>



-----


### Changement de rôle

<div class="slide-content">

  * Rôle par défaut
    * celui de la connexion
  * Rôle emprunté :
    * après un `SET ROLE`
    * pour tout rôle dont il est membre
</div>


<div class="notes">


Par défaut, un utilisateur se connecte avec un rôle de connexion. Il obtient
les droits et la configuration spécifique de ce rôle. Dans le cas où il
hérite automatiquement des droits des rôles dont il est membre, il obtient
aussi ces droits qui s'ajoutent aux siens. Dans le cas où il n'hérite pas
automatiquement de ces droits, il peut temporairement les obtenir en utilisant
la commande `SET ROLE`. Il ne peut le faire qu'avec les rôles dont il est
membre.

```
b1=# CREATE ROLE r31 LOGIN;
CREATE ROLE
b1=# CREATE ROLE r32 LOGIN NOINHERIT IN ROLE r31;
CREATE ROLE
b1=# \c b1 r31
You are now connected to database "b1" as user "r31".
b1=> CREATE TABLE t1(id integer);
CREATE TABLE
b1=> INSERT INTO t1 VALUES (1), (2);
INSERT 0 2
b1=> \c b1 r32
You are now connected to database "b1" as user "r32".
b1=> SELECT * FROM t1;
ERROR:  permission denied for relation t1
b1=> SET ROLE TO r31;
SET
b1=> SELECT * FROM t1;
 id
----
  1
  2
(2 rows)

b1=> \c b1 postgres
You are now connected to database "b1" as user "postgres".
b1=# ALTER ROLE r32 INHERIT;
ALTER ROLE
b1=# \c b1 r32
You are now connected to database "b1" as user "r32".
b1=> SELECT * FROM t1;
 id
----
  1
  2
(2 rows)

b1=> \c b1 postgres
You are now connected to database "b1" as user "postgres".
b1=# REVOKE r31 FROM r32;
REVOKE ROLE
b1=# \c b1 r32
You are now connected to database "b1" as user "r32".
b1=> SELECT * FROM t1;
ERROR:  permission denied for relation t1
b1=> SET ROLE TO r31;
ERROR:  permission denied to set role "r31"
```

Le changement de rôle peut se faire uniquement au niveau de la
transaction. Pour cela, il faut utiliser la clause `LOCAL`. Il peut se faire
aussi sur la session, auquel cas il faut passer par la clause `SESSION`.
</div>


-----

### Row-Level Security

<div class="slide-content">
  * Politique de sécurité pour l'accès aux lignes d'une table
  * Deux possibilités pour l'instruction *CREATE POLICY*
    * *PERMISSIVE* : politiques d’une table reliées par des *OR*
    * *RESTRICTIVE* : politiques d’une table reliées par des *AND*
  * *PERMISSIVE* par défaut
</div>

<div class="notes">
Les tables peuvent avoir des politiques de sécurité pour l'accès aux lignes qui
restreignent, utilisateur par utilisateur, les lignes qui peuvent être renvoyées
par les requêtes d'extraction ou les commandes d'insertions, de mises à jour ou
de suppressions. Cette fonctionnalité est aussi connue sous le nom de `Row-Level
Security`.

Lorsque la protection des lignes est activée sur une table, tous les accès
classiques à la table pour sélectionner ou modifier des lignes doivent être
autorisés par une politique de sécurité.

Cependant, le propriétaire de la table n'est typiquement pas soumis aux
politiques de sécurité. Si aucune politique n'existe pour la table, une
politique de rejet est utilisé par défaut, ce qui signifie qu'aucune ligne n'est
visible ou ne peut être modifiée.

Par défaut, les politiques sont permissives, ce qui veut dire que quand
plusieurs politiques sont appliquées, elles sont combinées en utilisant
l'opérateur booléen *OR*. Depuis la version 10, il est possible de combiner des
politiques permissives avec des politiques restrictives (combinées en utilisant
l'opérateur booléen *AND*).

**Remarque** 

Afin de rendre l'exemple suivant plus lisible, le prompt psql a été modifié avec
la commande suivante :

```
\set PROMPT1 '%n@%/%R%# '
```

Il est possible de rendre ce changement permanent en ajoutant la commande
ci-dessus dans le fichier *~/.psqlrc*.

**Exemple**

Soit 2 utilisateurs :

```sql
postgres@postgres=# CREATE ROLE u1 WITH LOGIN;
CREATE ROLE

postgres@postgres=# CREATE ROLE u2 WITH LOGIN;
CREATE ROLE
```

Créons une table *comptes*, insérons-y des données et permettons aux
utilisateurs d'accéder à ces données :

```sql
u1@db1=> CREATE TABLE comptes (admin text, societe text, contact_email text);
CREATE TABLE

u1@db1=> INSERT INTO comptes VALUES ('u1', 'dalibo', 'u1@dalibo.com');
INSERT 0 1

u1@db1=> INSERT INTO comptes VALUES ('u2', 'dalibo', 'u2@dalibo.com');
INSERT 0 1

u1@db1=> INSERT INTO comptes VALUES ('u3', 'paris', 'u3@paris.fr');
INSERT 0 1

u1@db1=> GRANT SELECT ON comptes TO u2;
GRANT
```

Activons maintenant une première politique permissive :

```sql
u1@db1=> ALTER TABLE comptes ENABLE ROW LEVEL SECURITY;
ALTER TABLE

u1@db1=> CREATE POLICY compte_admins ON comptes
   USING (admin = current_user);
CREATE POLICY

u1@db1=> SELECT * FROM comptes;
 admin | societe | contact_email 
-------+---------+---------------
 u1    | dalibo  | u1@dalibo.com
 u2    | dalibo  | u2@dalibo.com
 u3    | paris   | u3@paris.fr
(3 rows)
```

Connectons-nous avec l'utilisateur *u2* et vérifions que la politique est bien
appliquée :

```sql
u1@db1=> \c db1 u2
You are now connected to database "db1" as user "u2".

u2@db1=> SELECT * FROM comptes;
 admin | societe | contact_email 
-------+---------+---------------
 u2    | dalibo  | u2@dalibo.com
(1 row)
```

Créons une autre politique de sécurité permissive :

```sql
u2@db1=> \c db1 u1
You are now connected to database "db1" as user "u1".

u1@db1=> CREATE POLICY pol_societe ON comptes USING (societe = 'paris');
CREATE POLICY
```

Vérifions maintenant comment elle s'applique à l'utilisateur *u2* :

```sql
u1@db1=> \c db1 u2
You are now connected to database "db1" as user "u2".

u2@db1=> SELECT * FROM comptes;
 admin | societe | contact_email 
-------+---------+---------------
 u2    | dalibo  | u2@dalibo.com
 u3    | paris   | u3@paris.fr
(2 rows)

```

*u1* étant propriétaire de cette table, les politiques ne s'appliquent pas à
lui, au contraire de *u2*.

Comme le montre ce plan d'exécution, les deux politiques permissives se
combinent bien en utilisant l'opérateur booléen *OR* :

```sql
u2@db1=> EXPLAIN(COSTS off) SELECT * FROM comptes;
                               QUERY PLAN
-------------------------------------------------------------------------
 Seq Scan on comptes
   Filter: ((societe = 'paris'::text) OR (admin = (CURRENT_USER)::text))
(2 rows)
```

Remplaçons maintenant l'une de ces politiques permissives par une politique
restrictive :

```sql
u2@db1=> \c db1 u1
You are now connected to database "db1" as user "u1".

u1@db1=> DROP POLICY compte_admins ON comptes;
DROP POLICY

u1@db1=> DROP POLICY pol_societe ON comptes;
DROP POLICY

u1@db1=> CREATE POLICY compte_admins ON comptes AS RESTRICTIVE
  USING (admin=current_user);
CREATE POLICY

u1@db1=> CREATE POLICY pol_societe ON comptes USING (societe = 'dalibo');
CREATE POLICY
```

Vérifions enfin avec l'utilisateur *u2* :
```sql
u1@db1=> \c db1 u2
You are now connected to database "db1" as user "u2".

u2@db1=> SELECT * FROM comptes;
 admin | societe | contact_email 
-------+---------+---------------
 u2    | dalibo  | u2@dalibo.com
(1 row)

u2@db1=> EXPLAIN(COSTS off) SELECT * FROM comptes;
                                QUERY PLAN
---------------------------------------------------------------------------
 Seq Scan on comptes
   Filter: ((societe = 'dalibo'::text) AND (admin = (CURRENT_USER)::text))
(2 rows)
```
Le plan d'exécution indique bien l'application de l'opérateur booléen *AND*.
</div>


-----

### Questions

<div class="slide-content">
N'hésitez pas, c'est le moment !
</div>


<div class="notes">

</div>

-----
