
## Travaux Dirigés 2

<div class="slide-content">
  * _MVCC_ et Vacuum
</div>

-----

#### Question de cours

<div class="slide-content">

  * Que signifie l'acronyme `ACID` ?
  * Combien existe-t-il de niveaux d'isolations dans PostgreSQL ?
  * Quels sont les inconvénients du MVCC ?

</div>

-----

<div class="slide-content">

### Énoncé

</div>

<div class="notes">

#### Limites dans PostgreSQL

« _block_size (integer): reports the size of a disk block. It is determined by
the value of `BLCKSZ` when building the server. The default value is 8192
bytes. The meaning of some configuration variables (such as shared_buffers) is
influenced by block_size._ »

![Heap file page](medias/heap_file_page.png)
\

« The `oid` type is currently implemented as an unsigned four-byte
integer. Therefore, it is not large enough to provide database-wide uniqueness
in large databases, or even in large individual tables. »

« _`ctid`: The physical location of the row version within its table. Note that
although the `ctid` can be used to locate the row version very quickly, a row's
`ctid` will change if it is updated or moved by `VACUUM FULL`. Therefore `ctid`
is useless as a long-term row identifier. A primary key should be used to
identify logical rows._ »

« _`tid`, or tuple identifier (row identifier). This is the data type of the
system column `ctid`. A tuple ID is a pair (block number, tuple index within
block) that identifies the physical location of the row within its table._ »

Définitions dans le fichier source `src/include/storage/block.h` :

```C
typedef uint32 BlockNumber;
#define InvalidBlockNumber		((BlockNumber) 0xFFFFFFFF)
#define MaxBlockNumber			((BlockNumber) 0xFFFFFFFE)
```

**Questions**

  * Quelle est la taille maximum d'une table sur disque ?
  * Quelle est le nombre maximal de lignes dans une table ?

#### Exploration des `ctid`

Soit la table t1 :

```sql
=# CREATE TABLE t1 (c1 int, c2 text);
CREATE TABLE
=# INSERT INTO t1 (c1, c2) SELECT i, md5(i::text) FROM generate_series(1,1000) i;
INSERT 0 1000
=# SELECT ctid, xmin, xmax, c1, c2 FROM t1;
  ctid   |  xmin   | xmax |  c1  |                c2
---------+---------+------+------+----------------------------------
 (0,1)   | 2760651 |    0 |    1 | c4ca4238a0b923820dcc509a6f75849b
 (0,2)   | 2760651 |    0 |    2 | c81e728d9d4c2f636f067f89cc14862c
 (0,3)   | 2760651 |    0 |    3 | eccbc87e4b5ce2fe28308fd9f2a7baf3
(...)
 (0,119) | 2760651 |    0 |  119 | 07e1cd7dca89a1678042477183b7ac3f
 (0,120) | 2760651 |    0 |  120 | da4fb5c6e93e74d3df8527599fa62642
 (1,1)   | 2760651 |    0 |  121 | 4c56ff4ce4aaf9573aa5dff913df997a
 (1,2)   | 2760651 |    0 |  122 | a0a080f42e6f13b3a2df133f073095dd
(...)
 (8,38)  | 2760651 |    0 |  998 | 9ab0d88431732957a618d4a469a0d4c3
 (8,39)  | 2760651 |    0 |  999 | b706835de79a2b4e80506f582af3676a
 (8,40)  | 2760651 |    0 | 1000 | a9b7ba70783b617e9998dc4dd82eb3c5

(1000 lignes)
```

**Questions**

  * Quel est le nombre de lignes stockées par pages ?
  * Quel est le nombre de pages utilisés pour stocker les données dans t1 ?


Et la table t2 :

```sql
=# CREATE TABLE t2 (c1 int);
CREATE TABLE
=# INSERT INTO t2 (c1) SELECT i FROM generate_series(1,1000) i;
INSERT 0 1000
=# SELECT ctid, xmin, xmax, c1 FROM t2;
   ctid   |  xmin   | xmax |  c1
----------+---------+------+-------
 (0,1)    | 2760654 |    0 |     1
 (0,2)    | 2760654 |    0 |     2
 (0,3)    | 2760654 |    0 |     3
 (0,4)    | 2760654 |    0 |     4
 (0,5)    | 2760654 |    0 |     5
 (0,6)    | 2760654 |    0 |     6
(...)
 (0,224)  | 2760654 |    0 |   224
 (0,225)  | 2760654 |    0 |   225
 (0,226)  | 2760654 |    0 |   226
 (1,1)    | 2760654 |    0 |   227
 (1,2)    | 2760654 |    0 |   228
 (1,3)    | 2760654 |    0 |   229
(...)
 (4,94)   | 2760657 |    0 |   998
 (4,95)   | 2760657 |    0 |   999
 (4,96)   | 2760657 |    0 |  1000

(1000 lignes)
```

**Questions**

  * Quel est le nombre de lignes stockées par pages dans t2 ?
  * Quel est le nombre de pages utilisés pour stocker les données dans t2 ?
  * Pourquoi la valeur maximum du deuxième champ du `ctid` (l'index du tuple)
    n'est pas le même pour les 2 tables ?
  * Pourquoi le nombre de pages utilisé est-il différent pour ces 2 tables ?
  * Sachant que les informations stockées dans le page header font 24 octets et
    qu'un item occupe 4 octets, estimer la taille d'un tuple pour ces 2 tables.

\newpage

#### Etude du MVCC

Session 1 :
```sql
BEGIN ISOLATION LEVEL REPEATABLE READ;
SELECT ctid, xmin, xmax, c1 FROM t2;
```

Session 2 :
```sql
UPDATE t2 SET c1=c1+1;
```

  * Que contient physiquement la table t2 ?

Session 1 :
```sql
TRUNCATE t2;
```

Session 2 :
```sql
SELECT ctid, xmin, xmax, c1 FROM t2;
```

  * Quel est le résulat de la requête dans la session 2 ?

Session 1 :
```sql
ROLLBACK;
VACUUM t2;
INSERT INTO t2 (c1) SELECT i FROM generate_series(1002,1500) i;
SELECT ctid, xmin, xmax, c1 FROM t2;
```

  * Que contient physiquement la table t2 ?

</div>

-----
