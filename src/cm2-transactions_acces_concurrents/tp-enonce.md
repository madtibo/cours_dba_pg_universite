
## Travaux Pratiques 2

<div class="slide-content">
  * _MVCC_, _VACUUM_ et verrous
</div>

<div class="notes">

**Rappel**

Durant ces travaux pratiques, nous allons utiliser la machine virtuelle du TP 1
pour héberger notre serveur de base de données PostgreSQL.

Effectuez les manipulations nécessaires pour réaliser les actions listées dans
la section _Énoncés_.

Vous pouvez vous aider du cours, du dernier TD, des précédents TP, ainsi que de
l'aide en ligne ou des pages de manuels (`man`).

-----

### Machine Virtuelle

Si vous ne l'avez pas encore fait, la machine virtuelle du TP 1 doit-être
déployée.  
Se référer à ce TP pour la déployer.

Pour lancer votre machine virtuelle, lancer la commande suivante dans votre
terminal :

```bash
vboxmanage startvm TP_bdd --type headless
```

A la fin de votre TP, n'oubliez pas de l'éteindre, elle restera sinon allumée et
accessible à quiconque se connecte sur la machine que vous avez utilisé. Vous
pouvez éteindre votre machine virtuelle avec la commande :

```bash
vboxmanage controlvm TP_bdd poweroff
```

Pour se connecter à votre machine virtuelle en ssh, utilisez la commande
suivante :

```bash
ssh -p 2222 tp@127.0.0.1
```

Les mots de passes sont les mêmes que les logins (_tp_ et _login_).

```bash
sudo -iu postgres
```

### Enoncés

#### Effets de MVCC

  * Créez une nouvelle base de données et s'y connecter.
  * Créez une nouvelle table t1 avec une colonne d'entier et une colonne de texte.
  * Ajoutez cinq lignes dans cette table.
  * Lisez la table.
  * Commencez une transaction et modifiez une ligne.
  * Lisez la table.
  * Que remarquez-vous ?
  * Ouvrez une autre session et lisez la table.
  * Qu'observez-vous ?
  * Lisez la table ainsi que les informations systèmes _xmin_ et _xmax_ pour
    les deux sessions.
  * Récupérez maintenant en plus le _ctid_.
  * Validez la transaction.
  * Installez l'extension _pageinspect_.
  * Décodez le bloc 0 de la table t1 à l'aide de cette extension.
  * Que remarquez-vous ?

#### Traiter la fragmentation

`VACUUM VERBOSE` :

  * Exécutez un `VACUUM VERBOSE` sur la table t1.
  * Des lignes ont-elles été nettoyées ?

Suppression en début de table :

  * Créez une table t2 avec une colonne de type integer.
  * Désactivez l'autovacuum pour cette table.
  * Insérez un million de lignes dans cette table.
  * Récupérez la taille de la table.
  * Supprimez les 500000 premières lignes.
  * Récupérez la taille de la table. Qu'en déduisez-vous ?
  * Exécutez un `VACUUM`.
  * Récupérez la taille de la table. Qu'en déduisez-vous ?
  * Exécutez un `VACUUM FULL`.
  * Récupérez la taille de la table. Qu'en déduisez-vous ?

Suppression en fin de table :

  * Créez une table t3 avec une colonne de type integer.
  * Désactivez l'autovacuum pour cette table.
  * Insérez un million de lignes dans cette table.
  * Récupérez la taille de la table.
  * Supprimez les 500000 dernières lignes.
  * Récupérez la taille de la table. Qu'en déduisez-vous ?
  * Exécutez un `VACUUM`.
  * Récupérez la taille de la table. Qu'en déduisez-vous ?

#### Détecter la fragmentation

  * Installez l'extension _pg_freespacemap_.
  * Créez une nouvelle table t4 avec une colonne d'entier et une colonne de texte.
  * Désactivez l'autovacuum pour cette table.
  * Insérer un million de lignes dans cette table.
  * Que rapporte _pg_freespacemap_ quant à l'espace libre de la table ?
  * Modifier les 200 000 premières lignes.
  * Que rapporte _pg_freespacemap_ quant à l'espace libre de la table ?
  * Exécutez un `VACUUM` sur la table.
  * Que rapporte _pg_freespacemap_ quant à l'espace libre de la table ? Qu'en
    déduisez-vous ?
  * Récupérez la taille de la table.
  * Exécutez un `VACUUM FULL` sur la table.
  * Récupérez la taille de la table et l'espace libre rapporté par
    pg_freespacemap. Qu'en déduisez-vous ?

#### Gestion de l'autovacuum

  * Créez une table t5 avec une colonne d'entier.
  * Insérer un million de lignes dans cette table.
  * Que contient la vue _pg_stat_user_tables_ pour cette table ?
  * Modifier les 200 000 premières lignes.
  * Attendez une minute.
  * Que contient la vue _pg_stat_user_tables_ pour cette table ?
  * Modifier 60 lignes supplémentaires de cette table.
  * Attendez une minute.
  * Que contient la vue pg_stat_user_tables pour cette table ? Qu'en
    déduisez-vous ?
  * Descendez le facteur d'échelle de cette table à 10 % pour l'autovacuum.
  * Modifiez les 200 000 lignes suivantes de cette table ?
  * Attendez une minute.
  * Que contient la vue _pg_stat_user_tables_ pour cette table ? Qu'en
    déduisez-vous ?

#### Verrous

  * Ouvrez une transaction et lisez la table t1.
  * Ouvrez une autre transaction, et tentez de supprimer la table t1.
  * Listez les processus du serveur PostgreSQL. Que remarquez-vous ?
  * Récupérez la liste des sessions en attente d'un verrou avec la vue
    _pg_stat_activity_.
  * Récupérez la liste des verrous en attente pour la requête bloquée.
  * Récupérez le nom de l'objet dont on n'arrive pas à récupérer le verrou.
  * Récupérez la liste des verrous sur cet objet. Quel processus a verrouillé
    la table t1 ?
  * Retrouvez les informations sur la session bloquante.

</div>

-----
