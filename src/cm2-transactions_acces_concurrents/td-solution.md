
<div class="notes">

### Solutions du TD 2

#### Question de cours

  * Que signifie l'acronyme `ACID` ?
    * Atomicité
    * Cohérence
    * Isolation
    * Durabilité

  * Combien existe-t-il de niveaux d'isolations dans PostgreSQL ?
    * `READ COMMITTED`.
    * `REPEATABLE READ`.
    * `SERIALIZABLE`

  * Quels sont les inconvénients du MVCC ?
    * Besoin de nettoyer les enregistrements grâce au `VACUUM`,
    * Les tables sont plus volumineuses car elles contiennent des
      enregistrements morts,
    * Pas de visibilité dans les index.

#### Limites dans PostgreSQL

  * Quelle est le nombre maximal de lignes dans une table ?

De façon théorique, on peut lister au maximum 65536 (`2^16`) éléments dans une
page (le numéro de ligne étant stocké sur 2 octets). Avec un maximum de `2^32`
pages, on arrive, de façon théorique, à plus de 280 mille milliards de lignes
dans une table.

Ce n'est pas le nombre de lignes qui va poser problème en premier.

  * Quelle est la taille maximum d'une table sur disque ?

Au point de vue logique, une table sur disque est découpée en page de 8 Ko. Un
numéro de bloc étant stocké sur 4 octets, la taille maximum sur disque d'une
table est de 32 To :  
`2^32 * 8 * 1024 = 32 * 1024^4`

Dans le cas où cette limite était atteinte, on pourra utiliser les tables
partitionnées pour permettre de stocker plus de données.

#### Exploration des `ctid`

  * Quel est le nombre de lignes stockées par pages ?

En observant les ctid, on observe que le _tuple index within block_ maximum
dans la table t1 est 120. Et 226 dans la table t2.

  * Quel est le nombre de pages utilisés pour stocker les données ?

En observant les ctid, on observe que les pages vont de 0 à 8 pour la table t1,
soit 9 pages. Et de 0 à 4 pour la table t2, soit 5 pages.

  * Pourquoi la valeur maximum du deuxième champ du `ctid` (l'index du tuple)
    n'est pas le même pour les 2 tables ?

Pour la table t1, un tuple est composé d'une colonne entier et d'une colonne
texte de 16 octets. Pour la table t2 un tuple est composé uniquement d'une
colonne entier.  
Un tuple occupe plus de place dans la table t1. Une page étant d'une taille
fixe de 8 Ko, on pourra stocker plus de tuple dans une page pour la table t2
que pour la table t1.

  * Pourquoi le nombre de pages utilisé est-il différent pour ces 2 tables ?

Pour un même nombre de tuple, la table t1 est plus volumineuse et utilise donc
plus de pages.

  * Sachant que les informations stockées dans le page header font 24 octets et
    qu'un item occupe 4 octets, estimer la taille d'un tuple pour ces 2 tables.

Table 1 :
```
24 + 4 × 120 + t × 120 = 8192 <=> t = 64
```

Table 2 :
```
24 + 4 × 226 + t × 226 = 8192 <=> t = 32
```

On peut vérifier ces valeurs en utilisant l'extension _pageinspect_ :

```sql
=# CREATE EXTENSION pageinspect ;
CREATE EXTENSION

=# SELECT lp_len FROM heap_page_items(get_raw_page('t1', 0)) LIMIT 1;
 lp_len 
--------
     61
(1 ligne)

=# SELECT lp_len FROM heap_page_items(get_raw_page('t2', 0)) LIMIT 1;
 lp_len 
--------
     28
(1 ligne)
```

La différence entre les 2 calculs tient au fait que notre calcul approximatif
ne tenait compte ni du padding, ni de l'espace disponible entre les blocs items
et les blocs tuples.

On constate avec ce calcul que le stockage d'une entier sur 4 octets implique
un overhead de 85%. le stockage de plus d'information vient réduire ce ratio.

#### Etude du MVCC

Jouer les commandes avec _psql_.

  * Que contient physiquement la table t2 ?

La table t2 contient toutes les lignes insérée initialement ainsi que les
nouvelles lignes issues de notre mise à jour. Le nombre de lignes stocké a donc
doublé. La table occupe 9 blocs.

  * Quel est le résulat de la requête dans la session 2 ?

La requête est en attente de la fin de la transaction dans la session 1. En
effet, un TRUNCATE prend un verrou de très haut niveau : ACCESS EXCLUSIVE. Ce
type de verrou bloque toute autre opération.

  * Que contient physiquement la table t2 ?

La table t2 contient toujours 9 blocs. Cependant, la première moitié des blocs
a été nettoyée par le VACUUM car les lignes n'étaient plus visibles. Cet espace
libéré a été réutilisé pour stocker les 500 nouvelles lignes. Il reste des
espaces réutilisables pour de nouvelles mises (insertion ou mises à jour).

</div>

-----
