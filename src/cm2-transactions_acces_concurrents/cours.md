# Transactions et accès concurrents

![PostgreSQL](medias/common-postgresql_logo.png)
\

<div class="notes">
</div>

-----

## Programme de ce cours

<div class="slide-content">

  * Semaine 1 : découverte de PostgreSQL
  * **Semaine 2** : transactions et accès concurrents
    * MultiVersion Concurrency Control
	* Les transactions dans PostgreSQL
	* Le fonctionnement du _VACUUM_
	* Gestion des verrous
  * Semaine 3 : missions du DBA
  * Semaine 4 : optimisation et indexation
  * Semaine 5 : _PL/PgSQL_ et triggers

</div>

<div class="notes">

</div>

-----

## MultiVersion Concurrency Control (MVCC)

<div class="slide-content">

  * Le « noyau » de PostgreSQL
  * Garantit ACID
  * Permet les écritures concurrentes sur la même table
</div>

<div class="notes">

MVCC (Multi Version Concurrency Control) est le mécanisme interne de PostgreSQL
utilisé pour garantir la cohérence des données lorsque plusieurs processus
accèdent simultanément à la même table.

C'est notamment MVCC qui permet de sauvegarder facilement une base _à chaud_ et
d'obtenir une sauvegarde cohérente alors même que plusieurs utilisateurs sont
potentiellement en train de modifier des données dans la base.

C'est la qualité de l'implémentation de ce système qui fait de PostgreSQL un
des meilleurs SGBD au monde : chaque transaction travaille dans son image de la
base, cohérent du début à la fin de ses opérations. Par ailleurs les écrivains
ne bloquent pas les lecteurs et les lecteurs ne bloquent pas les écrivains,
contrairement aux SGBD s'appuyant sur des verrous de lignes. Cela assure de
meilleures performances, un fonctionnement plus fluide des outils s'appuyant
sur PostgreSQL.

</div>


-----

### MVCC et les verrous

<div class="slide-content">

  * Une lecture ne bloque pas une écriture
  * Une écriture ne bloque pas une lecture
  * Une écriture ne bloque pas les autres écritures...
  * ...sauf pour la mise à jour de la **même ligne**.

</div>


<div class="notes">


MVCC maintient toutes les versions nécessaires de chaque tuple, ainsi
**chaque transaction voit une image figée de la base** (appelée _snapshot_).
Cette image correspond à l'état de la base lors du démarrage de la requête
ou de la transaction, suivant le niveau d'_isolation_ demandé par l'utilisateur
à PostgreSQL pour la transaction.

MVCC fluidifie les mises à jour en évitant les blocages trop contraignants
(verrous sur UPDATE) entre sessions et par conséquent de meilleures
performances en contexte transactionnel.

Voici un exemple concret :

```sql
# SELECT now();
              now
-------------------------------
 2022-01-23 16:28:13.679663+02
(1 row)

# BEGIN;
BEGIN
# SELECT now();
             now
------------------------------
 2022-01-23 16:28:34.888728+02
(1 row)

# SELECT pg_sleep(2);
 pg_sleep
----------

(1 row)

# SELECT now();
             now
------------------------------
 2022-01-23 16:28:34.888728+02
(1 row)
```


</div>



-----


### Transactions

<div class="slide-content">

  * Intimement liées à ACID et MVCC :
    * Une transaction est un ensemble d'opérations atomique
    * Le résultat d'une transaction est « tout ou rien »
  * mots clés `BEGIN`, `COMMIT` et `ROLLBACK`
  
</div>


<div class="notes">


Voici un exemple de transaction:

```sql
=> BEGIN;
BEGIN
=> CREATE TABLE capitaines (id serial, nom text, age integer);
CREATE TABLE
=> INSERT INTO capitaines VALUES (1, 'Haddock', 35);
INSERT 0 1
=> SELECT age FROM capitaines;
 age
-----
  35
(1 ligne)

=> ROLLBACK;
ROLLBACK
=> SELECT age FROM capitaines;
ERROR:  relation "capitaines" does not exist
LINE 1: SELECT age FROM capitaines;
                        ^
```

On voit que la table capitaine a existé **à l'intérieur** de la transaction.
Mais puisque cette transaction a été annulée (`ROLLBACK`), la table n'a pas été
créée au final. Cela montre aussi le support du DDL transactionnel au sein de
PostgreSQL.

```sql
=> BEGIN;
BEGIN
=> CREATE TABLE capitaines (id serial, nom text, age integer);
CREATE TABLE
=> INSERT INTO capitaines VALUES (1,'Haddock',35);
INSERT 0 1
=> COMMIT;
COMMIT
=> SELECT age FROM capitaines WHERE nom='Haddock';
 age
-----
  35
(1 row)
```

Grâce au mot clé `COMMIT`, la transaction a été validée. Aucune erreur n'ayant
eu lieu, la table est bien créée et l'enregistrement est visible.

```
=> BEGIN;
BEGIN
=> UPDATE capitaines SET age=42;
UPDATE 1
=> SELECT * FROM capitaines;
 id |   nom   | age 
----+---------+-----
  1 | Haddock |  42
(1 ligne)

=> DELETE * FROM capitaines;
ERROR:  syntax error at or near "*"
LIGNE 1 : DELETE * FROM capitaines;
                 ^
=> COMMIT;
ROLLBACK
=> SELECT * FROM capitaines;
 id |   nom   | age 
----+---------+-----
  1 | Haddock |  35
(1 ligne)
```

Dans ce dernier cas, une erreur a été détectée durant la transaction. La
réponse du serveur à la commande de validation, `COMMIT` est
`ROLLBACK`. L'ensemble des modifications effectuées lors de cette transaction
sont annulées.

</div>

-----

### Niveaux d'isolation

<div class="slide-content">

  * Chaque transaction (et donc session) est isolée à un certain point :
    * elle ne voit pas les opérations des autres
    * elle s'exécute indépendamment des autres
  * On peut spécifier le niveau d'isolation au démarrage d'une transaction:
    * `BEGIN ISOLATION LEVEL xxx;`

</div>

<div class="notes">

Chaque transaction, en plus d'être atomique, s'exécute séparément des
autres. Le niveau de séparation demandé sera un compromis entre le besoin
applicatif (pouvoir ignorer sans risque ce que font les autres transactions) et
les contraintes imposées au niveau de PostgreSQL (performances, risque d'échec
d'une transaction).

</div>

-----

### Niveaux d'isolation dans PostgreSQL

<div class="slide-content">

  * Niveaux d'isolation supportés
    * `READ COMMITED`
    * `REPEATABLE READ`
    * `SERIALIZABLE`

</div>


<div class="notes">


Le standard SQL spécifie quatre niveaux, mais PostgreSQL n'en supporte que
trois.

</div>

-----

### Niveau `READ UNCOMMITED`

<div class="slide-content">

  * Autorise la lecture de données modifiées mais non validées par d’autres
transactions
  * Aussi appelé `DIRTY READS` par d’autres moteurs
  * Pas de blocage entre les sessions
  * Inutile sous PostgreSQL en raison du MVCC
  * Si demandé, la transaction s’exécute en `READ COMMITTED`

</div>

<div class="notes">

Ce niveau d’isolation n’est nécessaire que pour les SGBD non-MVCC. Il est très
dangereux : on peut lire des données invalides, ou temporaires, puisqu’on lit
tous les enregistrements de la table, quel que soit leur état. Il est utilisé
dans certains cas où les performances sont cruciales, au détriment de la
justesse des données.

Sous PostgreSQL, ce mode est totalement inutile. Une transaction qui demande le
niveau d’isolation `READ UNCOMMITTED` s’exécute en fait en `READ COMMITTED`.

</div>

-----

### Niveau `READ COMMITTED`

<div class="slide-content">

  * La transaction ne lit que les données validées en base
  * Niveau d’isolation par défaut
  * Un ordre SQL s’exécute dans un instantané (les tables semblent figées sur
la durée de l’ordre)
  * L’ordre suivant s’exécute dans un instantané différent

</div>

<div class="notes">

Ce mode est le mode par défaut, et est suffisant dans de nombreux contextes.
PostgreSQL étant MVCC, les écrivains et les lecteurs ne se bloquent pas
mutuellement, et chaque ordre s’exécute sur un instantané de la base (ce n’est
pas un pré-requis de `READ COMMITTED` dans la norme SQL). On ne souffre plus des
lectures d’enregistrements non valides (`dirty reads`). On peut toutefois avoir
deux problèmes majeurs d’isolation dans ce mode :

  * Les lectures non-répétables (`non-repeatable reads`) : une transaction peut
ne pas voir les mêmes enregistrements d’une requête sur l’autre, si d’autres
transaction ont validé des modifications entre temps.
  * Les lectures fantômes (`phantom reads`) : des enregistrements peuvent ne
plus satisfaire une clause `WHERE` entre deux requêtes d’une même transaction.

</div>

-----

### Niveau `REPEATABLE READ`

<div class="slide-content">

  * Instantané au début de la transaction
  * Ne voit donc plus les modifications des autres transactions
  * Voit toujours ses propres modifications
  * Peut entrer en conflit avec d’autres transactions en cas de modification
des mêmes enregistrements

</div>

<div class="notes">

Ce mode, comme son nom l’indique, permet de ne plus avoir de lectures
non-répétables. Deux ordres SQL consécutifs dans la même transaction
retourneront les mêmes enregistrements, dans la même version. En lecture
seule, ces transactions ne peuvent pas échouer (elles sont entre autres
utilisées pour réaliser des exports des données, par pg_dump).

En écriture, par contre (ou `SELECT FOR UPDATE`, `FOR SHARE`), si une autre
transaction a modifié les enregistrements ciblés entre temps, une transaction
en `REPEATABLE READ` va échouer avec l’erreur suivante :
```
ERROR: could not serialize access due to concurrent update
```
Il faut donc que l’application soit capable de la rejouer au besoin.

Dans la norme, ce niveau d’isolation souffre toujours des lectures fantômes,
c’est-à-dire de lecture d’enregistrements qui ne satisfont plus la même clause
`WHERE` entre deux exécutions de requêtes.  
Cependant, PostgreSQL est plus strict que la norme et ne permet pas ces
lectures fantômes en `REPEATABLE READ`.

</div>

-----

### Niveau `SERIALIZABLE`

<div class="slide-content">

  * Niveau d’isolation maximum
  * Plus de lectures non répétables
  * Plus de lectures fantômes
  * Instantané au démarrage de la transaction
  * Verrouillage informatif des enregistrements consultés (verrouillage des
prédicats)
  * Erreurs de sérialisation en cas d’incompatibilité

</div>

<div class="notes">

Le niveau `SERIALIZABLE` permet de développer comme si chaque transaction se
déroulait seule sur la base. En cas d’incompatibilité entre les opérations
réalisées par plusieurs transactions, PostgreSQL annule celle qui déclenchera
le moins de perte de données. Tout comme dans le mode `REPEATABLE READ`, il est
essentiel de pouvoir rejouer une transaction si on développe en mode
`SERIALIZABLE`. Par contre, on simplifie énormément tous les autres points du
développement.

Ce mode empêche les erreurs dues à une transaction effectuant un `SELECT`
d’enregistrements, puis d’autres traitements, pendant qu’une autre transaction
modifie les enregistrements vus par le `SELECT` : il est probable que le
`SELECT` initial de notre transaction utilise les enregistrements récupérés, et
que le reste du traitement réalisé par notre transaction dépende de ces
enregistrements. Si ces enregistrements sont modifiés par une transaction
concurrente, notre transaction ne s’est plus déroulée comme si elle était
seule sur la base, et on a donc une violation de sérialisation.

</div>

-----

## L’implémentation MVCC de PostgreSQL

<div class="slide-content">

  * Colonnes `ctid`/`xmin`/`xmax`
  * Fichiers `clog`
  * Avantages/inconvénients
  * Opération `VACUUM`
  * `Wrap-Around`

</div>

<div class="notes">

</div>

-----

### `ctid`

<div class="slide-content">

  * Colonne masquée par défaut
  * Codée sur 6 octets
    * 4 octets pour la page
	* 2 octets pour la ligne
  * Fournit une adresse physique dans une table
  
</div>

<div class="notes">

La localisation physique de la version de ligne au sein de sa table. Bien que
le `ctid` puisse être utilisé pour trouver la version de ligne très rapidement,
le `ctid` d'une ligne change si la ligne est actualisée ou déplacée par un
`VACUUM FULL`. Le `ctid` est donc inutilisable comme identifiant de ligne sur
le long terme.

</div>

-----

### xmin et xmax (1/4)

<div class="slide-content">

Table initiale :

+------+------+------------+-------+
| xmin | xmax | Nom        | Solde |
+======+======+============+=======+
| 100  |      | M. Durand  | 1500  |
+------+------+------------+-------+
| 100  |      | Mme Martin | 2200  |
+------+------+------------+-------+

</div>

<div class="notes">

PostgreSQL stocke des informations de visibilité dans chaque version
d’enregistrement.

  * `xmin` : l’identifiant de la transaction créant cette version.
  * `xmax` : l’identifiant de la transaction invalidant cette version.

Ici, les deux enregistrements ont été créés par la transaction 100. Il
s’agit peut-être, par exemple, de la transaction ayant importé tous les soldes
à l’initialisation de la base.

</div>

-----

### xmin et xmax (2/4)

<div class="slide-content">

```sql
BEGIN;
UPDATE soldes SET solde=solde-200 WHERE nom = 'M. Durand';
```

+---------+---------+------------+-------+
| xmin    | xmax    | Nom        | Solde |
+=========+=========+============+=======+
| 100     | **150** | M. Durand  | 1500  |
+---------+---------+------------+-------+
| 100     |         | Mme Martin | 2200  |
+---------+---------+------------+-------+
| **150** |         | M. Durand  | 1300  |
+---------+---------+------------+-------+

</div>

<div class="notes">

On décide d’enregistrer un virement de 200 € du compte de M. Durand vers celui
de Mme Martin. Ceci doit être effectué dans une seule transaction : l’opération
doit être atomique, sans quoi de l’argent pourrait apparaître ou disparaître
de la table.

Nous allons donc tout d’abord démarrer une transaction (ordre `SQL BEGIN`).
PostgreSQL fournit donc à notre session un nouveau numéro de transaction (150
dans notre exemple).
Puis nous effectuerons :

```sql
UPDATE soldes SET solde=solde-200 WHERE nom = 'M. Durand';
```

</div>

-----

### xmin et xmax (3/4)

<div class="slide-content">

```sql
UPDATE soldes SET solde=solde+200 WHERE nom = 'Mme Martin';
```

+---------+---------+------------+-------+
| xmin    | xmax    | Nom        | Solde |
+=========+=========+============+=======+
| 100     | 150     | M. Durand  | 1500  |
+---------+---------+------------+-------+
| 100     | **150** | Mme Martin | 2200  |
+---------+---------+------------+-------+
| 150     |         | M. Durand  | 1300  |
+---------+---------+------------+-------+
| **150** |         | Mme Martin | 2400  |
+---------+---------+------------+-------+

</div>

<div class="notes">
Puis nous effectuerons :

```sql
UPDATE soldes SET solde=solde+200 WHERE nom = 'Mme Martin';
```

Nous avons maintenant deux versions de chaque enregistrement.

Notre session ne voit bien sûr plus que les nouvelles versions de ces
enregistrements, sauf si elle décidait d’annuler la transaction, auquel cas
elle reverrait les anciennes données.

Pour une autre session, la version visible de ces enregistrements dépend de
plusieurs critères :

  * La transaction 150 a-t-elle été validée ? Sinon elle est invisible
  * La transaction 150 est-elle _postérieure_ à la nôtre (numéro supérieur au
notre), et sommes-nous dans un niveau d’isolation (_serializable_) qui nous
interdit de voir les modifications faites depuis le début de notre transaction
?
  * La transaction 150 a-t-elle été validée après le démarrage de la requête
en cours ? Une requête, sous PostgreSQL, voit un instantané cohérent de la
base, ce qui implique que toute transaction validée après le démarrage de la
requête doit être ignorée.

Dans le cas le plus simple, 150 ayant été validée, une transaction 160 ne verra
pas les premières versions : `xmax` valant 150, ces enregistrements ne sont pas
visibles. Elle verra les secondes versions, puisque `xmin`=150, et pas de
`xmax`.

</div>

-----

### xmin et xmax (4/4)

<div class="slide-content">

+---------+---------+------------+-------+
| xmin    | xmax    | Nom        | Solde |
+=========+=========+============+=======+
| 100     | 150     | M. Durand  | 1500  |
+---------+---------+------------+-------+
| 100     | **150** | Mme Martin | 2200  |
+---------+---------+------------+-------+
| 150     |         | M. Durand  | 1300  |
+---------+---------+------------+-------+
| **150** |         | Mme Martin | 2400  |
+---------+---------+------------+-------+

  * Comment est effectuée la suppression d’un enregistrement ?
  * Comment est effectuée l’annulation de la transaction 150 ?

</div>

<div class="notes">

  * La suppression d’un enregistrement s’effectue simplement par l’écriture d’un
`xmax` dans la version courante.
  * Il n’y a rien à écrire dans les tables pour annuler une transaction. Il
suffit de marquer la transaction comme étant annulée dans la `CLOG`.

</div>

-----

### CLOG

<div class="slide-content">

  * La `CLOG` (Commit Log) enregistre l’état des transactions.
  * Chaque transaction occupe 2 bits de `CLOG`

</div>

<div class="notes">
La `CLOG` est stockée dans une série de fichiers de 256 ko, stockés dans le
répertoire `pg_xact` de `PGDATA` (répertoire racine de l’instance PostgreSQL).

Chaque transaction est créée dans ce fichier dès son démarrage et est
encodée sur deux bits puisqu’une transaction peut avoir quatre états.

  * `TRANSACTION_STATUS_IN_PROGRESS` : transaction en cours, c’est l’état
initial
  * `TRANSACTION_STATUS_COMMITTED` : la transaction a été validée
  * `TRANSACTION_STATUS_ABORTED` : la transaction a été annulée
  * `TRANSACTION_STATUS_SUB_COMMITTED` : ceci est utilisé dans le cas où la
transaction comporte des sous-transactions, afin de valider l’ensemble des
sous-transactions de façon atomique.

On a donc un million d’états de transactions par fichier de 256 ko.

Annuler une transaction (`ROLLBACK`) est quasiment instantané sous PostgreSQL :
il suffit d’écrire `TRANSACTION_STATUS_ABORTED` dans l’entrée de `CLOG`
correspondant à la transaction.

Toute modification dans la `CLOG`, comme toute modification d’un fichier de
données (table, index, séquence), est bien sûr enregistrée tout d’abord dans
les journaux de transactions (fichiers `WAL` dans le répertoire `pg_wal`).

</div>

-----

### Avantages du MVCC PostgreSQL

<div class="slide-content">

  * Avantages :
    * avantages classiques de MVCC (concurrence d’accès)
    * implémentation simple et performante
    * peu de sources de contention
    * verrouillage simple d’enregistrement
    * rollback instantané
    * données conservées aussi longtemps que nécessaire

</div>

<div class="notes">

  * Les lecteurs ne bloquent pas les écrivains, ni les écrivains les lecteurs.
  * Le code gérant les instantanés est simple, ce qui est excellent pour la
fiabilité, la maintenabilité et les performances.
  * Les différentes sessions ne se gênent pas pour l’accès à une ressource
commune (l’`UNDO`).
  * Un enregistrement est facilement identifiable comme étant verrouillé en
écriture : il suffit qu’il ait une version ayant un `xmax` correspondant à une
transaction en cours.
  * L’annulation est instantanée : il suffit d’écrire le nouvel état de la
transaction dans la `clog`. Pas besoin de restaurer les valeurs précédentes,
elles redeviennent automatiquement visibles.
  * Les anciennes versions restent en ligne aussi longtemps que nécessaire.
Elles ne pourront être effacées de la base qu’une fois qu’aucune transaction ne
les considérera comme visibles.

</div>

-----

### Inconvénients du MVCC PostgreSQL

<div class="slide-content">

  * Inconvénients :
    * Nettoyage des enregistrements (`VACUUM`)
    * Tables plus volumineuses
    * Pas de visibilité dans les index

</div>

<div class="notes">

Comme toute solution complexe, l’implémentation MVCC de PostgreSQL est un
compromis. Les avantages cités précédemment sont obtenus au prix de
concessions :

  * Il faut nettoyer les tables de leurs enregistrements morts. C’est le
travail de la commande `VACUUM`. On peut aussi voir ce point comme un avantage :
contrairement à la solution `UNDO`, ce travail de nettoyage n’est pas effectué
par le client faisant des mises à jour (et créant donc des enregistrements
morts). Le ressenti est donc meilleur.

  * Les tables sont forcément plus volumineuses que dans l’implémentation par
`UNDO`, pour deux raisons :
    * Les informations de visibilité qui y sont stockées. Il y a un surcoût
d’une douzaine d’octets par enregistrement.
    * Il y a toujours des enregistrements morts dans une table, une sorte de
_fond de roulement_, qui se stabilise quand l’application est en régime
stationnaire.
Ces enregistrements sont recyclés à chaque passage de `VACUUM`.

  * Les index n’ont pas d’information de visibilité. Il est donc nécessaire
d’aller vérifier dans la table associée que l’enregistrement trouvé dans
l’index est bien visible. Cela a un impact sur le temps d’exécution de requêtes
comme `SELECT count(*)` sur une table : dans le cas le plus défavorable, il est
nécessaire d’aller visiter tous les enregistrements pour s’assurer qu’ils sont
bien visibles. La _visibility map_ permet de limiter cette vérification aux
données les plus récentes.
  * Le `VACUUM` ne s'occupe pas de l'espace libéré par des colonnes supprimées
(fragmentation verticale).

</div>

-----

### Fonctionnement de VACUUM (1/3)

![Algorithme du vacuum 1/3](medias/c2-vacuum_1.png)

<div class="notes">
Le traitement `VACUUM` se déroule en trois passes. Cette première passe
parcourt la table à nettoyer, à la recherche d’enregistrements morts.
Un enregistrement est mort s’il possède un `xmax` qui correspond à une
transaction validée, et que cet enregistrement n’est plus visible dans
l’instantané d’aucune transaction en cours sur la base.

L’enregistrement mort ne peut pas être supprimé immédiatement : des
enregistrements d’index pointent vers lui et doivent aussi être nettoyés. Les
adresses (`tid` ou `tuple id`) des enregistrements sont donc mémorisés par la
session effectuant le vacuum, dans un espace mémoire dont la taille est à
hauteur de `maintenance_work_mem`. Si `maintenance_work_mem` est trop petit pour
contenir tous les enregistrements morts en une seule passe, vacuum effectue
plusieurs séries de ces trois passes.

Un `tid` est composé du numéro de bloc et du numéro d’enregistrement dans le
bloc.

</div>

-----

### Fonctionnement de VACUUM (2/3)

![Algorithme du vacuum 2/3](medias/c2-vacuum_2.png)

<div class="notes">

La seconde passe se charge de nettoyer les entrées d’index. Vacuum possède une
liste de `tid` à invalider. Il parcourt donc tous les index de la table à la
recherche de ces tid et les supprime. En effet, les index sont triés afin de
mettre en correspondance une valeur de clé (la colonne indexée par exemple)
avec un `tid`. Il n’est par contre pas possible de trouver un `tid`
directement. Les pages entièrement vides sont supprimées de l’arbre et stockées
dans la liste des pages réutilisables, la **Free Space Map** (`FSM`).

</div>

-----

### Fonctionnement de VACUUM (3/3)

![Algorithme du vacuum 3/3](medias/c2-vacuum_3.png)

<div class="notes">

Maintenant qu’il n’y a plus d’entrée d’index pointant sur les enregistrements
identifiés, nous pouvons supprimer les enregistrements de la table elle-même.
C’est le rôle de cette passe, qui quant à elle, peut accéder directement aux
enregistrements. Quand un enregistrement est supprimé d’un bloc, ce bloc est
réorganisé afin de consolider l’espace libre, et cet espace libre est
consolidé dans la **Free Space Map** (`FSM`).

Une fois cette passe terminée, si le parcours de la table n’a pas été
terminé lors de la passe 1 (la `maintenance_work_mem` était pleine), le
travail reprend où il en était du parcours de la table.

</div>

-----

### Le problème du Wraparound

<div class="slide-content">

_Wraparound_ : bouclage d’un compteur

  * Le compteur de transactions : 32 bits
  * 4 milliards de transactions
  * Qu’arrive-t-il si on boucle ?
  * Quelles protections ?

</div>

<div class="notes">

Le compteur de transactions de PostgreSQL est stocké sur 32 bits. Il peut donc,
en théorie, y avoir un dépassement de ce compteur au bout de 4 milliards de
transactions. En fait, le compteur est cyclique, et toute transaction considère
que les 2 milliards de transactions supérieures à la sienne sont dans le
futur, et les 2 milliards inférieures dans le passé. Le risque de bouclage est
donc plus proche des 2 milliards.

En théorie, si on bouclait, de nombreux enregistrements deviendraient
invisibles, car validés par des transactions futures. Heureusement PostgreSQL
l’empêche. Au fil des versions, la protection est devenue plus efficace.

  * Le moteur trace la plus vieille transaction d’une base, et refuse toute
nouvelle transaction à partir du moment où le stock de transaction disponible
est à 10 millions. Il suffit de lancer un `VACUUM` sur la base incriminée à ce
point, qui débloquera la situation, en nettoyant les plus anciens `xmin`.
  * Depuis l’arrivée d’`AUTOVACUUM`, celui-ci déclenche automatiquement un
`VACUUM` quand le _Wraparound_ se rapproche trop. Ceci se produit même si
`AUTOVACUUM` est désactivé.

Si vous voulez en savoir plus,
[la documentation officielle](https://docs.postgresql.fr/current/maintenance.html)
contient un paragraphe sur ce sujet.

Ce qu’il convient d’en retenir, c’est que le système empêchera le wraparound en
se bloquant. En cas de blocage, une protection contre ce phénomène se déclenche
automatiquement en déclenchant un `VACUUM`, quel que soit le paramétrage
d’`AUTOVACUUM`.

</div>

-----

## commande `VACUUM`

<div class="slide-content">

  * Lancement manuel du `VACUUM`
  * option `FULL`
    * défragmente la table
    * verrou exclusif
  * option `ANALYZE`
    * met en plus à jour les statistiques

</div>

<div class="notes">

La commande `VACUUM` permet de récupérer l'espace utilisé par les lignes non
visibles afin d'éviter un accroissement continuel du volume occupé sur le
disque.

Cependant, un `VACUUM` fait rarement gagner de l'espace disque. Il faut
utiliser l'option `FULL` pour cela. La commande `VACUUM FULL` libère l'espace
consommé par les lignes périmées ou les colonnes supprimées, et le rend au
système d'exploitation.

Cette variante de la commande `VACUUM` acquiert un verrou exclusif sur chaque
table. Elle peut donc avoir un effet extrêmement négatif sur les performances
de la base de données.

Quand faut-il utiliser `VACUUM` ?

  * pour des nettoyages réguliers ;
  * il s'agit d'une maintenance de base.

Quand faut-il utiliser `VACUUM FULL` ?

  * après des suppressions massives de données ;
  * lorsque la base n'est pas en production ;
  * il s'agit d'une maintenance exceptionnelle.

Des `VACUUM` standards et une fréquence modérée sont une meilleure approche
que des `VACUUM FULL`, même non fréquents, pour maintenir des tables mises à
jour fréquemment.

`VACUUM FULL` est recommandé dans les cas où vous savez que vous avez
supprimé ou modifié une grande partie des lignes d'une table, de façon à ce
que la taille de la table soit réduite de façon conséquente.  
Un `REINDEX` est exécuté lors d'un `VACUUM FULL`.

La commande `vacuumdb` permet d'exécuter facilement un `VACUUM` sur une ou
toutes les bases, elle permet également la parallélisation de `VACUUM` sur
plusieurs tables.

</div>

-----

## Verrouillage et MVCC

<div class="slide-content">

La gestion des verrous est liée à l’implémentation de MVCC.

  * Verrouillage d’objets en mémoire
  * Verrouillage d’objets sur disque
  * Paramètres

</div>

<div class="notes">

</div>

-----

### Le gestionnaire de verrous

<div class="slide-content">

PostgreSQL possède un gestionnaire de verrous

  * Verrous d’objet
  * Niveaux de verrouillage
  * Deadlock
  * Vue pg_locks

</div>


<div class="notes">

PostgreSQL dispose d’un gestionnaire de verrous, comme tout SGBD.

Ce gestionnaire de verrous est capable de gérer des verrous sur des tables, sur
des enregistrements, sur des ressources virtuelles. De nombreux types de verrous
- 8 - sont disponibles, chacun entrant en conflit avec d’autres.

Chaque opération doit tout d’abord prendre un verrou sur les objets à
manipuler.

Les noms des verrous peuvent prêter à confusion : `ROW SHARE` par exemple est
un verrou de table, pas un verrou d’enregistrement. Il signifie qu’on a pris un
verrou sur une table pour y faire des `SELECT FOR UPDATE` par exemple. Ce verrou
est en conflit avec les verrous pris pour un `DROP TABLE`, ou pour un
`LOCK TABLE`.

Le gestionnaire de verrous détecte tout verrou mortel (`deadlock`) entre
deux sessions. Un `deadlock` est la suite de prise de verrous entraînant le
blocage mutuel d’au moins deux sessions, chacune étant en attente d’un des
verrous acquis par l’autre.

On peut accéder aux verrous actuellement utilisés sur un cluster par la vue
`pg_locks`.

</div>

-----

### Verrous sur enregistrement

<div class="slide-content">

  * Le gestionnaire de verrous possède des verrous sur enregistrements.
  * Ils sont :
    + transitoires
    + pas utilisés pour prendre les verrous définitifs
  * Utilisation de verrous sur disque.
  * Pas de risque de pénurie de verrous.

</div>

<div class="notes">
Le gestionnaire de verrous fournit des verrous sur enregistrement. Ceux-ci sont
utilisés pour verrouiller un enregistrement le temps d’y écrire un `xmax`, puis
libérés immédiatement.

Le verrouillage réel est implémenté comme suit :

  * Chaque transaction verrouille son objet « identifiant de transaction » de
façon exclusive.
  * Une transaction voulant mettre à jour un enregistrement consulte le `xmax`.
S’il constate que ce `xmax` est celui d’une transaction en cours, il demande un
verrou exclusif sur l’objet « identifiant de transaction » de cette transaction.
Qui ne lui est naturellement pas accordé. Il est donc placé en attente.
  * Quand la transaction possédant le verrou se termine (`COMMIT` ou
`ROLLBACK`), son verrou sur l’objet « identifiant de transaction » est libéré,
débloquant ainsi l’autre transaction, qui peut reprendre son travail.

Ce mécanisme ne nécessite pas un nombre de verrous mémoire proportionnel au
nombre d’enregistrements à verrouiller, et simplifie le travail du gestionnaire
de verrous, celui-ci ayant un nombre bien plus faible de verrous à gérer.

Le mécanisme exposé ici est légèrement simplifié. Pour une explication
approfondie, n’hésitez pas à consulter
[l’article suivant](https://kb.dalibo.com/verrouillage)
issu de la base de connaissance Dalibo.

</div>

-----

## Conclusion

<div class="slide-content">

  * PostgreSQL dispose d’une implémentation MVCC complète, permettant :
    + Que les lecteurs ne bloquent pas les écrivains
    + Que les écrivains ne bloquent pas les lecteurs
    + Que les verrous en mémoire soient d’un nombre limité

  * Cela impose par contre une mécanique un peu complexe, dont les parties
visibles sont la commande `VACUUM` et le processus d’arrière-plan Autovacuum.

</div>

<div class="notes">

</div>

-----
