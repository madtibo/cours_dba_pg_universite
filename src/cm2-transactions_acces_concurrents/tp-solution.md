
<div class="notes">

### Solutions du TP 2

#### Effets de MVCC

  * Créez une nouvelle base de données et s'y connecter.

```sql
postgres=# CREATE DATABASE mvcc;
CREATE DATABASE
postgres=# \c mvcc
```

  * Créez une nouvelle table t1 avec une colonne d'entier et une colonne de texte.

```sql
mvcc=# CREATE TABLE t1 (c1 integer, c2 text);
CREATE TABLE
```

  * Ajoutez cinq lignes dans cette table.


```sql
mvcc=# INSERT INTO t1 VALUES
  (1, 'un'), (2, 'deux'), (3, 'trois'), (4, 'quatre'), (5, 'cinq');
INSERT 0 5
```

  * Lisez la table.


```sql
mvcc=# SELECT * FROM t1;
 c1 |   c2
----+--------
  1 | un
  2 | deux
  3 | trois
  4 | quatre
  5 | cinq
(5 rows)
```

  * Commencez une transaction et modifiez une ligne.


```sql
mvcc=# BEGIN;
BEGIN
mvcc=# UPDATE t1 SET c2=upper(c2) WHERE c1=3;
UPDATE 1
```

  * Lisez la table.


```sql
mvcc=# SELECT * FROM t1;
 c1 |   c2
----+--------
  1 | un
  2 | deux
  4 | quatre
  5 | cinq
  3 | TROIS
(5 rows)
```

  * Que remarquez-vous ?

La ligne mise à jour n'apparaît plus, ce qui est normal. Elle apparaît en fin
de table. En effet, quand un UPDATE est exécuté, la ligne courante est
considérée comme morte et une nouvelle ligne est ajoutée, avec les valeurs
modifiées. Comme nous n'avons pas demandé de récupérer les résultats dans un
certain ordre, les lignes sont affichées dans leur ordre de stockage dans les
blocs de la table.

  * Ouvrez une autre session et lisez la table.


```sql
mvcc=# SELECT * FROM t1;
 c1 |   c2
----+--------
  1 | un
  2 | deux
  3 | trois
  4 | quatre
  5 | cinq
(5 rows)
```

  * Qu'observez-vous ?

Les autres sessions voient toujours l'ancienne version de ligne, tant que la
transaction n'a pas été validée. Et du coup, l'ordre des lignes en retour n'est
pas le même vu que cette version de ligne était introduite avant.

  * Lisez la table ainsi que les informations systèmes _xmin_ et _xmax_ pour
    les deux sessions.

Voici ce que renvoie la session qui a fait la modification ::

```sql
mvcc=# SELECT xmin, xmax, * FROM t1;
 xmin | xmax | c1 |   c2
------+------+----+--------
 1930 |    0 |  1 | un
 1930 |    0 |  2 | deux
 1930 |    0 |  4 | quatre
 1930 |    0 |  5 | cinq
 1931 |    0 |  3 | TROIS
(5 rows)
```

Et voici ce que renvoie l'autre session :

```sql
mvcc=# SELECT xmin, xmax, * FROM t1;
 xmin | xmax | c1 |   c2
------+------+----+--------
 1930 |    0 |  1 | un
 1930 |    0 |  2 | deux
 1930 | 1931 |  3 | trois
 1930 |    0 |  4 | quatre
 1930 |    0 |  5 | cinq
(5 rows)
```

La transaction 1931 est celle qui a réalisé la modification. La colonne xmin de
la nouvelle version de ligne contient ce numéro. De même pour la colonne xmax
de l'ancienne version de ligne. PostgreSQL se base sur cette information pour
savoir si telle transaction peut lire telle ou telle ligne.

  * Récupérez maintenant en plus le _ctid_.

Voici ce que renvoie la session qui a fait la modification :

```sql
mvcc=# SELECT ctid, xmin, xmax, * FROM t1;
 ctid  | xmin | xmax | c1 |   c2
-------+------+------+----+--------
 (0,1) | 1930 |    0 |  1 | un
 (0,2) | 1930 |    0 |  2 | deux
 (0,4) | 1930 |    0 |  4 | quatre
 (0,5) | 1930 |    0 |  5 | cinq
 (0,6) | 1931 |    0 |  3 | TROIS
(5 rows)
```

Et voici ce que renvoie l'autre session :

```sql
mvcc=# SELECT ctid, xmin, xmax, * FROM t1;
 ctid  | xmin | xmax | c1 |   c2
-------+------+------+----+--------
 (0,1) | 1930 |    0 |  1 | un
 (0,2) | 1930 |    0 |  2 | deux
 (0,3) | 1930 | 1931 |  3 | trois
 (0,4) | 1930 |    0 |  4 | quatre
 (0,5) | 1930 |    0 |  5 | cinq
(5 rows)
```

La colonne ctid contient une paire d'entiers. Le premier indique le numéro de bloc, le second le numéro de l'enregistrement dans le bloc. Autrement, elle précise la position de l'enregistrement sur le fichier de la table.

En récupérant cette colonne, on voit bien que la première session voit la nouvelle position (enregistrement 6 du bloc 0) et que la deuxième session voit l'ancienne (enregistrement 3 du bloc 0).

  * Validez la transaction.

```sql
mvcc=# COMMIT;
COMMIT
```

  * Installez l'extension _pageinspect_.

```sql
mvcc=# CREATE EXTENSION pageinspect;
CREATE EXTENSION
```

  * Décodez le bloc 0 de la table t1 à l'aide de cette extension.

```sql
mvcc=# SELECT * FROM heap_page_items(get_raw_page('t1',0));
 lp | lp_off | lp_flags | lp_len | t_xmin | t_xmax | t_field3 | t_ctid | 
----+--------+----------+--------+--------+--------+----------+--------+-
  1 |   8160 |        1 |     31 |   2169 |      0 |        0 | (0,1)  |
  2 |   8120 |        1 |     33 |   2169 |      0 |        0 | (0,2)  |
  3 |   8080 |        1 |     34 |   2169 |   2170 |        0 | (0,6)  |
  4 |   8040 |        1 |     35 |   2169 |      0 |        0 | (0,4)  |
  5 |   8000 |        1 |     33 |   2169 |      0 |        0 | (0,5)  |
  6 |   7960 |        1 |     34 |   2170 |      0 |        0 | (0,6)  |
	
 lp | t_infomask2 | t_infomask | t_hoff | t_bits | t_oid
----+-------------+------------+--------+--------+-------
  1 |           2 |       2306 |     24 |        |
  2 |           2 |       2306 |     24 |        |
  3 |       16386 |        258 |     24 |        |
  4 |           2 |       2306 |     24 |        |
  5 |           2 |       2306 |     24 |        |
  6 |       32770 |      10242 |     24 |        |
(6 rows)
```

  * Que remarquez-vous ?
    * les six lignes sont bien présentes ;
    * le t_ctid ne contient plus (0,3) mais l'adresse de la nouvelle ligne (ie,
      (0,6] ;
    * _t_infomask2_ est un champ de bits, la valeur 16386 pour l'ancienne
      version nous indique que le changement a eu lieu en utilisant la
      technologie HOT.

#### Traiter la fragmentation

  * Exécutez un `VACUUM VERBOSE` sur la table t1.


```sql
mvcc=# VACUUM VERBOSE t1;
INFO:  vacuuming "public.t1"
INFO:  "t1": found 1 removable, 5 nonremovable row versions in 1 out of 1 pages
DÉTAIL : 0 dead row versions cannot be removed yet, oldest xmin: 2760700
There were 0 unused item pointers.
Skipped 0 pages due to buffer pins, 0 frozen pages.
0 pages are entirely empty.
CPU: user: 0.00 s, system: 0.00 s, elapsed: 0.00 s.
INFO:  vacuuming "pg_toast.pg_toast_18739"
INFO:  index "pg_toast_18739_index" now contains 0 row versions in 1 pages
DÉTAIL : 0 index row versions were removed.
0 index pages have been deleted, 0 are currently reusable.
CPU: user: 0.00 s, system: 0.00 s, elapsed: 0.00 s.
INFO:  "pg_toast_18739": found 0 removable, 0 nonremovable row versions in 0 out of 0 pages
DÉTAIL : 0 dead row versions cannot be removed yet, oldest xmin: 2760700
There were 0 unused item pointers.
Skipped 0 pages due to buffer pins, 0 frozen pages.
0 pages are entirely empty.
CPU: user: 0.00 s, system: 0.00 s, elapsed: 0.00 s.
VACUUM

```

  * Des lignes ont-elles été nettoyées ?

```sql
INFO:  "t1": found 1 removable, 5 nonremovable row versions in 1 out of 1 pages
```

Il y a une ligne obsolète (et récupérable) et cinq lignes vivantes sur le seul
bloc de la table.

  * Créez une table t2 avec une colonne de type integer.

```sql
mvcc=# CREATE TABLE t2(c1 integer);
CREATE TABLE
```

  * Désactivez l'autovacuum pour cette table.

```sql
mvcc=# ALTER TABLE t2 SET (autovacuum_enabled=false);
ALTER TABLE
```

  * Insérez un million de lignes dans cette table.

```sql
mvcc=# INSERT INTO t2 SELECT generate_series(1, 1000000);
INSERT 0 1000000
```

  * Récupérez la taille de la table.

```sql
mvcc=# SELECT pg_size_pretty(pg_table_size('t2'));
 pg_size_pretty
----------------
 35 MB
(1 row)
```

  * Supprimez les 500000 premières lignes.

```sql
mvcc=# DELETE FROM t2 WHERE id<500000;
DELETE 499999
```

  * Récupérez la taille de la table. Qu'en déduisez-vous ?

```sql
mvcc=# SELECT pg_size_pretty(pg_table_size('t2'));
 pg_size_pretty
----------------
 35 MB
(1 row)
```

Un DELETE ne permet pas de regagner de la place sur le disque. Les lignes
supprimées sont uniquement marquées comme étant mortes.

  * Exécutez un `VACUUM`.

```sql
mvcc=# VACUUM t2;
VACUUM
```

  * Récupérez la taille de la table. Qu'en déduisez-vous ?

```sql
mvcc=# SELECT pg_size_pretty(pg_table_size('t2'));
 pg_size_pretty
----------------
 35 MB
(1 row)
```

  * Exécutez un `VACUUM FULL`.

```sql
mvcc=# VACUUM FULL t2;
VACUUM
```

  * Récupérez la taille de la table. Qu'en déduisez-vous ?

```sql
mvcc=# SELECT pg_size_pretty(pg_table_size('t2'));
 pg_size_pretty
----------------
 17 MB
(1 row)
```

Dans le cas du `VACUUM FULL`, on gagne en place disque. L'opération défragmente
la table et permet, on de récupérer les espaces morts.

  * Créez une table t3 avec une colonne de type integer.

```sql
mvcc=# CREATE TABLE t3(id integer);
CREATE TABLE
```

  * Désactivez l'autovacuum pour cette table.

```sql
mvcc=# ALTER TABLE t3 SET (autovacuum_enabled=false);
ALTER TABLE
```

  * Insérez un million de lignes dans cette table.

```sql
mvcc=# INSERT INTO t3 SELECT generate_series(1, 1000000);
INSERT 0 1000000
```

  * Récupérez la taille de la table.

```sql
mvcc=# SELECT pg_size_pretty(pg_table_size('t3'));
 pg_size_pretty
----------------
 35 MB
(1 row)
```

  * Supprimez les 500000 dernières lignes.

```sql
mvcc=# DELETE FROM t3 WHERE id>500000;
DELETE 500000
```

  * Récupérez la taille de la table. Qu'en déduisez-vous ?

```sql
mvcc=# SELECT pg_size_pretty(pg_table_size('t3'));
 pg_size_pretty
----------------
 35 MB
(1 row)
```

Un DELETE ne permet pas de regagner de la place sur le disque. Les lignes
supprimées sont uniquement marquées comme étant mortes.

  * Exécutez un `VACUUM`.

```sql
mvcc=# VACUUM t3;
VACUUM
```

  * Récupérez la taille de la table. Qu'en déduisez-vous ?

```sql
mvcc=# SELECT pg_size_pretty(pg_table_size('t3'));
 pg_size_pretty
----------------
 17 MB
(1 row)
```

En fait, il existe un cas où on peut gagner de l'espace disque suite à un
`VACUUM` simple : quand l'espace récupéré se trouve en fin de table et qu'il
est possible de prendre rapidement un verrou exclusif sur la table pour la
tronquer. C'est assez peu fréquent mais c'est une optimisation intéressante.

#### Détecter la fragmentation

  * Installez l'extension _pg_freespacemap_.

```sql
mvcc=# CREATE EXTENSION pg_freespacemap;
CREATE EXTENSION
```

  * Créez une nouvelle table t4 avec une colonne d'entier et une colonne de texte.

```sql
mvcc=# CREATE TABLE t4 (c1 integer, c2 text);
CREATE TABLE
```

  * Désactivez l'autovacuum pour cette table.

```sql
mvcc=# ALTER TABLE t4 SET (autovacuum_enabled=false);
ALTER TABLE
```

  * Insérer un million de lignes dans cette table.

```sql
mvcc=# INSERT INTO t4 SELECT i, 'Ligne '||i FROM generate_series(1, 1000000) AS i;
INSERT 0 1000000
```

  * Que rapporte _pg_freespacemap_ quant à l'espace libre de la table ?

```sql
mvcc=# SELECT sum(avail) FROM pg_freespace('t4'::regclass);
 sum
-----
   0
(1 row)
```

  * Modifier les 200 000 premières lignes.

```sql
mvcc=# UPDATE t4 SET c2=upper(c2) WHERE c1<200000;
UPDATE 199999
```

  * Que rapporte _pg_freespacemap_ quant à l'espace libre de la table ?

```sql
mvcc=# SELECT sum(avail) FROM pg_freespace('t4'::regclass);
 sum
-----
  32
(1 row)
```

  * Exécutez un `VACUUM` sur la table.

```sql
mvcc=# VACUUM t4;
VACUUM
```

  * Que rapporte _pg_freespacemap_ quant à l'espace libre de la table ? Qu'en
    déduisez-vous ?

```sql
mvcc=# SELECT sum(avail) FROM pg_freespace('t4'::regclass);
   sum
---------
 8806784
(1 row)
```

Il faut exécuter un VACUUM pour que PostgreSQL renseigne la structure FSM, ce
qui nous permet de connaître le taux de fragmentation de la table.

  * Récupérez la taille de la table.

```sql
mvcc=# SELECT pg_size_pretty(pg_table_size('t4'));
 pg_size_pretty
----------------
 58 MB
(1 row)
```

  * Exécutez un `VACUUM FULL` sur la table.

```sql
mvcc=# VACUUM FULL t4;
VACUUM
```

  * Récupérez la taille de la table et l'espace libre rapporté par
    pg_freespacemap. Qu'en déduisez-vous ?

```sql
mvcc=# SELECT sum(avail) FROM pg_freespace('t4'::regclass);
 sum
-----
   0
(1 row)
```

```sql
mvcc=# SELECT pg_size_pretty(pg_table_size('t4'));
 pg_size_pretty
----------------
 49 MB
(1 row)
```

`VACUUM FULL` a supprimé les espaces morts, ce qui nous a fait gagner entre 8
et 9 Mo. La taille de la table maintenant correspond bien à celle de l'ancienne
table, moins la place prise par les lignes mortes.

#### Gestion de l'autovacuum

  * Créez une table t5 avec une colonne d'entier.

```sql
mvcc=# CREATE TABLE t5 (id integer);
CREATE TABLE
```

  * Insérer un million de lignes dans cette table.

```sql
mvcc=# INSERT INTO t5 SELECT generate_series(1, 1000000);
INSERT 0 1000000
```

  * Que contient la vue _pg_stat_user_tables_ pour cette table ?

```sql
mvcc=# \x
Expanded display is on.
mvcc=# SELECT * FROM pg_stat_user_tables WHERE relname='t5';
-[ RECORD 1 ]-------+--------
relid               | 18745
schemaname          | public
relname             | t5
seq_scan            | 0
seq_tup_read        | 0
idx_scan            | 
idx_tup_fetch       | 
n_tup_ins           | 1000000
n_tup_upd           | 0
n_tup_del           | 0
n_tup_hot_upd       | 0
n_live_tup          | 1000000
n_dead_tup          | 0
n_mod_since_analyze | 1000000
last_vacuum         | 
last_autovacuum     | 
last_analyze        | 
last_autoanalyze    | 
vacuum_count        | 0
autovacuum_count    | 0
analyze_count       | 0
autoanalyze_count   | 0
```

  * Modifier les 200 000 premières lignes.

```sql
mvcc=# UPDATE t5 SET id=2000000 WHERE id<200001;
UPDATE 200000
```

  * Attendez une minute.

```sql
mvcc=# SELECT pg_sleep(60);
```

  * Que contient la vue _pg_stat_user__tables pour cette table ?

```sql
mvcc=# SELECT * FROM pg_stat_user_tables WHERE relname='t5';
-[ RECORD 1 ]-------+------------------------------
relid               | 18745
schemaname          | public
relname             | t5
seq_scan            | 1
seq_tup_read        | 1000000
idx_scan            | 
idx_tup_fetch       | 
n_tup_ins           | 1000000
n_tup_upd           | 200000
n_tup_del           | 0
n_tup_hot_upd       | 0
n_live_tup          | 1000000
n_dead_tup          | 200000
n_mod_since_analyze | 0
last_vacuum         | 
last_autovacuum     | 
last_analyze        | 
last_autoanalyze    | 2021-11-15 22:37:18.039358+01
vacuum_count        | 0
autovacuum_count    | 0
analyze_count       | 0
autoanalyze_count   | 2
```

  * Modifier 60 lignes supplémentaires de cette table.

```sql
mvcc=# UPDATE t5 SET id=2000000 WHERE id<200060;
UPDATE 59
```

  * Attendez une minute.

```sql
mvcc=# SELECT pg_sleep(60);
```

  * Que contient la vue pg_stat_user_tables pour cette table ? Qu'en
    déduisez-vous ?

```sql
mvcc=# SELECT * FROM pg_stat_user_tables WHERE relname='t5';
-[ RECORD 1 ]-------+------------------------------
relid               | 18745
schemaname          | public
relname             | t5
seq_scan            | 2
seq_tup_read        | 2000000
idx_scan            | 
idx_tup_fetch       | 
n_tup_ins           | 1000000
n_tup_upd           | 200059
n_tup_del           | 0
n_tup_hot_upd       | 10
n_live_tup          | 1000000
n_dead_tup          | 0
n_mod_since_analyze | 59
last_vacuum         | 
last_autovacuum     | 2020-11-15 22:39:18.597124+01
last_analyze        | 
last_autoanalyze    | 2020-11-15 22:37:18.039358+01
vacuum_count        | 0
autovacuum_count    | 1
analyze_count       | 0
autoanalyze_count   | 2
```

Un `VACUUM` a été automatiquement exécuté sur cette table, suite à la
suppression de plus de 200 050 lignes (`threshold + scale factor * #lines`). Il
a fallu attendre que l'autovacuum vérifie l'état des tables, d'où l'attente de
60 secondes.

Notez aussi que `n_dead_tup` est revenu à 0 après le `VACUUM`. C'est le compteur
qui est comparé à la limite avant exécution d'un `VACUUM`.

  * Descendez le facteur d'échelle de cette table à 10 % pour le `VACUUM`.

```sql
mvcc=# ALTER TABLE t5 SET (autovacuum_vacuum_scale_factor=0.1);
ALTER TABLE
```

  * Modifiez les 200 000 lignes suivantes de cette table ?

```sql
mvcc=# UPDATE t5 SET id=2000000 WHERE id<=400060;
UPDATE 200000
```

  * Attendez une minute.

```sql
mvcc=# SELECT pg_sleep(60);
```

  * Que contient la vue _pg_stat_user_tables_ pour cette table ? Qu'en
    déduisez-vous ?

```sql
mvcc=# SELECT * FROM pg_stat_user_tables WHERE relname='t5';
-[ RECORD 1 ]-------+------------------------------
relid               | 18745
schemaname          | public
relname             | t5
seq_scan            | 3
seq_tup_read        | 3000000
idx_scan            | 
idx_tup_fetch       | 
n_tup_ins           | 1000000
n_tup_upd           | 400060
n_tup_del           | 0
n_tup_hot_upd       | 59
n_live_tup          | 1000000
n_dead_tup          | 0
n_mod_since_analyze | 0
last_vacuum         | 
last_autovacuum     | 2021-11-15 22:40:17.799498+01
last_analyze        | 
last_autoanalyze    | 2021-11-15 22:40:18.464116+01
vacuum_count        | 0
autovacuum_count    | 2
analyze_count       | 0
autoanalyze_count   | 3
```

Avec un facteur d'échelle à 10%, il ne faut plus attendre que la modification
de 100050 lignes.

#### Verrous

  * Ouvrez une transaction et lisez la table t1.

```sql
mvcc=# BEGIN;
BEGIN
mvcc=# SELECT * FROM t1;
 c1 |   c2
----+--------
  1 | un
  2 | deux
  3 | TROIS
  4 | QUATRE
  5 | CINQ
(5 rows)
```

  * Ouvrez une autre transaction, et tentez de supprimer la table t1.

```sql
$ psql mvcc
psql (11)
Type "help" for help.

mvcc=# DROP TABLE t1;
```

  * Listez les processus du serveur PostgreSQL. Que remarquez-vous ?

```sql
$ ps -o pid,cmd fx
23138 /usr/lib/postgresql/11/bin/postgres -D /var/lib/postgresql/11/main (...)
23140  \_ postgres: 11/main: checkpointer
23141  \_ postgres: 11/main: background writer
23142  \_ postgres: 11/main: walwriter
23143  \_ postgres: 11/main: autovacuum launcher
23145  \_ postgres: 11/main: stats collector
23146  \_ postgres: 11/main: logical replication launcher
 8538  \_ postgres: 11/main: postgres mvcc [local] idle in transaction
 9320  \_ postgres: 11/main: postgres mvcc [local] DROP TABLE waiting
```

La ligne intéressante est la ligne du `DROP TABLE`. Elle contient le mot clé
`waiting`. Ce dernier indique que l'exécution de la requête est en attente d'un
verrou sur un objet.

  * Récupérez la liste des sessions en attente d'un verrou avec la vue
    _pg_stat_activity_.

```sql
-[ RECORD 1 ]----+--------------------------------------------------------------------
datid            | 16385
datname          | postgres
pid              | 9320
usesysid         | 16384
usename          | postgres
application_name | psql
client_addr      | 
client_hostname  | 
client_port      | -1
backend_start    | 2021-11-15 22:41:55.747066+01
xact_start       | 2021-11-15 22:42:00.068183+01
query_start      | 2021-11-15 22:42:00.068183+01
state_change     | 2021-11-15 22:42:00.068185+01
wait_event_type  | Lock
wait_event       | relation
state            | active
backend_xid      | 2760709
backend_xmin     | 2760709
query            | DROP TABLE t1;
backend_type     | client backend
-[ RECORD 2 ]----+--------------------------------------------------------------------
datid            | 16385
datname          | postgres
pid              | 8538
usesysid         | 16384
usename          | postgres
application_name | psql
client_addr      | 
client_hostname  | 
client_port      | -1
backend_start    | 2021-11-15 22:03:40.831073+01
xact_start       | 2021-11-15 22:41:47.548844+01
query_start      | 2021-11-15 22:41:51.420636+01
state_change     | 2021-11-15 22:41:51.421359+01
wait_event_type  | Client
wait_event       | ClientRead
state            | idle in transaction
backend_xid      | 
backend_xmin     | 
query            | SELECT * FROM t1;
backend_type     | client backend
```

  * Récupérez la liste des verrous en attente pour la requête bloquée.

```sql
mvcc=# SELECT * FROM pg_locks WHERE pid=9320 AND NOT granted;
-[ RECORD 1 ]------+--------------------
locktype           | relation
database           | 16385
relation           | 18682
page               | 
tuple              | 
virtualxid         | 
transactionid      | 
classid            | 
objid              | 
objsubid           | 
virtualtransaction | 3/4804
pid                | 9320
mode               | AccessExclusiveLock
granted            | f
fastpath           | f
```

  * Récupérez le nom de l'objet dont on n'arrive pas à récupérer le verrou.

```sql
mvcc=# SELECT relname FROM pg_class WHERE oid=18682;
-[ RECORD 1 ]
relname | t1
```

  * Récupérez la liste des verrous sur cet objet. Quel processus a verrouillé
    la table t1 ?

```sql
mvcc=# SELECT * FROM pg_locks WHERE relation=18682;
-[ RECORD 1 ]------+--------------------
locktype           | relation
database           | 16385
relation           | 18682
page               | 
tuple              | 
virtualxid         | 
transactionid      | 
classid            | 
objid              | 
objsubid           | 
virtualtransaction | 4/69
pid                | 8538
mode               | AccessShareLock
granted            | t
fastpath           | f
-[ RECORD 2 ]------+--------------------
locktype           | relation
database           | 16385
relation           | 18682
page               | 
tuple              | 
virtualxid         | 
transactionid      | 
classid            | 
objid              | 
objsubid           | 
virtualtransaction | 3/4804
pid                | 9320
mode               | AccessExclusiveLock
granted            | f
fastpath           | f
```

Le processus de PID 8538 a un verrou sur t1. Ce processus avait été listé plus
haut en attente du client (`idle in transaction`).

  * Retrouvez les informations sur la session bloquante.

```sql
mvcc=# SELECT * FROM pg_stat_activity WHERE pid=8538;
-[ RECORD 1 ]----+------------------------------
datid            | 16385
datname          | postgres
pid              | 8538
usesysid         | 16384
usename          | postgres
application_name | psql
client_addr      | 
client_hostname  | 
client_port      | -1
backend_start    | 2021-11-15 22:03:40.831073+01
xact_start       | 2021-11-15 22:41:47.548844+01
query_start      | 2021-11-15 22:41:51.420636+01
state_change     | 2021-11-15 22:41:51.421359+01
wait_event_type  | Client
wait_event       | ClientRead
state            | idle in transaction
backend_xid      | 
backend_xmin     | 
query            | SELECT * FROM t1;
backend_type     | client backend
```

À partir de là, il est possible d'arrêter l'exécution de l'ordre `DROP TABLE`
avec la fonction `pg_cancel_backend()` ou de déconnecter le processus en cours
de transaction avec la fonction `pg_terminate_backend()`.

</div>

------
