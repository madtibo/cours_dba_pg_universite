Pour chacune de ses instructions, indiquer s'il s'agit d'une activité DDL ou DML :

  * DELETE
  * ALTER
  * SELECT
  * INSERT
  * DROP
  
------------

Qu'est-ce qu'une clé étrangère ?

------------

Quelles sont les 4 propriétés ACID d'une base de donnée relationnelle ?

------------

Quels sont les 3 élèments définissant une instance ?

------------

Citer un outil permettant d'accéder aux données des bases PostgreSQL

------------

Les outils pg_dump et pg_restore permettent-ils de migrer des données de
PostgreSQL quelque soit la version d'origine et la version cible ?
(développez votre réponse)

------------

Quel est le résultat de chacunes des commandes suivantes :

```bash
admin$ psql -d ojm3 -h 192.168.0.5 -U adb1 -p 5454
```

```sql
postgres$ psql -p 5433 -c 'SELECT pg_reload_conf();' -h /tmp
```

```sql
$ pg_dump -Fd -U ojm3 -h 192.168.0.5 -f /tmp/ma_db jon1
```

------------

Quel est le résultat de la suite d’opérations suivantes ?

```sql
mondial=> BEGIN;
mondial=> DELETE FROM mergeswith
  WHERE sea1='Atlantic Ocean' AND sea2='Mediterranean Sea';
mondial=> ROLLBACK;
```


```sql
mondial=> BEGIN;
mondial=> DELETE FROM island WHERE height<10;
mondial=> UDPATE lake SET depth=depth+10;
mondial=> COMMIT;
```

------------

Soit le plan de requête suivant :


```sql
                          QUERY PLAN
-----------------------------------------------------------------------------
 HashAggregate  (cost=56.36..59.93 rows=238 width=41)
                (actual time=3.241..3.366 rows=120 loops=1)
   Group Key: country.name
   Filter: (sum(borders.length) >= '1000'::numeric)
   Rows Removed by Filter: 44
   ->  Hash Join  (cost=29.96..51.55 rows=640 width=41)
                  (actual time=1.555..2.434 rows=640 loops=1)
         Hash Cond: ((borders.country1)::text = (country.code)::text)
         ->  HashAggregate  (cost=21.60..28.00 rows=640 width=72)
	                    (actual time=1.162..1.447 rows=640 loops=1)
               Group Key: borders.country1, borders.country2, borders.length
               ->  Append  (cost=0.00..16.80 rows=640 width=72)
	                   (actual time=0.015..0.402 rows=640 loops=1)
                     ->  Seq Scan on borders
		            (cost=0.00..5.20 rows=320 width=11)
			    (actual time=0.015..0.105 rows=320 loops=1)
                     ->  Seq Scan on borders borders_1
			    (cost=0.00..5.20 rows=320 width=11)
			    (actual time=0.013..0.150 rows=320 loops=1)
         ->  Hash  (cost=5.38..5.38 rows=238 width=12)
	           (actual time=0.363..0.363 rows=238 loops=1)
               Buckets: 1024  Batches: 1  Memory Usage: 19kB
               ->  Seq Scan on country
	              (cost=0.00..5.38 rows=238 width=12)
	              (actual time=0.029..0.132 rows=238 loops=1)
 Planning time: 0.536 ms
 Execution time: 3.529 ms
(16 lignes)
```

  1. Combien de noeuds sont présent dans ce plan ? 
  2. Quel est le nombre de lignes que le planificateur a prévu de remonter ?
  3. Quel est le nombre de lignes qui seront effectivement remontés par cette
  requête ?
  4. S'il y a une différence entre les deux nombres, quelle peut en est la
  raison ?
  5. Quelle est le résultat recherché par la requête ayant fourni ce plan ?

------------

Soit le résultat de 2 commandes systèmes :

```bash
postgres$ ps ax|grep postgres
26063 S   0:01 /usr/lib/postgresql/10/bin/postgres -D /var/lib/postgresql/10/main
26064 S   0:00 /usr/lib/postgresql/9.6/bin/postgres -D /var/lib/postgresql/9.6/fo
26065 S   0:02 /usr/lib/postgresql/9.6/bin/postgres -D /var/lib/postgresql/9.6/ma
26066 S   0:00 /usr/lib/postgresql/9.4/bin/postgres -D /var/lib/postgresql/9.4/ma
26069 Ss  0:00 postgres: 9.6/foreign_cluster: checkpointer process
26070 Ss  0:00 postgres: 9.6/foreign_cluster: writer process
26071 Ss  0:00 postgres: 9.6/foreign_cluster: wal writer process
26072 Ss  0:00 postgres: 9.6/foreign_cluster: autovacuum launcher process
26073 Ss  0:00 postgres: 9.6/foreign_cluster: stats collector process
26075 Ss  0:00 postgres: checkpointer process
26076 Ss  0:00 postgres: writer process
26077 Ss  0:00 postgres: wal writer process
26078 Ss  0:00 postgres: autovacuum launcher process
26079 Ss  0:00 postgres: stats collector process
26080 Ss  0:00 postgres: 10/main: checkpointer process
26081 Ss  0:00 postgres: 10/main: writer process
26082 Ss  0:00 postgres: 10/main: wal writer process
26083 Ss  0:01 postgres: 10/main: autovacuum launcher process
26084 Ss  0:01 postgres: 10/main: stats collector process
26085 Ss  0:00 postgres: 10/main: bgworker: logical replication launcher
26087 Ss  0:00 postgres: 9.6/main: checkpointer process
26088 Ss  0:00 postgres: 9.6/main: writer process
26089 Ss  0:00 postgres: 9.6/main: wal writer process
26090 Ss  0:02 postgres: 9.6/main: autovacuum launcher process
26091 Ss  0:03 postgres: 9.6/main: stats collector process
postgres$ netstat -anp | grep pos
tcp   0    0 127.0.0.1:5432      0.0.0.0:*            LISTEN      26063/postgres
tcp   0    0 0.0.0.0:5433        0.0.0.0:*            LISTEN      26065/postgres
tcp   0    0 0.0.0.0:5434        0.0.0.0:*            LISTEN      26066/postgres
tcp   0    0 127.0.0.1:5435      0.0.0.0:*            LISTEN      26064/postgres
tcp   0    0 192.168.2.209:5434  10.50.0.24:59223     ESTABLISHED -
tcp   0   68 192.168.2.209:5433  10.50.0.75:55129     ESTABLISHED -
tcp   0    0 192.168.2.209:5434  192.168.2.59:38119   ESTABLISHED -
tcp   0    0 192.168.2.209:5434  192.168.2.180:36172  ESTABLISHED -
tcp   0    0 192.168.2.209:5433  192.168.2.180:40207  ESTABLISHED -
tcp   0    0 192.168.2.209:5434  10.50.0.101:43272    ESTABLISHED -
tcp   0    0 127.0.0.1:5432      127.0.0.1:37634      ESTABLISHED -
tcp   0    0 192.168.2.209:5433  192.168.2.180:59837  ESTABLISHED -
unix 2 [ ACC ] STREAM  LISTENING 2647629  26066/postgres      /tmp/.s.PGSQL.5434
unix 2 [ ACC ] STREAM  LISTENING 2648693  26063/postgres      /tmp/.s.PGSQL.5432
unix 2 [ ACC ] STREAM  LISTENING 2653347  26064/postgres      /tmp/.s.PGSQL.5435
unix 2 [ ACC ] STREAM  LISTENING 2646719  26065/postgres      /tmp/.s.PGSQL.5433
unix 3 [ ]     STREAM  CONNECTE  2828358  8354/main: postgres /tmp/.s.PGSQL.5433
unix 3 [ ]     STREAM  CONNECTE  2816832  7902/foreign_cluste /tmp/.s.PGSQL.5435
unix 3 [ ]     STREAM  CONNECTE  2824345  8365/postgres: post /tmp/.s.PGSQL.5434
unix 3 [ ]     STREAM  CONNECTE  2826360  8363/postgres: post /tmp/.s.PGSQL.5434
unix 3 [ ]     STREAM  CONNECTE  2824344  8364/postgres: post /tmp/.s.PGSQL.5434
unix 3 [ ]     STREAM  CONNECTE  2828360  8356/main: postgres /tmp/.s.PGSQL.5433
```

  1. Combien d'instances sont-elles démarrées sur le serveur ?
  2. Donner le nom et les 3 paramètres définissant chaque instance
  3. Les instances acceptent-elles les connexions ? Si oui, sur quelles
  interfaces ?

------------

Normaliser quelques tables avec ordres SQL

------------

Exercice sur les droits ?

pg_hba.conf ?

------------

Écrire une fonction qui permet de retrouver ...
