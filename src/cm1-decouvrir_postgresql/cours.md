# Découvrir PostgreSQL

![PostgreSQL](medias/common-postgresql_logo.png)
\

<div class="notes">
</div>


-----

## Mon entreprise : GitGuardian

<div class="slide-content">

  * Git Security Scanning & Secrets Detection
  * DBA Dev & Performance
  * Site : <https://www.gitguardian.com>

</div>


<div class="notes">

Équipe : <https://www.welcometothejungle.com/fr/companies/gitguardian>

</div>

-----

## Programme de ce cours

<div class="slide-content">
  * **Semaine 1** : découverte de PostgreSQL
    * Histoire
    * Concepts et fonctionnalités
	* l'outil `psql`
  * Semaine 2 : transactions et accès concurrents
  * Semaine 3 : missions du DBA
  * Semaine 4 : optimisation et indexation
  * Semaine 5 : _PL/PgSQL_ et triggers

</div>

<div class="notes">

</div>

-----

### Organisation du cours

<div class="slide-content">
  * Des Cours Magistraux : à lire !
  * Des Travaux Dirigés
  * Des Travaux Pratiques
  * Une séance de TP notée
  * Un examen final sur table
</div>


<div class="notes">

</div>

-----

## PostgreSQL : c'est...

<div class="slide-content">

Le Système de Gestion de Base de Données relationnelle  
Open Source le plus avancé au monde

</div>

<div class="notes">

</div>

-----

## Définitions

<div class="slide-content">

  * Modèle relationnelle
  * Base de données
  * Système de gestion de base de données

</div>

<div class="notes">

Recherchons ces concepts sur Wikipedia.

</div>

-----

### Modèle relationnel

<div class="slide-content">

  * Inventé par Edgar F. Codd en 1970
  * Organisation de l'information dans des _relations_  ou _tables_
    * _attributs_ : les colonnes
    *  _nuplets_ ou _enregistrements_ : les lignes
  * _base de données_ : regroupement de plusieurs relations

</div>

<div class="notes">

En informatique, une **base de données relationnelle** est une base de données
où l'information est organisée dans des tableaux à deux dimensions appelés des
_relations_ ou _tables_, selon le modèle introduit par Edgar F. Codd
en 1970.  
Selon ce modèle relationnel, une base de données consiste en une ou plusieurs
relations. Les lignes de ces relations sont appelées des _nuplets_ ou
_enregistrements_. Les colonnes sont appelées des _attributs_.

</div>

-----

### Une base de données...

<div class="slide-content">

est un _conteneur_ stockant des données pouvant être retraités par des moyens
informatiques pour produire une information.

</div>


<div class="notes">

Une **base de données** est un _conteneur_ stockant des données pouvant être
retraités par des moyens informatiques pour produire une information.

</div>

-----

### Un système de gestion de base de données...

<div class="slide-content">

est un logiciel système servant à  
stocker, manipuler ou gérer, et à partager  
des informations dans une base de données.

</div>


-----

### Un SGBD garantie :

<div class="slide-content">

  * la qualité, la pérennité et la confidentialité des informations,
  * tout en cachant la complexité des opérations.

</div>

-----

## Histoire de PostgreSQL

<div class="slide-content">

</div>

-----

### PostgreSQL : un logiciel Open Source

<div class="slide-content">

  * sous licence PostgreSQL 
  * Droit de :
    * utiliser
	* copier
	* modifier
	* distribuer sans coût de licence
  
</div>


<div class="notes"> 

La [licence PostgreSQL](http://www.postgresql.org/about/licence/) est issue des
licences BSD et MIT. Elle offre les droits de utiliser, copier, modifier,
distribuer sans coût de licence. Elles est reconnu par l'[Open Source
Initiative](http://opensource.org/licenses/PostgreSQL). Elle est utilisée par
un grand nombre de projets de l'écosystème

Cette licence vous donne le droit de distribuer PostgreSQL, de l'installer, de
le modifier... et même de le vendre. Certaines sociétés, comme EnterpriseDB,
produisent leur version de PostgreSQL de cette façon.

</div>

-----

### PostgreSQL : quelle histoire !

<div class="slide-content">
  * Parmi les plus vieux logiciels libres
  * Et les plus sophistiqués
  * Souvent cité comme exemple :
    * qualité du code
    * indépendance des développeurs
    * réactivité de la communauté
</div>


<div class="notes">

L'histoire de PostgreSQL est longue, riche et passionnante. Au côté des
projets libres Apache et Linux, PostgreSQL est l'un des plus vieux logiciels
libres en activité et fait partie des SGBD les plus sophistiqués à l'heure
actuelle.

Au sein des différentes communautés libres, PostgreSQL est souvent utilisé
comme exemple à différents niveaux :

  * qualité du code ;
  * indépendance des développeurs et gouvernance du projet ;
  * réactivité de la communauté ;
  * stabilité et puissance du logiciel.

Tous ces atouts font que PostgreSQL est désormais reconnu et adopté par des
milliers de grandes sociétés de par le monde.

</div>

-----

### ¿¡¿¡ PostgreSQL ?!?!

<div class="slide-content">

  * Michael Stonebraker recode Ingres
  * post « ingres » => post~~in~~gres => postgres
  * support de SQL : postgre => PostgreSQL
</div>


<div class="notes">


L'origine du nom PostgreSQL remonte à la base de données Ingres,
développée à l'université de Berkeley par Michael Stonebraker. En 1985, il
prend la décision de reprendre le développement à partir de zéro et nomme ce
nouveau logiciel Postgres, comme raccourci de post-Ingres.

En 1995, avec l'ajout du support du langage SQL, Postgres fut renommé
Postgres95 puis PostgreSQL.

Aujourd'hui, le nom officiel est « PostgreSQL » (prononcez « post - gresse -
Q - L » ). Cependant, le nom « Postgres » est accepté comme alias.

</div>

-----


### Progression du code

![Évolution du nombre de lignes de code dans les sources de PostgreSQL](medias/a1-postgres-code-repartition.png)
\


<div class="notes">


Ce graphe représente l'évolution du nombre de lignes de code dans les sources
de PostgreSQL. Cela permet de bien visualiser l'évolution du projet en terme de
développement.

On note une augmentation constante depuis 1998 avec une croissance régulière
d'environ 100 000 lignes de code C par an. Le plus intéressant est certainement
de noter que l'évolution est constante.

Actuellement, PostgreSQL est composé de plus de 1 856 505 de lignes (dont
452 306 lignes de commentaires), pour environ [200 développeurs
actifs](https://www.postgresql.org/community/contributors/).


<div class="box">
[Source](https://www.openhub.net/p/postgres).
</div>


</div>

-----


## Concepts de base

<div class="slide-content">

  * ACID
  * MVCC
  * Transactions
  * WAL (journaux de transactions)
  * Politique de versionnage
</div>


<div class="notes">

</div>



-----


### ACID

<div class="slide-content">

  * **Atomicité** (Atomic)
  * **Cohérence** (Consistent)
  * **Isolation** (Isolated)
  * **Durabilité** (Durable)
</div>


<div class="notes">


Les propriétés ACID sont le fondement même de tout système transactionnel.
Il s'agit de quatre règles fondamentales :


   * A : Une transaction est entière : « tout ou rien ».
   * C : Une transaction amène le système d'un état stable à un autre.
   * I : Les transactions n'agissent pas les unes sur les autres.
   * D : Une transaction validée provoque des changements permanents.

Les propriétés ACID sont quatre propriétés essentielles d'un sous-système de
traitement de transactions d'un système de gestion de base de données.
Certains SGBD ne fournissent pas les garanties ACID. C'est le cas de la plupart
des SGBD non-relationnels (« NoSQL »). Cependant, la plupart des applications
ont besoin de telles garanties et la décision d'utiliser un système ne
garantissant pas ces propriétés ne doit pas être prise à la légère.

</div>



-----


### MultiVersion Concurrency Control (MVCC)

<div class="slide-content">

  * Le « noyau » de PostgreSQL
  * Garantit ACID
  * Permet les écritures concurrentes sur la même table
</div>


<div class="notes">

MVCC (Multi Version Concurrency Control) est le mécanisme interne de PostgreSQL
utilisé pour garantir la cohérence des données lorsque plusieurs processus
accèdent simultanément à la même table.

C'est notamment MVCC qui permet de sauvegarder facilement une base _à chaud_ et
d'obtenir une sauvegarde cohérente alors même que plusieurs utilisateurs sont
potentiellement en train de modifier des données dans la base.

C'est la qualité de l'implémentation de ce système qui fait de PostgreSQL un
des meilleurs SGBD au monde : chaque transaction travaille dans son image de la
base, cohérent du début à la fin de ses opérations. Par ailleurs les
écrivains ne bloquent pas les lecteurs et les lecteurs ne bloquent pas les
écrivains, contrairement aux SGBD s'appuyant sur des verrous de lignes. Cela
assure de meilleures performances, un fonctionnement plus fluide des outils s'appuyant sur PostgreSQL.

</div>


-----


### Transactions

<div class="slide-content">

  * Intimement liées à ACID et MVCC :
    * Une transaction est un ensemble d'opérations atomique
    * Le résultat d'une transaction est « tout ou rien »
  * mots clés `BEGIN`, `COMMIT` et `ROLLBACK`
</div>


<div class="notes">

`BEGIN` initie une transaction. `COMMIT` valide toutes les commandes depuis
l'ordre `BEGIN`. `ROLLBACK` annule toutes les commandes depuis l'ordre `BEGIN`.

</div>

-----


### Write Ahead Logs, aka WAL

<div class="slide-content">

  * Chaque donnée est écrite **2 fois** sur le disque !
  * Sécurité quasiment infaillible
  * Comparable à la journalisation des systèmes de fichiers

</div>


<div class="notes">


Les journaux de transactions (appelés parfois WAL ou XLOG) sont une garantie
contre les pertes de données.

Il s'agit d'une technique standard de journalisation appliquée à toutes les
transactions.

Ainsi lors d'une modification de donnée, l'écriture au niveau du disque se
fait en deux temps :

 - écriture immédiate dans le journal de transactions ;
 - écriture à l'emplacement final lors du prochain `CHECKPOINT`.

Ainsi en cas de crash :

 - PostgreSQL redémarre ;
 - PostgreSQL vérifie s'il reste des données non intégrées aux fichiers de
données dans les journaux (mode recovery) ;
 - si c'est le cas, ces données sont recopiées dans les fichiers de données
afin de retrouver un état stable et cohérent.


**Plus d'information** :
<https://public.dalibo.com/archives/publications/glmf108_postgresql_et_ses_journaux_de_transactions.pdf>

</div>


-----


#### Avantages des WAL

<div class="slide-content">

  * Un seul _sync_ sur le fichier de transactions
  * Le fichier de transactions est écrit de manière séquentielle
  * Les fichiers de données sont écrits de façon asynchrone
  * Point In Time Recovery
  * Réplication (*WAL shipping*)

</div>


<div class="notes">

Les écritures se font de façon séquentielle, donc sans grand déplacement de la
tête d'écriture. Généralement, le déplacement des têtes d'un disque est
l'opération la plus coûteuse. L'éviter est un énorme avantage.

De plus, comme on n'écrit que dans un seul fichier de transactions, la
synchronisation sur disque peut se faire sur ce seul fichier, à condition que
le système de fichiers le supporte.

L'écriture asynchrone dans les fichiers de données permet là-aussi de gagner
du temps.

Mais les performances ne sont pas la seule raison des journaux de transactions.
Ces journaux ont aussi permis l'apparition de nouvelles fonctionnalités très
intéressantes, comme le PITR et la réplication physique.

</div>

-----

#### Versions majeures

<div class="slide-content">
  * 5 versions majeures supportées
  * une nouvelle version majeure par an
    * apporte des nouvelles fonctionnalités
  * actuellement : 12, 13, 14, 15 et 16
  * prochaine version : 17
</div>


<div class="notes">


La philosophie générale des développeurs de PostgreSQL peut se résumer ainsi :


> _Notre politique se base sur la qualité, pas sur les dates de sortie._


Toutefois, même si cette philosophie reste très présente parmi les
développeurs, depuis quelques années, les choses évoluent et la tendance
actuelle est de livrer une version stable majeure tous les 12 à 15 mois, tout
en conservant la qualité des versions. De ce fait, toute fonctionnalité
supposée pas suffisamment stable est repoussée à la version suivante.

La tendance actuelle est de garantir un support pour chaque version courante
pendant une durée minimale de 5 ans.

Le support de la version 11 s'arrêtera en novembre 2023. La version 10 n'est
plus supportée depuis novembre 2022. La prochaine version qui subira ce sort
est la 12, en novembre 2024. Le support de la version 16 devrait durer jusque
novembre 2028.

**Pour plus de détails** : <http://www.postgresql.org/support/versioning/>

* **Politique de versionnage**

  * Depuis la version 10
    * X : version majeure (11, 14)
    * X.Y : version mineure (13.5)

  * Avant la version 10
    * X.Y : version majeure (8.4, 9.6)
    * X.Y.Z : version mineure (9.5.8)

</div>

-----


#### Versions mineures

<div class="slide-content">
  * au moins une mise à jour mineure par trimestre
  * ... et en cas de faille de sécurité
  * pour toutes les versions majeures supportées
  * pas de nouvelle fonctionnalité
</div>

<div class="notes">

Une version mineure ne comporte que des corrections de bugs ou de failles de
sécurité. Elles sont plus fréquentes que les versions majeures, avec un
rythme de sortie trimestriel, sauf bug majeur ou faille de
sécurité. Chaque bug est corrigé dans toutes les versions stables
actuellement maintenues par le projet.

Dernières mise à jour le 14 septembre 2023 :
  * version 11.21
  * version 12.16
  * version 13.12
  * version 14.9
  * version 15.4
  * version 16.0
  
Prochaine mise à jour prévue le 9 novembre 2023.

**Pour plus de détails** : <http://www.postgresql.org/developer/roadmap>

</div>

-----

#### Compatibilité

<div class="slide-content">

  * mise à jour majeure : migration des données
  * mise à jour mineure : redémarrage

</div>

<div class="notes">

Une version majeure apporte de nouvelles fonctionnalités, des changements de
comportement, etc. Le format de stockages des données internes est souvent
modifié. Le passage d'une version majeure à une nouvelle version majeure
nécessitera une migration des données.

Les mises à jour mineures se font en général sans soucis, et ne nécessitent en
général qu'un redémarrage. Mais comme pour toute mise à jour, il faut être
prudent sur d'éventuels effets de bord. En particulier on lira les *Release
Notes* et si possible on testera ailleurs qu'en production.

</div>

-----

## Structuration de PostgreSQL 

<div class="slide-content">

  * Organisation physique
  * Organisation logique

</div>

<div class="notes">

</div>

-----

### Organisation physique

<div class="slide-content">

  * stockage dans le répertoire de données
  * souvent appelé `PGDATA`
  * localisations des fichiers différentes suivant les OS

</div>

<div class="notes">

Le répertoire de données est souvent appelé `PGDATA`, du nom de la variable
d’environnement que l’on peut faire pointer vers lui pour simplifier
l’utilisation de nombreux utilitaires PostgreSQL. On peut aussi le connaître,
étant connecté à la base, en interrogeant le paramètre `data_directory`.

```
postgres=# SHOW data_directory ;
      data_directory
-----------------------------
 /var/lib/postgresql/14/main
(1 row)
```

En version 14 :

```
postgres$ ls $PGDATA
# ls $PGDATA
base	      pg_notify     pg_subtrans  postgresql.auto.conf
global	      pg_replslot   pg_tblspc	 postmaster.opts
pg_commit_ts  pg_serial     pg_twophase  postmaster.pid
pg_dynshmem   pg_snapshots  PG_VERSION
pg_logical    pg_stat	    pg_wal
pg_multixact  pg_stat_tmp   pg_xact
```

Vous pouvez trouver une description de tous les fichiers et répertoires dans
[la documentation officielle](https://www.postgresql.org/docs/current/static/storage-file-layout.html).

Sur les système d'exploitation de type RedHat et CentOS, le répertoire des
données est situé par défaut dans `/var/lib/pgsql`.

Sur les système d'exploitation de type Debian et Ubuntu, le répertoire des
données est situé par défaut dans `/var/lib/postgresql`, les fichiers de
configuration est situé dans `/etc/postgresql` et le répertoire de log
applicatif est situé dans `/var/log/postgresql`.

</div>

-----


### Organisation logique



![Organisation logique d'une instance](medias/common-organisation_logique_d-une_instance_small.jpg)
\

<div class="notes">

</div>

-----


#### Schémas

<div class="slide-content">

  * Espace de noms
  * Concept différent des schémas d'Oracle
  * Sous-ensemble de la base
</div>


<div class="notes">


Un utilisateur peut avoir accès à tous les schémas ou à un sous-ensemble.
Tout dépend des droits dont il dispose. PostgreSQL vérifie la présence des
objets par rapport au paramètre `search_path` valable pour la session en cours
lorsque le schéma n'est pas indiqué explicitement pour les objets d'une
requête.

À la création d'un utilisateur, un schéma n'est pas forcément associé.

Le comportement et l'utilisation des schémas diffèrent donc d'avec Oracle.

Les schémas sont des espaces de noms dans une base de données permettant :

  * de grouper les objets d'une base de données ;
  * de séparer les utilisateurs entre eux ;
  * de contrôler plus efficacement les accès aux données ;
  * d'éviter les conflits de noms dans les grosses bases de données.

Les schémas sont très utiles pour les systèmes de réplication (Slony,
bucardo).

Exemple d'utilisation de schéma :

```sql
=> CREATE SCHEMA pirates;
CREATE SCHEMA
=> SET search_path TO pirates,public;
SET
=> CREATE TABLE capitaines (id serial, nom text);
CREATE TABLE

=> INSERT INTO capitaines (nom)
 VALUES ('Anne Bonny'), ('Francis Drake');
INSERT 0 2

-- Sélections des capitaines... pirates
=> SELECT * FROM capitaines;
 id |      nom       
----+----------------
  1 | Anne Bonny
  2 | Francis Drake
(2 rows)

=> CREATE SCHEMA corsaires;
CREATE SCHEMA
=> SET search_path TO corsaires,pirates,public;
SET
=> CREATE TABLE capitaines (id serial, nom text);
=> INSERT INTO corsaires.capitaines (nom)
 VALUES ('Robert Surcouf'), ('Francis Drake');
INSERT 0 2
-- Sélections des capitaines... français
=> SELECT * FROM capitaines;
 id |         nom          
----+----------------------
  1 | Robert Surcouf
  2 | Francis Drake
(2 rows)


-- Quels capitaine portant un nom identique
-- fut à la fois pirate et corsaire ?
=> SELECT pirates.capitaines.nom
  FROM pirates.capitaines,corsaires.capitaines
  WHERE pirates.capitaines.nom=corsaires.capitaines.nom;
      nom      
---------------
 Francis Drake
(1 row)

```

</div>

-----

## Intégration logicielle de PostgreSQL

<div class="slide-content">

  * PostgreSQL est une plate-forme de développement !
  * 15 langages de procédures stockées
  * Interfaces natives pour ODBC, JDBC, C, PHP, Perl, etc.
  * API ouverte et documentée
  * Un nouveau langage peut être ajouté __sans recompilation__ de PostgreSQL

</div>


<div class="notes">

Voici la liste non exhaustive des langages procéduraux supportés :

<div class="box">
  * pl/pgsql
  * pl/sql
  * pl/perl
  * pl/python
  * pl/tcl
  * pl/sh
  * pl/R
  * pl/java
  * pl/js
  * pl/lolcode
  * pl/scheme
  * pl/php
  * pl/ruby
  * pl/j
  * pl/lua
  * pl/pgpsm
  * pl/v8
</div>

PostgreSQL peut donc être utilisé comme un serveur d'applications ! Vous
pouvez ainsi placer votre code au plus près des données.

Chaque langage a ses avantages et inconvénients. Par exemple, PL/pgsql est
très simple à apprendre mais n'est pas performant quand il s'agit de traiter
des chaînes de caractères. Pour ce traitement, il serait préférable
d'utiliser PL/perl, voire PL/python. Évidemment, une procédure en C aura les
meilleures performances mais sera beaucoup moins facile à coder et à
maintenir. Par ailleurs, les procédures peuvent s'appeler les unes les autres
quel que soit le langage.

Les applications externes peuvent accéder aux données du serveur PostgreSQL
grâce à des connecteurs. Ils peuvent passer par l'interface native, la
__libpq__ . C'est le cas du connecteur PHP et du connecteur Perl par
exemple. Ils peuvent aussi ré-implémenter cette interface, ce que fait le
pilote ODBC (psqlodbc) ou le driver JDBC.

</div>

-----

## PostgreSQL à l'heure du NoSQL

<div class="slide-content">

  * 4 technologies majeures dans le monde NoSQL
    * Stockage clé->valeur (Redis, Apache Cassandra, Riak, MongoDB)
    * Stockage documents (Apache CouchDB, MongoDB)
    * Stockage colonnes (Apache Hbase, Google BigTable)
    * Stockage graphes (Neo4j)	
  * PostgreSQL est-il déprécié ?

</div>


<div class="notes">

Les SGBD relationnels créés dans les années 1970 se sont progressivement
imposés jusqu'à devenir le paradigme de bases de données très largement
dominant au début des années 1990.

C'est dans le courant des années 2000 avec le développement de grandes
entreprises internet (Google, Amazon, eBay...) brassant des quantités énormes de
données et le développement de l'informatique en grappes que la domination sans
partage du modèle relationnel a été remise en question car souffrant de limites
rédhibitoires pour ces nouvelles pratiques : l'extensibilité.

Les performances restent bonnes avec la montée en charge en multipliant
simplement le nombre de serveurs, solution raisonnable avec la baisse des
coûts, en particulier si les revenus croissent en même temps que
l'activité. Les systèmes géants sont les premiers concernés : énormes
quantités de données, structuration relationnelle faible (ou de moindre
importance que la capacité d'accès très rapide, quitte à multiplier les
serveurs).

(texte issu de l'article Wikipedia [NoSQL](https://fr.wikipedia.org/wiki/NoSQL))

</div>

-----


### PostgreSQL vs. NoSQL

<div class="slide-content">

  * PostgreSQL réunit le monde relationnel et le monde NoSQL
    * Stockage clé->valeur : `hstore`
    * Stockage documents : 
	  * `xml`, `json` et `jsonb` (plus performant que MongoDB)
      * procédure stockée en Javascript : PL/V8
    * Stockage colonnes : `cstore_fdw`
    * Stockage graphes : agensgraph (fork de PostgreSQL)

</div>


<div class="notes">

PostgreSQL n'est pas en reste vis à vis des bases de données NoSQL. PostgreSQL
permet de stocker des données au format clé->valeur.
Couplé aux index GiST et GIN, ce mode de stockage s'avère comparable à
MongoDB (<https://wiki.postgresql.org/images/b/b4/Pg-as-nosql-pgday-fosdem-2013.pdf>) ou à Redis.

PostgreSQL peut stocker des données au format JSON. Le type natif `jsonb` est
indexable, et les performances de PostgreSQL avec ce nouveau type de stockage
de documents sont très supérieures à MongoDB
(<http://blogs.enterprisedb.com/2014/09/24/postgres-outperforms-mongodb-and-ushers-in-new-developer-reality/>).

Couplé au langage PL/V8, le type de données JSON permet de traiter
efficacement ce type de données. PL/V8 permet d'écrire des fonctions en
langage Javascript, elles seront exécutées avec l'interpréteur V8 écrit par
Google. Ces fonctions sont bien entendus indexables par PostgreSQL si elles
respectent un certain nombre de pré-requis.

Le stockage colonne pour PostgreSQL consiste actuellement en une extension 
_Foreign Data Wrapper_ nommée `cstore_fdw`, elle est développée par le
société CitusData 
(<http://www.citusdata.com/blog/76-postgresql-columnar-store-for-analytics>).

Malgré cela, PostgreSQL conserve les fonctionnalités qui ont fait sa
réputation et que les moteurs NoSQL ont dû abandonner :

 * un langage de requête puissant ;
 * un optimiseur de requêtes sophistiqué ;
 * la normalisation des données ;
 * les jointures ;
 * l'intégrité référentielle ;
 * la durabilité.

Un fork de PostgreSQL, agensgraph permet d'effectuer du stockage graph.

Voici enfin un lien vers une excellente présentation sur les différences entre
PostgreSQL et les solutions NoSQL : 
<http://fr.slideshare.net/EnterpriseDB/the-nosql-way-in-postgres>

ainsi que l'[avis de Bruce Momjian sur le choix du NoSQL pour de nouveaux projets](https://momjian.us/main/blogs/pgblog/2017.html#June_12_2017)
</div>

-----

## Sponsors & Références

<div class="slide-content">

  * Sponsors
  * Références
    * françaises
    * et internationales
</div>

<div class="notes">

Au delà de ses qualités, PostgreSQL suscite toujours les mêmes questions
récurrentes :

  * qui finance les développements ? (et pourquoi ?)
  * qui utilise PostgreSQL ?

</div>

-----


### Sponsors

<div class="slide-content">

  * Sociétés se consacrant à PostgreSQL :
    * Crunchy Data (Tom Lane, Stephen Frost, Joe Conway, Greg Smith)
    * EnterpriseDB (Bruce Momjian, Dave Page, Simon Riggs, ...)
    * PostgresPro (Russie)
    * Cybertec (Autriche), Dalibo (France), Redpill Linpro (Suède)
  * Société vendant un fork ou une extension :
    * Citusdata, Pivotal (Heikki Linnakangas)
  * Autres sociétés :
    * VMWare, Rackspace, Heroku, Conova, Red Hat
    * NTT (_streaming Replication_), Fujitsu, NEC

</div>


<div class="notes">

La liste des sponsors de PostgreSQL contribuant activement au développement
figure sur le [site officiel](https://www.postgresql.org/about/sponsors/). Ce
qui suit n'est qu'un aperçu.

EDB est une société américaine qui a décidé de fournir une version de
PostgreSQL propriétaire fournissant une couche de compatibilité avec Oracle.
Ils emploient plusieurs codeurs importants du projet PostgreSQL (dont deux font
partie de la _Core Team_), et reversent un certain nombre de leurs travaux au
sein du moteur communautaire. Ils ont aussi un poids financier qui leur permet
de sponsoriser de nombreux événements autour de PostgreSQL : Postgres Vision.

Crunchy Data offre sa propre version certifiée et finance de nombreux
développements.

De nombreuses autres sociétés dédiées à PostgreSQL existent dans de nombreux
pays. Parmi les sponsors officiels, on peut compter Cybertec en Autriche ou
Redpill Linpro en Suède. En Russie, PostgresPro maintient une version locale
et reverse aussi ses contributions à la communauté.

En Europe francophone, Dalibo participe pleinement à la communauté. La société
est [Major Sponsor du projet
PostgreSQL](https://www.postgresql.org/about/sponsors/). Elle développe et
maintient plusieurs outils plébiscités par la communauté, comme par exemple
Temboard ou postgresql_anonymizer, avec de nombreux autres projets en cours, et
une participation active au développement de patchs pour PostgreSQL.  Elle
sponsorise également des événements comme les PGDay français et européens,
ainsi que la communauté francophone. [Plus
d'informations](https://www.dalibo.org/contributions).

Des sociétés comme Timescale proposent ou ont proposé leur version
dérivée mais « jouent le jeu » et participent au développement de la version
communautaire, notamment en cherchant à ce que leur produit n'en diverge pas.

Ont également contribué à PostgreSQL nombre de sociétés non centrées autour
des bases de données.

Entre 2006 et 2016, le système d'exploitation Unix Solaris 10 de Sun
embarquait PostgreSQL dans sa distribution de base, comme base de données de
référence. Cela a pris fin avec le rachat par Oracle, sans que cela ait
représenté un danger pour PostgreSQL.

NTT a financé de nombreux patchs pour PostgreSQL, notamment liés à la
réplication et [inclus dans la version de la communauté depuis la
9.0](https://wiki.postgresql.org/wiki/Streaming_Replication).

Fujitsu a participé à de nombreux développements aux débuts de PostgreSQL.

VMWare a longtemps employé le développeur finlandais Heikki Linnakangas.
Celui-ci travaille à présent pour Pivotal mais VMWare peut compter sur Michael
Paquier.

Red Hat a longtemps employé Tom Lane à plein temps pour travailler sur
PostgreSQL. Il a pu dédier une très grande partie de son temps de travail à ce
projet, bien qu'il ait eu d'autres affectations au sein de Red Hat. Tom Lane a
travaillé également chez SalesForce, ensuite il a rejoint Crunchy Data
Solutions fin 2015.

Skype a offert  un certain nombre d'outils très intéressants : pgBouncer
(pooler de connexion), Londiste (réplication par trigger), etc. Ce sont des
outils utilisés en interne et publiés sous licence BSD comme retour à la
communauté. Le rachat par Microsoft n'a pas affecté le développement de ces
outils.

Des sociétés liées au cloud comme Conova (Autriche), Heroku ou Rackspace
(États-Unis) figurent aussi parmi les sponsors.

</div>



-----


### Références

<div class="slide-content">

  * Météo France
  * IGN
  * RATP, SNCF, Autolib
  * CNAF
  * MAIF, MSA
  * Le Bon Coin
  * Doctolib
  * Air France-KLM
  * Société Générale
  * Carrefour, Leclerc, Leroy Merlin
  * Instagram, Zalando, TripAdvisor
  * Yandex
  * ...et plein d'autres

</div>

<div class="notes">

Météo France utilise PostgreSQL depuis plus d'une décennie pour l'essentiel de
ses bases, dont des instances critiques de plusieurs téraoctets ([témoignage
sur postgresql.fr](https://www.postgresql.fr/temoignages/meteo_france)).

L'IGN utilise PostGIS et PostgreSQL [depuis
2006](http://postgis.refractions.net/documentation/casestudies/ign/).

La RATP a fait ce choix depuis [2007
également](https://www.journaldunet.com/solutions/dsi/1013631-la-ratp-integre-postgresql-a-son-systeme-d-information/).

La Caisse Nationale d'Allocations Familiales [a remplacé ses mainframes par
des instances
PostgreSQL](https://www.silicon.fr/cnaf-debarrasse-mainframes-149897.html?inf_by=5bc488a1671db858728b4c35)
dès 2010 (4 To et 1 milliard de requêtes par jour).

Instagram utilise [PostgreSQL depuis le
début](https://media.postgresql.org/sfpug/instagram_sfpug.pdf).

Zalando [a décrit plusieurs fois son infrastructure
PostgreSQL](http://gotocon.com/dl/goto-berlin-2013/slides/HenningJacobs_and_ValentineGogichashvili_WhyZalandoTrustsInPostgreSQL.pdf)
et [annonçait en
2018](https://www.postgresql.eu/events/pgconfeu2018/schedule/session/2135-highway-to-hell-or-stairway-to-cloud/)
utiliser pas moins de 300 bases de données en interne et 650 instances dans un
cloud AWS. Zalando contribue à la communauté, notamment par son outil de haute
disponibilité
[patroni](https://jobs.zalando.com/tech/blog/zalandos-patroni-a-template-for-high-availability-postgresql/).

Le DBA de TripAdvisor témoigne de leur utilisation de PostgreSQL dans
l'[interview
suivante](https://www.citusdata.com/blog/25-terry/285-matthew-kelly-tripadvisor-talks-about-pgconf-silicon-valley).

Dès 2009, [Leroy Merlin migrait vers PostgreSQL des milliers de logiciels de
caisse](https://wiki.postgresql.org/images/6/63/Adeo_PGDay.pdf).

Yandex, équivalent russe de Google a décrit en 2016 la [migration des 300 To
de données de Yandex.Mail depuis Oracle vers
PostgreSQL](https://www.pgcon.org/2016/schedule/attachments/426_2016.05.19%20Yandex.Mail%20success%20story.pdf).

La Société Générale a [publié son outil de migration d'Oracle à
PostgreSQL](https://github.com/societe-generale/code2pg).

Autolib à Paris utilisait PostgreSQL. Le logiciel est encore utilisé dans les
autres villes où le service continue. [Ils ont décrit leur infrastructure au
PG Day 2018 à Marseille](https://www.youtube.com/watch?v=vd8B7B-Zca8).

De nombreuses autres sociétés participent au [Groupe de Travail
Inter-Entreprises de
PostgresSQLFr](https://www.postgresql.fr/entreprises/accueil) : Air France,
Carrefour, Leclerc, le CNES, la MSA, la MAIF, PeopleDoc, EDF...

Cette liste ne comprend pas les innombrables sociétés qui n'ont pas communiqué
sur le sujet. PostgreSQL étant un logiciel libre, il n'existe nulle part de
dénombrement des instances actives.

</div>

-----

#### Le Bon Coin

<div class="slide-content">

  * Site de petites annonces
  * 4è site le plus consulté en France (2017)
  * 27 millions d'annonces en ligne, 800 000 nouvelles chaque jour
  * Instance PostgreSQL principale : 3 To de volume, 3 To de RAM
  * 20 serveurs secondaires

</div>

<div class="notes">

PostgreSQL tient la charge sur de grosses bases de données et des serveurs de
grande taille.

Le Bon Coin privilégie des serveurs physiques dans ses propres datacenters.

Pour plus de détails et l'évolution de la configuration,
voir les témoignages de ses directeurs
[technique](https://www.postgresql.fr/temoignages:le_bon_coin) (témoignage de
juin 2012) et
[infrastructure](https://www.kissmyfrogs.com/jean-louis-bergamo-leboncoin-ce-qui-a-ete-fait-maison-est-ultra-performant/)
(juin 2017), ou la conférence de son DBA Flavio Gurgel
au [pgDay Paris 2019](https://www.postgresql.eu/events/pgdayparis2019/schedule/session/2376-large-databases-lots-of-servers-on-premises-on-the-cloud-get-them-all/).

Ce dernier s'appuie sur les outils classiques fournis par la communauté :
pg_dump (pour archivage, car ses exports peuvent être facilement restaurés),
barman, pg_upgrade.

</div>

-----

### Doctolib

<div class="slide-content">

  * Site de rendez-vous médicaux en ligne
  * Base transactionnelle de 3 To (2018)
    * SSD chiffrés, full NVMe, RAID 50
  * 25 millions de visites/mois
  * pointes à 40 000 commits/s
  * 6 serveurs répartis sur 2 sites avec réplication en _streaming_
  * Primaire avec 4x18 cœurs (144 _threads_), 512 Go de RAM
  * Scaling horizontal sur un seul secondaire

</div>

<div class="notes">

Doctolib est le site de rendez-vous médicaux en ligne dominant sur le marché
français, couvrant 65 000 professionnels de santé, 1300 cliniques, hôpitaux et
centres de santé, et des millions de patients.

Une seule instance primaire PostgreSQL suffit, sans partitionnement, épaulée
par une instance secondaire qui assure une fraction du trafic web, et 4 autres
secondaires pour d'autres tâches.

Le RAID50 est un compromis entre le RAID 5 (pénalisant pour les écritures
à cause du calcul de parité) et le RAID 10 (trop consommateur d'espace disque).

</div>

-----

#### Zalando

<div class="slide-content">
Site de ventes par correspondances :

  * Bases transactionnelles de plus de 5 To
  * 90 instances principales
  * 800 serveurs Tomcat
  * Serveurs HP G8
  * Utilisation du partitionnement 

</div>


<div class="notes">

[Présentation](https://www.slideshare.net/try_except_/goto-2013whyzalandotrustsinpostgre-sql20131018) 
</div>

-----

## Bibliographie

<div class="slide-content">

  * Documentation officielle (préface)
  * Articles fondateurs de M. Stonebracker
  * Présentation du projet PostgreSQL
</div>


<div class="notes">


« Préface : 2. [Bref historique de
PostgreSQL](http://docs.postgresqlfr.org/16/history.html) ». PGDG, 2013


« [The POSTGRES™ data model](http://db.cs.berkeley.edu/papers/ERL-M85-95.pdf) ».
Rowe and Stonebraker, 1987


« [Présentation du projet PostgreSQL](https://public.dalibo.com/exports/conferences/_archives/_2008/200807_presentation_postgresql/presentationPG.pdf) ».
Guillaume Lelarge, 2008


**Iconographie** :

La photo initiale est le [logo officiel de PostgreSQL](
http://wiki.postgresql.org/wiki/Trademark_Policy).
</div>

-----

### Serveurs

<div class="slide-content">

  * Site officiel : <http://www.postgresql.org/>
  * La doc : <http://www.postgresql.org/docs/>
  * Actualité : <http://planet.postgresql.org/>
  
</div>


<div class="notes">


Le site officiel de la communauté se trouve sur <http://www.postgresql.org/>. Ce
site contient des informations sur PostgreSQL, la documentation des versions
maintenues, les archives des listes de discussion, etc.

Le site « Planet PostgreSQL » est un agrégateur réunissant les blogs des 
_core hackers_, des contributeurs, des traducteurs et des utilisateurs de
PostgreSQL.

</div>

-----

### Serveurs francophones

<div class="slide-content">

  * Site officiel : <http://www.postgresql.fr/>
  * La doc  : <http://docs.postgresql.fr/>
  * Forum : <http://forums.postgresql.fr/>
  * Actualité : <https://www.postgresql.fr/planete>
  * Wiki : <http://wiki.postgresql.fr/>
</div>


<div class="notes">

Le site postgresql.fr est le site de l'association des utilisateurs francophones
du logiciel. La communauté francophone se charge de la traduction de toutes les
documentations.

</div>

-----

## La console psql

<div class="slide-content">

  * Un outil simple pour
    * les opérations courantes,
    * les tâches de maintenance,
    * les tests.

```
postgres$ psql
  base=#
```


</div>


<div class="notes">

La console `psql` permet d'effectuer l'ensemble des tâches courantes d'un
utilisateur de bases de données. Si ces tâches peuvent souvent être
effectuées à l'aide d'un outil graphique, la console présente l'avantage de
pouvoir être utilisée en l'absence d'environnement graphique ou de scripter
les opérations à effectuer sur la base de données.

Nous verrons également qu'il est possible d'administrer la base de données
depuis cette console.

Enfin, elle permet de tester l'activité du serveur, l'existence d'une base, la
présence d'un langage de programmation...

</div>



-----


### Obtenir de l'aide et quitter

<div class="slide-content">

  * Obtenir de l'aide sur les commandes internes `psql`
    * `\? [motif]`
  * Obtenir de l'aide sur les ordres SQL
    * `\h [motif]`
  * Quitter
    * `\q` ou `ctrl-D`
</div>


<div class="notes">



`\h [NOM]` affiche l'aide en ligne des commandes SQL. Sans argument, la liste
des commandes disponibles est affichée.

**Exemple :**

```
postgres=# \h savepoint
Command:     SAVEPOINT
Description: define a new savepoint within the current transaction
Syntax:
SAVEPOINT savepoint_name
```

`\q` ou `Ctrl-D` permettent de quitter la console.

</div>


-----


### Gestion de la connexion

<div class="slide-content">

  * Obtenir des informations sur la connexion courante:
    * `\conninfo`
  * Se connecter à une autre base
    * `\connect [DBNAME|- USER|- HOST|- PORT|-]`
    * `\c [DBNAME|- USER|- HOST|- PORT|-]`
  * Modifier le mot de passe d'un utilisateur
    * `\password [USERNAME]`
</div>


<div class="notes">

`\c` permet de changer d'utilisateur et/ou de base de données sans
quitter le client.

**Exemple :**

```
postgres@serveur_pg:~$ psql -q postgres
postgres=# \c formation stagiaire1
You are now connected to database "formation" as user "stagiaire1".
formation=> \c - stagiaire2
You are now connected to database "formation" as user "stagiaire2".
formation=> \c prod admin
You are now connected to database "prod" as user "admin".
```

Le gros intérêt de `\password` est d'envoyer le mot de passe
chiffré au serveur. Ainsi, même si les traces contiennent toutes les requêtes
SQL exécutées, il est impossible de retrouver les mots de passe via le fichier
de traces. Ce n'est pas le cas avec un `CREATE USER` ou un `ALTER USER` (à
moins de chiffrer soi-même le mot de passe).
</div>



-----


### Gestion de l'environnement système

<div class="slide-content">

  * Chronométrer les requêtes
    * `\timing`
  * Exécuter une commande OS
    * `\! [COMMAND]`
  * Changer de répertoire courant
    * `\cd [DIR]`
</div>


<div class="notes">


`\timing` active le chronométrage des commandes. Il accepte un argument
indiquant la nouvelle valeur (soit on, soit off). Sans argument, la valeur
actuelle est basculée.

`\! [COMMANDE]` ouvre un shell interactif en l'absence d'argument ou exécute la
commande indiquée.

`\cd` permet de changer de répertoire courant. Cela peut s'avérer utile lors
de lectures ou écritures sur disque.
</div>



-----


### Catalogue système: objets utilisateurs

<div class="slide-content">

  * Lister les bases de données
    * `\l`
  * Lister les schémas
    * `\dn`
  * Lister les fonctions
    * `\df[+] [motif]`
</div>


<div class="notes">


Ces commandes permettent d'obtenir des informations sur les objets
utilisateurs : tables, index, vues, séquences, fonctions, agrégats,
etc. stockés dans la base de données.

Pour les commandes qui acceptent un motif, celui-ci permet de restreindre les
résultats retournés à ceux dont le nom d'opérateur
correspond au motif précisé.

`\l[+]` dresse la liste des bases de données sur le serveur. Avec `+`, les
commentaires et les tailles des bases sont également affichés.

`\d[+] [motif]` permet d'afficher la liste des tables de la base lorsqu'aucun
motif n'est indiqué. Dans le cas contraire, la table précisée est décrite.
Le `+` permet d'afficher également les commentaires associés aux tables ou aux
lignes de la table, ainsi que la taille de chaque table.

`\db [motif]` dresse la liste des tablespaces actifs sur le serveur.

`\d{t|i|s|v}[S] [motif]` permet d'afficher respectivement :


  * la liste des tables de la base active ;
  * la liste des index ;
  * la liste des séquences ;
  * la liste des vues ;
  * la liste des tables systèmes.


`\da` dresse la liste des fonctions d'agrégats.

`\df` dresse la liste des fonctions.

</div>



-----


### Catalogue système: rôles et accès

<div class="slide-content">

  * Lister les rôles
    * `\du[+]`
  * Lister les droits d'accès
    * `\dp`
  * Lister les droits d'accès par défaut
    * `\ddp`
  * Lister les configurations par rôle et par base
    * `\drds`
</div>


<div class="notes">

L'ensemble des informations concernant les objets, les utilisateurs, les
procédures... sont accessibles par des commandes internes débutant par `\d`.

Pour connaître les rôles stockés en base, cette commande est `\du` (u pour
user) ou `\dg` (g pour group). Dans les versions antérieures à la 8.0, les
groupes et les utilisateurs étaient deux notions distinctes. Elles sont
aujourd'hui regroupées dans une notion plus générale, les rôles.

Les droits sont accessibles par les commandes `\dp` (p pour permissions) ou
`\z`.

La commande `\ddp` permet de connaître les droits accordés par
défaut à un utilisateur sur les nouveaux objets avec l'ordre `ALTER DEFAULT
PRIVILEGES`.

```
cave=# \ddp
             Default access privileges
  Owner  | Schema | Type  |    Access privileges
---------+--------+-------+-------------------------
 caviste |        | table | caviste=arwdDxt/caviste+
         |        |       | u1=r/caviste
(1 row)
```

Enfin, la commande `\drds` permet d'obtenir la liste des paramètres
appliqués spécifiquement à un utilisateur ou une base de données.

```
cave=# \drds
                List of settings
  Role   | Database |          Settings
---------+----------+----------------------------
 caviste |          | maintenance_work_mem=256MB
         | cave     | work_mem=32MB
(2 rows)
```


</div>



-----


### Catalogue système: tablespaces et extensions

<div class="slide-content">

  * Lister les tablespaces
    * `\db`
  * Lister les extensions
    * `\dx`
</div>


<div class="notes">

</div>


-----


### Visualiser le code des objets

<div class="slide-content">

  * Code d'une vue
    * `\sv`
  * Code d'une procédure stockée
    * `\sf`
</div>


<div class="notes">

Ceci permet de visualiser le code de certains objets sans avoir besoin de
l'éditer.

</div>

-----


### Exécuter des requêtes

<div class="slide-content">

  * Exécuter une requête
    * terminer une requête par `;`
    * ou par `\g`
    * ou encore par `\gx`
  * Rappel des requêtes:
    * flèche vers le haut
    * `ctrl-R` suivi d'un extrait de texte représentatif
</div>


<div class="notes">


Sauf si `psql` est exécuté avec l'option `-S` (mode _single-line_), toutes les
requêtes SQL doivent se terminer par `;` ou, pour marquer la parenté de
PostgreSQL avec Ingres, `\g`.

En version 10, il est aussi possible d'utiliser `\gx` pour avoir l'action
conjuguée de `\g` (ré-exécution de la requête) et de `\x` (affichage étendu).

La console `psql`, lorsqu'elle est compilée avec la bibliothèque libreadline ou
la bibliothèque libedit (cas des distributions Linux courantes),
dispose des mêmes possibilités de rappel de commande que le shell bash.

</div>

-----


### Afficher le résultat d'une requête

<div class="slide-content">

  * Affichage par paginateur si l'écran ne suffit pas
  * `\x` pour afficher un champ par ligne
</div>


<div class="notes">

En mode interactif, `psql` cherche d'abord à afficher directement le résultat :
```
postgres=# SELECT relname,reltype, relchecks, oid,oid FROM pg_class LIMIT 5;
              relname              | reltype | relchecks | oid  | oid
-----------------------------------+---------+-----------+------+------
 pg_statistic                      |   11258 |         0 | 2619 | 2619
 pg_type                           |      71 |         0 | 1247 | 1247
 pg_toast_2604                     |   11515 |         0 | 2830 | 2830
 pg_toast_2604_index               |       0 |         0 | 2831 | 2831
 pg_toast_2606                     |   11516 |         0 | 2832 | 2832
```

S'il y a trop de colonnes, on peut préférer n'avoir qu'un champ par ligne 
grâce au commutateur `\x` :
```
postgres=# \x on
postgres=# SELECT relname,reltype, relchecks, oid FROM pg_class LIMIT 3;
-[ RECORD 1 ]------------------
relname   | pg_statistic
reltype   | 11258
relchecks | 0
oid       | 2619
-[ RECORD 2 ]------------------
relname   | pg_type
reltype   | 71
relchecks | 0
oid       | 1247
-[ RECORD 3 ]------------------
relname   | pg_toast_2604
reltype   | 11515
relchecks | 0
oid       | 2830
```

`\x on` et `\x off` sont alternativement appelés si l'on tape plusieurs fois
`\x`.  `\x auto` délègue à `psql` la décision du meilleur affichage, en général
à bon escient.

S'il n'y a pas la place à l'écran, `psql` appelle le paginateur par défaut du
système.  Il s'agit souvent de `more`, parfois de `less`. Ce dernier est bien
plus puissant, permet de parcourir le résultat en avant et en arrière, avec la
souris, de chercher une chaîne de caractères, de tronquer les lignes trop
longues (avec l'option `-S`) pour naviguer latéralement.

Le paramétrage du paginateur s'effectue par des variables d'environnement :
```bash
export PAGER='less -S'
psql
```
ou :
```bash
PAGER='less -S'
```
ou dans `psql` directement, ou `.psqlrc` :
```
\setenv PAGER 'less -S'
```

</div>

-----


### Entrées/sorties

<div class="slide-content">

  * Charger et exécuter un script SQL
    * `\i FICHIER`
  * Rediriger la sortie dans un fichier
    * `\o FICHIER`
  * Écrire un texte sur la sortie standard
    * `\echo texte...`
  * Écrire un texte dans le fichier
    * `\qecho texte...`
</div>


<div class="notes">


`\i FICHIER` lance l'exécution des commandes placées dans le fichier passé en
argument. `\ir` fait la même chose sauf que le chemin est relatif au chemin
courant.

`\o [FICHIER | |COMMANDE]` envoie les résultats de la requête vers le fichier
indiqué ou vers la commande UNIX au travers du tube.

**Exemple :**

```
postgres=# \o |a2ps
postgres=# SELECT ... ;
```

`\echo [TEXTE]` affiche le texte passé en argument sur la sortie
standard.

`\qecho [TEXTE]` offre le même fonctionnement que `\echo [TEXTE]`, à ceci
près que la sortie est dirigée sur la sortie des requêtes (fixée par `\o [
FICHIER]`) et non sur la sortie standard.
</div>



-----


### Exécuter un script SQL avec psql

<div class="slide-content">

  * Exécuter un seul ordre SQL
    * `-c "ordre SQL"`
  * Spécifier un script SQL en ligne de commande
    * `-f nom_fichier.sql`
  * Possible de les spécifier plusieurs fois
    * exécutés dans l'ordre d'apparition
  * Charger et exécuter un script SQL depuis psql
    * `\i nom_fichier.sql`
</div>


<div class="notes">


L'option `-c` permet de spécifier la requête SQL en ligne de commande.

Il est cependant souvent préférable de les enregistrer dans des fichiers si on
veut les exécuter plusieurs fois sans se tromper. L'option `-f` est très utile
dans ce cas.

</div>


-----

### Gestion des transactions

<div class="slide-content">

  * `psql` est en mode auto-commit par défaut
    * variable `AUTOCOMMIT`
  * Ouvrir une transaction explicitement
    * `BEGIN;`
  * Terminer une transaction
    * `COMMIT;` ou `ROLLBACK;`
  * Ouvrir une transaction implicitement
    * option `-1` ou `--single-transaction`
</div>


<div class="notes">


Par défaut, `psql` est en mode auto-commit, c'est-à-dire que tous les ordres
SQL sont automatiquement validés après leur exécution.

Pour exécuter une suite d'ordres SQL dans une seule et même transaction, il
faut soit ouvrir explicitement une transaction avec `BEGIN;` et la valider avec
`COMMIT;` ou l'annuler avec `ROLLBACK;`.

Une autre possibilité est d'utiliser `psql -1` ou `psql --single-transation`
afin d'ouvrir une transaction avant
le début de l'exécution du script et de faire un `COMMIT` implicite à la fin
de l'exécution du script, ou un `ROLLBACK` explicite le cas échéant. La
présence d'ordres `BEGIN`, `COMMIT` ou `ROLLBACK` modifiera ce comportement
en conséquence.
</div>



-----


### Gestion des erreurs

<div class="slide-content">

  * Ignorer les erreurs dans une transaction
    * `ON_ERROR_ROLLBACK`
  * Gérer des erreurs SQL en shell
    * `ON_ERROR_STOP`
</div>


<div class="notes">


La variable interne `ON_ERROR_ROLLBACK` n'a de sens que si elle est utilisée
dans une transaction. Elle peut prendre trois valeurs :

  * `off` (défaut) ;
  * `on` ;
  * `interactive`.

Lorsque `ON_ERROR_ROLLBACK` est à `on`, psql crée un `SAVEPOINT`
systématiquement avant d'exécuter une requête SQL. Ainsi, si la requête SQL
échoue, psql effectue un `ROLLBACK TO SAVEPOINT` pour annuler cette requête.
Sinon il relâche le `SAVEPOINT`.

Lorsque `ON_ERROR_ROLLBACK` est à `interactive`, le comportement de psql est le
même seulement si il est utilisé en interactif. Si psql exécute un script, ce
comportement est désactivé. Cette valeur permet de se protéger d'éventuelles
fautes de frappe.

Utiliser cette option n'est donc pas neutre, non seulement en terme de
performances, mais également en terme d'intégrité des données. Il ne faut
donc pas utiliser cette option à la légère.

Enfin, la variable interne `ON_ERROR_STOP` a deux objectifs : arrêter
l'exécution d'un script lorsque psql rencontre une erreur et retourner un code
retour shell différent de 0. Si cette variable reste à `off`, psql retournera
toujours la valeur 0 même s'il a rencontré une erreur dans l'exécution d'une
requête. Une fois activée, psql retournera un code d'erreur 3 pour signifier
qu'il a rencontré une erreur dans l'exécution du script.

L'exécution d'un script qui comporte une erreur retourne le code 0, signifiant
que psql a pu se connecter à la base de données et exécuté le script :

```
$ psql -f script_erreur.sql postgres
psql:script_erreur.sql:1: ERROR:  relation "vin" does not exist
LINE 1: SELECT * FROM vin;
                      ^
$ echo $?
0
```


Lorsque la variable `ON_ERROR_STOP` est activée, psql retourne un code erreur
3, signifiant qu'il a rencontré une erreur

```
$ psql -v ON_ERROR_STOP=on -f script_erreur.sql postgres
psql:script_erreur.sql:1: ERROR:  relation "vin" does not exist
LINE 1: SELECT * FROM vin;
                      ^
$ echo $?
3
```

psql retourne les codes d'erreurs
suivant au shell :


  * 0 au shell s'il se termine normalement ;
  * 1 s'il y a eu une erreur fatale de son fait (pas assez de mémoire, fichier
introuvable) ;
  * 2 si la connexion au serveur s'est interrompue ou arrêtée ;
  * 3 si une erreur est survenue dans un script et si la variable `ON_ERROR_STOP` a
été initialisée.
</div>



-----


### Formatage des résultats

<div class="slide-content">

  * Afficher des résultats non alignés
    * `-A | --no-align`
  * Afficher uniquement les lignes
    * `-t | --tuples-only`
  * Utiliser le format étendu
    * `-x | --expanded`
  * Utiliser une sortie au format HTML
    * `-H | --html`
  * Positionner un attribut de tableau HTML
    * `-T TEXT | --table-attr TEXT`
</div>


<div class="notes">

</div>



-----


### Séparateurs de champs

<div class="slide-content">

  * Modifier le séparateur de colonnes
    * `-F CHAINE | --field-separator CHAINE`
  * Forcer un octet 0x00 comme séparateur de colonnes
    * `-z | --field-separator-zero`
  * Modifier le séparateur de lignes
    * `-R CHAINE | --record-separator CHAINE`
  * Forcer un octet 0x00 comme séparateur de lignes
    * `-0 | --record-separator-zero`
</div>


<div class="notes">


`-F CHAINE` permet de modifier le séparateur de champ. Par défaut, il s'agit du
caractère '|'. Cette option ne fonctionne qu'utilisée conjointement au
modificateur de non-alignement des champs.

**Exemple :**

```
postgres@serveur_pg:~$ psql -F';' -A
postgres=# \l
List of databases
Name;Owner;Encoding;Collate;Ctype;Access privileges
b1;postgres;UTF8;C;C;
cave;caviste;UTF8;C;C;
module_C1;postgres;UTF8;C;C;
postgres;postgres;UTF8;C;C;
prod;postgres;UTF8;C;C;
template0;postgres;UTF8;C;C;=c/postgres
postgres=CTc/postgres
template1;postgres;UTF8;C;C;=c/postgres
postgres=CTc/postgres
(7 rows)
```

`-R CHAINE` permet de modifier le séparateur d'enregistrements. il s'agit par
défaut du retour chariot. Ce commutateur ne fonctionne qu'utilisé conjointement
au mode non-aligné de sortie des enregistrements.

**Exemple :**

```
postgres@serveur_pg:~$ psql -R' #Mon_séparateur# ' -A
postgres=# \l
List of databases #Mon_séparateur# Name|Owner|Encoding|Collate|Ctype|
Access privileges #Mon_séparateur# b1|postgres|UTF8|C|C| #Mon_séparateur# cave|
caviste|UTF8|C|C| #Mon_séparateur# module_C1|postgres|UTF8|C|C| #Mon_séparateur#
postgres|postgres|UTF8|C|C| #Mon_séparateur# template0|postgres|UTF8|C|C|
=c/postgres postgres=CTc/postgres #Mon_séparateur# template1|postgres|UTF8|C|C|
=c/postgres postgres=CTc/postgres #Mon_séparateur# (7 rows)
```

Les options `-z` et `-0` modifient le caractère nul comme
séparateur de colonne et de ligne.
</div>

-----

\newpage
