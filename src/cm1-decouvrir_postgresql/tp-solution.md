
<div class="notes">

### Solutions du TP 1

#### Découverte de l'instance

1. Processus ID : 1224

```bash
$ systemctl status postgresql@15-main.service
* postgresql@15-main.service - PostgreSQL Cluster 15-main
     Loaded: loaded (/lib/systemd/system/postgresql@.service; enabled-runtime; vendor preset: enabled)
     Active: active (running) since Sun 2023-10-01 21:19:31 CEST; 11min ago
    Process: 45874 ExecStart=/usr/bin/pg_ctlcluster --skip-systemctl-redirect 15-main start (code=exited, status=0/SUCCESS)
   Main PID: 45879 (postgres)
      Tasks: 6 (limit: 9510)
     Memory: 19.1M
        CPU: 196ms
     CGroup: /system.slice/system-postgresql.slice/postgresql@15-main.service
             |-45879 /usr/lib/postgresql/15/bin/postgres -D /var/lib/postgresql/15/main -c config_file=/etc/postgresql/15/main/>
             |-45880 postgres: 15/main: checkpointer
             |-45881 postgres: 15/main: background writer
             |-45883 postgres: 15/main: walwriter
             |-45884 postgres: 15/main: autovacuum launcher
             |-45885 postgres: 15/main: logical replication launcher

oct. 01 21:19:29 tp-bdd systemd[1]: Starting PostgreSQL Cluster 15-main...
oct. 01 21:19:31 tp-bdd systemd[1]: Started PostgreSQL Cluster 15-main.
```

2. Ce répertoire se nomme usuellement _PGDATA_. Le fichier `PG_VERSION`
   contient la version de l'instance.

3. Le fichier `pg_hba.conf` (_host-based authentication_) contrôle les droits
   d'accès à l'instance.

4. Le port d'écoute est celui par défaut : 5432. Il y a plusieurs moyen pour le
   récupérer :
     * la commande `netstat -anp | grep postgres` liste les ports ouverts. On
       recherche dans les résultats le processus ayant le PID trouvé plus haut.
	 * la commande `psql -c 'SHOW port;'` va renvoyer l'information du port de
       l'instance par défaut.

5. L'utilisateur postgres n'est pas sudoer. Il faut soit retourner avec
   l'utilisateur root, soit lancer la commande `ps -o pid,cmd fx`

```bash
    PID CMD
  50890 ps -o pid,cmd fx
  50834 /usr/lib/postgresql/15/bin/postgres -D /var/lib/postgresql/15/main -c config_file=/etc/postgresql/15/main/postgresql.con
  50835  \_ postgres: 15/main: checkpointer 
  50836  \_ postgres: 15/main: background writer 
  50839  \_ postgres: 15/main: walwriter 
  50840  \_ postgres: 15/main: autovacuum launcher 
  50841  \_ postgres: 15/main: logical replication launcher 
  50833 /usr/lib/postgresql/13/bin/postgres -D /var/lib/postgresql/13/main -c config_file=/etc/postgresql/13/main/postgresql.con
  50842  \_ postgres: 13/main: checkpointer 
  50843  \_ postgres: 13/main: background writer 
  50844  \_ postgres: 13/main: walwriter 
  50845  \_ postgres: 13/main: autovacuum launcher 
  50846  \_ postgres: 13/main: stats collector 
  50847  \_ postgres: 13/main: logical replication launcher 
```

------

#### psql

1. `psql -d postgres` (avec l'utilisateur _postgres_ on obtient le même
   résultat avec la commande `psql`)

2. `\dS`

3. `\q` ou `<CRTL>+D`

4. On peut utiliser la commande _bash_ qui est l'équivalent de l'ordre SQL
   `CREATE DATABASE`

```bash
$ createdb mondial
```

5. `psql`

6. `\c mondial`

7. `\! wget -O schema_mondial.sql https://tribu-ml.fr/lstu/tp_mondial_schema`

8. `\i <chemin vers le fichier du schéma mondial>`

9. `\d`

10. La table est vide (la commande est `SELECT * FROM city`).

11. `\! wget -O data_mondial.sql https://tribu-ml.fr/lstu/tp_mondial_data`

12. `\i <chemin vers le fichier des données mondial>`

13. La table n'est plus vide.

14. `SHOW shared_buffers;`

15. Éditer le fichier _/etc/postgresql/15/main/postgresql.conf_. Fixer le
    paramètre _shared_buffers_ à `512MB`.

16. Il est noté dans le fichier de configuration que le changement nécessite un
    redémarrage de l'instance (_change requires restart_). L'instance doit être
    redémarrée par l'utilisateur root avec la commande :  
	`systemctl restart postgresql@15-main.service`

	
17. méthode via le terminal :

```bash
postgres$ createuser -P monde
```

méthode via psql :

```sql
CREATE ROLE monde LOGIN PASSWORD 'secret';
```

18. Ajouter la ligne suivante à votre fichier `pg_hba.conf` :

`host    mondial             monde             <IP workstation>/32       md5`

Recharger la configuration de votre instance via bash :

```bash
# systemctl reload postgresql
```

Ou via la console `psql` avec la commande SQL :

```sql
postgres=# SELECT pg_reload_conf();
 pg_reload_conf
----------------
 t
(1 ligne)
```

Cela ne suffit cepandant pas car votre serveur PostgreSQL n'écoute que sur
_localhost_. Il vous faut mettre à jour le paramètre `listen_addresses` de
votre fichier _postgresql.conf_. Le plus simple est de le fixer à `'*'`. Vous
devez alors redémarrer l'isntance (en tant que _root_) :  
`systemctl restart postgresql@15-main.service`

Puis vous pouvez vous connecter depuis votre poste de travail :


```bash
$ psql -h <IP VM> -U monde mondial
```

19. Insérer les lignes suivantes dans le fichier `~/.pgpass` sur votre machine
    hôte :

```
#hostname:port:database:username:password
*:5432:mondial:monde:secret
```

Changer les droits pour ce fichier :

```bash
$ chmod 600 ~/.pgpass
```

Se connecter à l'instance depuis le poste de travail :

```bash
$ psql -h <IP VM> -U monde mondial
```

20. 2 possibilités :

```sql
mondial=> \\o /tmp/liste_mountain.txt'
mondial=> SELECT  * FROM mountain;
```

Ou :

```sql
mondial=> \copy mountain to '/tmp/liste_mountain.txt'
```

21. `\h CREATE TABLE AS`


22.

```sql
CREATE TABLE largest_city AS
  SELECT c.name country,c.code,c1.name,c1.population
    FROM country c, city c1,
      (SELECT country,MAX(population) AS truc
        FROM city GROUP BY country) c2
    WHERE c1.population>= c2.truc
  	  AND c1.country = c2.country
  		AND c1.country=c.code;
```

23.

```sql
\! pwd
\cd /tmp
\! pwd
```

24.

```bash
postgres$ vi requete.sql
postgres$ psql -f requete.sql mondial
```

</div>

------

\newpage
