
## Travaux Pratiques 1

<div class="slide-content">
  * prise en main de PostgreSQL
</div>

<div class="notes">

Durant ces travaux pratiques, nous allons mettre en place notre serveur de base
de données PostgreSQL via une machine virtuelle.

Effectuez les manipulations nécessaires pour réaliser les actions listées dans
la section _Énoncés_.

Lorsque les commandes commencent par un dollar (`$`), il s'agit de commande à
exécuter dans votre terminal bash avec votre utilisateur courant, soit sur la
machine hôte (votre ordinateur), soit sur la machine virtuelle à travers une
connexion ssh.  
Si la commande débute par `#`, la commande shell est à lancer avec les droits
super-utilisateur.

Les autres commandes sont des commandes à lancer dans `psql`.

> En cas de difficulté, se rapporter au cours, aux annexes de ce TP ainsi que de
> l'aide en ligne de PostgreSQL ou des pages de manuels (`man`).


### Énoncés

#### Déployement de la machine virtuelle

Nous allons commencer par créer une machine virtuelle.

Si vous êtes dans une des salles de TP à l'université, utiliser la première
méthode : « TP sur place ». Si vous effectuez le TP à distance, rapportez vous
à la seconde méthode.

##### TP sur place

Listons les machines virtuelles disponibles :

```bash
$ virtualbox-createtp -l
```

Sélectionner la machine à créer (il y a _DB_ dans le nom) :

```bash
$ virtualbox-createtp -c TP_bdd 2023-10-01_bullseye_DB_Admin
```

La commande suivante viendra modifier les paramètres de votre machine virtuelle
afin qu'elle utilise 3 CPU et 8 Go de RAM :
```bash
$ vboxmanage modifyvm TP_bdd --cpus 3 --memory 8192
```

Pour lancer votre machine virtuelle, lancer la commande suivante dans votre
terminal :
```bash
$ vboxmanage startvm TP_bdd --type headless
```

> A la fin de votre TP, n'oubliez pas de l'éteindre, elle restera sinon allumée
> et accessible à quiconque se connecte sur la machine que vous avez utilisée.

Vous pouvez éteindre votre machine virtuelle avec la commande :

```bash
$ vboxmanage controlvm TP_bdd acpipowerbutton
```

Vous devriez pouvoir vous connecter via un transfert de port:


```bash
$ ssh -p 2222 tp@127.0.0.1
```

Si cela ne fonctionne pas, vous pouvez également tenter une connection via
l'adresse IP est de votre machine virtuelle. Elle n'est pas toujours la
même. Lancer la commande suivante pour la récupérer :

```bash
$ virtualbox-allocatedips
```

Vous devriez avoir un retour du type :
```bash
TP_bdd -> 192.168.56.102
```

Utilisez l'adresse IP récupérée par la commande ci-dessus pour vous connecter à
votre machine virtuelle en ssh, utilisez la commande suivante :

```bash
$ ssh <login>@<IP_TP_bdd>
```

Les mots de passe sont identiques aux logins : _tp_ et _root_.

Pour vous connecter au compte `postgres`, utilisez la commande à partir du
compte _root_ :

```bash
# sudo -iu postgres
```

Aide disponible sur la [faq](https://faq.info.unicaen.fr/logiciels:virtualbox)

##### TP à distance

Importer la machine virtuelle <https://tribu-ml.fr/lstu/tp_bdd_ova_2023_11>
dans Virtual Box. Renommer la machine virutelle en `TP_bdd`.

La commande suivante viendra modifier les paramètres de votre machine virtuelle
afin qu'elle utilise 3 CPU et 8 Go de RAM :
```bash
$ vboxmanage modifyvm TP_bdd --cpus 3 --memory 8192
```

Pour lancer votre machine virtuelle, lancer la commande suivante dans votre
terminal :
```bash
$ vboxmanage startvm TP_bdd --type headless
```

La commande suivante permet de créer une redirection de port pour permettre une
connexion par ssh à la machine virtuelle depuis votre poste :

```bash
vboxmanage controlvm TP_bdd natpf1 "ssh,tcp,,2222,,22"
```

Vous pouvez éteindre votre machine virtuelle avec la commande :

```bash
$ vboxmanage controlvm TP_bdd acpipowerbutton
```

Une fois la machine virtuelle allumée, vous pourrez vous y connecter en ssh en
utilisant une redirection de port :

```bash
$ ssh -p 2222 tp@127.0.0.1
```

Les mots de passe sont identiques aux logins : _tp_ et _root_.

Pour vous connecter au compte `postgres`, utilisez la commande :

```bash
# sudo -iu postgres
```

-----

#### Découverte de l'instance

Le but de ce TP est de découvrir les différents fichiers de l'instance
PostgreSQL ainsi que les moyens de lancer l'instance.

Effectuez ces manipulation à partir de l'utilisateur _root_.

1. Afficher le statut de l'instance avec la commande :

`systemctl status postgresql@15-main.service`

  * Quel est le PID du processus `postgres` ?

2. Lister les fichiers de votre instances :

`ls /var/lib/postgresql/15/main/`

  * Comment nomme-t-on ce répertoire ?
  * Y a-t-il un moyen de connaitre la version de l'instance ?

3. Lister les fichiers de configuration de votre instance :

`ls /etc/postgresql/15/main/`

  * Quel fichier contrôle les droits d'accès à l'instance ?

4. Quel est le port d'écoute de votre instance ?

5. Lister les processus de votre instances :

`sudo -iu postgres ps -o pid,cmd fx`

------

#### psql

Le but de ce TP est d'acquérir certains automatismes dans l'utilisation de
`psql`.

Effectuez ces manipulation à partir de l'utilisateur _postgres_.

1. Lancez la console en se connectant à la base _postgres_.

2. Affichez l'ensemble des tables systèmes.
 
3. Déconnectez-vous.
 
4. Créer la base de données `mondial`
 
5. Reconnectez-vous à la base _postgres_.
 
6. Sans vous déconnecter, connectez-vous à la base _mondial_.

7. Récupérer le script du schéma _mondial_ :  
`wget -O schema_mondial.sql https://tribu-ml.fr/lstu/tp_mondial_schema`

8. Charger le script que vous venez de récupérer dans la base de données
   _mondial_.

9. Affichez la liste des tables de la base de données.

10. Affichez toutes les lignes de la table `city`. Que remarquez vous ?
   
11. Récupérer le script du schéma _mondial_ :  
`wget -O data_mondial.sql https://tribu-ml.fr/lstu/tp_mondial_data`

12. Charger le script que vous venez de récupérer dans la base de données
   _mondial_.

13. Affichez de nouveaux toutes les lignes de la table `city`. Que remarquez
vous ?

14. Afficher la quantité de mémoire partagée de votre instance (paramètre
`shared_buffers`).

15. Modifier la quantité de mémoire partagée de votre instance (paramètre
`shared_buffers` dans le fichier `postgresql.conf` pour atteindre un quart de
votre RAM (aidez vous du résultat de la commande `free -m`).

16. Afficher de nouveaux la quantité de mémoire partagée de votre instance. Le
    nouveau paramétrage a-t-il été pris en compte ? Dans la négative, trouvez
    un moyen pour que l'instance utilise le nouveau paramétrage.

17. Créer un nouvel utilisateur `monde` avec le droit de connexion et un mot de
passe.

18. Modifier le fichier `pg_hba.conf` pour permettre à l'utilisateur `monde` de
se connecter à la base de données `mondial` avec la méthode d'authentification
`md5` depuis votre poste de travail (votre machine hôte).

19. Créer le fichier `.pgpass` afin de vous connecter sans mot de passe avec
l'utilisateur `monde` depuis votre poste de travail (votre machine hôte).

20. Effectuez une copie de la liste des montagnes dans le fichier
`/tmp/liste_mountain.txt`.

21. Affichez l'aide de la commande permettant de créer une table à partir des
informations contenue dans une autre.

22. Créez une table `largest_city` composée du nom du pays, du code du pays, du
nom de la plus grande ville du pays, de la population de cette ville.

23. Affichez le répertoire courant. Changez le pour `/tmp`. Vérifiez que vous
êtes bien dans ce répertoire.

24. Écrivez un fichier contenant deux requêtes. Exécutez ce fichier dans la base
de données `mondial`.

------

### Annexes

#### Administration via bash

  * `createdb`: ajouter une nouvelle base de données
  * `createuser`: ajouter un nouveau compte utilisateur
  * `dropdb`: supprimer une base de données
  * `dropuser`: supprimer un compte utilisateur

Chacune de ces commandes est un « alias », un raccourci qui permet d'exécuter
des commandes SQL de manière plus simple.

Par exemple, la commande système `dropdb` est équivalente à la commande SQL `
DROP DATABASE`. L'outil `dropdb` se connecte à la base de données nommée `
postgres` et exécute l'ordre SQL. Enfin, elle se déconnecte.

La création d'une nouvelle base se fait en utilisant l'outil `createdb` et en
lui indiquant le nom de la nouvele base. Par exemple, pour créer une base b1,
il faudrait faire ceci :

```bash
postgres$ createdb b1
```

#### Options connexion à PostgreSQL

Tous les logiciels se connectant aux instances PostgreSQL partagent les mêmes
paramètres de connexions.

Les options de connexion permettent de préciser l'utilisateur et la base de
données utilisés pour la connexion initiale.

Lorsque l'une des options n'est pas précisée, la bibliothèque cliente
PostgreSQL vérifie qu'il n'existe pas une variable shell correspondante et
prend sa valeur. S'il ne trouve pas de variable, il utilise une valeur par
défaut.

`-h HOTE` ou `$PGHOST` permet de préciser l'alias ou l'adresse `IP` de la
machine qui héberge le serveur. Sans indication, le client se connecte sur la
socket Unix dans `/tmp`.

`-p PORT` ou `$PGPORT` permet de préciser le port sur lequel le serveur écoute
les connexions entrantes. Sans indication, le port par défaut est le 5432.

`-U NOM` ou `$PGUSER` permet de préciser le nom de l'utilisateur utilisé pour
la connexion. L'option `-U` n'est toutefois pas nécessaire,
sous réserve que les arguments de la ligne de commande respectent l'ordre `
NOM_BASE NOM_UTILISATEUR`. Sans indication, le nom d'utilisateur PostgreSQL est
le nom de l'utilisateur utilisé au niveau système.

`-d base` ou `$PGDATABASE` permet de préciser le nom de la base de données
utilisée pour la connexion. Le drapeau `-d` n'est pas nécessaire sous réserve
que le nom de la base de données est le dernier argument de la ligne de
commande. Sans indication le nom de la base de données de connexion
correspondra au nom de l'utilisateur utilisé au niveau PostgreSQL.

Le tableau suivant résume ce qui est écrit plus haut :

+----------+--------------+-----------------------------+
| option   | variable     | défaut                      |
+==========+==============+=============================+
| -h HOTE  | $PGHOST      | /tmp ou /var/run/postgresql |
+----------+--------------+-----------------------------+
| -p PORT  | $PGPORT      | 5432                        |
+----------+--------------+-----------------------------+
| -U NOM   | $PGUSER      | nom de l'utilisateur OS     |
+----------+--------------+-----------------------------+
| -d base  | $PGDATABASE  | nom de l'utilisateur PG     |
+----------+--------------+-----------------------------+

#### Authentification via `.pgpass`

Le fichier `.pgpass`, situé dans le répertoire personnel de l'utilisateur ou
celui référencé par `$PGPASSFILE`, est un fichier contenant les mots
de passe à utiliser si la connexion requiert un mot de passe (et si aucun mot
de passe n'a été spécifié). 

Ce fichier devra être composé de lignes du format :

```
nom_hote:port:nom_base:nom_utilisateur:mot_de_passe
```

Chacun des quatre premiers
champs pourraient être une valeur littérale ou `*` (qui correspond à tout).
La première ligne réalisant une
correspondance pour les paramètres de connexion sera utilisée (du coup, placez
les entrées plus spécifiques en premier lorsque vous utilisez des
jokers). Si une entrée a besoin de contenir `*` ou `\`, échappez ce caractère
avec `\`. Un nom d'hôte `localhost` correspond à la fois aux
connexions `host` (`TCP`) et aux connexions `local` (`socket` de domaine _Unix_)
provenant de la machine locale.

Les droits sur `.pgpass` doivent interdire l'accès aux autres et au groupe ;
réalisez ceci avec la commande `chmod 0600 ~/.pgpass.` Si les droits du
fichier sont moins stricts, le fichier sera ignoré.

-----

#### Droits de connexion

Lors d'une connexion, indication :

  * de l'hôte (socket Unix ou alias/adresse IP)
  * du nom de la base de données
  * du nom du rôle
  * du mot de passe (parfois optionnel)

Lors d'une connexion, l'utilisateur fournit, explicitement ou non, plusieurs
informations. PostgreSQL va choisir une méthode d'authentification en se basant
sur les informations fournies et sur la configuration d'un fichier appelé
`pg_hba.conf`.

**`pg_hba.conf`**

Se présente sous la forme d'un tableau :

  * 4 colonnes d'informations
	  * type
    * database
    * user
    * adresse
  * 1 colonne indiquant la méthode à appliquer
  * 1 colonne optionnelle d'options

Lorsque le serveur PostgreSQL récupère une demande de connexion, il connaît
le type de connexion utilisé par le client (socket Unix, connexion TCP SSL,
connexion TCP simple, etc). Il connaît aussi l'adresse IP du client (dans le
cas d'une connexion via une socket TCP), le nom de la base et celui de l'
utilisateur. Il va donc parcourir les lignes du tableau enregistré dans le
fichier `pg_hba.conf`. Il les parcourt dans l'ordre. La première qui correspond
aux informations fournies lui précise la méthode d'authentification. Il ne lui
reste plus qu'à appliquer cette méthode. Si elle fonctionne, la connexion est
autorisée et se poursuit. Si elle ne fonctionne pas, quelle qu'en soit la
raison, la connexion est refusée. Aucune autre ligne du fichier ne sera lue.

Il est donc essentiel de bien configurer ce fichier pour avoir une protection
maximale.

Le tableau se présente ainsi :

```
# local      DATABASE  USER  METHOD  [OPTIONS]
# host       DATABASE  USER  ADDRESS  METHOD  [OPTIONS]
# hostssl    DATABASE  USER  ADDRESS  METHOD  [OPTIONS]
# hostnossl  DATABASE  USER  ADDRESS  METHOD  [OPTIONS]
```

La colonne *type* peut contenir quatre valeurs différentes. La valeur `local`
concerne les connexions via la socket Unix. La valeur `host`, concernent
les connexions via la socket TCP. 


La colonne *database* peut recueillir le nom d'une base, le nom de plusieurs
bases en les séparant par des virgules, le nom d'un fichier contenant la liste
des bases ou quelques valeurs en dur. La valeur all indique toutes les bases.


La colonne *user* peut recueillir le nom d'un rôle, le nom d'un groupe en le
précédant d'un signe plus, le nom de plusieurs rôles en les séparant par des
virgules, le nom d'un fichier contenant la liste des bases ou quelques valeurs
en dur. La valeur all indique tous les rôles.

La colonne *adresse* est présente uniquement dans le cas d'une connexion de
type `host`. Cette colonne permet d'indiquer une adresse IP ou un sous-réseau
IP. Il est donc possible de filtrer les connexions par rapport aux adresses IP,
ce qui est une excellente protection.

Voici deux exemples d'adresses IP au format adresse et masque de sous-réseau :

```
192.168.168.1 255.255.255.255
192.168.168.0 255.255.255.0
```

Et voici deux exemples d'adresses IP au format CIDR :

```
192.168.168.1/32
192.168.168.0/24
```

La colonne de `méthode` précise la méthode d'authentification à utiliser. Il
existe 2 types de méthodes, _internes_ ou _externes_.

Méthodes internes à privilégier : `md5`.

Les méthodes _externes_ permettent d'utiliser des annuaires d'entreprise
comme RADIUS, LDAP ou un ActiveDirectory. Certaines méthodes sont spécifiques
à Unix (comme `ident` et `peer`), voire à Linux (comme `pam`).

**Mise à jour des droits de connexion**

Pour permettre la prise en compte des modifications sur le fichier `pg_hba.conf`
il faut recharger la configuration de l'instance. Ceci peut se faire soit via
_bash_ avec la commande :

```bash
# systemctl reload postgresql
```

Ou via la console `psql` avec la commande SQL :

```sql
postgres=# SELECT pg_reload_conf();
 pg_reload_conf 
----------------
 t
(1 ligne)
```


</div>

-----
