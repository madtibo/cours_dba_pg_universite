## Travaux Dirigés 1

<div class="slide-content">
  * Rappels de réseau
  * Rappels de système
  * Préparation du TP
</div>

-----

### Question de cours

<div class="slide-content">

  * Quelle est la licence de PostgreSQL ?
  * Quelle est la dernière version majeure de PostgreSQL ?
  * Quel mot clé permet de valider une transaction ?

</div>

-----

### Rappels de réseau

<div class="slide-content">

  * Client-serveur
  * TCP/IP
  * Port
  * Socket Unix

</div>

<div class="notes">

#### Client-serveur

L'environnement client-serveur désigne un mode de communication à travers un
réseau entre plusieurs programmes : 

  * l'un, qualifié de client, envoie des requêtes ;
  * l'autre ou les autres, qualifiés de serveurs, attendent les requêtes des
    clients et y répondent.

Caractéristiques d'un programme serveur :

  * il attend une connexion entrante sur un ou plusieurs ports réseaux locaux ;
  * à la connexion d'un client sur le port en écoute, il ouvre un socket local
    au système d'exploitation ;
  * à la suite de la connexion, le processus serveur communique avec le client
    suivant le protocole prévu par la couche application du modèle OSI.

Caractéristiques d'un programme client :

  * il établit la connexion au serveur à destination d'un ou plusieurs ports
    réseaux ;
  * lorsque la connexion est acceptée par le serveur, il communique comme le
    prévoit la couche application du modèle OSI.

#### TCP/IP

La suite TCP/IP est l'ensemble des protocoles utilisés pour le transfert des
données sur Internet.

![Les modèles OSI et TCP/IP](medias/common_TCP-IP-model-vs-OSI-model.png)
\

PostgreSQL utilise le protocole TCP pour échanger des données entre le client
et le serveur.  
TCP est un protocole de transport « fiable ». Il fournit un flux d'octets
fiable assurant l'arrivée des données sans altérations et dans l'ordre, avec
retransmission en cas de perte, et élimination des données dupliquées.

#### Port

Nous avons vu qu'un programme serveur attend une connexion entrante sur un ou
plusieurs ports réseaux locaux.

Pour simplifier, on peut considérer les ports comme des portes donnant accès au
système d'exploitation. Pour fonctionner, un programme ouvre des portes pour
entrer dans le système d'exploitation.  
Grâce à cette abstraction, on peut exécuter plusieurs logiciels serveurs sur
une même machine.

Un numéro de port est codé sur 16 bits, ce qui fait qu'il existe un maximum de
2^16 soit 65 536 ports distincts par machine. Ces ports sont classés en 3
catégories en fonction de leur numéro :

  * les numéros de port de 0 à 1 023 correspondent aux ports "bien-connus"
    (well-known ports), utilisés pour les services réseaux les plus
    courants. Sur Linux, ils ne peuvent être ouverts par défaut que par
    l'utilisateur _root_.
  * les numéros de ports de 1 024 à 49 151 correspondent aux ports enregistrés
    (registered ports), assignés par l'IANA.
  * les numéros de ports de 49 152 à 65 535 correspondent aux ports dynamiques,
    utilisables pour tout type de requêtes TCP ou UDP autres que celle citées
    précédemment.

Le port par défaut de PostgreSQL est le 5432.

#### Socket Unix

Les sockets du domaine UNIX utilisent le système de fichiers comme espace de
noms. Cela permet à deux processus d'ouvrir le même socket pour
communiquer. Toutefois, la communication se produit entièrement dans le noyau
du système d'exploitation.

Il est possible d'établir une connexion à l'instance PostgreSQL en local en
utilisant des socket Unix.

</div>

-----

### Rappels de système

<div class="slide-content">

  * Processus
  * systemd
  * Bash
  * psql
  * OpenSSH
  * Éditeur de fichier

</div>

<div class="notes">

#### Bases

un système d'exploitation (souvent appelé OS — de l'anglais _Operating System_)
est un ensemble de programmes qui dirige l'utilisation des ressources d'un
ordinateur par des logiciels applicatifs. Il reçoit des demandes d'utilisation
des ressources de l'ordinateur :

  * ressources de stockage des mémoires : accès à la mémoire vive, aux disques
    durs,
  * ressources de calcul du processeur central,
  * ressources de communication vers des périphériques ou via le réseau.

Le système d'exploitation gère les demandes ainsi que les ressources
nécessaires, évitant les interférences entre les logiciels.

#### Processus

Un processus est un programme en cours d'exécution. Par exemple, chaque fois
que l'on lance la commande `ls`, un processus est créé durant l'exécution de la
commande.

Un processus est identifié par un numéro unique que l'on appelle le **PID**
(Process IDentifiant).

Un processus dispose d'un processus père que l'on appelle le **PPID** (Parent
PID).

On peut lister tous les processus actif d'un serveur grâce à la commande
`ps`. Cette commande possède de nombreuses options. Se rapporter à sa page de
manuel pour plus d'information.

On peut interagir avec un processus à travers des signaux. Ceux-ci offrent un
mécanisme permettant d'envoyer un message à un processus en cours
d'exécution. On se sert généralement des signaux pour terminer un processus,
lui indiquer de relire sa configuration, etc.

#### systemd

systemd est un système d’initialisation et un daemon qui a été spécifiquement
conçu pour le noyau Linux.  
Il est chargé de monter ou démonter les points de montage et de démarrer ou
éteindre l'ensemble des services du serveur.  
C'est une pièce maîtresse de l'architecture GNU/Linux. En effet, c'est le
premier programme lancé par le noyau (il a donc le PID numéro 1) et il se
charge de lancer tous les programmes suivants en ordre jusqu'à obtenir un
système opérationnel pour l'utilisateur, selon le mode déterminé (single user,
multi-user, graphique). C'est également à lui qu'incombe la tache de redémarrer
ou arrêter votre ordinateur proprement.

L'outil de gestion des services dans systemd est `systemctl`. Il est
généralement utilisé dans un terminal.

```bash
systemctl ACTION <Nom_du_service>.service
```

avec _ACTION_ parmi :

  * start : démarrer le service
  * stop : arrêter le service
  * restart : relancer le service
  * reload : recharger le service
  * status : connaitre l'état du service

Et _<Nom_du_service>_ : le nom du service visé.

Dans Debian, chaque instance sera contrôlée par un service avec la convention
de nommage : `postgresql@<version>-<nom_instance>.service`

#### Bash

Bash (acronyme de Bourne-Again shell) est un interpréteur en ligne de commande
de type script. C'est le shell Unix du projet GNU.

Il se présente sous la forme d'une interface en ligne de commande accessible
depuis la console ou un terminal. L'utilisateur lance des commandes sous forme
d'une entrée texte exécutée ensuite par le shell.

Une bonne maîtrise de Bash est primordiale pour administrer et travailler avec
PostgreSQL. La lecture de la page
[wikipedia](https://fr.wikipedia.org/wiki/Bourne-Again_shell), voir du
[man](https://linux.die.net/man/1/bash) est recommandée.

#### Commandes usuelles

Bash fournit de nombreux outils permettant d'interagir avec le serveur. Le
[tutoriel suivant de
Ubuntu](https://doc.ubuntu-fr.org/tutoriel/console_commandes_de_base) vous
présente les commandes de base que vous devez impérativement connaître pour
suivre ce cours.

La meilleure façon d'obtenir des informations sur un outil est d'utiliser son
manuel grâce à la commande `man`.

-----------   ------------  -------------
 `cd`           `ls`          `file`
 `cp`           `mv`          `rm`
 `mkdir`        `pwd`         `ps`
 `top`          `kill`        `netstat`
 `find`         `grep`        `locate`
 `cat`          `touch`       `more`
 `less`         `sed`         `awk`
 `chmod`        `chown`       `ln`
 `df`           `du`          `free`
 `mount`        `umount`      `fdisk`
 `sudo`         `apt`         `passwd`
 `adduser`      `usermod`     `shutdown`
-----------   ------------   ------------

#### psql

La console `psql` permet d'effectuer l'ensemble des tâches courantes d'un
utilisateur de bases de données. Si ces tâches peuvent souvent être
effectuées à l'aide d'un outil graphique, la console présente l'avantage de
pouvoir être utilisée en l'absence d'environnement graphique ou de scripter
les opérations à effectuer sur la base de données.

#### OpenSSH

OpenSSH est un ensemble d'outils informatiques libres permettant des
communications sécurisées sur un réseau informatique en utilisant le protocole
de communication sécurisé _SSH_.

La commande `ssh` est un programme permettant de se connecter à un système
distant de façon sécurisé et d'y exécuter des commandes.  
Nous l'utiliserons pour nous connecter à la machine virtuelle :

```bash
ssh <login>@<hostname>
```

La commande `scp` est un programme permettant le transfert sécurisé de fichiers entre deux ordinateurs.  
Nous l'utiliserons pour nous connecter à la machine virtuelle.

Pour copier un fichier local sur un serveur distant :
```bash
scp <fichier> <login>@<hostname>:<chemin>
```
Pour copier un fichier d'un serveur distant en local :
```bash
scp <login>@<hostname>:<fichier> <chemin>
```

Le nom d'hôte peut être une adresse IP ou bien un _FQDN_.

#### Éditeur de fichier

La plupart des serveurs de production ne possèdent pas de session
graphique. L'utilisation d'éditeur de fichier graphique est donc impossible. Il
existe un certain nombre d'outils permettant d'éditer des fichiers en ligne de
commande : _vi_, _nano_, _emacs_, ...

Vous devrez impérativement utiliser un éditeur de ce type pour effectuer des
modifications de configuration sur le serveur hébergeant votre instance
PostgreSQL.

</div>

-----

### Préparation du TP

<div class="slide-content">

  * Utilisation de VirtualBox
  * Travail sur la base de données _cave_

</div>

<div class="notes">

La virtualisation consiste à exécuter sur une machine hôte dans un
environnement isolé des systèmes d'exploitation ou des applications.

Lors des TP, nous utiliserons une machine virtuelle Debian sur VirtualBox.

VirtualBox est un hyperviseur de type 2. Il s'agit d'un logiciel qui tourne sur
l'OS hôte. Ce logiciel permet de lancer un ou plusieurs OS invités. La machine
virtualise ou/et émule le matériel pour les OS invités : ces derniers croient
dialoguer directement avec ledit matériel.

Cette solution est très comparable à un émulateur, et parfois même
confondue. Cependant l’unité centrale de calcul, c'est-à-dire le
microprocesseur, la mémoire système (RAM) ainsi que la mémoire de stockage (via
un fichier) sont directement accessibles aux machines virtuelles, alors que sur
un émulateur l’unité centrale est simulée, les performances en sont donc
considérablement réduites par rapport à la virtualisation.

Cette solution isole bien les OS invités, mais elle a un coût en
performance. Ce coût peut être très élevé si le processeur doit être émulé,
comme cela est le cas dans l’émulation. En échange cette solution permet de
faire cohabiter plusieurs OS hétérogènes sur une même machine grâce à une
isolation complète. Les échanges entre les machines se font via les canaux
standards de communication entre systèmes d’exploitation (TCP/IP et autres
protocoles réseau), un tampon d’échange permet d’émuler des cartes réseaux
virtuelles sur une seule carte réseau réelle.

Les TP seront effectués sur une machine virtuelle hébergée sur VirtualBox.

Nous utiliserons le système d'exploitation GNU/Linux Debian en version Stretch.

Plusieurs version de PostgreSQL seront installées.

La base _cave_ sera chargée dans l'une d'entre elles.

Il faudra dans un premier temps créer la machine virtuelle à partir d'un
template.

Vous pourrez ensuite démarrer votre copie personnelle, vous y connecter en ssh
et effectuer les actions demandées.

</div>

-----

### Bibliographie

<div class="notes">

La majeure partie des explications de ce travaux dirigés sont issus des pages
suivantes :

  * <https://fr.wikipedia.org/wiki/Suite_des_protocoles_Internet>
  * <https://fr.wikipedia.org/wiki/Transmission_Control_Protocol>
  * <https://fr.wikipedia.org/wiki/Client%E2%80%93serveur>
  * <https://fr.wikipedia.org/wiki/Port_%28logiciel%29>
  * <https://fr.wikipedia.org/wiki/Berkeley_sockets#Socket_unix>
  * <https://fr.wikipedia.org/wiki/Virtualisation>
  * <https://fr.wikipedia.org/wiki/Syst%C3%A8me_d%27exploitation>
  * <https://doc.ubuntu-fr.org/systemd>
  * <https://fr.wikipedia.org/wiki/Secure_Shell>

</div>

-----
