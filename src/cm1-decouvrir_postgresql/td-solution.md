
<div class="notes">

### Solutions du TD 1

#### Question de cours

  * Quelle est la licence de PostgreSQL ?
    * La licence Open Source
      [PostgreSQL](https://www.postgresql.org/about/licence/). Elle permet
      d'utiliser, de copier, de modifier et de distribuer le logiciel et sa
      documentation pour n'importe quel usage gratuitement et sans accord écrit
      préalable.
  
  * Quelle est la dernière version majeure de PostgreSQL ?
    * La dernière version majeure, sortie le 3 octobre 2019 est la 12.

  * Quel mot clé permet de valider une transaction ?
    * le mot clé `COMMIT` permet de terminer une transaction en demandant la
      validation de l'ensemble des opérations au serveur.

</div>

-----
