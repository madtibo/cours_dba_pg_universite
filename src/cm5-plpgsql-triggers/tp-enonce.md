## Travaux Pratiques 5

<div class="slide-content">
  * _PL/PgSQL_ et triggers
</div>

<div class="notes">

**Rappel**

Durant ces travaux pratiques, nous allons utiliser la machine virtuelle du TP 1
pour héberger notre serveur de base de données PostgreSQL.

Effectuez les manipulations nécessaires pour réaliser les actions listées dans
la section _Énoncés_. 

Vous pouvez vous aider du cours, de l'annexe de ce TP, des derniers TP,
ainsi que de l'aide en ligne ou des pages de manuels (`man`).

### Énoncés


#### Fonctions

**Hello World**

Fonctions traitant des chaines de caractères :

- Ecrire une fonction hello qui renvoie la chaîne de caractère « Hello World! »
en SQL.
- Ecrire une fonction hello pl qui renvoie la chaîne de caractère « Hello
World! » en PL/pgSQL.
- Comparer les coûts des deux plans d’exécutions de ces requêtes. Expliquer
les coûts.

**Arithmétique**

- Écrivez une fonction `get_bordeaux` qui retournent les libelles tous les vins
de Bordeaux.
- Écrivez une fonction vin region par type qui prend en paramètre un type
de vin (rouge, rose, blanc) et retourne les noms et régions de production des
vins correspondant.
- Écrivez une fonction `nb_par_recoltant` par an qui prend en paramètre
une année (`int`) et retourne le nombre de vins par récoltant de l’année en
paramètre
- Écrivez la fonction `nb_bouteilles_1` qui prend en paramètre d’entrée une
année et un type de vin et retourne le nombre de bouteille de ce type produite.
- Écrivez la fonction nb bouteilles 2 qui prend en paramètre d’entrée une
liste variable d’années à traiter et un type de vin et retourne le nombre de
bouteille du type produite par année.

#### Triggers

Dans cette partie, nous allons travailler avec la base de données cave

**Triggers-1**

* Tracer dans une table toutes les modifications du champ nombre de la table stock.
* On veut conserver l’ancienne et la nouvelle valeur. On veut aussi savoir qui a fait
la modification et quand.
* Interdire la suppression des lignes dans stock. Afficher un message dans les
logs dans ce cas.
* Afficher aussi un message `NOTICE` quand nombre devient inférieur à 5, et
`WARNING` quand il vaut 0.

**Triggers-2**

Interdire à tout le monde, sauf un compte `admin`, l’accès à la table des logs
précédemment créée . En conséquence, le trigger fonctionne-t-il ? Le cas
échéant, le modifier pour qu’il fonctionne.

**Triggers-3**

Lire toute la table stock avec un curseur.

Afficher dans les journaux applicatifs toutes les paires (`vin id, contenant id`)
pour chaque nombre supérieur à l’argument de la fonction.

</div>

-----
