## Travaux Dirigés 5

<div class="slide-content">
  * _PL/PgSQL_ et triggers

</div>

-----

### Question de cours

<div class="slide-content">

  * Quand est-il préférable d'utiliser un langage PL plutôt que SQL ?
  * Pourquoi utiliser PL/pgSQL plutôt que de traiter les données côté
    application ?
  * Sur quels types d'opération peut-on définir un trigger ?

</div>

-----

### Enoncés

<div class="slide-content">

</div>

<div class="notes">

#### Fonctions _PL/PgSQL_

Syntaxe de création d'une fonction :

```sql
CREATE [ OR REPLACE ] FUNCTION
    nom ( [ [ mode_argument ] [ nom_agrégat ] type_argument
		        [ { DEFAULT | = } expression_par_défaut ]
					[, ...] ] )
    [ RETURNS type_en_retour
      | RETURNS TABLE ( nom_colonne type_colonne [, ...] ) ]
  { LANGUAGE nom_langage
    | TRANSFORM { FOR TYPE nom_type } [, ... ]
    | WINDOW
    | IMMUTABLE | STABLE | VOLATILE | [ NOT ] LEAKPROOF
    | CALLED ON NULL INPUT | RETURNS NULL ON NULL INPUT | STRICT
    | [ EXTERNAL ] SECURITY INVOKER | [ EXTERNAL ] SECURITY DEFINER
    | PARALLEL { UNSAFE | RESTRICTED | SAFE }
    | COST coût_exécution
    | ROWS lignes_de_résultat
    | SET paramètre_configuration { TO valeur | = valeur | FROM CURRENT }
    | AS 'définition'
    | AS 'fichier_objet', 'symbole_link'
  } ...
    [ WITH ( attribut [, ...] ) ]
```

**Exercice 1**

  * Écrire une fonction de division appelée `division`. Elle acceptera en
	entrée deux arguments de type entier et renverra un nombre flottant.

  * Ré-écrire la fonction de division pour tracer le problème de division par
	zéro.

_Conseil_ : dans ce genre de calcul impossible, il est possible d'utiliser avec
PostgreSQL la constante `NaN` (*Not A Number*).

  * Modifier la fonction pour tracer le problème de division par zéro via les
	exceptions.

**Exercice 2**

Écrire une fonction de multiplication dont les arguments sont des chiffres en
toute lettre.

Par exemple, appeler la fonction avec comme arguments les chaînes « deux » et
« trois » doit renvoyer 6.

**Exercice 3**

  * En utilisant la base de données _cave_, créer une fonction  `nb_bouteilles`
qui renvoie le nombre de bouteilles en stock suivant une année et un type de
vin (donc passés en paramètre).

  * Utiliser la fonction `generate_series()` pour extraire le nombre de
	bouteilles en stock sur plusieurs années, de 1990 à 1999.
	
-----

#### Triggers

Syntaxe de création d'un trigger :

```sql
CREATE [ CONSTRAINT ] TRIGGER nom { BEFORE | AFTER | INSTEAD OF }
       { événement [ OR ... ] }
    ON nom_table
    [ FROM nom_table_référencée ]
    [ NOT DEFERRABLE
		  | [ DEFERRABLE ] [ INITIALLY IMMEDIATE | INITIALLY DEFERRED ] ]
    [ REFERENCING { { OLD | NEW } TABLE [ AS ] nom_relation_transition } [ ... ] ]
    [ FOR [ EACH ] { ROW | STATEMENT } ]
    [ WHEN ( condition ) ]
    EXECUTE PROCEDURE nom_fonction ( arguments )
```

**Exercice 4**

Tracer dans une table toutes les modifications du champ `nombre` dans `stock`.
On veut garder le trace de l'heure, de l'opération, du type de vin, du
contenant, de l'année, de l'ancienne valeur et de la nouvelle valeur.

Afficher un message `NOTICE` quand nombre devient inférieur à 5, et `WARNING`
quand il vaut 0.

</div>

-----
