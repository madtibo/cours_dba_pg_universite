# _PL/PgSQL_ et triggers

![PostgreSQL](medias/elephant-rock-valley-of-fire.jpg)
\

<div class="notes">

</div>

-----

## Programme de ce cours

<div class="slide-content">

  * Semaine 1 : découverte de PostgreSQL
  * Semaine 2 : transactions et accès concurrents
  * Semaine 3 : missions du DBA
  * Semaine 4 : optimisation et indexation
  * **Semaine 5** : _PL/PgSQL_ et triggers
    * Présentation du PL et des principes
    * Présentations de _PL/PgSQL_ et des autres langages PL
    * Installation d'un langage PL
    * Détails sur _PL/PgSQL_
    * Gestion des erreurs

</div>

<div class="notes">

</div>

-----

## Introduction aux PL

<div class="slide-content">

  * `PL` = Procedural Languages
  * 3 langages activés par défaut :
    * _C_
    * _SQL_
    * _PL/PgSQL_

</div>

<div class="notes">


PL est l'acronyme de « Procedural Languages ». En dehors du _C_ et du _SQL_,
tous les langages acceptés par PostgreSQL sont des PL.

Par défaut, trois langages sont installés et activés : _C_, _SQL_ et _PL/PgSQL_.

</div>

-----

### Autres langages de _PL_

<div class="slide-content">

  * Nombreux autres langages disponibles
  * Voici une liste non exhaustive :
    * _PL/Tcl_
    * _PL/Perl_ et _PL/PerlU_
    * _PL/R_
    * _PL/sh_
    * _PL/python_
    * _PL/php_
    * _PL/java_
    * _PL/ruby_
    * _PL/V8_

</div>

<div class="notes">


Une quinzaine de langages sont disponibles, ce qui fait que la plupart des
langages connus sont couverts. De plus, il est possible d'en ajouter d'autres.

</div>

-----

### Des langages de confiance... ou non

<div class="slide-content">

  * les langages sont dits :
    * _de confiance_ (_trusted_)
	* ou non (_untrusted_)
  * Langage de confiance
    * ne permet que l'accès à la base de données
    * donc pas d'accès aux systèmes de fichiers, aux sockets réseaux, etc.

</div>

<div class="notes">

Les langages de confiance ne peuvent qu'accèder à la base de données. Ils ne
peuvent pas accéder aux autres bases, aux systèmes de fichiers, au réseau,
etc. Ils sont donc confinés, ce qui les rend moins facilement utilisable pour
compromettre le système. _PL/PgSQL_ est l'exemple typique. Mais du coup, ils
offrent moins de possibilités que les autres langages.

Seuls les superutilisateurs peuvent créer une fonction dans un langage
_Untrusted_. Par contre, ils peuvent ensuite donner les droits d'exécution à ces
fonctions aux autres utilisateurs.

</div>

-----

### Qui est de confiance ?

<div class="slide-content">

  * _Trusted_ : _PL/PgSQL_, _SQL_, _PL/Perl_
  * _Untrusted_ : _PL/PerlU_, _C_, _PL/Python_…

</div>

<div class="notes">

</div>

-----

### Les langages PL de PostgreSQL

<div class="slide-content">
  * Les langages PL fournissent :

    * Des fonctionnalités procédurales dans un univers relationnel
    * Des fonctionnalités avancées du langage PL choisi
    * Des performances de traitement souvent supérieures à celles du même code
  côté client

</div>

<div class="notes">


Il peut y avoir de nombreuses raisons différentes à l'utilisation d'un langage
PL. Simplifier et centraliser des traitements clients directement dans la base
est l'argument le plus fréquent. Par exemple, une insertion complexe dans
plusieurs tables, avec mise en place d'identifiants pour liens entre ces tables,
peut évidemment être écrite côté client. Il est quelquefois plus pratique
de l'écrire sous forme de PL. On y gagne :

  * La centralisation du code : si plusieurs applications ont potentiellement
  besoin d'écrire le traitement, cela réduit d'autant les risques de bugs
  * Les performances : le code s'exécute localement, directement dans le moteur
  de la base. Il n'y a donc pas tous les changements de contexte et échanges de
  message réseaux dûs à l'exécution de nombreux ordres _SQL_ consécutifs
  * La simplicité : suivant le besoin, un langage PL peut être bien plus
  pratique que le langage client.

Il est par exemple très simple d'écrire un traitement d'insertion/mise à jour
en _PL/PgSQL_, le langage étant créé pour simplifier ce genre de traitements,
et la gestion des exceptions pouvant s'y produire. Si vous avez besoin de
réaliser du traitement de chaîne puissant, ou de la manipulation de fichiers,
_PL/Perl_ ou _PL/Python_ seront probablement des options plus intéressantes, car
plus performantes.

La grande variété des différents langages PL supportés par PostgreSQL permet
normalement d'en trouver un correspondant aux besoins et aux langages déjà
maîtrisés dans l'entreprise.

Les langages PL permettent donc de rajouter une couche d'abstraction et
d'effectuer des traitements avancés directement en base.

</div>

-----

### Intérêts de _PL/PgSQL_ en particulier

<div class="slide-content">

  * Ajout de structures de contrôle au langage _SQL_
  * Peut effectuer des traitements complexes
  * Hérite de tous les types, fonctions et opérateurs définis par les
  utilisateurs
  * Est « _Trusted_ »
    * et facile à utiliser

</div>

<div class="notes">

La structure du _PL/PgSQL_ est inspirée de l'ADA, donc proche du Pascal. La
plupart des ~~développeurs~~ vieux développeurs ont eu l'occasion de faire du
Pascal ou de l'ADA, et sont donc familiers avec la syntaxe de _PL/PgSQL_. Aussi,
cette syntaxe est très proche de celle de PLSQL d'Oracle.

Elle permet d'écrire des requêtes directement dans le code PL sans
déclaration préalable, sans appel à des méthodes complexes, ni rien de cette
sorte. Le code _SQL_ est mélangé naturellement au code PL, et on a donc un
sur-ensemble de _SQL_ qui est procédural.

_PL/PgSQL_ étant intégré à PostgreSQL, il hérite de tous les types déclarés
dans le moteur, même ceux que vous aurez rajouté. Il peut les manipuler de
façon transparente.

_PL/PgSQL_ est trusted. Tous les utilisateurs peuvent donc créer des procédures
dans ce langage (par défaut). Vous pouvez toujours soit supprimer le langage,
soit retirer les droits à un utilisateur sur ce langage (via la commande _SQL_
«REVOKE»).

_PL/PgSQL_ est donc raisonnablement facile à utiliser : il y a peu de
complications, peu de pièges, il dispose d'une gestion des erreurs évoluée
(gestion d'exceptions).

</div>

-----

### Les autres langages PL ont toujours leur intérêt

<div class="slide-content">

  * Avantages des autres langages PL par rapport à _PL/PgSQL_ :
    * Beaucoup plus de possibilités
    * Souvent plus performants pour la résolution de certains problèmes
  * Mais un gros défaut :
    * Pas spécialisés dans le traitement de requêtes

</div>

<div class="notes">


Les langages PL «autres», comme _PL/Perl_ et _PL/Python_ (les deux plus utilisés
après _PL/PgSQL_), sont bien plus évolués que _PL/PgSQL_. Par exemple, ils sont
bien plus efficaces en matière de traitement de chaînes de caractères, ils
possèdent des structures avancées comme des tables de hachages, permettent
l'utilisation de variables statiques pour maintenir des caches, voire, pour
leurs versions untrusted, peuvent effectuer des appels systèmes. Dans ce cas,
il devient possible d'appeler un Webservice par exemple, ou d'écrire des
données dans un fichier externe.

Il existe des langages PL spécialisés. Le plus emblématique d'entre eux est
_PL/R_. R est un langage utilisé par les statisticiens pour manipuler de gros
jeux de données. _PL/R_ permet donc d'effectuer ces traitements R directement en
base, traitements qui seraient très pénibles à écrire dans d'autres
langages.

Il existe aussi un langage qui est, du moins sur le papier, plus rapide que tous
les langages cités précédemment : vous pouvez écrire des procédures
stockées en C, directement. Elles seront compilées à l'extérieur de
PosgreSQL, en respectant un certain formalisme, puis seront chargées en
indiquant la bibliothèque _C_ qui les contient et leurs paramètres et types de
retour. Attention, toute erreur dans votre code _C_ est susceptible d'accéder à
toute la mémoire visible par le processus PostgreSQL qui l'exécute, et donc de
corrompre les données. Il est donc conseillé de ne faire ceci qu'en dernière
extrémité.

Le gros défaut est simple et commun à tous ces langages : vous utilisez par
exemple _PL/Perl_. Perl n'est pas spécialement conçu pour s'exécuter en tant
que langage de procédures stockées. Ce que vous utilisez quand vous écrivez
du _PL/Perl_ est donc du code Perl, avec quelques fonctions supplémentaires
(préfixées par spi) pour accéder à la base de données. L'accès aux données est
donc rapide, mais assez malaisé au niveau syntaxique, comparé à _PL/PgSQL_.

Un autre problème des langages PL (autre que _C_ et _PL/PgSQL_), c'est que ces
langages n'ont pas les mêmes types natifs que PostgreSQL, et s'exécutent dans
un interpréteur relativement séparé. Les performances sont donc moindres que
_PL/PgSQL_ et _C_ pour les traitements dont le plus consommateur est l'accès des
données. Souvent, le temps de traitement dans un de ces langages plus évolués
est tout de même meilleur grâce au temps gagné par les autres
fonctionnalités (la possibilité d'utiliser un cache, ou une table de hachage
par exemple).

</div>

-----

## Installation

<div class="slide-content">

  * _PL/PgSQL_ compilé et installé par défaut
  * Paquets Debian et RedHat :
    * _PL/PgSQL_ par défaut
    * de nombreux autres disponibles
  * Autres langages à compiler explicitement

</div>

<div class="notes">

Voici la liste des langages PL disponibles par paquets sur Debian :

```bash
$ apt-cache search postgresql | grep -i procedural | grep 15
postgresql-15-plr - Procedural language interface between PostgreSQL and R
postgresql-plperl-15 - PL/Perl procedural language for PostgreSQL 15
postgresql-pltcl-15 - PL/Tcl procedural language for PostgreSQL 15
postgresql-15-pllua - Lua procedural language for PostgreSQL 15
postgresql-15-plsh - PL/sh procedural language for PostgreSQL 15
```


L'installeur Windows contient aussi _PL/PgSQL_. _PL/Perl_ et _PL/Python_ sont
par contre plus compliqués de mise en œuvre. Il faut disposer d'exactement la
version qui a été utilisée par le packageur au moment de la compilation de la
version windows.

Pour savoir si _PL/Perl_ ou _PL/Python_ a été compilé, on peut à nouveau
demander à `pg_config` :

```
# pg_config --configure \
    --prefix=/usr/local/pgsql-15_icu --enable-thread-safety \
    --with-openssl --with-libxml --enable-nls --with-perl --enable-debug \
    ICU_CFLAGS=-I/usr/local/include/unicode/ \
    ICU_LIBS=-L/usr/local/lib -licui18n -licuuc -licudata --with-icu
```

### Vérification de la disponibilité

Comment vérifier la présence de la bibliothèque :

```bash
find $(pg_config --libdir) -name "plpgsql.so"
```

```bash
find $(pg_config --pkglibdir) -name "plpgsql.so"
```

La bibliothèque `plpgsql.so` contient les fonctions qui permettent
l'utilisation du langage _PL/PgSQL_. Elle est installée par défaut avec le
moteur PostgreSQL.

Elle est chargée par le moteur à la première utilisation d'une procédure
utilisant ce langage.

Toutefois, il est encore plus simple de demander à PostgreSQL d'activer le
langage. Il sera bien temps ensuite, si cela échoue, de lancer cette commande
de diagnostic.

</div>

-----

### Ajout d'un langage dans une base de données

<div class="slide-content">

  * Commande similaire pour tous les langages
  * Activer :
    `CREATE EXTENSION plpython;`
  * Désactiver :
    `DROP EXTENSION plpython;`

</div>

<div class="notes">


Activer le langage dans la base modèle template1 l'activera aussi pour toutes
les bases créées par la suite.

PostgreSQL fournit un outil, appelé `createlang`, pour activer un langage :

```
createlang plpgsql la_base_a_activer
```

L'outil se connecte à la base indiquée et exécute la commande `CREATE LANGUAGE`
pour le langage précisé en argument. Son pendant Windows se nomme
`createlang.exe`. Il existe aussi un outil pour désactiver un langage
(`droplang`).

Si vous optez pour exécuter vous-même l'ordre _SQL_, le langage est créé dans
la base dans laquelle la commande est lancée.

Pour installer un autre langage, utilisez la même commande tout en remplaçant
plpgsql par plperl, plperlu, plpython, pltcl, plsh…

#### PL/mon_langage est-il déjà installé ?

  * Vérification dans psql avec la commande `\dx`
  * Ou interroger le catalogue système `pg_language`
    * Il contient une ligne par langage installé
    * Un langage peut avoir `lanpltrusted` à `false`

Le langage _PL/PgSQL_ apparaît comme une extension :

```sql
base=# \dx
                       Liste des extensions installées
     Nom     | Version |   Schéma   |            Description
-------------+---------+------------+---------------------------------------
 plpgsql     | 1.0     | pg_catalog | _PL/PgSQL_ procedural language
```

C'est évidemment aussi applicable aux autres langages PL.

Voici un exemple d'interrogation de `pg_language` :

```sql
SELECT lanname, lanpltrusted
FROM pg_language
WHERE lanname='plpgsql';

 lanname | lanpltrusted
---------+--------------
 plpgsql | t
(1 ligne)
```

</div>

-----

## Une fonction _PL/PgSQL_

<div class="slide-content">

  * Fonction effectuant la somme de deux entiers :

```sql
CREATE FUNCTION addition(entier1 integer, entier2 integer)
RETURNS integer
LANGUAGE plpgsql
IMMUTABLE
AS '
DECLARE
  resultat integer;
BEGIN
  resultat := entier1 + entier2;
  RETURN resultat;
END';
```

</div>

<div class="notes">

Le langage _PL/PgSQL_ n'est pas sensible à la casse, tout comme _SQL_ (sauf les
noms de colonnes, si vous les mettez entre des guillemets doubles).

L'opérateur de comparaison est `=`, l'opérateur d'affectation `:=`

</div>

-----

## Création et structure

<div class="slide-content">

  * Ordre _SQL_ : `CREATE FUNCTION` ou `CREATE PROCEDURE`
  * Le langage est un paramètre de la définition de l'objet

</div>

<div class="notes">

Voici la syntaxe complète :

```sql
Syntax:
CREATE [ OR REPLACE ] FUNCTION
    name ( [ [ argmode ] [ argname ] argtype [ { DEFAULT | = } default_expr ]
           [, ...] ] )
    [ RETURNS rettype
      | RETURNS TABLE ( column_name column_type [, ...] ) ]
  { LANGUAGE lang_name
    | TRANSFORM { FOR TYPE type_name } [, ... ]
    | WINDOW
    | IMMUTABLE | STABLE | VOLATILE | [ NOT ] LEAKPROOF
    | CALLED ON NULL INPUT | RETURNS NULL ON NULL INPUT | STRICT
    | [ EXTERNAL ] SECURITY INVOKER | [ EXTERNAL ] SECURITY DEFINER
    | PARALLEL { UNSAFE | RESTRICTED | SAFE }
    | COST execution_cost
    | ROWS result_rows
    | SET configuration_parameter { TO value | = value | FROM CURRENT }
    | AS 'definition'
    | AS 'obj_file', 'link_symbol'
  } ...
    [ WITH ( attribute [, ...] ) ]
```

Et un exemple pour _PL/PgSQL_ :

```sql
CREATE FUNCTION ma_fonction () RETURNS integer
LANGUAGE plpgsql
(...)
```

Les objets `PROCEDURE` sont apparus dans la version 11 de PostgreSQL. À la
différence des objets `FUNCTION`, ils ne renvoient rien mais on peut utiliser
dans le corps de la fonction des ordres de contrôle de transaction (comme
COMMIT et ROLLBACK). La syntaxe complète :


```sql
CREATE [ OR REPLACE ] PROCEDURE
    name ( [ [ argmode ] [ argname ] argtype [ { DEFAULT | = } default_expr ] [, ...] ] )
  { LANGUAGE lang_name
    | TRANSFORM { FOR TYPE type_name } [, ... ]
    | [ EXTERNAL ] SECURITY INVOKER | [ EXTERNAL ] SECURITY DEFINER
    | SET configuration_parameter { TO value | = value | FROM CURRENT }
    | AS 'definition'
    | AS 'obj_file', 'link_symbol'
    | sql_body
  } ...
```

</div>

-----
	
### Modification d'une fonction

<div class="slide-content">

  * Dans le cas d'une modification : `CREATE OR REPLACE FUNCTION`
  * Une fonction est définie par son nom et ses arguments
  * Si type de retour différent, la fonction doit d'abord être supprimée puis
  recréée

</div>

<div class="notes">

Une fonction est surchargeable. La seule façon de les différencier est de
prendre en compte les arguments (nombre et type). Deux fonctions identiques aux
arguments près (on parle de prototype) ne sont pas identiques, mais bien deux
fonctions distinctes.

</div>

-----

### Suppression d'une fonction

<div class="slide-content">

  * Ordre _SQL_ : `DROP FUNCTION`
  * Arguments (en entrée) nécessaires à l'identification de la fonction à
  supprimer :

```sql
DROP FUNCTION addition(integer, integer);
DROP FUNCTION public.addition(integer, integer);
```

</div>

<div class="notes">


Les types des arguments en entrée doivent être indiqués. Par contre, leur nom
n'a aucune importance. Vous pouvez toutefois les passer quand même mais, comme
pour un `CREATE FUNCTION`, ils sont simplement ignorés.

</div>

-----

### Arguments d'une fonction

<div class="slide-content">

```sql
[ [ mode_argument ] [ nom_argument ] type_argument
[ { DEFAULT | = } expr_defaut ] [, ...] ]
```

  * _mode_argument_ : en entrée ( `IN `), en sortie ( `OUT `)
  * _nom_argument_ : nom
  * _type_argument_ : type
    * parmi tous les types de base et les types utilisateur
  * valeur par défaut : clause `DEFAULT`

</div>

<div class="notes">

Si le mode de l'argument est omis, `IN` est la valeur implicite.

Il existe d'autres modes pour les arguments : , en entrée/sortie ( `INOUT `) ou
à nombre variant ( `VARIADIC `).

L'option `VARIADIC` permet de définir une fonction avec un nombre d'arguments
libres à condition de respecter le type de l'argument (comme `printf` en _C_ par
exemple).  
Seul un argument `OUT` peut suivre un argument `VARIADIC` : l'argument
`VARIADIC` doit être le dernier de la liste des paramètres en entrée puisque
tous les paramètres en entrée suivant seront considérées comme faisant partie
du tableau variadic. Seuls les arguments `IN` et `VARIADIC` sont utilisables
avec une fonction déclarée renvoyant une table (clause `RETURNS TABLE`). S'il y
a plusieurs paramètres en `OUT`, un enregistrement composite de tous ces types
est renvoyé (c'est donc équivalent sémantiquement à un `RETURNS TABLE`).

La clause `DEFAULT` permet de rendre les paramètres optionnels. Après le
premier paramètre ayant une valeur par défaut, tous les paramètres qui suivent
doivent avoir une valeur par défaut. Pour rendre le paramètre optionnel, il
doit être le dernier argument ou alors les paramètres suivants doivent aussi
avoir une valeur par défaut.

</div>

-----

### Retour d'une fonction

<div class="slide-content">

```sql
RETURNS type_ret
```

  * Il faut aussi indiquer un type de retour
  * sauf si un ou plusieurs paramètres sont en mode OUT ou INOUT
  * _type_ret_ : type de la valeur en retour
  * `void` est un type de retour valide

</div>

<div class="notes">

Voici comment s'utilise le retour de type table :

```
RETURNS TABLE ( nom_colonne nom_type [, ...] )
```

On peut aussi indiquer que la fonction ne retourne pas un enregistrement (un 
scalaire en termes relationnels) mais un jeu d'enregistrements (c'est-à-dire 
une relation, une table). Il faut utiliser le mot clé `SETOF`

</div>

-----

### Language d'une fonction

<div class="slide-content">

```sql
LANGUAGE <nomlang>
```

  * Le langage de la fonction doit être précisé
  * Dans notre cas, nous utiliserons `plpgsql`.
  * Mais il est possible de créer des fonctions en `plphp`, `plruby`, voire des
  langages spécialisés comme `plproxy`.

</div>

<div class="notes">

</div>

-----

### Politique de sécurité

<div class="slide-content">

```
[EXTERNAL] SECURITY INVOKER | [EXTERNAL] SECURITY DEFINER
```

  * Permet de déterminer l'utilisateur avec lequel sera exécutée la fonction
  * Le « _sudo_ » de la base de données
    * Potentiellement dangereux

</div>

<div class="notes">

Une fonction `SECURITY INVOKER` s'exécute avec les droits de l'appelant. C'est
le mode par défaut.

Une fonction `SECURITY DEFINER` s'exécute avec les droits du créateur. Cela
permet, au travers d'une fonction, de permettre à un utilisateur d'outrepasser
ses droits de
façon contrôlée.

Bien sûr, une fonction `SECURITY DEFINER` doit faire l'objet d'encore plus
d'attention qu'une fonction normale. Elle peut facilement constituer un trou
béant dans la sécurité de votre base.

Des choses importantes sont à noter pour `SECURITY DEFINER` :

  * Toute fonction, par défaut, est exécutable par public. La première chose à
faire est donc de révoquer ce droit.
  * Il faut se protéger des variables de session qui pourraient être utilisées
pour modifier le comportement de la fonction, en particulier le **search_path**.
Il doit donc **impérativement** être positionné en dur dans cette fonction (soit
d'emblée, avec un SET dans la fonction, soit en positionnant un `SET` dans le
`CREATE FUNCTION`).

Le mot clé `EXTERNAL` est facultatif, et n'est là que pour être en conformité
avec la norme _SQL_ : en effet, dans PostgreSQL, on peut modifier le security
definer pour toutes les fonctions, qu'elles soient externes ou pas.

</div>

-----

### Mode de la fonction

<div class="slide-content">

```
IMMUTABLE | STABLE | VOLATILE
```

  * Ce mode précise la « volatilité » de la fonction.

</div>

<div class="notes">


On peut indiquer à PostgreSQL le niveau de volatilité (ou de stabilité) d'une
fonction. Ceci permet d'aider PostgreSQL à optimiser les requêtes utilisant
ces fonctions, mais aussi d'interdire leur utilisation dans certains contextes.


  * Une fonction est `IMMUTABLE` (immuable) si son exécution ne dépend que de
	ses paramètres. Elle ne doit donc dépendre ni du contenu de la base (pas de
  `SELECT`, ni de modification de donnée de quelque sorte), ni d'**aucun** autre
  élément qui ne soit pas un de ses paramètres. Par exemple, `now()` n'est
  évidemment pas immuable. Une fonction sélectionnant des données d'une table
  non plus. `to_char()` n'est pas non plus immuable : son comportement dépend
  des paramètres de session, par exemple `to_char(timestamp with time zone,
  text)` dépend du paramètre de session timezone…


  * Une fonction est `STABLE` si son exécution donne toujours le même résultat
	sur toute la durée d'un ordre _SQL_, pour les mêmes paramètres en entrée. Cela
  signifie que la fonction ne modifie pas les données de la base. `to_char()`
  est `STABLE`.


  * Une fonction est `VOLATILE` dans tous les autres cas. now() est `VOLATILE`.
	Une fonction non déclarée comme `STABLE` ou `IMMUTABLE` est `VOLATILE` par
	défaut.

Quelle importance ?

  * Une fonction `IMMUTABLE` peut être remplacée par son résultat avant même la
  planification d'une requête l'utilisant. L'exemple le plus simple est une
  simple opération arithmétique. Si vous exécutez :

```sql
SELECT * FROM ma_table WHERE mon_champ> abs(-2)
```

et PostgreSQL substitue `abs(-2)` par `2` et planifie ensuite la requête. Cela
fonctionne aussi, bien sûr, avec les opérateurs (comme `+`), qui ne sont qu'un
habillage syntaxique au-dessus d'une fonction.

  * Une fonction `STABLE` peut être remplacée par son résultat pendant
	l'exécution de la requête. On n'a par contre aucune idée de sa valeur avant
	de commencer à exécuter la requête parce qu'on ne sait pas encore, à ce
	moment là, quelles sont les données qui seront visibles en base. Par exemple,
	avec :

```sql
SELECT * FROM ma_table WHERE mon_timestamp > now()
```

PostgreSQL sait que `now()` (le timestamp de démarrage de la transaction) va
être constant pendant toute la durée de la transaction. Néanmoins, `now()` n'est
pas _IMMUTABLE_, il ne va donc pas le remplacer par sa valeur avant d'exécuter
la requête. Il n'exécutera par contre `now()` qu'une seule fois.


  * Une fonction `VOLATILE` doit systématiquement être exécutée à chaque appel.

On comprend donc l'intérêt de se poser la question à l'écriture de chaque
fonction.

Une autre importance existe, pour la création d'index sur fonction. Par exemple,

```sql
CREATE INDEX mon_index ON ma_table ((ma_fonction(ma_colonne))
```

Ceci n'est possible que si la fonction est `IMMUTABLE`. En effet, si le
résultat de la fonction dépend de l'état de la base, la fonction calculée au
moment de la création de la clé d'index ne retournera plus le même résultat
quand viendra le moment de l'interroger. PostgreSQL n'acceptera donc que les
fonctions `IMMUTABLE` dans la déclaration des index fonctionnels.

</div>

-----

### Autres paramètres

<div class="slide-content">

  * Coût d'appel de la fonction avec les paramètres `COST` et `ROWS`

  * la fonction peut-elle être exécutée en mode parallèle ?
  `PARALLEL [UNSAFE | RESTRICTED | SAFE]`

  * Gestion des valeurs _NULL_ dans les arguments

</div>

<div class="notes">

#### Coût d'appel de la fonction

* `COST cout_execution`
  * coût estimé pour l'exécution de la fonction
  * `COST` est représenté en unité de `cpu_operator_cost` (100 par défaut).

* `ROWS nb_lignes_resultat`
  * nombre estimé de lignes que la fonction renvoie
  * `ROWS` vaut par défaut 1000 pour les fonctions `SETOF`. Pour les autres
fonctions, la valeur de ce paramètre est ignorée et remplacée par 1.

Ces deux paramètres ne modifient pas le comportement de la fonction. Ils ne
servent que pour aider l'optimiseur de requête à estimer le coût d'appel à
la fonction, afin de savoir, si plusieurs plans sont possibles, lequel est le
moins coûteux par rapport au nombre d'appels de la fonction et au nombre
d'enregistrements qu'elle retourne.

#### Exécution en parallèle

`PARALLEL UNSAFE` indique que la fonction ne peut pas être exécutée dans le mode
parallèle. La présence d'une fonction de ce type dans une requête _SQL_ force un
plan d'exécution en série. C'est la valeur par défaut.

Une fonction est non parallélisable si elle modifie l'état d'une base ou si
elle fait des changements sur la transaction.

`PARALLEL RESTRICTED` indique que la fonction peut être exécutée en mode
parallèle mais l'exécution est restreinte au processus principal d'exécution.

Une fonction peut être déclarée comme restreinte si elle accède aux tables
temporaires, à l'état de connexion des clients, aux curseurs, aux requêtes
préparées.

`PARALLEL SAFE` indique que la fonction s'exécute correctement dans le mode
parallèle sans restriction.

En général, si une fonction est marquée sûre ou restreinte à la parallélisation
alors qu'elle ne l'est pas, elle pourrait renvoyer des erreurs ou fournir de
mauvaises réponses lorsqu'elle est utilisée dans une requête parallèle.

En cas de doute, les fonctions doivent être marquées comme `UNSAFE`, ce qui
correspond à la valeur par défaut. 

#### Gestion des valeurs `NULL`

```
CALLED ON NULL INPUT | RETURNS NULL ON NULL INPUT | STRICT
```

  * `CALLED ON NULL INPUT` : fonction appelée même si certains arguments sont
  NULL.
  * `RETURNS NULL ON NULL INPUT` ou `STRICT` : la fonction renvoie _NULL_ à
  chaque fois qu'au moins un argument est _NULL_.


Si un des arguments est NULL, PostgreSQL n'exécute même pas la fonction et
utilise NULL comme résultat.

Dans la logique relationnelle, NULL signifie « la valeur est inconnue ». La
plupart du temps, il est logique qu'une fonction ayant un paramètre à une
valeur inconnue retourne aussi une valeur inconnue, ce qui fait que cette
optimisation est très souvent pertinente.

On gagne à la fois en temps d'exécution, mais aussi en simplicité du code (il
n'y a pas à gérer les cas NULL pour une fonction dans laquelle NULL ne doit
jamais être injecté).

</div>

-----

### Code de la fonction

<div class="slide-content">

```
AS 'definition' | AS 'fichier_obj', 'symbole_lien'
```

  * Premier cas : chaîne « definition » contenant le code réel de la fonction
  * Deuxième cas : `fichier_obj` est le nom de la bibliothèque, `symbole_lien`
	est le nom de la fonction dans le code source _C_

</div>

<div class="notes">

`symbole_lien` n'est à utiliser que quand le nom de la fonction diffère du nom
de la fonction _C_ qui l'implémente.

</div>

-----

### Structure de la définition

<div class="slide-content">

  * `DECLARE` : déclaration des variables locales
  * `BEGIN` : début du code de la fonction
  * `END` : fin du code de la fonction
  * Instructions séparées par des points-virgules
  * Commentaires :
    * commençent par `--`
    * ou compris entre `/*` et `*/`

</div>

<div class="notes">

</div>

-----

### Utilisation des guillemets

<div class="slide-content">

  * L'usage des guillemets problématique.
  * le source se place directement entre guillemets
    * besoin de doubler les guillemets du code source
  * Encadrement du source par `$$`

</div>

<div class="notes">

Exemple :

Syntaxe utilisant uniquement des guillemets :

```
requete := requete || '' AND vin LIKE ''''bordeaux%'''' AND xyz''
```

Simplification grâce aux dollars :

```
requete := requete || $sql$ AND vin LIKE 'bordeaux%' AND xyz$sql$
```

Si
vous avez besoin de mettre entre guillemets du texte qui inclut `$$`, vous pouvez
utiliser $Q$, et ainsi de suite. Le plus simple étant de définir un marqueur
de fin de fonction plus complexe…

</div>

-----

## Déclaration de variables

<div class="slide-content">

  * Variables déclarées dans le source, dans la partie `DECLARE` :

```sql
DECLARE
nombre integer;
contenu text;
```

  * Les variables peuvent se voir associées une valeur initiale :

```sql
nombre integer := 5;
```

</div>

<div class="notes">

</div>

-----

### Type des variables

<div class="slide-content">

  * Types natifs de PostgreSQL intégralement supportés
  * Quelques types spécifiques à _PL/PgSQL_

</div>

<div class="notes">

En dehors des types natifs de PostgreSQL, _PL/PgSQL_ y ajoute des types
spécifiques pour faciliter l'écriture des fonctions.

</div>

-----

### Type `ROW`

<div class="slide-content">

  * But :
    * utilisation de structures,
    * renvoi de plusieurs valeurs à partir d'une fonction
  * Utiliser un type composite :

```sql
CREATE TYPE ma_structure AS (
  un_entier integer,
  une_chaine text,
  ...);
CREATE FUNCTION ma_fonction ()
  RETURNS ma_structure...;
```

</div>

<div class="notes">

</div>

-----

### Type `RECORD`

<div class="slide-content">

  * Identique au type ROW
    * mais type connu que lors de son affectation
  * Une variable de type RECORD peut changer de type au cours de l'exécution de
	la fonction, suivant les affectations réalisées

</div>

<div class="notes">

RECORD est beaucoup utilisé pour manipuler des curseurs : cela évite de devoir
se préoccuper de déclarer un type correspondant exactement aux colonnes de la
requête associée à chaque curseur.

Ici la variable `ligne` change de type entre les 2 traitements :

```sql
CREATE FUNCTION ma_fonction () RETURNS integer
AS $$
DECLARE
  ligne RECORD;
  (...)
BEGIN
  SELECT INTO ligne * FROM ma_premiere_table;
  -- traitement de la ligne
  FOR ligne IN SELECT * FROM ma_deuxieme_table LOOP
  -- traitement de cette nouvelle ligne
  (...)
```

</div>

-----

### Type `ROWTYPE`

<div class="slide-content">

  * Possible d'utiliser le type composite défini par la ligne d'une table :

```sql
CREATE FUNCTION ma_fonction () RETURNS integer
AS $$
DECLARE
ligne ma_table%ROWTYPE;
(...)
```

</div>

<div class="notes">

L'utilisation de « `%ROWTYPE` » permet de définir une variable qui contient la
structure d'un enregistrement de la table spécifiée. « `%ROWTYPE` » n'est pas
obligatoire, il est néanmoins préférable d'utiliser cette forme, bien plus
portable. En effet, dans PostgreSQL, toute création de table créé un type
associé de même nom, le nom de la table seul est donc suffisant.

</div>

-----

### Affectation d'une valeur à une variable

<div class="slide-content">

  * À l'ordre `SELECT INTO` :

```sql
SELECT INTO un_entier 5;
```

  * Préférez l'opérateur `:=` :

```sql
un_entier := 5;
un_entier := une_colonne FROM ma_table WHERE id = 5;
```

</div>

<div class="notes">

</div>

-----

### Affectation via une requête

<div class="slide-content">

```sql
SELECT * INTO ma_variable_ligne FROM ma_table...;
```

  * Affectation dans une variable `RECORD` ou `ROW`
  * Si plusieurs enregistrements renvoyés, seul le premier est récupéré
  * Pour récupérer plus d’un enregistrement : écrire une boucle

</div>

<div class="notes">

Dans le cas du type `ROW`, la définition de la ligne doit correspondre
parfaitement à la définition de la ligne renvoyée. Utiliser un type `RECORD`
permet d'éviter ce type de problème. La variable obtient directement le type
`ROW` de la ligne renvoyée.

Pour contrôler qu'un seul enregistrement est renvoyé, remplacer `INTO` par
`INTO STRICT`.

L'ordre SQL est statique : on ne peut pas faire varier les colonnes retournées,
la clause `WHERE`, les tables…
	
</div>

------

### Exécution d'une requête via _EXECUTE_

<div class="slide-content">

```sql
EXECUTE '<chaine>' [INTO [STRICT] cible];
```

  * Exécute la requête comprise dans la variable chaîne
  * La variable `chaine` peut être construite à partir d'autres variables
  * `cible` contient le résultat de l'exécution de la requête

</div>

<div class="notes">

Ne fonctionne que si la requête ne retourne qu'une unique ligne. Si plusieurs lignes sont retournées, utiliser une boucle.

#### Mot clé `USING`

Le mot clé `USING` peut-être utiliser. Il permet de créer une requête dynamique
avec des variables de substitution. Il est de plus beaucoup plus lisible que
des `quote_nullable`.

```
EXECUTE command-string [ INTO [STRICT] target ] [ USING expression [, ... ] ];
```

```sql
EXECUTE 'SELECT count(*) FROM mytable
WHERE inserted_by = $1 AND inserted <= $2'
INTO c
USING checked_user, checked_date;
``` 

  * Le nombre de paramètres de la requête doit être fixe, ainsi que leur type
  * Ne concerne pas les identifiants !

#### Mot clé `STRICT`

  * Sans `STRICT`, cible contient la première ligne d'un résultat multi-lignes
	ou NULL s'il n'y a pas de résultat.
  * Avec `STRICT`, une exception est levée si le résultat ne contient aucune
	ligne (`NO_DATA_FOUND`) ou en contient plusieurs (`TOO_MANY_ROWS`).

Nous verrons comment traiter les exceptions plus loin.

</div>

-----

### Exécution d'une requête via _PERFORM_

<div class="slide-content">

```
PERFORM <query>
```

  * Permet l'exécution
    * d'un `INSERT`, `UPDATE`, `DELETE`
    * ou même `SELECT`, si le résultat importe peu
  * Permet aussi d'appeler une autre fonction sans en récupérer de résultat

</div>

<div class="notes">

Pour appeler une fonction, il suffit d'utiliser `PERFORM` de
la manière suivante :

```sql
PERFORM mafonction(argument1);
```

Ne fonctionne que si la clause `RETURNING` n'est pas utilisée.

</div>

-----

### Protection contre l'injection

<div class="slide-content">

  * `quote_ident` : met entre guillemets un identifiant d'un objet PostgreSQL
  * `quote_literal` : met entre guillemets une valeur (chaîne de caractères)
  * Pour concaténer tous les morceaux de la requête :
    * Utiliser l'opérateur de concaténation `||` 
    * Ou utiliser `format(...)`, équivalent de sprintf

</div>

<div class="notes">

Une autre fonction, `quote_nullable`, met entre guillemets une valeur (chaîne
de caractères), sauf `NULL` qui sera alors renvoyé sans les guillemets
	
La fonction `format` est l'équivalent de la fonction `sprintf` : elle formate
une chaine en fonction d'un patron et de valeurs à appliquer à ses paramètres
et la retourne. Les type de paramètre reconnus par format sont :

  * `%I` : est remplacé par un identifiant d'objet. C'est l'équivalent de la
	fonction `quote_ident`. L'objet en question est entouré en double-guillemet
	si nécessaire ;
  * `%L` : est remplacé par une valeur littérale. C'est l'équivalent de la
	fonction `quote_literal`. Des simple-guillemet sont ajoutés à la valeur et
	celle-ci est correctement échappée si nécessaire ;
  * `%s` : est remplacé par la valeur donnée sans autre forme de
	transformation ;
  * `%%` : est remplacé par un simple `%`.

Voici un exemple d'utilisation de cette fonction, utilisant des paramètre
positionnels :

```sql
select format(
    'SELECT %I FROM %I WHERE %1$I=%3$L',
    'MaColonne',
    'ma_table',
    $$l'été$$
);
                           format
-------------------------------------------------------------
 SELECT "MaColonne" FROM ma_table WHERE "MaColonne"='l''été'
```

</div>

-----

### Diagnostics d'une exécution

<div class="slide-content">

  * Affectation de la variable `FOUND` si une ligne est affectée par
	l'instruction
  * Pour obtenir le nombre de lignes affectées :
    `GET DIAGNOSTICS variable = ROW_COUNT;`

</div>

<div class="notes">

On peut déterminer qu'aucune ligne n'a été trouvé par la requête en
utilisant la variable `FOUND` :

```
PERFORM * FROM ma_table WHERE une_colonne>0;
IF NOT FOUND THEN
...
END IF;
```

Il est à noter que `ROW_COUNT` s'applique à l'ordre SQL précédent, quel qu'il soit :

  * PERFORM ;
  * EXECUTE ;
  * Ou même à un ordre statique directement dans le code PL/PgSQL.	

</div>

-----

### Fonction renvoyant un ensemble

<div class="slide-content">

  * Doit renvoyer un ensemble d'un type `SETOF`
  * Chaque ligne sera récupérée par l'instruction `RETURN NEXT`

</div>

<div class="notes">

</div>

-----

### Exemple d'une fonction `SETOF`

<div class="slide-content">

```sql
CREATE FUNCTION liste_entier (limite integer)
RETURNS SETOF integer
AS $$
BEGIN
  FOR i IN 1..limite LOOP
    RETURN NEXT i;
  END LOOP;
END
$$ LANGUAGE plpgsql;
```

</div>

<div class="notes">

  * Le retour de l'exécution de cette requête :
	
```sql
ma_base=# SELECT * FROM liste_entier(3);
 
liste_entier
--------------
1
2
3
(3 lignes)
```

</div>

-----

## Structures de contrôles

<div class="slide-content">

  * Pourquoi du PL?
  * Le but du PL est de pouvoir effectuer des traitements procéduraux.
  * Nous allons donc maintenant aborder les structures de contrôle :
    * Tests
	* Boucles

</div>

<div class="notes">

</div>

-----

### Tests `IF/THEN/ELSE/END IF`

<div class="slide-content">

```sql
IF condition THEN
  instructions
[ELSEIF condition THEN
 instructions]
[ELSEIF condition THEN
  instructions]
[ELSE
  instructions]
END IF
```

</div>

<div class="notes">

Ce dernier est l'équivalent d'un `CASE` en _C_ pour une vérification de
plusieurs alternatives.

</div>

-----

### Tests `IF/THEN/ELSE/END IF` - exemple

<div class="slide-content">

```sql
IF nombre = 0 THEN
  resultat := 'zero';
ELSEIF nombre > 0 THEN
   resultat := 'positif';
ELSEIF nombre < 0 THEN
   resultat := 'négatif';
ELSE
   resultat := 'indéterminé';
END IF;
```


</div>

<div class="notes">

</div>

-----

### Tests `CASE`

<div class="slide-content">

Deux possibilités :
    
```sql
CASE variable
WHEN expression THEN instructions
ELSE instructions
END CASE
```

```sql
CASE
WHEN expression-booléene THEN instructions
ELSE instructions
END CASE
```

</div>

<div class="notes">

Quelques exemples :

```sql
CASE x
  WHEN 1, 2 THEN
    msg := 'un ou deux';
  ELSE
    msg := 'autre valeur que un ou deux';
END CASE;
```

```sql
CASE
  WHEN x BETWEEN 0 AND 10 THEN
    msg := 'la valeur est entre 0 et 10';
  WHEN x BETWEEN 11 AND 20 THEN
  msg := 'la valeur est entre 11 et 20';
END CASE;
```

</div>

-----

### Boucle `LOOP/EXIT/CONTINUE`

<div class="slide-content">

  * Créer une boucle (label possible)
    * `LOOP` / `END LOOP` :
  * Sortir de la boucle
    * `EXIT [label] [WHEN expression_booléenne]`
  * Commencer une nouvelle itération de la boucle
    * `CONTINUE [label] [WHEN expression_booléenne]`

</div>

<div class="notes">

Label de blocs

Dans les boucles :

  * Labels de bloc possibles
  * Plusieurs blocs d'exception possibles dans une fonction
  * Permet de préfixer des variables avec le label du bloc
  * De donner un label à une boucle itérative et de préciser de quelle boucle
  on veut sortir, quand plusieurs d'entre elles sont imbriquées

Indiquer le nom d'un label ainsi :

```
<<mon_label>>
-- le code (blocs DECLARE, BEGIN-END, et EXCEPTION)
```

ou bien (pour une boucle)

```
[ <<mon_label>> ]
LOOP
    ordres …
END LOOP [ mon_label ];
```

Il est aussi bien sûr possible d'utiliser des labels pour des boucles `FOR`,
`WHILE`, `FOREACH`.

On sort d'un bloc ou d'une boucle avec la commande `EXIT`, on peut aussi
utiliser `CONTINUE` pour passer à l'exécution suivante d'une boucle sans
terminer l'itération courante.

Par exemple :

```
EXIT [mon_label] WHEN compteur > 1;
```

</div>

-----

### Boucle `LOOP/EXIT/CONTINUE` - exemple

<div class="slide-content">

```sql
LOOP
  resultat := resultat + 1;
  EXIT WHEN resultat > 100;
  CONTINUE WHEN resultat < 50;
  resultat := resultat + 1;
END LOOP;
```

</div>

<div class="notes">

Cette boucle incrémente le resultat de 1 à chaque itération tant que la
valeur de resultat est inférieur à 50. Ensuite, resultat est incrémenté de 1
deux fois. Arrivé à 100, la procédure sort de la boucle.

</div>

-----

### Boucle `WHILE`

<div class="slide-content">

```sql
WHILE condition LOOP instructions END LOOP;
```

  * Boucle jusqu'à ce que la condition soit fausse
  * Label possible

</div>

<div class="notes">

</div>

-----

### Boucle `FOR`

<div class="slide-content">

```sql
FOR variable in [REVERSE] entier1..entier2 [BY incrément]
LOOP
instructions
END LOOP;
```

  * `variable` va obtenir les différentes valeurs entre entier1 et entier2
  * Label possible.

</div>

<div class="notes">

  * L'option `BY` permet d'augmenter l'incrémentation :
    ```sql
    FOR variable in 1..10 BY 5...
    ```
s  * L'option `REVERSE` permet de faire défiler les valeurs en ordre inverse :
    ```sql
    FOR variable in REVERSE 10..1 ...
    ```

</div>

-----

### Boucle `FOR... IN... LOOP`

<div class="slide-content">

```sql
FOR ligne IN SELECT * FROM ma_table LOOP
  instructions
END LOOP;
```

  * Permet de boucler dans les lignes résultats d'une requête
  * Label possible
  * `ligne` de type `RECORD`, `ROW` ou liste de variables séparées par des
    virgules
  * Utilise un curseur en interne

</div>

<div class="notes">

Exemple :

```sql
FOR a, b, c, d IN SELECT col_a, col_b, col_c, col_d FROM ma_table
LOOP
  -- instructions
END LOOP;
```

</div>

-----

### Boucle `FOREACH`

<div class="slide-content">

```sql
FOREACH variable [SLICE n] IN ARRAY expression LOOP
    instructions
END LOOP
```

  * Permet de boucler sur les éléments d'un tableau
  * `variable` va obtenir les différentes valeurs du tableau retourné par
	`expression`
  * `SLICE` permet de jouer sur le nombre de dimensions du tableau à passer à
	la variable

</div>

<div class="notes">

Voici deux exemples permettant d'illustrer l'utilité de `SLICE` :

  * sans `SLICE` :

```sql
do $$
declare a int[] := ARRAY[[1,2],[3,4],[5,6]];
        b int;
begin
  foreach b in array a loop
  raise info 'var: %', b;
end loop;
end $$;
INFO:  var: 1
INFO:  var: 2
INFO:  var: 3
INFO:  var: 4
INFO:  var: 5
INFO:  var: 6
```

  * Avec `SLICE` :

```sql
do $$
declare a int[] := ARRAY[[1,2],[3,4],[5,6]];
        b int[];
begin
  foreach b slice 1 in array a loop
  raise info 'var: %', b;
end loop;
end $$;
INFO:  var: {1,2}
INFO:  var: {3,4}
INFO:  var: {5,6}
```

</div>

-----

## Retour d'une fonction

<div class="slide-content">

  * `RETURN [expression]`
  * Renvoie cette expression à la requête appelante
  * `expression` optionnelle si argument(s) déclarés OUT
    * `RETURN` lui-même optionnel si argument(s) déclarés OUT

</div>

<div class="notes">

</div>

-----
### `RETURN NEXT`

<div class="slide-content">

  * Fonction `SETOF`, aussi appelé fonction SRF (`Set Returning Function`)
  * `RETURN NEXT` renvoie une ligne du `SETOF`
  * Cette fonction s'appelle de cette façon :

```sql
SELECT * FROM ma_fonction();
```

</div>

<div class="notes">

`RETURN NEXT` fonctionne avec des types scalaires (normaux) et des types
composites.

Tout est conservé en mémoire jusqu'à la fin de la fonction. Donc, si beaucoup
de données sont renvoyées, cela pourrait occasionner quelques lenteurs.

Par ailleurs, il est possible d'appeler une SRF par

```sql
SELECT ma_fonction();
```

Dans ce cas, on récupère un résultat d'une seule colonne, de type composite.

</div>

-----

### RETURN QUERY

<div class="slide-content">

  * Fonctionne comme `RETURN NEXT`

```sql
RETURN QUERY la_requete
```

```sql
RETURN QUERY EXECUTE chaine_requete
```

</div>

<div class="notes">


Par ce mécanisme, on peut très simplement produire une fonction retournant le
résultat d'une requête complexe fabriquée à partir de quelques paramètres.

</div>

-----

## Exemple de Fonction _PL/PgSQL_

<div class="slide-content">

  * Permet d'insérer une facture associée à un client
  * Si le client n'existe pas, une entrée est créée
  * L'accès aux données est simple et naturel
  * Les types de données _SQL_ sont natifs
  * La capacité de traitement est limitée par le langage
  * **Attention** au nommage des variables et paramètres

</div>

<div class="notes">

Pour éviter les conflits avec les objets de la base, il est conseillé de
préfixer les variables.

```sql
CREATE OR REPLACE FUNCTION
public.demo_insert_plpgsql(p_nom_client text, p_titre_facture text)
 RETURNS integer
 LANGUAGE plpgsql
 STRICT
AS $function$
DECLARE
  v_id_facture int;
  v_id_client int;
BEGIN
  -- Le client existe-t-il ?
  SELECT id_client
  INTO v_id_client
  FROM mes_clients
  WHERE nom_client = p_nom_client;
  -- Sinon on le crée :
  IF (NOT FOUND) THEN
    INSERT INTO mes_clients (nom_client)
    VALUES (p_nom_client)
    RETURNING id_client INTO v_id_client;
  END IF;
  -- Dans les deux cas, l'id client est dans v_id_client

  -- Insérons maintenant la facture
  INSERT INTO mes_factures (titre_facture, id_client)
  VALUES (p_titre_facture, v_id_client)
  RETURNING id_facture INTO v_id_facture;

  return v_id_facture;
END;
$function$ ;
```

</div>

-----

## Gestion des erreurs

<div class="slide-content">

**¡ Sans exceptions !**

  * Toute erreur provoque un arrêt de la fonction
  * Toute modification suite à une instruction _SQL_ (`INSERT, UPDATE, DELETE`)
	est annulée
  * gestion personnalisée des erreurs possible grâce au bloc `EXCEPTION`

</div>

<div class="notes">

</div>

-----

### Gestion des erreurs : bloc `EXCEPTION`

<div class="slide-content">

La fonction comporte un bloc supplémentaire :

```sql
DECLARE
  -- déclaration des variables locales
BEGIN
  -- instructions de la fonction
EXCEPTION
WHEN condition THEN
  -- instructions traitant cette erreur
WHEN condition THEN
  -- autres instructions traitant cette autre erreur
  -- etc.
END
```

</div>

<div class="notes">

#### Flot dans une fonction

  * L'exécution de la fonction commence après le `BEGIN`
  * Si aucune erreur ne survient, le bloc EXCEPTION est ignoré
  * Si une erreur se produit
    * tout ce qui a été modifié dans la base dans le bloc est annulé
    * les variables gardent par contre leur état
    * l'exécution passe directement dans le bloc de gestion de l'exception

#### Flot dans une exception

  * Recherche d'une condition satisfaisante
  * Si cette condition est trouvée
    * exécution des instructions correspondantes
  * Si aucune condition n'est compatible
    * sortie du bloc `BEGIN/END` comme si le bloc d'exception n'existait pas
    * passage de l'exception au bloc `BEGIN/END` contenant (après annulation de
		ce que ce bloc a modifié en base)
  * Dans un bloc d'exception, les instructions `INSERT`, `UPDATE`, `DELETE` de
	la fonction ont été annulées
  * Dans un bloc d'exception, les variables locales de la fonction ont gardé
	leur ancienne valeur

#### Codes d'erreurs

  * `SQLSTATE` : code d'erreur
  * `SQLERRM` : message d'erreur
  * par exemple :
    * `Data Exception` : division par zéro, overflow, argument invalide pour
		certaines fonctions, etc.
    * `Integrity Constraint Violation` : unicité, CHECK, clé étrangère, etc.
    * `Syntax Error`
    * `PL/pgsql Error` : `RAISE EXCEPTION`, pas de données, trop de lignes, etc.
  * Les erreurs sont contenues dans des classes d'erreurs plus génériques, qui
	peuvent aussi être utilisées

Toutes les erreurs sont référencées [dans la documentation](
http://docs.postgresql.fr/current/errcodes-appendix.html)

Attention, des codes d'erreurs nouveaux apparaissent à chaque version.

La classe `data_exception` contient de nombreuses erreurs, comme
`datetime_field_overflow`, `invalid_escape_character`,
`invalid_binary_representation`… On peut donc, dans la déclaration de
l'exception, intercepter toutes les erreurs de type `data_exception` d'un coup,
ou une par une.

L'instruction `GET STACKED DIAGNOSTICS` permet d'avoir une vision plus précise
de l'erreur récupéré par le bloc de traitement des exceptions. La liste de
toutes les informations que l'on peut collecter est disponible [dans la
documentation](
http://docs.postgresql.fr/current/plpgsql-control-structures.html#plpgsql-exception-diagnostics-values)
.

La démonstration ci-dessous montre comment elle peut être utilisée.

```sql
# CREATE TABLE t5(c1 integer PRIMARY KEY);
CREATE TABLE
# INSERT INTO t5 VALUES (1);
INSERT 0 1
# CREATE OR REPLACE FUNCTION test(INT4) RETURNS void AS $$
DECLARE
    v_state   TEXT;
    v_msg     TEXT;
    v_detail  TEXT;
    v_hint    TEXT;
    v_context TEXT;
BEGIN
    BEGIN
        INSERT INTO t5 (c1) VALUES ($1);
    EXCEPTION WHEN others THEN
        GET STACKED DIAGNOSTICS
            v_state   = RETURNED_SQLSTATE,
            v_msg     = MESSAGE_TEXT,
            v_detail  = PG_EXCEPTION_DETAIL,
            v_hint    = PG_EXCEPTION_HINT,
            v_context = PG_EXCEPTION_CONTEXT;
        raise notice E'Et une exception :
            state  : %
            message: %
            detail : %
            hint   : %
            context: %', v_state, v_msg, v_detail, v_hint, v_context;
    END;
    RETURN;
END;
$$ LANGUAGE plpgsql;
# SELECT test(2);
 test
------

(1 row)

# SELECT test(2);
NOTICE:  Et une exception :
            state  : 23505
            message: duplicate key value violates unique constraint "t5_pkey"
            detail : Key (c1)=(2) already exists.
            hint   :
            context: SQL statement "INSERT INTO t5 (c1) VALUES ($1)"
PL/pgSQL function test(integer) line 10 at SQL statement
 test
------

(1 row)
```

</div>

-----

### Pour aller plus loin

<div class="slide-content">

  * Documentation officielle
    * « Chapitre 43. _PL/PgSQL_ - Langage de procédures _SQL_ »

</div>

<div class="notes">

La documentation officielle sur le langage _PL/PgSQL_ peut être consultée en
français [à cette adresse](http://docs.postgresql.fr/15/plpgsql.html).

</div>

-----

## Les triggers

<div class="slide-content">

  * Aperçu du comportement des déclencheurs
  * Les variables disponibles
  * Traitement du retour
  * Options de `CREATE TRIGGER`

</div>

<div class="notes">

</div>

-----

### Procédures trigger : introduction

<div class="slide-content">

  * Action déclenchée par un ordre de _DML_ :
    * INSERT (incluant COPY), UPDATE, DELETE ou TRUNCATE
  * Mode **par ligne** ou **par instruction**
  * Exécution d'une procédure stockée
    * codée dans un des langages de procédure disponible

</div>

<div class="notes">


Un déclencheur est une spécification précisant que la base de données doit
exécuter une fonction particulière quand un certain type d'opération est
traité. Les fonctions déclencheurs peuvent être définies pour s'exécuter avant
ou après une commande `INSERT`, `UPDATE`, `DELETE` ou `TRUNCATE`.

Un déclencheur peut être attaché à une table ou à une vue.

La fonction déclencheur doit être définie avant que le déclencheur lui-même
puisse être créé. La fonction déclencheur doit être déclarée comme une fonction
ne prenant aucun argument et retournant un type trigger.

Une fois qu'une fonction déclencheur est créée, le déclencheur est créé avec
CREATE TRIGGER. La même fonction déclencheur est utilisable par plusieurs
déclencheurs.

Un trigger `TRUNCATE` ne peut utiliser que le mode par instruction,
contrairement aux autres triggers pour lesquels vous avez le choix entre « par
ligne » et « par instruction ».

Enfin, l'instruction `COPY` est traitée comme s'il s'agissait d'une commande
`INSERT`.

</div>

-----

### Les variables `OLD` et `NEW` (1/2)

<div class="slide-content">

  * `OLD` :
    * type de données `RECORD` correspondant à la ligne avant modification
    * valable pour un `DELETE` et un `UPDATE`
  * `NEW` :
    * type de données `RECORD` correspondant à la ligne après modification
    * valable pour un `INSERT` et un `UPDATE`

</div>

<div class="notes">

</div>

-----

### Les variables `OLD` et `NEW` (2/2)

<div class="slide-content">

  * Ces deux variables sont valables uniquement pour les triggers en mode ligne
    * pour les triggers en mode instruction : tables de transition
  * Accès aux champs par la notation pointée
    * `NEW.champ1` pour accéder à la nouvelle valeur de `champ1`

</div>

<div class="notes">

</div>

-----

### Les variables d'opération

<div class="slide-content">

  * `TG_NAME` : nom du trigger ayant déclenché l'appel de la fonction
  * `TG_WHEN` : type du trigger (`BEFORE`, `AFTER` ou `INSTEAD OF`)
  * `TG_LEVEL` : mode du tigger (`ROW` ou `STATEMENT`)
  * `TG_OP` : opération ayant déclenchée le trigger (`INSERT`, `UPDATE`,
    `DELETE`, `TRUNCATE`)

</div>

<div class="notes">

</div>

-----

### Les variables de relations

<div class="slide-content">

  * `TG_TABLE_SCHEMA` : nom du schéma contenant la table...
  * `TG_TABLE_NAME` : nom de la table...
  * `TG_RELID` : `OID` de la table...

... ayant déclenché le trigger

</div>

<div class="notes">

Vous pourriez aussi rencontrer dans du code `TG_RELNAME`. C'est aussi le nom de
la table qui a déclenché le trigger. Attention, cette variable est obsolète, il
est préférable d'utiliser maintenant TG_TABLE_NAME

</div>

-----

### Les variables d'arguments

<div class="slide-content">

  * `TG_NARGS` : nombre d'arguments donnés à la fonction trigger
  * `TG_ARGV` : les arguments donnés à la fonction trigger (le tableau commence
	à 0)

</div>

<div class="notes">

La fonction trigger est déclarée sans arguments mais il est possible de lui en
passer dans la déclaration du trigger. Dans ce cas, il faut utiliser les deux
variables ci-dessus pour y accéder. Attention, tous les arguments sont
convertis en texte. Il faut donc se cantonner à des informations simples, sous
peine de compliquer le code.

```sql
CREATE OR REPLACE FUNCTION verifier_somme()
RETURNS trigger AS $$
DECLARE
    fact_limit integer;
    arg_color varchar;
BEGIN
    fact_limit   := TG_ARGV[0];

    IF NEW.somme > fact_limit THEN
       RAISE NOTICE 'La facture % necessite une verification. '
                    'La somme % depasse la limite autorisee de %.',
                    NEW.idfact, NEW.somme, fact_limit;
    END IF;

    NEW.datecreate := current_timestamp;

    return NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trig_verifier_debit
   BEFORE INSERT OR UPDATE ON test
   FOR EACH ROW
   EXECUTE PROCEDURE verifier_somme(400);

CREATE TRIGGER trig_verifier_credit
   BEFORE INSERT OR UPDATE ON test
   FOR EACH ROW
   EXECUTE PROCEDURE verifier_somme(800);
```

</div>

-----

### Traitement du retour

<div class="slide-content">

Type de retour spécial : le type `trigger`

```sql
CREATE OR REPLACE FUNCTION mon_trigger()
RETURNS trigger AS $$
    (...)
$$ LANGUAGE plpgsql;
```

</div>

<div class="notes">

Une fonction de trigger retourne le type spécial `trigger`, pour cette raison
ces fonctions ne peuvent être utilisées que dans le contexte d'un ou plusieurs
triggers.

</div>

-----

### Retour des type `ROW` - `BEFORE`

<div class="slide-content">

  * Si retour `NULL`, annulation de l'opération, sans déclencher d'erreur
  * Sinon, poursuite de l'opération avec cette valeur de ligne

</div>

<div class="notes">

Il est possible d'annuler l'action d'un trigger de type ligne avant l'opération
en retournant `NULL`. Ceci annule purement et simplement le trigger sans
déclencher d'erreur.

</div>

-----

### Retour des type `ROW` - `AFTER` et `STATEMENT`

<div class="slide-content">

  * Valeur de retour ignorée
  * Annulation possible si erreur à l’exécution de la fonction

</div>

<div class="notes">

Pour les triggers de type ligne intervenant après l'opération, comme pour les
triggers à l'instruction, une valeur de retour est inutile. Elle est ignorée.

Il est possible d'annuler l'action d'un trigger de type ligne intervenant après
l'opération ou d'un trigger à l'instruction, en remontant une erreur à
l'exécution de la fonction.

</div>

-----

### Options de `CREATE TRIGGER`

<div class="slide-content">

* Déclenchement conditionnel :

```sql
CREATE TRIGGER name WHEN ( condition )
```

* Déclenchement limité à une colonne d'une table :

```sql
CREATE TRIGGER name BEFORE UPDATE OF colx ON my_table
```

* Action de _DML_ sur une vue :

```sql
CREATE TRIGGER view_insert INSTEAD OF INSERT ON my_view`
```

</div>

<div class="notes">

On peut ne déclencher un trigger que si une condition est vérifiée. Cela
simplifie souvent le code du trigger, et gagne en performances : plus du
tout besoin pour le moteur d'aller exécuter la fonction.

On peut ne déclencher un trigger que si une colonne spécifique a été
modifiée. Il ne s'agit donc que de triggers sur update. Encore un moyen de
simplifier le code et de gagner en performances en évitant les
déclenchements inutiles.

On peut créer un trigger sur une vue. C'est un trigger `INSTEAD OF`, qui permet
de programmer de façon efficace les `INSERT/UPDATE/DELETE/TRUNCATE` sur les
vues. Auparavant, il fallait passer par le système de règles (`RULES`),
complexe et sujet à erreurs.

On peut créer un trigger en le déclarant comme étant un trigger de
contrainte. Il peut alors être 'deferrable', 'deferred', comme tout autre
contrainte, c'est à dire n'être exécutée qu'au moment de la validation de
la transaction, ce qui permet de ne vérifier les contraintes implémentées
par le trigger qu'au moment de la validation finale :

```sql
CREATE CONSTRAINT TRIGGER
```

</div>

-----

### Procédures trigger : exemple - 1

<div class="slide-content">

Horodater une opération sur une ligne :

```sql
CREATE TABLE ma_table (
  id serial,
  -- un certain nombre de champs informatifs
  date_ajout timestamp,
  date_modif timestamp);
```

</div>

<div class="notes">

</div>

-----

### Procédures trigger : exemple - 2

<div class="slide-content">

```sql
CREATE OR REPLACE FUNCTION horodatage() RETURNS trigger
AS $$
BEGIN
  IF TG_OP = 'INSERT' THEN
    NEW.date_ajout := now();
  ELSEIF TG_OP = 'UPDATE' THEN
    NEW.date_modif := now();
  END IF;
  RETURN NEW;
END; 
$$ LANGUAGE plpgsql;
```

</div>

<div class="notes">

</div>

-----

### Tables de transition

<div class="slide-content">

  * `REFERENCING OLD TABLE`
  * `REFERENCING NEW TABLE`
  * Par exemple :

```sql
CREATE TRIGGER tr1
  AFTER DELETE ON t1
  REFERENCING OLD TABLE AS oldtable
  FOR EACH STATEMENT
  EXECUTE PROCEDURE log_delete();
```

</div>

<div class="notes">

Dans le cas d'un trigger en mode instruction, il n'est pas possible d'utiliser
les variables `OLD` et `NEW` car elles ciblent une seule ligne. Pour cela, le
standard SQL parle de tables de transition. Voici un exemple de leur
utilisation.

Nous allons créer une table t1 qui aura le trigger et une table poubelle qui a
pour but de récupérer les enregistrements supprimés de la table t1.

```
CREATE TABLE t1 (c1 integer, c2 text);
CREATE TABLE poubelle (id integer GENERATED ALWAYS AS IDENTITY,
  dlog timestamp DEFAULT now(),
  t1_c1 integer, t1_c2 text);
```

Maintenant, il faut créer le code de la procédure stockée :

```
CREATE OR REPLACE FUNCTION log_delete() RETURNS trigger LANGUAGE plpgsql AS $$
BEGIN
  INSERT INTO poubelle (t1_c1, t1_c2) SELECT c1, c2 FROM oldtable;
  RETURN null;
END
$$;
```

Et ajouter le trigger sur la table t1 :

```
CREATE TRIGGER tr1
AFTER DELETE ON t1
REFERENCING OLD TABLE AS oldtable
FOR EACH STATEMENT
EXECUTE PROCEDURE log_delete();
```

Maintenant, insérons un million de ligne dans t1 et supprimons-les :

```
$ INSERT INTO t1 SELECT i, 'Ligne '||i FROM generate_series(1, 1000000) i;
INSERT 0 1000000
$ \timing on
$ DELETE FROM t1;
DELETE 1000000
Time: 6753.294 ms (00:06.753)
```

La suppression avec le trigger prend 6.7 secondes. Il est possible de connaître
le temps à supprimer les lignes et le temps à exécuter le trigger en utilisant
l'ordre `EXPLAIN ANALYZE` :

```
$ TRUNCATE poubelle;
TRUNCATE TABLE
Time: 545.840 ms
$ INSERT INTO t1 SELECT i, 'Ligne '||i FROM generate_series(1, 1000000) i;
INSERT 0 1000000
Time: 4259.430 ms (00:04.259)
$ EXPLAIN (ANALYZE) DELETE FROM t1;
                        QUERY PLAN
------------------------------------------------------------------------
 Delete on t1  (cost=0.00..17880.90 rows=1160690 width=6)
               (actual time=2552.210..2552.210 rows=0 loops=1)
   ->  Seq Scan on t1  (cost=0.00..17880.90 rows=1160690 width=6)
	                     (actual time=0.071..183.026 rows=1000000 loops=1)
 Planning time: 0.094 ms
 Trigger tr1: time=4424.651 calls=1
 Execution time: 6982.290 ms
(5 rows)
```

Donc la suppression des lignes met 2,5 secondes alors que l'exécution du
trigger met 4,4 secondes.

Le gros intérêt des tables de transition est le gain en performance que cela
apporte. Sur la suppression d'un million de lignes, il est deux fois plus
rapide de passer par un trigger en mode instruction que par un trigger en mode
ligne.

</div>

-----

### Déclencheurs sur évènement 

<div class="slide-content">

  * Trigger global à une base de données
	* Capable de capturer tous les évènements _DDL_
  * Type de retour spécial de la fonction : `event_trigger`
  * ne peut pas être écrit en _SQL_

</div>

<div class="notes">

La syntaxe de l'ordre de crétion est le suivant :

```
CREATE EVENT TRIGGER nom
  ON evenement
  [ WHEN variable_filtre IN (valeur_filtre [, ... ]) [ AND ... ] ]
  EXECUTE PROCEDURE nom_fonction()
```

Les évènements possibles sont :

  * `ddl_command_start` : se déclenche juste avant l'exécution d'une commande
	`CREATE, ALTER, DROP, SECURITY LABEL, COMMENT, GRANT ou REVOKE`.
	* `ddl_command_end` : se déclenche juste après l'exécution de ces même
	ensembles de commandes.
	* `sql_drop` : se déclenche juste avant le trigger sur évènement
	`ddl_command_end` pour toute opération qui supprime des objets de la base.
	* `table_rewrite` : se déclenche juste avant qu'une table soit modifiée par
	certaines actions des commandes ALTER TABLE et ALTER TYPE.

</div>

-----

### Pour aller plus loin

<div class="slide-content">

  * Documentation officielle
    * « Chapitre 39. Déclencheurs (_triggers_) »
	* « Chapitre 40. Déclencheurs (_triggers_) sur évènement »

</div>

<div class="notes">


La documentation officielle sur le langage _PL/PgSQL_ peut être consultée en
français [à cette adresse](https://docs.postgresql.fr/15/triggers.html).

</div>

-----
