<div class="notes">

### Solutions du TD 5

#### Question de cours

  * Quand est-il préférable d'utiliser un langage PL plutôt que SQL ?
    * SLQ est un langage _déclaratif_. On demande au logiciel le résultat en
      lui laissant le soin de sélectionner l'algorithme le plus efficace. SQL
      ne permet pas l'utilisation de construct habituel en programmation :
      boucles ou instruction conditionnelles. Les langages de procédures (_PL_)
      permettent d'une part une programmation impérative. Ils permettent
      également de réaliser des appels systèmes.
  * Pourquoi utiliser PL/pgSQL plutôt que de traiter les données côté
    application ?
    *  Grâce à PL/pgSQL vous pouvez grouper un bloc de traitement et une série
       de requêtes au sein du serveur de bases de données, et bénéficier ainsi
       de la puissance d'un langage de procédures, mais avec de gros gains en
       terme de communication client/serveur.
      * Les allers/retours entre le client et le serveur sont éliminés
      * Il n'est pas nécessaire de traiter ou transférer entre le client et le
        serveur les résultats intermédiaires dont le client n'a pas besoin.
      * Les va-et-vient des analyses de requêtes peuvent être évités.
      * L'exécution sur le serveur de base de données permet d'éviter de
        surcharger le serveur client.
      * Le traitement au sein d'un fonction permet de masquer les résultats
        intermédiaires du traitement au client. Il est cependant à noter que
        l'utilisateur a accès à la définition de la fonction.
  * Sur quels types d'opération peut-on définir un trigger ?
    * Sur des opération _DML_ modifiant les données (`INSERT`, `UPDATE`,
      `DELETE`, `TRUNCATE`) mais également sur des opérations _DDL_ (`CREATE,
      ALTER, DROP, SECURITY LABEL, COMMENT, GRANT ou REVOKE`)

#### Fonctions _PL/PgSQL_

**Exercice 1**

  * version de base : 

```sql
CREATE OR REPLACE FUNCTION division(arg1 integer, arg2 integer)
RETURNS float4
AS $BODY$
  BEGIN
    RETURN arg1::float4/arg2::float4;
  END
$BODY$
LANGUAGE plpgsql;
```

Requêtage :

cave=# SELECT division(1,5);
 division
----------
      0.2
(1 ligne)

cave=# SELECT division(1,0);
ERREUR:  division par zéro
CONTEXTE : PL/pgSQL function "division" line 2 at return
```

  * gestion de la division par 0 :

```sql
CREATE OR REPLACE FUNCTION division(arg1 integer, arg2 integer)
RETURNS float4
AS $BODY$
  BEGIN
    IF arg2 = 0 THEN
      RETURN 'NaN';
    ELSE
      RETURN arg1::float4/arg2::float4;
    END IF;
  END $BODY$
LANGUAGE plpgsql;
```

Requêtage 2 :

```sql
cave=# SELECT division(1,5);
 division
----------
      0.2
(1 ligne)

cave=# SELECT division(3,0);
 division
----------
      NaN
(1 ligne)
```

  * Tracer la division par zéro via les exceptions :

```sql
  CREATE OR REPLACE FUNCTION division(arg1 integer, arg2 integer)
    RETURNS float4 AS
  $BODY$BEGIN
    RETURN arg1::float4/arg2::float4;
    EXCEPTION WHEN OTHERS THEN
      -- attention, division par zéro
      RAISE LOG 'attention, [%]: %', SQLSTATE, SQLERRM;
      RETURN 'NaN';
  END $BODY$
    LANGUAGE 'plpgsql' VOLATILE;
```

Requêtage 3 :

```sql
  cave=# SET client_min_messages TO log;
  SET
  cave=# SELECT division(1,5);
   division
  ----------
        0.2
  (1 ligne)

  cave=# SELECT division(1,0);
  LOG:  attention, [22012]: division par zéro
   division
  ----------
        NaN
  (1 ligne)
```

**Exercice 2**

```sql
CREATE OR REPLACE FUNCTION multiplication(arg1 text, arg2 text)
RETURNS integer
AS $BODY$
  DECLARE
    a1 integer;
    a2 integer;
  BEGIN

    -- en utilisant des IF ELSEIF
    IF arg1 = 'zéro' THEN
      a1 := 0;
    ELSEIF arg1 = 'un' THEN
      a1 := 1;
    ELSEIF arg1 = 'deux' THEN
      a1 := 2;
    ELSEIF arg1 = 'trois' THEN
      a1 := 3;
    ELSEIF arg1 = 'quatre' THEN
      a1 := 4;
    ELSEIF arg1 = 'cinq' THEN
      a1 := 5;
    ELSEIF arg1 = 'six' THEN
      a1 := 6;
    ELSEIF arg1 = 'sept' THEN
      a1 := 7;
    ELSEIF arg1 = 'huit' THEN
      a1 := 8;
    ELSEIF arg1 = 'neuf' THEN
      a1 := 9;
    END IF;

    -- en utilisant le construct CASE WHEN
    CASE WHEN arg2 = 'zéro' THEN
      a1 := 0;
    WHEN arg2 = 'un' THEN
      a1 := 1;
    WHEN arg2 = 'deux' THEN
      a1 := 2;
    WHEN arg2 = 'trois' THEN
      a1 := 3;
    WHEN arg2 = 'quatre' THEN
      a1 := 4;
    WHEN arg2 = 'cinq' THEN
      a1 := 5;
    WHEN arg2 = 'six' THEN
      a1 := 6;
    WHEN arg2 = 'sept' THEN
      a1 := 7;
    WHEN arg2 = 'huit' THEN
      a1 := 8;
    WHEN arg2 = 'neuf' THEN
      a1 := 9;
    END CASE;

    RETURN a1*a2;
  END
$BODY$
LANGUAGE plpgsql;
```

Requêtage:

```sql
cave=# SELECT multiplication('deux', 'trois');
 multiplication
----------------
              6
(1 ligne)
```

-----

**Exercice 3**

```sql
CREATE OR REPLACE FUNCTION nb_bouteilles(v_annee integer, v_typevin text)
RETURNS integer
AS $BODY$
  DECLARE
    nb integer;

  BEGIN
    SELECT INTO nb SUM(stock.nombre)
    FROM vin
    JOIN stock ON stock.vin_id = vin.id
    JOIN type_vin ON type_vin.id = vin.type_vin_id
    WHERE
      stock.annee=v_annee
      AND type_vin.libelle=v_typevin;

    RETURN nb;
  END
$BODY$
LANGUAGE 'plpgsql';
```

Requêtage :


  * Sur une seule année :

```sql
SELECT nb_bouteilles(2000, 'rouge');
```

  * Sur une période :

```sql
SELECT a, nb_bouteilles(a, 'rosé')
  FROM generate_series(1990, 1999) AS a
  ORDER BY a;
```

#### Triggers

**Exercice 4**


La table de log :

```sql
  CREATE TABLE log_stock (
  id serial,
  dateheure timestamp,
  operation char(1),
  vin_id integer,
  contenant_id integer,
  annee integer,
  anciennevaleur integer,
  nouvellevaleur integer);
```

La fonction trigger :

```sql
  CREATE OR REPLACE FUNCTION log_stock_nombre()
    RETURNS TRIGGER AS
  $BODY$DECLARE
    v_requete text;
    v_operation char(1);
    v_vinid integer;
    v_contenantid integer;
    v_annee integer;
    v_anciennevaleur integer;
    v_nouvellevaleur integer;
    v_atracer boolean := false;
  BEGIN

    -- ce test a pour but de vérifier que le contenu de nombre a bien changé
    -- c'est forcément le cas dans une insertion et dans une suppression
    -- mais il faut tester dans le cas d'une mise à jour en se méfiant
    -- des valeurs NULL
    v_operation := substr(TG_OP, 1, 1);
    IF TG_OP = 'INSERT'
    THEN
      -- cas de l'insertion
      v_atracer := true;
      v_vinid := NEW.vin_id;
      v_contenantid := NEW.contenant_id;
      v_annee := NEW.annee;
      v_anciennevaleur := NULL;
      v_nouvellevaleur := NEW.nombre;
    ELSEIF TG_OP = 'UPDATE'
    THEN
      -- cas de la mise à jour
      v_atracer := OLD.nombre != NEW.nombre;
      v_vinid := NEW.vin_id;
      v_contenantid := NEW.contenant_id;
      v_annee := NEW.annee;
      v_anciennevaleur := OLD.nombre;
      v_nouvellevaleur := NEW.nombre;
    ELSEIF TG_OP = 'DELETE'
    THEN
      -- cas de la suppression
      v_atracer := true;
      v_vinid := OLD.vin_id;
      v_contenantid := OLD.contenant_id;
      v_annee := OLD.annee;
      v_anciennevaleur := OLD.nombre;
      v_nouvellevaleur := NULL;
    END IF;

    IF v_nouvellevaleur < 1
    THEN
      RAISE WARNING 'Il ne reste plus que % bouteilles dans le stock (%, %, %)',
                    v_nouvellevaleur, OLD.vin_id, OLD.contenant_id, OLD.annee;
    ELSEIF v_nouvellevaleur < 5
    THEN
      RAISE LOG 'Il ne reste plus que % bouteilles dans le stock (%, %, %)',
                v_nouvellevaleur, OLD.vin_id, OLD.contenant_id, OLD.annee;
    END IF;

    IF v_atracer
    THEN
      INSERT INTO log_stock
       (utilisateur, dateheure, operation, vin_id, contenant_id,
        annee, anciennevaleur, nouvellevaleur)
      VALUES
       (current_user, now(), v_operation, v_vinid, v_contenantid,
        v_annee, v_anciennevaleur, v_nouvellevaleur);
    END IF;

    RETURN NEW;

  END $BODY$
    LANGUAGE 'plpgsql' VOLATILE;
```

Le trigger :

```sql
  CREATE TRIGGER log_stock_nombre_trig
  AFTER INSERT OR UPDATE OR DELETE
  ON stock
  FOR EACH ROW
  EXECUTE PROCEDURE log_stock_nombre();
```


</div>

-----

