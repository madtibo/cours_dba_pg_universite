<div class="notes">

### Solutions du TD 4

Voici la correction pour un serveur PostgreSQL en version 10.

```sql
CREATE DOMAIN percentage AS real CHECK ((value > 0) AND (value <= 100));
CREATE DOMAIN positive_real AS real CHECK (value >= 0);

CREATE DOMAIN type_longitude AS real CHECK ((value >= -180) AND (value <= 180));
CREATE DOMAIN type_latitude AS real CHECK ((value >= -90) AND (value <= 90));
CREATE TYPE geo_coordinate AS (longitude type_longitude, latitude type_latitude);


CREATE TABLE country (
 country_id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
 name VARCHAR(35) NOT NULL UNIQUE,
 code VARCHAR(4) NOT NULL UNIQUE,
 area positive_real,
 population positive_real
);

CREATE TABLE province (
 province_id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
 name VARCHAR(35) NOT NULL,
 country_id bigint REFERENCES country (country_id) ON DELETE SET NULL,
 population positive_real,
 area positive_real,
 CONSTRAINT unique_country_name UNIQUE (name,country_id)
);

CREATE TABLE city (
 city_id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
 name VARCHAR(35),
 province_id bigint REFERENCES province (province_id) ON DELETE SET NULL,
 population positive_real,
 city_position geo_coordinate
);

CREATE TABLE country_capital (
 country_capital_id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
 city_id bigint REFERENCES country (country_id) ON DELETE CASCADE, 
 country_id bigint REFERENCES country (country_id) ON DELETE CASCADE
);


CREATE TABLE province_capital (
 province_capital_id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
 city_id bigint REFERENCES country (country_id) ON DELETE CASCADE, 
 province_id bigint REFERENCES province (province_id) ON DELETE CASCADE,
 province_capital_text VARCHAR(35)
);

CREATE TABLE mountain_range (
 mountain_range_id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
 name VARCHAR(35)
);

CREATE TABLE mountain (
 mountain_id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
 name VARCHAR(35) NOT NULL UNIQUE,
 mountain_range_id bigint REFERENCES mountain_range (mountain_range_id)
       ON DELETE SET NULL,
 height positive_real,
 type VARCHAR(10),
 mountain_position geo_coordinate
);

CREATE TABLE mountain_province (
 mountain_province_id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
 mountain_id bigint REFERENCES mountain (mountain_id) ON DELETE CASCADE,
 province_id bigint REFERENCES province (province_id) ON DELETE CASCADE
);

CREATE TABLE spoken_language (
 spoken_language_id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
 name VARCHAR(50) NOT NULL
);

CREATE TABLE country_spoken_language (
 country_spoken_language_id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
 country_id bigint REFERENCES province (province_id) ON DELETE CASCADE,
 spoken_language_id bigint REFERENCES province (province_id) ON DELETE CASCADE,
 spoken_percentage percentage
);

```

</div>

-----

