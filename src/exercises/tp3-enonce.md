## Travaux Pratiques 3

<div class="slide-content">
  * Révision PostgreSQL
</div>

<div class="notes">

**Rappel**

Durant ces travaux pratiques, nous allons mettre en place notre serveur de base
de données PostgreSQL via une nouvelle machine virtuelle.

Effectuez les manipulations nécessaires pour réaliser les actions listées dans
la section _Énoncés_. 

Vous pouvez vous aider des derniers TP, des sections _Administration via bash_
et _psql_ ainsi que de l'aide en ligne ou les pages de manuels (`man`).

### Énoncés

#### Découverte des instances

  1. Déployez la nouvelle machine virtuelle.
  2. Quels versions de PostgreSQL sont-elles disponibles sur votre nouvelle
  machine virtuelle ?
  3. Quelle instance est active ?
  4. Activez et démarrez l'instance de la version la plus récente.
  5. La base cave est disponible sur une seule des instance. Procédez à la
  migration de cette base de données sur l'instance de la version la plus
  récente.

-------

### Machine Virtuelle

TODO
  * Utilisation d'une nouvelle VM centos 7 avec PG 9.4 et 10
  * 3 instances : 9.4-data (active et démarrée), 9.6-data (éteinte),
  10-data (éteinte)
  * bdd : cave sur la 9.4-data, magasin sur la 10-data

Listons les machines virtuelles disponibles :

```bash
virtualbox-createtp -l
```

Sélectionner la machine à créer (il y a _DB_ dans le nom) :

```bash
virtualbox-createtp -c TP_bdd_2 2017-10-05_strech_DB_Admin
```

La commande suivante viendra modifier les paramètres de votre machine virtuelle afin qu'elle utilise 3 CPU et 2 Go de RAM. On ajoute également des routes pour nous permettre de se connecter à notre machine virtuelle en ssh et psql à partir de notre machine hôte :

```bash
vboxmanage modifyvm TP_bdd_2 --cpus 3 --memory 2048
```

Pour lancer votre machine virtuelle, lancer la commande suivante dans votre terminal :

```bash
vboxmanage startvm TP_bdd_2 --type headless
```

A la fin de votre TP, n'oubliez pas de l'éteindre, elle restera sinon allumée et
accessible à quiconque se connecte sur la machine que vous avez utilisé. Vous
pouvez éteindre votre machine virtuelle avec la commande :

```bash
vboxmanage controlvm TP_bdd_2 poweroff
```

Pour se connecter à votre machine virtuelle en ssh, utilisez la commande suivante :

```bash
ssh <login>@192.168.56.102
```

Les mots de passe sont identiques aux logins : _tp_ et _root_.

Pour vous connecter au compte `postgres`, utilisez la commande à partir du
compte _root_ :

```bash
sudo -iu postgres
```

Aide disponible sur la [faq](https://faq.info.unicaen.fr/logiciels:virtualbox)

</div>

-----
