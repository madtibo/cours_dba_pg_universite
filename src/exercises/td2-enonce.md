## Travaux Dirigés 4

<div class="slide-content">
Normalisation de la base de données `mondial`
</div>

<div class="notes">

### Enoncés

  * Soit les objets suivants : 

```sql

CREATE TABLE Country
(Name VARCHAR(35) NOT NULL UNIQUE,
 Code VARCHAR(4) CONSTRAINT CountryKey PRIMARY KEY,
 Capital VARCHAR(35),
 Province VARCHAR(35),
 Area NUMERIC CONSTRAINT CountryArea
   CHECK (Area >= 0),
 Population NUMERIC CONSTRAINT CountryPop
   CHECK (Population >= 0));

CREATE TABLE City
(Name VARCHAR(35),
 Country VARCHAR(4),
 Province VARCHAR(35),
 Population NUMERIC CONSTRAINT CityPop
   CHECK (Population >= 0),
 Longitude NUMERIC CONSTRAINT CityLon
   CHECK ((Longitude >= -180) AND (Longitude <= 180)) ,
 Latitude NUMERIC CONSTRAINT CityLat
   CHECK ((Latitude >= -90) AND (Latitude <= 90)) ,
 CONSTRAINT CityKey PRIMARY KEY (Name, Country, Province));

CREATE TABLE Province
(Name VARCHAR(35) CONSTRAINT PrName NOT NULL ,
 Country  VARCHAR(4) CONSTRAINT PrCountry NOT NULL ,
 Population NUMERIC CONSTRAINT PrPop
   CHECK (Population >= 0),
 Area NUMERIC CONSTRAINT PrAr
   CHECK (Area >= 0),
 Capital VARCHAR(35),
 CapProv VARCHAR(35),
 CONSTRAINT PrKey PRIMARY KEY (Name, Country));
 
CREATE TYPE GeoCoord AS (Longitude DECIMAL, Latitude DECIMAL);

CREATE TABLE Mountain
(Name VARCHAR(35) CONSTRAINT MountainKey PRIMARY KEY,
 Mountains VARCHAR(35),
 Height NUMERIC,
 Type VARCHAR(10),
 Coordinates GeoCoord CONSTRAINT MountainCoord
     CHECK (((Coordinates).Longitude >= -180) AND 
            ((Coordinates).Longitude <= 180) AND
            ((Coordinates).Latitude >= -90) AND
            ((Coordinates).Latitude <= 90)));

CREATE TABLE geo_Mountain
(Mountain VARCHAR(35) ,
 Country VARCHAR(4) ,
 Province VARCHAR(35) ,
 CONSTRAINT GMountainKey PRIMARY KEY (Province,Country,Mountain) );

CREATE TABLE Language
(Country VARCHAR(4),
 Name VARCHAR(50),
 Percentage NUMERIC CONSTRAINT LanguagePercent 
   CHECK ((Percentage > 0) AND (Percentage <= 100)),
 CONSTRAINT LanguageKey PRIMARY KEY (Name, Country));

```

  * Créez le modèle de données de ces objets
  * Normalisez votre modèle de données
  * Créez les ordres SQL créant les objets normalisés

Pour les ordres SQL :

  * Utilisez des _underscore_case_ à la place des _CamelCase_
  * Utilisez des clés primaires de type entier générées automatiquement
  * Créez des _DOMAIN_ pour les contraintes de lignes
  * Ne pas utiliser des mots clés _SQL_ pour les noms des objets
  * Utilisez des clés étrangères pour garantir l'intégrité référentielle

</div>

-----