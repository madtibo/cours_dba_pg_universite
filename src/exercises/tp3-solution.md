<div class="notes">

### Solutions du TP 3


#### Découverte des instances

  1. Quels versions de PostgreSQL sont-elles disponibles sur votre machine
  virtuelle ?

La commande suivante permet de connaitre les version installées :

```bash
$ dpkg -l | grep postgresql
```

Il est également possible de chercher les binaires :

```bash
$ find /usr -name postgres
```

Les versions 9.4 et 9.6 sont installées.

  2. Quelle instance est active ?
  
La commande suivante permet de savoir les instances démarrées :

```bash
systemctl status postgresql@*
```

Seule l'instance main en version 9.4 est démarrée.

  3. Activez et démarrez l'instance main de l'autre version.

```bash
systemctl enable postgresql@9.6-main.service
systemctl start postgresql@9.6-main.service
```

La première commande permet de s'assurer que l'instance main en version 9.6
démarrera en même temps que le serveur. 

  4. La base cave est disponible sur une seule des instance main. Procédez à la
  migration de cette base de données sur l'instance main de l'autre version.

S'agissant de version majeures différentes, nous devons migrer les données et
ne pas oublier de récupérer les objets globaux. Ces opérations se font dans le
terminal avec l'utilisateur _postgres_ :

```bash
pg_dump -p 5432 -Fc -f ~postgres/backup_cave.dump cave
pg_dumpall -p 5432 -g -f ~postgres/global.sql
```

Pour la restauration, nous devons créer la base de données _cave_, recharger les
objets globaux puis la sauvegarde de la base de données :

```bash
createdb -p 5433 cave
psql -p 5433 -d cave -c '\i ~postgres/global.sql'
pg_restore -p 5433 -d cave ~postgres/backup_cave.dump
```

</div>

------

