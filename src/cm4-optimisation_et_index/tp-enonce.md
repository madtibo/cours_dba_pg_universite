
## Travaux Pratiques 4

<div class="slide-content">
  * Optimisation avec PostgreSQL
</div>

<div class="notes">

**Rappel**

Durant ces travaux pratiques, nous allons utiliser la machine virtuelle du TP 1
pour héberger notre serveur de base de données PostgreSQL.

Effectuez les manipulations nécessaires pour réaliser les actions listées dans
la section _Énoncés_. 

Vous pouvez vous aider du cours, de l'annexe de ce TP, des derniers TP,
ainsi que de l'aide en ligne ou des pages de manuels (`man`).

### Énoncés

#### Utilisation de EXPLAIN et indexation

Ces exercices sont à effectuer sur la base `cave` nouvellement restaurée dans
votre instance en version 16.

**Indexation simple**

  1. Écrire la requête permettant de sélectionner le nombre de bouteilles en
  stock de l'année 2012. Quel est son plan d'exécution ?
  2. Rajoutez un index pour que la requête soit plus rapide.
  
**Index partiels**

  1. Créer un index se limitant aux vins avec moins de 10 bouteilles en stock.
  2. Exécuter une requête pour rechercher les vins possédant un stock inférieur
  à 7 bouteilles. Que remarquez-vous ?
  2. Exécuter une requête pour rechercher les vins possédant un stock inférieur
  à 11 bouteilles. Que remarquez-vous ?

**Indexation multi-colonnes**

  1. Créez l'index optimum pour cette requête :

```sql
SELECT * FROM stock
WHERE vin_id IN (SELECT vin_id FROM stock TABLESAMPLE BERNOULLI (0.01))
  AND annee BETWEEN 2004 AND 2007;
```

  2. Quel est la taille sur disque de la table _stock_ ainsi que pour les index
  sur cette table ?

**Recherche de motif texte**

  1. Affichez le plan de cette requête (sur la base `cave`) :

```sql
SELECT * FROM appellation WHERE libelle LIKE 'Brouilly%';
```

  2. Que constatez-vous ?
  3. Affichez maintenant le nombre de blocs accédés par cette requête.
  4. Cette requête ne passe pas par un index. Essayez de lui forcer la main.
  5. L'index n'est toujours pas utilisé. L'index « par défaut » n'est pas
  capable de répondre à des questions sur motif.
  6. Créez un index capable de réaliser ces opérations (indice : vérifier les
	classes d'opérateurs de votre index). Testez à nouveau le plan.
  7. Réactivez `enable_seqscan`. Testez à nouveau le plan.
  8. Quelle est la conclusion ?

**Index trigrammes**

**Pour cet exercice, nous allons utiliser la base de données _magasin_**

  1. Effectuer une recherche textuelle dans les commentaires de la table
  _magasin.commandes_ en sélectionnant les lignes commençant par `qui`.
  2. Créer un index btree capable d'aider à exécuter la requête.
  3. Quels sont les gains de performances ?
  4. Tenter de chercher les lignes se terminant par `qui`, puis les lignes
  contenant `qui`. Que remarquez vous ?
  5. Tenter de chercher les lignes commençant par `qui` mais sans tenir compte
  de la casse ? Que remarquez vous ?
  6. Créer un index `GIN` par trigramme pour des recherche textuelle sur la
  colonne _commentaire_ de la table _magasin.commandes_.
  7. Quel est la taille sur disque de la table _magasin.commandes_ ainsi que
  celle des index que l'on vient de créer ?
  8. Relancer toutes les requêtes précédentes. Que remarquez-vous ?

#### Optimisation de requête

Soit la requête suivante :

```sql
EXPLAIN ANALYZE
SELECT
      m.annee||' - '||a.libelle AS millesime_region,
      SUM(s.nombre) AS contenants,
      SUM(s.nombre*c.contenance) AS litres
FROM
     contenant c
     JOIN stock s
       ON s.contenant_id = c.id
     JOIN (SELECT ROUND(RANDOM()*50)+1970 AS annee) m
       ON s.annee = m.annee
     JOIN vin v
       ON s.vin_id = v.id
     LEFT JOIN appellation a
       ON v.appellation_id = a.id
GROUP BY m.annee||' - '||a.libelle;
```

  1. Exécutez la requête et étudiez son plan d'exécution.
  2 Utilisez le site explain.depesz.com pour clarifier le plan d'exécution.
  3. Sans créer de nouvel index, optimisez cette requête pour diviser son temps
  d'exécution par 4 (3 optimisations possibles).

Indices :

  * une jointure avec une sous-requête peut-être déplacée,
  * joindre de façon externe n'est pas utile dans une relation 1-1,
  * les opérateurs d'index ont un type.

-----

### Machine Virtuelle

Si vous ne l'avez pas encore fait, la machine virtuelle du TP 1 doit-être
déployée.  
Se référer à ce TP pour la déployer.

Pour lancer votre machine virtuelle, lancer la commande suivante dans votre
terminal :

```bash
vboxmanage startvm TP_bdd --type headless
```

A la fin de votre TP, n'oubliez pas de l'éteindre, elle restera sinon allumée et
accessible à quiconque se connecte sur la machine que vous avez utilisé. Vous
pouvez éteindre votre machine virtuelle avec la commande :

```bash
vboxmanage controlvm TP_bdd poweroff
```

Pour se connecter à votre machine virtuelle en ssh, utilisez la commande
suivante :

```bash
ssh -p 2222 tp@127.0.0.1
```

Les mots de passes sont les mêmes que les logins (_tp_ et _login_).

```bash
sudo -iu postgres
```

-----

### Annexe : index dans Postgresql

De nombreuses fonctionnalités d'indexation sont disponibles dans PostgreSQL :

  * Index multi-colonnes
  * Index fonctionnels
  * Index partiels
  * _Covering indexes_
  * Classes d'opérateurs
  * GIN
  * GiST
  * BRIN
  * Hash

PostgreSQL fournit de nombreux types d'index, afin de répondre à des
problématiques de recherches complexes. L'index classique, créé comme ceci :

```sql
CREATE INDEX mon_index ON TABLE t1(a) ;
```

est l'index le plus fréquemment utilisé. C'est un index « BTree », c'est-à-dire
un index stocké sous forme d'arbre balancé.


Toutefois ils ne permettent de répondre qu'à des questions très simples : il
faut qu'elles ne portent que sur la colonne indexée, et uniquement sur des
opérateurs courants (égalité, comparaison). Cela couvre le gros des cas, mais
connaître les autres possibilités du moteur vous permettra d'accélérer des
requêtes plus complexes, ou d'indexer des types de données inhabituels.

#### Index Multi-Colonnes

Un index peut référencer plus d'une colonne :


  * CREATE INDEX idx ON ma_table (col1,col2,col3)
  * Index trié sur le n-uplet (col1,col2,col3)
  * Accès direct à n'importe quelle valeur de
    * (col1,col2,col3)
    * (col1,col2)
    * (col1)

### Index Fonctionnels

Il s'agit d'un index sur le résultat d'une fonction :


```sql
WHERE upper(a)='DUPOND'
```

  * l'index classique ne fonctionne pas

```sql
CREATE INDEX mon_idx ON ma_table ((UPPER(a))
```

  * La fonction doit être IMMUTABLE


#### Index partiel

  * Un index partiel n'indexe qu'une partie des données d'une table, en précisant une clause WHERE à la création de l'index :

```sql
CREATE INDEX idx_partiel ON trapsnmp (date_reception)
WHERE est_acquitte=false;
```

  * Beaucoup plus petit que l'index complet.
  * Souvent dédié à une requête précise :

```sql
SELECT * FROM trapsnmp WHERE est_acquitte=false
ORDER BY date_reception
```

  * La clause WHERE ne porte pas forcément sur la colonne indexée, c'est même souvent plus intéressant de la faire porter sur une autre colonne.

#### _Covering Indexes_

Les _Covering Indexes_ (on trouve parfois « index couvrants » dans la littérature française) :


  * Répondent à la clause WHERE
  * ET contiennent toutes les colonnes demandées par la requête
  * `SELECT col1,col2 FROM t1 WHERE col1>12`
  * `CREATE INDEX idx1 on T1 (col1,col2)`
  * Pas de visite de la table (donc peu d'accès aléatoires, l'index étant à peu près trié physiquement)


#### Classes d'opérateurs

Un index utilise des opérateurs de comparaison :

  * Il peut exister plusieurs façons de comparer deux données du même type
  * Par exemple, pour les chaînes de caractères
    * Différentes collations
    * Tri sans collation (pour LIKE)
  * CREATE INDEX idx1 ON ma_table (col_varchar varchar_pattern_ops)
  * Permet SELECT ... FROM ma_table WHERE col_varchar LIKE 'chaine%'


#### Index GIN

GIN : **G**eneralized **I**nverted i**N**dex

  * Index inversé généralisé
  * Index inversé ?
    * Index associe une valeur à la liste de ses adresses
    * Utile pour tableaux, listes…
  * Pour chaque entrée du tableau
    * Liste d'adresses (TID) où le trouver

#### Index GiST

GiST : **G**eneral**i**zed **S**earch **T**ree


  * Arbre de recherche généralisé
  * Indexation non plus des valeurs mais de la véracité de prédicats
  * Moins performants car moins sélectifs que Btree
  * Mais peuvent indexer à peu près n'importe quoi
  * Multi-colonnes dans n'importe quel ordre
  * Sur-ensemble de Btree et Rtree

#### Contraintes d'Exclusion avec les Index GiST

Contrainte d'exclusion : Une extension du concept d'unicité

  * Unicité :
`n-uplet1 = n-uplet2` interdit dans une table

  * Contrainte d'exclusion :
`n-uplet1 op n-uplet2` interdit dans une table

  * op est n'importe quel opérateur indexable par GiST

```sql
CREATE TABLE circles
    ( c circle,
      EXCLUDE USING gist (c WITH &&));
```

#### Index BRIN

BRIN : **B**lock **R**ange **IN**dex

  * Utile pour les tables très volumineuses
    * L'index produit est petit
  * Performant lorsque les valeurs sont corrélées à leur emplacement physique
  * Types qui peuvent être triés linéairement (pour obtenir min/max)-----

#### Index Hash

  * Journalisés uniquement depuis la version 10
  * Moins performants que les Btree
  * Ne gèrent que les égalités, pas < et >
  * Mais plus compacts
  * Ne pas utiliser avant la version 10

#### Utilisation d'index

Index inutilisé :


  * L'optimiseur pense qu'il n'est pas rentable
    * Il a le plus souvent raison
    * S'il se trompe : statistiques ? bug ?
  * La requête n'est pas compatible
    * Clause WHERE avec fonction ?
    * Cast ?
  * C'est souvent tout à fait normal

</div>

-----
