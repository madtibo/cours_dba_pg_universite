
<div class="notes">

### Solutions du TD 4

#### Question de cours

  * Quels sont les différents moyens disponibles pour optimiser le
    fonctionnement d'une instance ?
	* Pour optimiser le fonctionnement d'une instance, on doit rechercher le ou
      les goulots d'étranglement gênant la bonne marche du système. Cela
      nécessite une analyse de tous les composants : requêtes et schéma SQL,
      configuration physique et logicielle de l'instance, configuration du
      système de fichiers et du noyau et enfin le matériel utilisé.

  * Quelle est la méthode utilisée par PostgreSQL pour sélectionner le meilleur
    plan afin d'obtenir le résultat d'une requête ?
	* Le planificateur de PostgreSQL dispose de nombreux algorithmes pour
	calculer la bonne réponse à une requête. En se basant sur un mécanisme de
	coût, il fixe un prix à tous les plans et choisit le moins cher.

  * Comment obtenir ce meilleur plan à partir d'une requête ?
	* Le mot clé `EXPLAIN` placé avant le texte de la requête permet d'obtenir
      le plan optimum choisi par le planificateur.


#### Exercice 1

  1. Combien de noeuds sont présent dans ce plan ?

4 noeuds d'exécution sont présents :

  * Scan séquentiel sur vin,
  * Hachage du noeud précédent (pour préparer la jointure),
  * Scan séquentiel sur stock,
  * Jonction par hachage des 2 noeuds précédents sur `s.vin_id = v.id`

  2. Quel est le nombre de lignes qui seront remontés par cette requête ?

861 611 lignes seront remontées.

  3. Ce résultat est-il certain ? Pourquoi ?

L'opération effectuée est un `EXPLAIN`. Le nombre de ligne retourné est calculé
en utilisant des statistiques sur les données. Le nombre de lignes estimé est
donc approximatif. Un `EXPLAIN ANALYSE` effectue l'opération et permet d'obtenir
une certitude sur le résultat.

  4. Que remarque-t-on au niveau des jointures ?

Un jointure a été effectuée sur _appellation_. Cette table n'étant d'aucune
utilité pour remonter le bon résultat, le planificateur a supprimé cette
joinure.

  5. Combien de noeuds sont présent dans ce plan ?

Comme précédemment, 4 noeuds d'exécution sont présents :

  * Scan séquentiel sur vin,
  * Hachage du noeud précédent (pour préparer la jointure),
  * Scan séquentiel sur stock,
  * Jonction par hachage des 2 noeuds précédents sur `s.vin_id = v.id`

  6. Comment peut-on expliquer la différence dans les temps d'exécution ?

Dans le premier cas, les données ont dûes être lues par PostgreSQL et chargées
dans son cache. Ces données ont pu être lues à partir des disques ou bien depuis
le cache de l'OS.

Dans le deuxième plan d'exécution, les données étaient présentes dans le cache
de PostgreSQL. La plannification et l'exécution de la requête ont été plus
rapide.

Pour tester les temps pris par une requête, il est important de la lancer
plusieurs fois de suite. Afin de ne pas prendre en compte le temps pris pour la
lecture des données.

Si votre serveur a des latences à cause du besoin de lectures de données, la
solution le plus simple est d'ajouter de la RAM.


#### Exercice 2


  1. Quelle est l'opération choisie par l'optimiseur ?

L'optimiseur a choisi un parcours séquentiel de la table stock.

  2. Que remarque-t-on entre le plan choisi et l'opération effectuée ?

L'optimiseur avait prévu, en utilisant ses statistiques, de remonter 287 243
lignes. L'option `ANALYSE` du `EXPLAIN` remonte le nombre réel de lignes soit
34 388.

  3. Quelle est cette fois l'opération choisie par l'optimiseur ?

L'optimiseur a choisi de parcourir la table stock avec un scan séquentiel, mais
cette fois-ci, de façon parallèle avec 2 _workers_.

  4. Que remarque-t-on entre le plan choisi et l'opération effectuée ?

Le nombre de lignes estimées est comme précédement faux. Il est cependant bien
plus précis (moins d'1% d'erreur).

  5. Que s'est-il produit entre les deux exécutions des requêtes ?

Les statistiques n'étaient pas à jour lors de la première exécution de la
requête. Un `ANALYZE stock` a permis de créer ou mettre à jour les statistiques.
Dans le premier cas, les statistiques étaient vides car la table venait d'être
créée. L'optimiseur doit évidemment fournir un plan d'exécution même non
optimal. Il estime que le filtre remontera 30% des lignes.

#### Exercice 3

  1. Que s'est-il produit entre les deux exécutions des requêtes ?

L'optimiseur a choisi d'utiliser un scan d'index seul à la place d'un scan
séquentiel parallélisé.

  2. Quelle opération a pu être effectuée pour gagner un facteur 1200 sur le
  temps d'exécution de cette requête ?

Un index a été créé sur la table _stock_. Cet index comprenait en premier la
colonne _annee_. Voici une possibilité d'ordre SQL :

```sql
cave=> CREATE INDEX idx_stock_annee ON stock (annee);
```

#### Exercice 4

  1. Que s'est-il produit entre les deux exécutions des requêtes ?

Le temps d'exécution est passé de 4,5 secondes à moins de 1 seconde. Ce gain de
temps est dû au passage d'un tri sur disque (_external merge Disk_) a un tri
en mémoire (_quicksort Memory_).

Dans le premier cas, 2 273 x 8 Ko soit 18 Mo de données ont dûes être écrites
et lues sur disque.

  2. Quelle opération a été effectuée pour gagner un facteur 4 sur le temps
  d'exécution de cette requête ?

Les tris utilisent la mémoire de travail de la connection (`work_mem`) et non
la mémoire partagée de PostgreSQL (`shared_buffers`). La méthode pour augmenter
La mémoire de travail pour une requête particulière est d'exécuter la requête
suivante au préalable :

```sql
cave=> SET work_mem='40MB';
```

Ce paramètre peut être configuré au niveau global, au niveau d'une base de
données, pour un role spécifique, pour une connection.

#### Exercice 5

  1. Pourquoi le plannificateur n'a-t-il pas utilisé l'index créé ?

L'index créé est un index _btree_ sur les colonnes _nombre_ et _annee_.
L'odre des colonnes est important. Si la première colonne de l'index n'est pas
utilisé dans le filtre, il ne peut pas être utilisé.

  2. Proposez un autre index pour optimiser le première requête.

Un index sur la colonne _annee_ uniquement pourra être utilisé :

```sql
cave=> CREATE INDEX idx_stock_annee ON stock (annee);
```

  3. Pourquoi le plannificateur a-t-il utilisé l'index créé lors de
  l'exécution de la troisième requête ?

La requête possédait un prédicat sur les 2 colonnes de l'index, il pouvait donc
l'utiliser pour trouver le bon résultat.

  4. Pourquoi le plannificateur n'a-t-il pas utilisé l'index créé lors de
  l'exécution de cette requête ?

Il est plus coûteux de lire toute une table en passant par un index plutôt qu'en
effectuant un scan séquentiel. En effet le coût de lecture aléatoire est par
défaut de 4 pour un coût de lecture séquentiel à 1.

L'optimiseur a vu que la sélectivité du filtre était faible (36% des lignes
remontées). Il a donc préféré passer par un scan séquentiel.

Si vous possédez des disques en RAID 10, des disques SSD ou un SAN performant,
vous pouvez baisser le coût de la lecture aléatoire. Le plannificateur utilisera
alors plus souvent les index :

```sql
cave=> set random_page_cost=3;
SET
cave=> EXPLAIN ANALYSE SELECT nombre
  FROM stock s
   WHERE annee>1960 AND nombre >12;
                          QUERY PLAN
----------------------------------------------------------------
 Bitmap Heap Scan on stock s
      (cost=7247.47..16539.30 rows=308922 width=4)
      (actual time=30.905..82.448 rows=309201 loops=1)
   Recheck Cond: ((nombre > 12) AND (annee > 1960))
   Heap Blocks: exact=3351
   ->  Bitmap Index Scan on idx_stock_nombre_annee
            (cost=0.00..7170.23 rows=308922 width=0)
            (actual time=30.340..30.340 rows=309201 loops=1)
         Index Cond: ((nombre > 12) AND (annee > 1960))
 Planning time: 0.092 ms
 Execution time: 93.748 ms
(7 lignes)
```



#### Exercice 6

  1. Quelle a été l'impact de la création de l'index _idx_stock_annee_ ?

L'index n'a pas été utilisé.

  2. Comment pourrait-on améliorer les performances de cette requête ?

Nous pouvons passer par un index fonctionnel :

```sql
cave=> CREATE INDEX idx_stock_annee_bisextile ON stock (annee)
       WHERE (annee%4=0) AND ((annee%100<>0) OR (annee%400=0));
CREATE INDEX
cave=> EXPLAIN ANALYZE SELECT annee,nombre
  FROM stock s WHERE (annee%4=0) AND ((annee%100<>0) OR (annee%400=0));
                          QUERY PLAN
----------------------------------------------------------------
 Index Scan using idx_stock_annee_bisextile on stock s
    (cost=0.42..3327.68 rows=4287 width=8)
    (actual time=0.020..70.139 rows=219937 loops=1)
 Planning time: 0.117 ms
 Execution time: 82.230 ms
(3 lignes)
```


#### Exercice 7

Voici la requête demandée :

```sql
SELECT r.libelle, SUM(nombre)
  FROM stock s
  INNER JOIN vin v ON s.vin_id=v.id
  INNER JOIN appellation a ON v.appellation_id=a.id
  INNER JOIN region r ON a.region_id=r.id
  INNER JOIN recoltant re ON v.recoltant_id=re.id
  WHERE s.nombre<10
    AND re.adresse LIKE '%33%'
    AND a.libelle LIKE '%eau%'
  GROUP BY r.libelle ORDER BY SUM(nombre);
```

Son plan d'exécution est le suivant :

```
                               QUERY PLAN
-------------------------------------------------------------------------------
 Sort  (cost=1091.43..1091.48 rows=19 width=40)
  Sort Key: (sum(s.nombre))
  ->  HashAggregate  (cost=1090.84..1091.03 rows=19 width=40)
    Group Key: r.libelle
    ->  Nested Loop  (cost=10.43..1068.95 rows=4378 width=36)
       ->  Hash Join  (cost=10.00..132.55 rows=106 width=36)
          Hash Cond: (a.region_id = r.id)
          ->  Hash Join  (cost=8.58..129.66 rows=106 width=8)
            Hash Cond: (v.appellation_id = a.id)
            ->  Hash Join  (cost=1.10..117.88 rows=867 width=8)
               Hash Cond: (v.recoltant_id = re.id)
               ->  Seq Scan on vin v  (cost=0.00..93.71 rows=6071 width=12)
               ->  Hash  (cost=1.09..1.09 rows=1 width=4)
                  ->  Seq Scan on recoltant re  (cost=0.00..1.09 rows=1 width=4)
                    Filter: (adresse ~~ '%33%'::text)
            ->  Hash  (cost=6.99..6.99 rows=39 width=8)
               ->  Seq Scan on appellation a  (cost=0.00..6.99 rows=39 width=8)
                  Filter: (libelle ~~ '%eau%'::text)
          ->  Hash  (cost=1.19..1.19 rows=19 width=36)
            ->  Seq Scan on region r  (cost=0.00..1.19 rows=19 width=36)
       ->  Index Scan using stock_pkey on stock s
              (cost=0.42..8.42 rows=41 width=8)
          Index Cond: (vin_id = v.id)
          Filter: (nombre < 10)
(23 lignes)
```

</div>

-----

\newpage
