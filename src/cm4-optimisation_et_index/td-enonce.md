
## Travaux Dirigés 4

<div class="slide-content">

  * Optimisation et index
    * Les problèmes les plus courants
    * Comprendre EXPLAIN

</div>

-----

### Question de cours

<div class="slide-content">

  * Quels sont les différents moyens disponibles pour optimiser le
    fonctionnement d'une instance ?
  * Quelle est la méthode utilisée par PostgreSQL pour sélectionner le meilleur
    plan afin d'obtenir le résultat d'une requête ?
  * Comment obtenir ce meilleur plan à partir d'une requête ?

</div>

-----

## Problèmes les plus courants

<div class="slide-content">

  * L'optimiseur se trompe parfois
    * mauvaises statistiques
    * écriture particulière de la requête
    * problèmes connus de l'optimiseur

</div>


<div class="notes">

L'optimiseur de PostgreSQL est sans doute la partie la plus complexe de
PostgreSQL. Il se trompe rarement, mais certains facteurs peuvent entraîner des
temps d'exécution très lent voire catastrophiques de certaines requêtes.

</div>

-----

### Colonnes corrélées

<div class="slide-content">

```sql
SELECT * FROM t1 WHERE c1=1 AND c2=1
```

  * `c1=1` est vrai pour 20% des lignes
  * `c2=1` est vrai pour 10% des lignes
  * Le planificateur va penser que le résultat complet ne récupérera que 20% * 10% (soit 2%) des lignes
    * En réalité, ça peut aller de 0 à 10% des lignes
  * Problème corrigé en version 10
    * `CREATE STATISTICS` pour des statistiques multi-colonnes

</div>


<div class="notes">

PostgreSQL conserve des statistiques par colonne simple. Dans l'exemple ci-
dessus, le planificateur sait que l'estimation pour `c1=1` est de 20% et que
l'estimation pour `c2=1` est de 10%. Par contre, il n'a aucune idée de
l'estimation pour `c1=1 AND c2=1`. En réalité, l'estimation pour cette formule
va de 0 à 10% mais le planificateur doit statuer sur une seule valeur. Ce sera
le résultat de la multiplication des deux estimations, soit 2% (20% * 10%).

La version 10 de PostgreSQL corrige cela en ajoutant la possibilité d'ajouter
des statistiques sur plusieurs colonnes spécifiques. Ce n'est pas automatique,
il faut créer un objet statistique avec l'ordre `CREATE STATISTICS`.

</div>

-----

### Mauvaise écriture de prédicats



<div class="slide-content">

```sql
SELECT *
FROM commandes
WHERE extract('year' from date_commande) = 2014;
```

  * L'optimiseur n'a pas de statistiques sur le résultat de la fonction `extract`
    * il estime la sélectivité du prédicat à 0.5%.

</div>


<div class="notes">

Dans un prédicat, lorsque les valeurs des colonnes sont transformées par un
calcul ou par une fonction, l'optimiseur n'a aucun moyen pour connaître la
sélectivité d'un prédicat. Il utilise donc une estimation codée en dur dans le
code de l'optimiseur : 0,5% du nombre de lignes de la table.

Dans la requête suivante, l'optimiseur estime que la requête va ramener 834
lignes :

```sql
sql1=# EXPLAIN SELECT * FROM commandes
sql1=# WHERE extract('year' from date_commande) = 2014;
                                          QUERY PLAN
-------------------------------------------------------------------------------
 Seq Scan on commandes  (cost=0.00..7739.69 rows=834 width=80)
   Filter:
    (date_part('year'::text, (date_commande)::timestamp without time zone) =
        2014::double precision)
(2 lignes)
```

Ces 834 lignes correspondent à 0,5% de la table `commandes` :

```sql
sql1=# SELECT relname, reltuples, round(reltuples*0.005) AS estimé
  FROM pg_class
 WHERE relname = 'commandes';
  relname  | reltuples | estimé
-----------+-----------+--------
 commandes |    166725 |    834
(1 ligne)
```

</div>

-----

### Problème avec LIKE

<div class="slide-content">

```sql
SELECT * FROM t1 WHERE c2 LIKE 'x%';
```

  * PostgreSQL peut utiliser un index dans ce cas
  * Si l'encodage n'est pas C, il faut déclarer l'index avec une classe d'opérateur
    * varchar_pattern_ops, text_pattern_ops, etc
  * Attention au collationnement
  * Ne pas oublier pg_trgm et FTS

</div>


<div class="notes">

Dans le cas d'une recherche avec préfixe, PostgreSQL peut utiliser un index sur
la colonne. Il existe cependant une spécificité à PostgreSQL. Si l'encodage
est autre chose que C, il faut utiliser une classe d'opérateur lors de la
création de l'index. Cela donnera par exemple :


```sql
CREATE INDEX i1 ON t1 (c2 varchar_pattern_ops);
```

Une autre possibilité pour la non utilisation d'un index est le
collationnement. Si le collationnement de la requête diffère du collationnement
de la colonne de l'index, l'index ne pourra pas être utilisé.

</div>

-----

### DELETE lent

<div class="slide-content">

  * DELETE lent
  * Généralement un problème de clé étrangère

```sql
Delete  (actual time=111.251..111.251 rows=0 loops=1)
  ->  Hash Join  (actual time=1.094..21.402 rows=9347 loops=1)
        ->  Seq Scan on lot_a30_descr_lot
            (actual time=0.007..11.248 rows=34934 loops=1)
        ->  Hash  (actual time=0.501..0.501 rows=561 loops=1)
              ->  Bitmap Heap Scan on lot_a10_pdl
                  (actual time=0.121..0.326 rows=561 loops=1)
                    Recheck Cond: (id_fantoir_commune = 320013)
                    ->  Bitmap Index Scan on...
                        (actual time=0.101..0.101 rows=561 loops=1)
                          Index Cond: (id_fantoir_commune = 320013)
Trigger for constraint fk_lotlocal_lota30descrlot:
  time=1010.358 calls=9347
Trigger for constraint fk_nonbatia21descrsuf_lota30descrlot:
  time=2311695.025 calls=9347
Total runtime: 2312835.032 ms
```

</div>


<div class="notes">

Parfois, un `DELETE` peut prendre beaucoup de temps à s'exécuter. Cela peut
être dû à un grand nombre de lignes à supprimer. Cela peut aussi être dû
à la vérification des contraintes étrangères.

Dans l'exemple ci-dessus, le `DELETE` met 38 minutes à s'exécuter (2312835 ms),
pour ne supprimer aucune ligne. En fait, c'est la vérification de la contrainte
`fk_nonbatia21descrsuf_lota30descrlot` qui prend pratiquement tout le temps.
C'est d'ailleurs pour cette raison qu'il est recommandé de positionner des index
sur les clés étrangères car cet index permet d'accélérer la recherche liée à la
contrainte.

Attention donc aux contraintes de clés étrangères pour les instructions DML.

</div>

-----

### Dédoublonnage

<div class="slide-content">

```sql
SELECT DISTINCT t1.* FROM t1 JOIN t2 ON (t1.id=t2.t1_id);
```

  * `DISTINCT` est souvent utilisé pour dédoublonner les lignes de t1
    * mais génère un tri qui pénalise les performances
  * `GROUP BY` est plus rapide
  * Une clé primaire permet de dédoublonner efficacement des lignes
    * à utiliser avec `GROUP BY`

</div>


<div class="notes">

L'exemple ci-dessous montre une requête qui récupère les commandes qui ont des
lignes de commandes et réalise le dédoublonnage avec DISTINCT. Le plan
d'exécution montre une opération de tri qui a nécessité un fichier temporaire de
60Mo. Toutes ces opérations sont assez gourmandes, la requête répond en 5,9s
:

```sql
tpc=# EXPLAIN (ANALYZE on, COSTS off)
tpc-# SELECT DISTINCT commandes.* FROM commandes
sql1=# JOIN lignes_commandes USING (numero_commande);
                                   QUERY PLAN
-------------------------------------------------------------------------------
 Unique (actual time=5146.904..5833.600 rows=168749 loops=1)
   ->  Sort (actual time=5146.902..5307.633 rows=675543 loops=1)
         Sort Key: commandes.numero_commande, commandes.client_id,
                   commandes.etat_commande, commandes.prix_total,
                   commandes.date_commande, commandes.priorite_commande,
                   commandes.vendeur, commandes.priorite_expedition,
                   commandes.commentaire
         Sort Method: external sort  Disk: 60760kB
         ->  Merge Join (actual time=0.061..601.674 rows=675543 loops=1)
               Merge Cond: (commandes.numero_commande =
                            lignes_commandes.numero_commande)
               ->  Index Scan using orders_pkey on commandes
                        (actual time=0.026..71.544 rows=168750 loops=1)
               ->  Index Only Scan using lignes_com_pkey on lignes_commandes
                        (actual time=0.025..175.321 rows=675543 loops=1)
                     Heap Fetches: 0
 Total runtime: 5849.996 ms
```

En restreignant les colonnes récupérées à celle réellement intéressante et en
utilisant `GROUP BY` au lieu du `DISTINCT`, le temps d'exécution tombe à 4,5s :

```sql

tpc=# EXPLAIN (ANALYZE on, COSTS off)
SELECT commandes.numero_commande, commandes.etat_commande,
    commandes.prix_total, commandes.date_commande,
    commandes.priorite_commande, commandes.vendeur,
    commandes.priorite_expedition
 FROM commandes
 JOIN lignes_commandes
      USING (numero_commande)
 GROUP BY commandes.numero_commande, commandes.etat_commande,
          commandes.prix_total, commandes.date_commande,
          commandes.priorite_commande, commandes.vendeur,
          commandes.priorite_expedition;
                                        QUERY PLAN
-------------------------------------------------------------------------------
 Group (actual time=4025.069..4663.992 rows=168749 loops=1)
   ->  Sort (actual time=4025.065..4191.820 rows=675543 loops=1)
         Sort Key: commandes.numero_commande, commandes.etat_commande,
                   commandes.prix_total, commandes.date_commande,
                   commandes.priorite_commande, commandes.vendeur,
                   commandes.priorite_expedition
         Sort Method: external sort  Disk: 46232kB
         ->  Merge Join (actual time=0.062..579.852 rows=675543 loops=1)
               Merge Cond: (commandes.numero_commande =
                            lignes_commandes.numero_commande)
               ->  Index Scan using orders_pkey on commandes
                            (actual time=0.027..70.212 rows=168750 loops=1)
               ->  Index Only Scan using lignes_com_pkey on lignes_commandes
                            (actual time=0.026..170.555 rows=675543 loops=1)
                     Heap Fetches: 0
 Total runtime: 4676.829 ms
```

Mais, à partir de PostgreSQL 9.1, il est possible d'améliorer encore
les temps d'exécution de cette requête. Dans le
plan d'exécution précédent, on voit que l'opération `Sort` est très
gourmande car le tri des lignes est réalisé sur plusieurs colonnes. Or, la
table `commandes` a une clé primaire sur la colonne `numero_commande`. Cette
clé primaire permet d'assurer que toutes les lignes sont uniques dans la table
`commandes`. Si l'opération `GROUP BY` ne porte plus que la clé primaire,
PostgreSQL peut utiliser le résultat de la lecture par index sur commandes pour
faire le regroupement. Le temps d'exécution passe à environ 580ms :

```sql
tpc=# EXPLAIN (ANALYZE on, COSTS off)
SELECT commandes.numero_commande, commandes.etat_commande,
    commandes.prix_total, commandes.date_commande,
    commandes.priorite_commande, commandes.vendeur,
    commandes.priorite_expedition
  FROM commandes
  JOIN lignes_commandes
       USING (numero_commande)
 GROUP BY commandes.numero_commande;
                                           QUERY PLAN
-------------------------------------------------------------------------------
 Group (actual time=0.067..580.198 rows=168749 loops=1)
   ->  Merge Join (actual time=0.061..435.154 rows=675543 loops=1)
         Merge Cond: (commandes.numero_commande =
                      lignes_commandes.numero_commande)
         ->  Index Scan using orders_pkey on commandes
                        (actual time=0.027..49.784 rows=168750 loops=1)
         ->  Index Only Scan using lignes_commandes_pkey on lignes_commandes
                        (actual time=0.025..131.606 rows=675543 loops=1)
               Heap Fetches: 0
 Total runtime: 584.624 ms
```

Les opérations de dédoublonnages sont régulièrement utilisées pour assurer que
les lignes retournées par une requête apparaissent de manière unique. Elles
sont souvent inutiles, ou peuvent à minima être largement améliorées en
utilisant les propriétés du modèle de données (les clés primaires) et des
opérations plus adéquates (`GROUP BY clé_primaire`).  Lorsque vous rencontrez
des requêtes utilisant DISTINCT, vérifiez que le DISTINCT est vraiment
pertinent ou s'il ne peut pas être remplacé par un GROUP BY qui pourrait tirer
partie de la lecture d'un index.

Pour aller plus loin, n'hésitez pas à consulter [cet article de blog](
http://www.depesz.com/index.php/2010/04/19/getting-unique-elements/).

</div>

-----

### Index inutilisés

<div class="slide-content">

  * Trop de lignes retournées
  * Prédicat incluant une transformation :
    ```sql
    WHERE col1 + 2 > 5
    ```
  * Statistiques pas à jour ou peu précises
  * Opérateur non-supporté par l'index :
    ```sql
    WHERE col1 <> 'valeur';
    ```
  * Paramétrage de PostgreSQL : `effective_cache_size`

</div>


<div class="notes">

PostgreSQL offre de nombreuses possibilités d'indexation des données :

  * Type d'index : B-tree, GiST, GIN, SP-GiST, BRIN et hash.
  * Index multi-colonnes : `CREATE INDEX ... ON (col1, col2...);`
  * Index partiel : `CREATE INDEX ... WHERE colonne = valeur`
  * Index fonctionnel : `CREATE INDEX ... ON (fonction(colonne))`
  * Extension offrant des fonctionnalités supplémentaires : pg_trgm

Malgré toutes ces possibilités, une question revient souvent lorsqu'un index
vient d'être ajouté : pourquoi cet index n'est pas utilisé ?

L'optimiseur de PostgreSQL est très avancé et il y a peu de cas où il est mis
en défaut. Malgré cela, certains index ne sont pas utilisés comme on le
souhaiterait. Il peut y avoir plusieurs raisons à cela.


**Problèmes de statistiques**

Le cas le plus fréquent concerne les statistiques qui ne sont pas à jour. Cela
arrive souvent après le chargement massif d'une table ou une mise à jour massive
sans avoir fait une nouvelle collecte des statistiques à l'issue de ces
changements.

On utilisera l'ordre `ANALYZE table` pour déclencher explicitement la collecte
des statistiques après un tel traitement. En effet, bien qu'autovacuum soit
présent, il peut se passer un certain temps entre le moment où le traitement
est fait et le moment où autovacuum déclenche une collecte de statistiques. Ou
autovacuum peut ne simplement pas se déclencher car le traitement complet est
imbriqué dans une seule transaction.

Un traitement batch devra comporter des ordres `ANALYZE` juste après les ordres
SQL qui modifient fortement les données :

```sql
COPY table_travail FROM '/tmp/fichier.csv';
ANALYZE table_travail;
SELECT ... FROM table_travail;
```

Un autre problème qui peut se poser avec les statistiques concerne les tables
de très forte volumétrie. Dans certain cas, l'échantillon de données ramené par
`ANALYZE` n'est pas assez précis pour donner à l'optimiseur de PostgreSQL une
vision suffisamment précise des données. Il choisira alors de mauvais plans
d'exécution.

Il est possible d'augmenter la précision de l'échantillon de données ramené
à l'aide de l'ordre :

```sql
ALTER TABLE ... ALTER COLUMN ... SET STATISTICS ...;
```

**Problèmes de prédicats**

Dans d'autres cas, les prédicats d'une requête ne permettent pas à l'optimiseur
de choisir un index pour répondre à une requête. C'est le cas lorsque le
prédicat inclut une transformation de la valeur d'une colonne.

L'exemple suivant est assez naïf mais démontre bien le problème :

```sql
SELECT * FROM table WHERE col1 + 10 = 10;
```

Avec une telle construction, l'optimiseur ne saura pas tirer partie d'un
quelconque index, à moins d'avoir créé un index fonctionnel sur `col1 + 10`,
mais cet index est largement contre-productif par rapport à une réécriture de
la requête.

Ce genre de problème se rencontre plus souvent sur des prédicats sur des dates
:

```sql
SELECT * FROM table WHERE date_trunc('month', date_debut) = 12
```

ou encore plus fréquemment rencontré :

```sql
SELECT * FROM table WHERE extract('year' from date_debut) = 2013
```

**Opérateurs non-supportés**

Les index B-tree supportent la plupart des opérateurs généraux sur les
variables scalaires ((entiers, chaînes, dates, mais pas types composés comme
géométries, hstore...)), mais pas la différence (`<>` ou `!=`). Par nature, il
n'est pas possible d'utiliser un index pour déterminer _toutes les valeurs sauf
une_. Mais ce type de construction est parfois utilisé pour exclure les valeurs
les plus fréquentes d'une colonne. Dans ce cas, il est possible d'utiliser un
index partiel, qui en plus sera très petit car il n'indexera qu'une faible
quantité de données par rapport à la totalité de la table :

```sql
CREATE TABLE test (id serial PRIMARY KEY, v integer);
INSERT INTO test (v) SELECT 0 FROM generate_series(1, 10000);
INSERT INTO test (v) SELECT 1;
ANALYZE test;
CREATE INDEX idx_test_v ON test(v);
EXPLAIN SELECT * FROM test WHERE v <> 0;
                      QUERY PLAN
------------------------------------------------------
 Seq Scan on test  (cost=0.00..170.03 rows=1 width=8)
   Filter: (v <> 0)

DROP INDEX idx_test_v;
```

La création d'un index partiel permet d'en tirer partie :

```sql
CREATE INDEX idx_test_v_partiel ON test (v) WHERE v<>0;
CREATE INDEX
Temps : 67,014 ms
postgres=# EXPLAIN SELECT * FROM test WHERE v <> 0;
                                  QUERY PLAN
-------------------------------------------------------------------------------
 Index Scan using idx_test_v_partiel on test  (cost=0.00..8.27 rows=1 width=8)

```

**Paramétrage de PostgreSQL**

Plusieurs paramètres de PostgreSQL influencent le choix ou non d'un index :

  * `random_page_cost` : indique à PostgreSQL la vitesse d'un accès aléatoire
par rapport à un accès séquentiel (`seq_page_cost`).
  * `effective_cache_size` : indique à PostgreSQL une estimation de la taille du
cache disque du système.

Le paramètre `random_page_cost` a une grande influence sur l'utilisation des
index en général. Il indique à PostgreSQL le coût d'un accès disque aléatoire.
Il est à comparer au paramètre `seq_page_cost` qui indique à PostgreSQL le coût
d'un accès disque séquentiel. Ces coûts d'accès sont purement arbitraires et
n'ont aucune réalité physique. Dans sa configuration par défaut, PostgreSQL
estime qu'un accès aléatoire est 4 fois plus coûteux qu'un accès séquentiel. Les
accès aux index étant par nature aléatoires alors que les parcours de table
étant par nature séquentiels, modifier ce paramètre revient à favoriser l'un par
rapport à l'autre. Cette valeur est bonne dans la plupart des cas. Mais si le
serveur de bases de données dispose d'un système disque rapide, c'est-à-dire une
bonne carte RAID et plusieurs disques SAS rapides en RAID10, ou du SSD, il est
possible de baisser ce paramètre à 3 voir 2.

Enfin, le paramètre `effective_cache_size` indique à PostgreSQL une estimation
de la taille du cache disque du système. Une bonne pratique est de positionner
ce paramètre à 2/3 de la quantité totale de RAM du serveur. Sur un système
Linux, il est possible de donner une estimation plus précise en s'appuyant sur
la valeur de colonne `cached` de la commande `free`. Mais si le cache n'est que
peu utilisé, la valeur trouvée peut être trop basse pour pleinement favoriser
l'utilisation des index.

Pour aller plus loin, n'hésitez pas à consulter [cet article de blog](
http://www.depesz.com/index.php/2010/09/09/why-is-my-index-not-being-used/)

</div>

-----

### Écriture du SQL

<div class="slide-content">

  * `NOT IN` avec une sous-requête
    * à remplacer par `NOT EXISTS`
  * Utilisation systématique de `UNION ALL` au lieu de `UNION`
    * `UNION` entraîne un tri systématique
  * Sous-requête dans le `SELECT`
    * utiliser `LATERAL`

</div>

<div class="notes">

La façon dont une requête SQL est écrite peut aussi avoir un effet négatif
sur les performances. Il n'est pas possible d'écrire tous les cas possibles,
mais certaines formes d'écritures reviennent souvent.

La clause `NOT IN` n'est pas performance lorsqu'elle est utilisée avec une
sous-requête. L'optimiseur ne parvient pas à exécuter ce type de requête
efficacement.

```sql
SELECT *
  FROM commandes
 WHERE numero_commande NOT IN (SELECT numero_commande
                               FROM lignes_commandes);
```

Il est nécessaire de la réécrire avec la clause `NOT EXISTS`,
par exemple :

```sql
SELECT *
  FROM commandes
 WHERE NOT EXISTS (SELECT 1
                   FROM lignes_commandes l
                   WHERE l.numero_commande = commandes.numero_commande);
```

</div>

-----

\newpage

### Enoncé


<div class="notes">

### Comprendre EXPLAIN

**Schéma de la base cave**

![Schéma de la base cave](medias/common-schema_cave.png)

#### Exercice 1

```sql
cave=> EXPLAIN SELECT s.nombre
  FROM stock s
  JOIN vin v ON s.vin_id = v.id
  LEFT JOIN appellation a ON v.appellation_id = a.id;
                        QUERY PLAN                              
----------------------------------------------------------------------
 Hash Join  (cost=169.60..25290.86 rows=861611 width=4)
   Hash Cond: (s.vin_id = v.id)
   ->  Seq Scan on stock s  (cost=0.00..13274.11 rows=861611 width=8)
   ->  Hash  (cost=93.71..93.71 rows=6071 width=8)
         ->  Seq Scan on vin v  (cost=0.00..93.71 rows=6071 width=8)
(5 lignes)
```

  1. Combien de noeuds sont présent dans ce plan ?
  2. Quel est le nombre de lignes qui seront remontés par cette requête ?
  3. Ce résultat est-il certain ? Pourquoi ?
  4. Que remarque-t-on au niveau des jointures ?

```sql
cave=> EXPLAIN (ANALYSE,BUFFERS) SELECT nombre
  FROM stock s
  JOIN vin v ON s.vin_id = v.id
  LEFT JOIN appellation a ON v.appellation_id = a.id;
                              QUERY PLAN
------------------------------------------------------------------------------
 Hash Join  (cost=169.60..25290.86 rows=861611 width=4) 
            (actual time=7.378..461.477 rows=861611 loops=1)
   Hash Cond: (s.vin_id = v.id)
   Buffers: shared hit=3 read=4691
   ->  Seq Scan on stock s  (cost=0.00..13274.11 rows=861611 width=8) 
                            (actual time=0.060..134.329 rows=861611 loops=1)
         Buffers: shared read=4658
   ->  Hash  (cost=93.71..93.71 rows=6071 width=8) 
             (actual time=7.168..7.168 rows=6071 loops=1)
         Buckets: 8192  Batches: 1  Memory Usage: 302kB
         Buffers: shared read=33
         ->  Seq Scan on vin v  (cost=0.00..93.71 rows=6071 width=8) 
                                (actual time=0.047..3.505 rows=6071 loops=1)
               Buffers: shared read=33
 Planning time: 1.146 ms
 Execution time: 511.313 ms
(12 lignes)

cave=> EXPLAIN (ANALYSE,BUFFERS) SELECT nombre
  FROM stock s
  JOIN vin v ON s.vin_id = v.id
  LEFT JOIN appellation a ON v.appellation_id = a.id;
                              QUERY PLAN
------------------------------------------------------------------------------
 Hash Join  (cost=169.60..25290.86 rows=861611 width=4) 
            (actual time=5.291..370.185 rows=861611 loops=1)
   Hash Cond: (s.vin_id = v.id)
   Buffers: shared hit=4691
   ->  Seq Scan on stock s  (cost=0.00..13274.11 rows=861611 width=8) 
                            (actual time=0.022..93.276 rows=861611 loops=1)
         Buffers: shared hit=4658
   ->  Hash  (cost=93.71..93.71 rows=6071 width=8) 
             (actual time=5.228..5.228 rows=6071 loops=1)
         Buckets: 8192  Batches: 1  Memory Usage: 302kB
         Buffers: shared hit=33
         ->  Seq Scan on vin v  (cost=0.00..93.71 rows=6071 width=8) 
                                (actual time=0.021..2.392 rows=6071 loops=1)
               Buffers: shared hit=33
 Planning time: 0.303 ms
 Execution time: 414.245 ms
(12 lignes)
```

  5. Combien de noeuds sont présent dans ces plans ?
  6. Comment peut-on expliquer la différence dans les temps d'exécution ?

-----

#### Exercice 2

```sql
cave=> EXPLAIN ANALYSE SELECT annee FROM stock s WHERE annee<1952;
                              QUERY PLAN
-------------------------------------------------------------------------
 Seq Scan on stock s  (cost=0.00..15429.62 rows=287243 width=4)
                      (actual time=0.146..208.687 rows=34388 loops=1)
   Filter: (annee < 1952)
   Rows Removed by Filter: 827223
 Planning time: 0.295 ms
 Execution time: 212.568 ms
(5 lignes)
```

  1. Quelle est l'opération choisie par l'optimiseur ?
  2. Que remarque-t-on entre le plan choisi et l'opération effectuée ?

```sql
cave=> EXPLAIN ANALYSE SELECT annee FROM stock s WHERE annee<1952;
                              QUERY PLAN
-------------------------------------------------------------------------
 Gather  (cost=1000.00..13606.36 rows=34608 width=4)
         (actual time=0.326..77.223 rows=34388 loops=1)
   Workers Planned: 2
   Workers Launched: 2
   ->  Parallel Seq Scan on stock s
            (cost=0.00..9145.56 rows=14420 width=4)
            (actual time=0.091..59.782 rows=11463 loops=3)
         Filter: (annee < 1952)
         Rows Removed by Filter: 275741
 Planning time: 0.152 ms
 Execution time: 82.376 ms
(8 lignes)
```

  3. Quelle est cette fois l'opération choisie par l'optimiseur ?
  4. Que remarque-t-on entre le plan choisi et l'opération effectuée ?
  5. Que s'est-il produit entre les deux exécutions des requêtes ?

-----

#### Exercice 3

```sql
cave=> EXPLAIN ANALYSE SELECT annee
  FROM stock s
   WHERE annee<1950;
                              QUERY PLAN
-------------------------------------------------------------------------
 Gather  (cost=1000.00..10145.66 rows=1 width=4) 
         (actual time=89.613..89.613 rows=0 loops=1)
   Workers Planned: 2
   Workers Launched: 2
   ->  Parallel Seq Scan on stock s  
          (cost=0.00..9145.56 rows=1 width=4) 
          (actual time=85.824..85.824 rows=0 loops=3)
         Filter: (annee < 1950)
         Rows Removed by Filter: 287204
 Planning time: 0.155 ms
 Execution time: 92.065 ms
(8 lignes)

cave=> (...)

cave=> EXPLAIN ANALYSE SELECT annee
  FROM stock s
   WHERE annee<1950;
                              QUERY PLAN
-------------------------------------------------------------------------
 Index Only Scan using idx_stock_annee on stock s  
      (cost=0.42..5.52 rows=1 width=4)
      (actual time=0.024..0.024 rows=0 loops=1)
   Index Cond: (annee < 1950)
   Heap Fetches: 0
 Planning time: 0.195 ms
 Execution time: 0.071 ms
(5 lignes)
```

  1. Que s'est-il produit entre les deux exécutions des requêtes ?
  2. Quelle opération a pu être effectuée pour gagner un facteur 1200 sur le
  temps d'exécution de cette requête ?

-----

#### Exercice 4

```sql
cave=> EXPLAIN (ANALYZE,BUFFERS) SELECT * FROM stock
  WHERE annee>1980 ORDER BY (annee,nombre);
                              QUERY PLAN
-----------------------------------------------------------------------------
 Sort  (cost=56382.10..57217.79 rows=334276 width=48) 
       (actual time=2832.406..2967.067 rows=338057 loops=1)
   Sort Key: (ROW(annee, nombre))
   Sort Method: external merge  Disk: 18176kB
   Buffers: shared hit=4658, temp read=2273 written=2273
   ->  Seq Scan on stock  (cost=0.00..15428.14 rows=334276 width=48) 
                          (actual time=116.801..248.923 rows=338057 loops=1)
         Filter: (annee > 1980)
         Rows Removed by Filter: 523554
         Buffers: shared hit=4658
 Planning time: 0.220 ms
 Execution time: 3003.787 ms
(10 lignes)

cave=> (...)

cave=> EXPLAIN (ANALYZE,BUFFERS) SELECT * FROM stock
  WHERE annee>1980 ORDER BY (annee,nombre);
                              QUERY PLAN
-----------------------------------------------------------------------------
 Sort  (cost=46099.10..46934.79 rows=334276 width=48)
       (actual time=984.267..1022.301 rows=338057 loops=1)
   Sort Key: (ROW(annee, nombre))
   Sort Method: quicksort  Memory: 38227kB
   Buffers: shared hit=4658
   ->  Seq Scan on stock  (cost=0.00..15428.14 rows=334276 width=48) 
                          (actual time=112.079..248.697 rows=338057 loops=1)
         Filter: (annee > 1980)
         Rows Removed by Filter: 523554
         Buffers: shared hit=4658
 Planning time: 0.200 ms
 Execution time: 1058.208 ms
(10 lignes)
```

  1. Que s'est-il produit entre les deux exécutions des requêtes ?
  2. Quelle opération a été effectuée pour gagner un facteur 4 sur le temps
  d'exécution de cette requête ?

-----

#### Exercice 5

```sql
cave=> EXPLAIN ANALYSE SELECT nombre
  FROM stock s
   WHERE annee>1990;
                              QUERY PLAN
-------------------------------------------------------------------------
 Seq Scan on stock s  (cost=0.00..15428.14 rows=167095 width=4)
                      (actual time=117.058..160.155 rows=169437 loops=1)
   Filter: (annee > 1990)
   Rows Removed by Filter: 692174
 Planning time: 0.137 ms
 Execution time: 167.216 ms
(5 lignes)

cave=> CREATE INDEX idx_stock_nombre_annee ON stock (nombre,annee) ;
CREATE INDEX
cave=> EXPLAIN ANALYSE SELECT nombre
  FROM stock s
   WHERE annee>1990;
                              QUERY PLAN
-------------------------------------------------------------------------
 Seq Scan on stock s  (cost=0.00..15428.14 rows=167095 width=4) 
                      (actual time=145.586..192.560 rows=169437 loops=1)
   Filter: (annee > 1990)
   Rows Removed by Filter: 692174
 Planning time: 0.395 ms
 Execution time: 200.287 ms
(5 lignes)
```

  1. Pourquoi le plannificateur n'a-t-il pas utilisé l'index créé lors de
  l'exécution de la seconde requête ?
  2. Proposez un autre index pour optimiser la première requête.


```sql
cave=> EXPLAIN ANALYSE SELECT nombre
  FROM stock s
   WHERE annee>1990 AND nombre >20;
                              QUERY PLAN
-------------------------------------------------------------------------
 Index Only Scan using idx_stock_nombre_annee on stock s
      (cost=0.42..8.44 rows=1 width=4)
      (actual time=0.031..0.031 rows=0 loops=1)
   Index Cond: ((nombre > 20) AND (annee > 1990))
   Heap Fetches: 0
 Planning time: 0.294 ms
 Execution time: 0.084 ms
(5 lignes)
```

  3. Pourquoi le plannificateur a-t-il utilisé l'index créé lors de
  l'exécution de cette requête ?
  
```sql
cave=> EXPLAIN ANALYSE SELECT nombre
  FROM stock s
   WHERE annee>1960 AND nombre>12;
                              QUERY PLAN
-------------------------------------------------------------------------
 Seq Scan on stock s  (cost=0.00..17582.17 rows=308922 width=4)
                      (actual time=40.806..190.476 rows=309201 loops=1)
   Filter: ((annee > 1960) AND (nombre > 12))
   Rows Removed by Filter: 552410
 Planning time: 0.198 ms
 Execution time: 202.977 ms
(5 lignes)
```

  4. Pourquoi le plannificateur n'a-t-il pas utilisé l'index créé lors de
  l'exécution de cette requête ?
 
-----

#### Exercice 6

```sql
cave=> CREATE INDEX idx_stock_annee ON stock (annee);
CREATE INDEX
cave=> EXPLAIN ANALYZE SELECT annee,nombre
  FROM stock s WHERE (annee%4=0) AND ((annee%100<>0) OR (annee%400=0));
                              QUERY PLAN
--------------------------------------------------------------------------------
 Gather  (cost=1000.00..15061.81 rows=4287 width=8)
         (actual time=10.898..158.395 rows=219937 loops=1)
   Workers Planned: 2
   Workers Launched: 2
   ->  Parallel Seq Scan on stock_bis s
          (cost=0.00..13633.11 rows=1786 width=8)
          (actual time=6.077..96.088 rows=73312 loops=3)
         Filter: (((annee%4) = 0) AND (((annee%100) <> 0) OR ((annee%400) = 0)))
         Rows Removed by Filter: 213891
 Planning time: 0.166 ms
 Execution time: 172.671 ms
(8 lignes)
```

  1. Quelle a été l'impact de la création de l'index _idx_stock_annee_ ?
  2. Comment pourrait-on améliorer les performances de cette requête ?


-----

#### Exercice 7

Écrire la requête permettant de récupérer la somme du nombre de bouteilles en
stock pour chaque region dont le stock pour chaque vin est inférieur à 10,
l'adresse du récoltant devant contenir un `33` et le libellé de l'appellation
la chaine de caractère `eau`. Le résultat devra être ordonné par le somme du
nombre de bouteille.

</div>

--------
