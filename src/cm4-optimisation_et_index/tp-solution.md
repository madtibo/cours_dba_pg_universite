
<div class="notes">

### Solutions du TP 4

#### Utilisation de EXPLAIN et indexation

**Indexation simple**

  1. Écrire la requête permettant de sélectionner le nombre de bouteilles en
  stock de l'année 2012. Quel est son plan d'exécution ?

```sql
cave=> EXPLAIN ANALYZE SELECT count(*) FROM stock WHERE annee=2012;
                                QUERY PLAN
------------------------------------------------------------------------
 Finalize Aggregate  (cost=10144.90..10144.91 rows=1 width=8)
                     (actual time=62.750..66.031 rows=1 loops=1)
   ->  Gather  (cost=10144.69..10144.90 rows=2 width=8)
               (actual time=62.586..66.005 rows=3 loops=1)
         Workers Planned: 2
         Workers Launched: 2
         ->  Partial Aggregate
                 (cost=9144.69..9144.70 rows=1 width=8)
                 (actual time=58.837..58.845 rows=1 loops=3)
               ->  Parallel Seq Scan on stock
                        (cost=0.00..9140.20 rows=1794 width=0)
                        (actual time=8.417..47.055 rows=5621 loops=3)
                     Filter: (annee = 2012)
                     Rows Removed by Filter: 281432
 Planning time: 0.126 ms
 Execution time: 69.812 ms
(10 lignes)
```


  2. Rajoutez un index pour que la requête soit plus rapide.

```sql
cave=> CREATE INDEX idx_stock_annee ON stock(annee);
CREATE INDEX
cave=> EXPLAIN ANALYZE SELECT count(*) FROM stock WHERE annee=2012;
                                QUERY PLAN
------------------------------------------------------------------------
 Aggregate  (cost=10144.90..10144.91 rows=1 width=8)
            (actual time=67.279..70.598 rows=1 loops=1)
   ->  Bitmap Heap Scan on stock
            (cost=10144.69..10144.90 rows=2 width=8)
            (actual time=67.124..70.574 rows=3 loops=1)
         Recheck Cond: (annee = 2012)
         Heap Blocks: exact=93
         ->  Bitmap Index Scan on idx_stock_annee
                  (cost=0.00..289.17 rows=15566 width=0)
                  (actual time=3.265..3.265 rows=16894 loops=1)
               Index Cond: (annee = 2012)
 Planning time: 0.469 ms
 Execution time: 10.538 ms
(8 lignes)
```

**Index partiels**

  1. Créer un index se limitant aux vins avec moins de 10 bouteilles en stock.

```sql
cave=> CREATE INDEX idx_stock_nombre_moins_10 ON stock(nombre) WHERE nombre<10;
```

  2. Exécuter une requête pour rechercher les vins possédant un stock inférieur
  à 7 bouteilles. Que remarquez-vous ?

```sql
cave=> EXPLAIN SELECT * FROM stock WHERE nombre < 7;
                             QUERY PLAN                                          
-------------------------------------------------------------------------
 Bitmap Heap Scan on stock  (cost=660.20..5756.55 rows=35068 width=16)
   Recheck Cond: (nombre < 7)
   ->  Bitmap Index Scan on idx_stock_nombre_moins_10
            (cost=0.00..651.43 rows=35068 width=0)
         Index Cond: (nombre < 7)
(4 lignes)
```

L'index que nous venons de créer est utilisé, même si la condition n'est pas la
même. L'optimiseur sait que tous les résultats sont accessibles via cet index.

  2. Exécuter une requête pour rechercher les vins possédant un stock inférieur
  à 11 bouteilles. Que remarquez-vous ?

```sql
cave=> EXPLAIN SELECT * FROM stock WHERE nombre < 11;
                                   QUERY PLAN
-------------------------------------------------------------------------------
 Bitmap Heap Scan on stock  (cost=6056.93..14754.46 rows=323162 width=16)
   Recheck Cond: (nombre < 11)
   ->  Bitmap Index Scan on idx_stock_nombre_annee
            (cost=0.00..5976.14 rows=323162 width=0)
         Index Cond: (nombre < 11)
(4 lignes)
```

L'index que nous venons de créer n'est pas utilisé. En effet, l'optimiseur sait
que tous les résultats ne pourront pas être trouvés via notre index partiel.

**Indexation multi-colonnes**

  1. Créez l'index optimum pour cette requête :

```sql
SELECT * FROM stock
WHERE vin_id IN (SELECT vin_id FROM stock TABLESAMPLE BERNOULLI (0.01))
  AND annee BETWEEN 2004 AND 2007;
```

```sql
cave=> CREATE INDEX idx_stock_vin_id_entre_2004_2007 ON stock(vin_id)
  WHERE annee>= 2004 AND annee <= 2007;
```

  2. Quel est la taille sur disque de la table _stock_ ainsi que pour les index
  sur cette table ?

```sql
cave=> SELECT
  pg_size_pretty(pg_total_relation_size('stock')) total_size
  ,pg_size_pretty(pg_relation_size('stock')) table_stock
 ,pg_size_pretty(pg_relation_size('idx_stock_annee')) idx_annee
 ,pg_size_pretty(pg_relation_size('idx_stock_nombre_moins_10')) idx_annee_nb
 ,pg_size_pretty(pg_relation_size('idx_stock_vin_id_entre_2004_2007)) idx_vin_id;
 total_size | table_stock | idx_annee | idx_annee_nb | idx_vin_id 
------------+-------------+-----------+--------------+------------
 106 MB     | 36 MB       | 18 MB     | 5528 kB      | 1496 kB
(1 ligne)
```

**Recherche de motif texte**

  1. Affichez le plan de cette requête (sur la base `cave`) :

```sql
SELECT * FROM appellation WHERE libelle LIKE 'Brouilly%';
```

```sql
cave=> EXPLAIN SELECT * FROM appellation WHERE libelle LIKE 'Brouilly%';
                         QUERY PLAN
------------------------------------------------------------
 Seq Scan on appellation  (cost=0.00..6.99 rows=1 width=24)
   Filter: (libelle ~~ 'Brouilly%'::text)
(2 lignes)
```

  2. Que constatez-vous ?

La requête ne passe pas par un index.

  3. Affichez maintenant le nombre de blocs accédés par cette requête.

```sql
cave=> EXPLAIN (ANALYZE,BUFFERS) SELECT * FROM appellation
 WHERE libelle LIKE 'Brouilly%';
                                      QUERY PLAN
--------------------------------------------------------------------------------
 Seq Scan on appellation  (cost=0.00..6.99 rows=1 width=24)
                          (actual time=0.066..0.169 rows=1 loops=1)
   Filter: (libelle ~~ 'Brouilly%'::text)
   Rows Removed by Filter: 318
   Buffers: shared hit=3
 Total runtime: 0.202 ms
(5 lignes)
```


  4. Cette requête ne passe pas par un index. Essayez de lui forcer la main.

```sql
cave=> SET enable_seqscan TO off;
SET
cave=> EXPLAIN (ANALYZE,BUFFERS) SELECT * FROM appellation
  WHERE libelle LIKE 'Brouilly%';
                                      QUERY PLAN
--------------------------------------------------------------------------------
 Seq Scan on appellation  (cost=10000000000.00..10000000006.99 rows=1 width=24)
                          (actual time=0.073..0.197 rows=1 loops=1)
   Filter: (libelle ~~ 'Brouilly%'::text)
   Rows Removed by Filter: 318
   Buffers: shared hit=3
 Total runtime: 0.238 ms
(5 lignes)
```

Passer `enable_seqscan` à « off » n'interdit pas l'utilisation des scans
séquentiels. Il ne fait que les défavoriser fortement : regardez le coût estimé
du scan séquentiel.

  5. L'index n'est toujours pas utilisé. L'index « par défaut » n'est pas
  capable de répondre à des questions sur motif.

En effet, l'index par défaut trie les données par la collation de la colonne de
la table. Il lui est impossible de savoir que `libelle LIKE 'Brouilly%'` est
équivalent à `libelle >= 'Brouilly' AND libelle < 'Brouillz' `. Ce genre de
transformation n'est d'ailleurs pas forcément trivial, ni même possible. Il
existe dans certaines langues des équivalences (_ß_ et _ss_ en allemand par
exemple) qui rendent ce genre de transformation au mieux hasardeuse.

  6. Créez un index capable de réaliser ces opérations (indice : _Index Operator
  Classes_). Testez à nouveau le plan.

Pour pouvoir répondre à cette question, on doit donc avoir un index spécialisé,
qui compare les chaînes non plus par rapport à leur collation, mais à leur
valeur binaire (octale en fait).

```sql
cave=> CREATE INDEX appellation_libelle_key_search
    ON appellation (libelle text_pattern_ops);
```

On indique par cette commande à PostgreSQL de ne plus utiliser la classe
d'opérateurs habituelle de comparaison de texte, mais la classe
`text_pattern_ops`, qui est spécialement faite pour les recherches
`LIKE 'xxxx%'` : cette classe ne trie plus les chaînes par leur ordre
alphabétique, mais par leur valeur octale.

Si on redemande le plan :

```sql
cave=> EXPLAIN (ANALYZE,BUFFERS) SELECT * FROM appellation
  WHERE libelle LIKE 'Brouilly%';
                                     QUERY PLAN
-------------------------------------------------------------------------------
 Index Scan using appellation_libelle_key_search on appellation
                              (cost=0.27..8.29 rows=1 width=24)
                              (actual time=0.057..0.059 rows=1 loops=1)
   Index Cond: ((libelle ~>=~ 'Brouilly'::text)
                AND (libelle ~<~ 'Brouillz'::text))
   Filter: (libelle ~~ 'Brouilly%'::text)
   Buffers: shared hit=1 read=2
 Total runtime: 0.108 ms
(5 lignes)
```

On utilise enfin un index.

  7. Réactivez `enable_seqscan`. Testez à nouveau le plan.


```sql
cave=> RESET enable_seqscan;
RESET
cave=> EXPLAIN (ANALYZE,BUFFERS) SELECT * FROM appellation
  WHERE libelle LIKE 'Brouilly%';
                                    QUERY PLAN
--------------------------------------------------------------------------------
 Seq Scan on appellation  (cost=0.00..6.99 rows=1 width=24)
                          (actual time=0.063..0.172 rows=1 loops=1)
   Filter: (libelle ~~ 'Brouilly%'::text)
   Rows Removed by Filter: 318
   Buffers: shared hit=3
 Total runtime: 0.211 ms
(5 lignes)
```

  8. Quelle est la conclusion ?

PostgreSQL choisit de ne pas utiliser cet index. Le temps d'exécution est
pourtant un peu meilleur avec l'index (60 microsecondes contre 172
microsecondes). Néanmoins, cela n'est vrai que parce que les données sont en
cache. En cas de données hors du cache, le plan par parcours séquentiel
(*seq scan*) est probablement meilleur. Certes il prend plus de temps CPU
puisqu'il doit consulter 318 enregistrements inutiles. Par contre, il ne fait
qu'un accès à 3 blocs séquentiels (les 3 blocs de la table), ce qui est le plus
sûr.

La table est trop petite pour que PostgreSQL considère l'utilisation d'un index.

-----


**Recherche de motif texte avancé**

  1. Effectuer une recherche textuelle dans les commentaires de la table
  _magasin.commandes_ en sélectionnant les lignes commençant par `qui`.

```sql
magasin=> EXPLAIN (ANALYZE,BUFFERS) SELECT * FROM magasin.commandes
  WHERE commentaire LIKE 'qui%';
                                     QUERY PLAN
-------------------------------------------------------------------------------
 Gather  (cost=1000.00..21251.06 rows=60598 width=51)
         (actual time=0.481..81.811 rows=60311 loops=1)
   Workers Planned: 3
   Workers Launched: 3
   Buffers: shared hit=10414
   ->  Parallel Seq Scan on commandes
            (cost=0.00..14191.26 rows=19548 width=51)
            (actual time=0.118..67.941 rows=15078 loops=4)
         Filter: (commentaire ~~ 'qui%'::text)
         Rows Removed by Filter: 234922
         Buffers: shared hit=10195
 Planning time: 0.318 ms
 Execution time: 91.008 ms
(10 lignes)
```

  2. Créer un index btree capable d'aider à exécuter la requête.

```sql
magasin=> CREATE INDEX idx_commandes_commentaire_btree
    ON magasin.commandes (commentaire text_pattern_ops);
CREATE INDEX
magasin=> EXPLAIN (ANALYZE,BUFFERS) SELECT * FROM magasin.commandes
  WHERE commentaire LIKE 'qui%';
                                     QUERY PLAN
-------------------------------------------------------------------------------
 Bitmap Heap Scan on commandes  (cost=1730.50..12528.17 rows=60598 width=51)
                                (actual time=17.027..45.687 rows=60311 loops=1)
   Filter: (commentaire ~~ 'qui%'::text)
   Heap Blocks: exact=10135
   Buffers: shared hit=10478
   ->  Bitmap Index Scan on idx_commandes_commentaire_btree
            (cost=0.00..1715.36 rows=51093 width=0)
            (actual time=15.130..15.130 rows=60311 loops=1)
         Index Cond: ((commentaire ~>=~ 'qui'::text)
                      AND (commentaire ~<~ 'quj'::text))
         Buffers: shared hit=343
 Planning time: 0.232 ms
 Execution time: 48.689 ms
(9 lignes)
```


  3. Quels sont les gains de performances ?

Le temps d'exécution est divisé par 2.

  4. Tenter de chercher les lignes se terminant par `qui`, puis les lignes
  contenant `qui`. Que remarquez vous ?

```sql
magasin=> EXPLAIN (ANALYZE,BUFFERS) SELECT * FROM magasin.commandes
  WHERE commentaire LIKE '%qui';
                                     QUERY PLAN
-------------------------------------------------------------------------------
 Gather  (cost=1000.00..20241.06 rows=50498 width=51)
         (actual time=0.534..103.126 rows=27866 loops=1)
   Workers Planned: 3
   Workers Launched: 3
   Buffers: shared hit=10414
   ->  Parallel Seq Scan on commandes
            (cost=0.00..14191.26 rows=16290 width=51)
            (actual time=0.113..91.170 rows=6966 loops=4)
         Filter: (commentaire ~~ '%qui'::text)
         Rows Removed by Filter: 243034
         Buffers: shared hit=10195
 Planning time: 0.272 ms
 Execution time: 107.653 ms
(10 lignes)

magasin=> EXPLAIN (ANALYZE,BUFFERS) SELECT * FROM magasin.commandes
  WHERE commentaire LIKE '%qui%';
                                     QUERY PLAN
-------------------------------------------------------------------------------
 Seq Scan on commandes  (cost=0.00..22659.00 rows=323189 width=51)
                        (actual time=0.028..308.985 rows=245799 loops=1)
   Filter: (commentaire ~~ '%qui%'::text)
   Rows Removed by Filter: 754201
   Buffers: shared hit=10159
 Planning time: 0.198 ms
 Execution time: 324.977 ms
(6 lignes)
```

L'index construit précédemment n'est pas utilisé.

  5. Tenter de chercher les lignes commençant par `qui` mais sans tenir compte
  de la casse ? Que remarquez vous ?

```sql
magasin=> EXPLAIN (ANALYZE,BUFFERS) SELECT * FROM magasin.commandes
  WHERE commentaire ILIKE 'qui%';
                                     QUERY PLAN
-------------------------------------------------------------------------------
 Gather  (cost=1000.00..21251.06 rows=60598 width=51)
         (actual time=0.648..385.081 rows=60311 loops=1)
   Workers Planned: 3
   Workers Launched: 3
   Buffers: shared hit=10414
   ->  Parallel Seq Scan on commandes
            (cost=0.00..14191.26 rows=19548 width=51)
            (actual time=0.334..370.751 rows=15078 loops=4)
         Filter: (commentaire ~~* 'qui%'::text)
         Rows Removed by Filter: 234922
         Buffers: shared hit=10195
 Planning time: 0.714 ms
 Execution time: 393.533 ms
(10 lignes)
```

L'index construit précédemment n'est pas utilisé.

  6. Créer un index `GIN` par trigramme pour des recherche textuelle sur la
  colonne _commentaire_ de la table _magasin.commandes_.

```sql
magasin=> CREATE INDEX idx_commandes_commentaire_gin ON magasin.commandes
   USING GIN (commentaire gin_trgm_ops);
CREATE INDEX
```

  7. Quel est la taille sur disque de la table _magasin.commandes_ ainsi que
  celle des index que l'on vient de créer ?

```sql
magasin=> SELECT
  pg_size_pretty(pg_total_relation_size('magasin.commandes')) total_size
  ,pg_size_pretty(pg_relation_size('magasin.commandes')) table_commandes
 ,pg_size_pretty(pg_relation_size('magasin.idx_commandes_commentaire_btree')) idx_btree
 ,pg_size_pretty(pg_relation_size('magasin.idx_commandes_commentaire_gin')) idx_gin;
 total_size | table_commandes | idx_btree | idx_gin 
------------+-----------------+-----------+---------
 208 MB     | 79 MB           | 46 MB     | 40 MB
(1 ligne)
```

  8. Relancer toutes les requêtes précédentes. Que remarquez-vous ?

```sql
magasin=> EXPLAIN (ANALYZE,BUFFERS) SELECT * FROM magasin.commandes
   WHERE commentaire LIKE '%qui';
                             QUERY PLAN 
----------------------------------------------------------------------------
 Bitmap Heap Scan on commandes
      (cost=475.36..11265.59 rows=50498 width=51)
      (actual time=59.380..136.851 rows=27866 loops=1)
   Recheck Cond: (commentaire ~~ '%qui'::text)
   Rows Removed by Index Recheck: 79357
   Heap Blocks: exact=10159
   Buffers: shared hit=10247
   ->  Bitmap Index Scan on idx_commandes_commentaire_gin
            (cost=0.00..462.74 rows=50498 width=0)
            (actual time=55.715..55.715 rows=107223 loops=1)
         Index Cond: (commentaire ~~ '%qui'::text)
         Buffers: shared hit=88
 Planning time: 0.381 ms
 Execution time: 139.196 ms
(10 lignes)
```

L'optimiseur fait le choix d'utiliser l'index GIN qui est moins performant que
l'index btree.

```sql
magasin=> EXPLAIN (ANALYZE,BUFFERS) SELECT * FROM magasin.commandes
  WHERE commentaire LIKE 'qui%';
                             QUERY PLAN 
----------------------------------------------------------------------------
 Bitmap Heap Scan on commandes  (cost=573.63..11490.11 rows=60598 width=51) (actual time=56.604..125.665 rows=60311 loops=1)
   Recheck Cond: (commentaire ~~ 'qui%'::text)
   Rows Removed by Index Recheck: 162216
   Heap Blocks: exact=10159
   Buffers: shared hit=10361
   ->  Bitmap Index Scan on idx_commandes_commentaire_gin  (cost=0.00..558.48 rows=60598 width=0) (actual time=54.436..54.436 rows=222527 loops=1)
         Index Cond: (commentaire ~~ 'qui%'::text)
         Buffers: shared hit=202
 Planning time: 0.185 ms
 Execution time: 128.731 ms
(10 lignes)
```

L'optimiseur fait le choix d'utiliser l'index GIN qui est un peu moins
performant que la scan séquentiel parallelisé.

```sql
magasin=> EXPLAIN (ANALYZE,BUFFERS) SELECT * FROM magasin.commandes
  WHERE commentaire LIKE '%qui%';
                             QUERY PLAN 
----------------------------------------------------------------------------
 Bitmap Heap Scan on commandes
      (cost=2984.72..17183.58 rows=323189 width=51)
      (actual time=36.787..126.013 rows=245799 loops=1)
   Recheck Cond: (commentaire ~~ '%qui%'::text)
   Heap Blocks: exact=10159
   Buffers: shared hit=10197
   ->  Bitmap Index Scan on idx_commandes_commentaire_gin
            (cost=0.00..2903.92 rows=323189 width=0)
            (actual time=34.788..34.788 rows=245799 loops=1)
         Index Cond: (commentaire ~~ '%qui%'::text)
         Buffers: shared hit=38
 Planning time: 0.145 ms
 Execution time: 138.501 ms
(9 lignes)

magasin=> EXPLAIN (ANALYZE,BUFFERS) SELECT * FROM magasin.commandes
  WHERE commentaire ILIKE 'qui%';
                             QUERY PLAN 
----------------------------------------------------------------------------
 Bitmap Heap Scan on commandes
      (cost=573.63..11490.11 rows=60598 width=51)
      (actual time=103.630..374.094 rows=60311 loops=1)
   Recheck Cond: (commentaire ~~* 'qui%'::text)
   Rows Removed by Index Recheck: 162216
   Heap Blocks: exact=10159
   Buffers: shared hit=10361
   ->  Bitmap Index Scan on idx_commandes_commentaire_gin
            (cost=0.00..558.48 rows=60598 width=0)
            (actual time=100.851..100.851 rows=222527 loops=1)
         Index Cond: (commentaire ~~* 'qui%'::text)
         Buffers: shared hit=202
 Planning time: 0.645 ms
 Execution time: 377.434 ms
(10 lignes)
```

Le choix de l'optimiseur est ici valide concernant les performances des requêtes
par rapport à une exécution parallelisée.

Le choix de l'optimiseur est cependant valide dans tous les cas. En effet, le
coût théorique est bien inférieur dans le cas de l'index GIN par
rapport à tous les autres opérations.

-------

#### Optimisation de requête

Soit la requête suivante :

```sql
EXPLAIN ANALYZE
SELECT
      m.annee||' - '||a.libelle AS millesime_region,
      SUM(s.nombre) AS contenants,
      SUM(s.nombre*c.contenance) AS litres
FROM
     contenant c
     JOIN stock s
       ON s.contenant_id = c.id
     JOIN (SELECT ROUND(RANDOM()*50)+1970 AS annee) m
       ON s.annee = m.annee
     JOIN vin v
       ON s.vin_id = v.id
     LEFT JOIN appellation a
       ON v.appellation_id = a.id
GROUP BY m.annee||' - '||a.libelle;
```

  1. Exécutez la requête et étudiez son plan d'exécution.
  2. Utilisez le site explain.depesz.com pour clarifier le plan d'exécution.
  3. Sans créer de nouvel index, optimisez cette requête pour diviser son temps
  d'exécution par 4 (3 optimisations possibles).

L'exécution de la requête donne le plan suivant, avec un temps qui peut varier
en fonction de la machine utilisée et de son activité:

```

HashAggregate  (cost=12763.56..12773.13 rows=319 width=32)
               (actual time=1542.472..1542.879 rows=319 loops=1)
   ->  Hash Left Join  (cost=184.59..12741.89 rows=2889 width=32)
                       (actual time=180.263..1520.812 rows=11334 loops=1)
         Hash Cond: (v.appellation_id = a.id)
         ->  Hash Join  (cost=174.42..12663.10 rows=2889 width=20)
                        (actual time=179.426..1473.270 rows=11334 loops=1)
               Hash Cond: (s.contenant_id = c.id)
               ->  Hash Join  (cost=173.37..12622.33 rows=2889 width=20)
                              (actual time=179.401..1446.687 rows=11334 loops=1)
                     Hash Cond: (s.vin_id = v.id)
                     ->  Hash Join  (cost=0.04..12391.22 rows=2889 width=20)
                            (actual time=164.388..1398.643 rows=11334 loops=1)
                           Hash Cond: ((s.annee)::double precision =
                                ((round((random() * 50::double precision)) +
                                1970::double precision)))
                           ->  Seq Scan on stock s
                                (cost=0.00..9472.86 rows=577886 width=16)
                                (actual time=0.003..684.039 rows=577886 loops=1)
                           ->  Hash  (cost=0.03..0.03 rows=1 width=8)
                                     (actual time=0.009..0.009 rows=1 loops=1)
                                 Buckets: 1024  Batches: 1  Memory Usage: 1kB
                                 ->  Result  (cost=0.00..0.02 rows=1 width=0)
                                     (actual time=0.005..0.006 rows=1 loops=1)
                     ->  Hash  (cost=97.59..97.59 rows=6059 width=8)
                               (actual time=14.987..14.987 rows=6059 loops=1)
                           Buckets: 1024  Batches: 1  Memory Usage: 237kB
                           ->  Seq Scan on vin v
                                (cost=0.00..97.59 rows=6059 width=8)
                                (actual time=0.009..7.413 rows=6059 loops=1)
               ->  Hash  (cost=1.02..1.02 rows=2 width=8)
                         (actual time=0.013..0.013 rows=2 loops=1)
                     Buckets: 1024  Batches: 1  Memory Usage: 1kB
                     ->  Seq Scan on contenant c
                            (cost=0.00..1.02 rows=2 width=8)
                            (actual time=0.003..0.005 rows=2 loops=1)
         ->  Hash  (cost=6.19..6.19 rows=319 width=20)
                   (actual time=0.806..0.806 rows=319 loops=1)
               Buckets: 1024  Batches: 1  Memory Usage: 17kB
               ->  Seq Scan on appellation a
                    (cost=0.00..6.19 rows=319 width=20)
                    (actual time=0.004..0.379 rows=319 loops=1)
 Planning time: 4.348 ms
 Execution time: 309.337 ms
(25 lignes)
```

L'index `idx_stock_annee` n'est pas utilisé.

Interdisons de faire les scans séquentiel à l'optimiseur :

```sql
SET enable_seqscan TO off;
```

Nous remarquons que le plan d'exécution est
encore pire :

```
 GroupAggregate  (cost=50217.50..50288.50 rows=319 width=48)
            (actual time=669.962..675.969 rows=319 loops=1)
  Group Key: ((((((round((random() * '50'::double precision))
          + '1970'::double precision)))::text || ' - '::text) || a.libelle))
  ->  Sort  (cost=50217.50..50228.27 rows=4308 width=40)
          (actual time=669.925..671.213 rows=16838 loops=1)
      Sort Key: ((((((round((random() * '50'::double precision))
            + '1970'::double precision)))::text || ' - '::text) || a.libelle))
      Sort Method: quicksort  Memory: 2106kB
      ->  Hash Left Join  (cost=398.75..49957.45 rows=4308 width=40)
                      (actual time=8.359..633.349 rows=16838 loops=1)
          Hash Cond: (v.appellation_id = a.id)
          ->  Hash Join  (cost=375.83..49832.22 rows=4308 width=20)
                         (actual time=7.880..603.339 rows=16838 loops=1)
              Hash Cond: (s.vin_id = v.id)
              ->  Nested Loop
                              (cost=0.60..49397.81 rows=4308 width=20)
                           (actual time=0.263..588.333 rows=16838 loops=1)
                  Join Filter: (s.contenant_id = c.id)
                  Rows Removed by Join Filter: 33676
                  ->  Hash Join
                         (cost=0.47..49191.77 rows=4308 width=20)
                         (actual time=0.243..571.664 rows=16838 loops=1)
                      Hash Cond:
                        ((s.annee)::double precision
                             = ((round((random() * '50'::double precision))
                                + '1970'::double precision)))
                      ->  Index Scan using stock_pkey on stock s
                            (cost=0.42..44840.59 rows=861611 width=16)
                            (actual time=0.038..430.953 rows=861611 loops=1)
                      ->  Hash
                            (cost=0.03..0.03 rows=1 width=8)
                            (actual time=0.023..0.023 rows=1 loops=1)
                          Buckets: 1024  Batches: 1  Memory Usage: 9kB
                          ->  Result
                                (cost=0.00..0.02 rows=1 width=8)
                                (actual time=0.016..0.017 rows=1 loops=1)
                  ->  Materialize
                         (cost=0.13..12.19 rows=3 width=8)
                         (actual time=0.000..0.000 rows=3 loops=16838)
                      ->  Index Scan using contenant_pkey on contenant c
                             (cost=0.13..12.18 rows=3 width=8)
                             (actual time=0.009..0.013 rows=3 loops=1)
              ->  Hash  (cost=299.35..299.35 rows=6071 width=8)
                            (actual time=7.589..7.589 rows=6071 loops=1)
                  Buckets: 8192  Batches: 1  Memory Usage: 302kB
                  ->  Index Scan using vin_pkey on vin v
                                (cost=0.28..299.35 rows=6071 width=8)
                                  (actual time=0.020..4.501 rows=6071 loops=1)
          ->  Hash  (cost=18.93..18.93 rows=319 width=20)
                      (actual time=0.440..0.440 rows=319 loops=1)
              Buckets: 1024  Batches: 1  Memory Usage: 25kB
              ->  Index Scan using appellation_pkey on appellation a
                           (cost=0.15..18.93 rows=319 width=20)
                            (actual time=0.018..0.259 rows=319 loops=1)
 Planning time: 1.701 ms
 Execution time: 676.301 ms
(28 lignes)
```

Que faire alors ?

Il convient d'autoriser à nouveau les scans séquentiel, puis, peut-être, de
réécrire la requête.

Nous réécrivons la requête comme suit :

```sql
EXPLAIN ANALYZE
SELECT
      s.annee||' - '||a.libelle AS millesime_region,
      sum(s.nombre) AS contenants,
      sum(s.nombre*c.contenance) AS litres
FROM
     contenant c
     JOIN stock s
       ON s.contenant_id = c.id
     JOIN vin v
       ON s.vin_id = v.id
     LEFT JOIN appellation a
       ON v.appellation_id = a.id
WHERE s.annee = (SELECT ROUND(RANDOM()*50)+1970 AS annee)
GROUP BY s.annee||' - '||a.libelle;
```

Il y a une jointure en moins, ce qui est toujours appréciable. Nous pouvons
faire cette réécriture parce que la requête
`SELECT ROUND(RANDOM()*50)+1970 AS annee` ne ramène qu'un seul enregistrement.

Voici le résultat :

```
 HashAggregate  (cost=12734.64..12737.10 rows=82 width=28)
                (actual time=265.899..266.317 rows=319 loops=1)
   InitPlan 1 (returns $0)
     ->  Result  (cost=0.00..0.02 rows=1 width=0)
                 (actual time=0.005..0.006 rows=1 loops=1)
   ->  Hash Left Join  (cost=184.55..12712.96 rows=2889 width=28)
                       (actual time=127.787..245.314 rows=11287 loops=1)
         Hash Cond: (v.appellation_id = a.id)
         ->  Hash Join  (cost=174.37..12634.17 rows=2889 width=16)
                        (actual time=126.950..208.077 rows=11287 loops=1)
               Hash Cond: (s.contenant_id = c.id)
               ->  Hash Join  (cost=173.33..12593.40 rows=2889 width=16)
                              (actual time=126.925..181.867 rows=11287 loops=1)
                     Hash Cond: (s.vin_id = v.id)
                     ->  Seq Scan on stock s
                            (cost=0.00..12362.29 rows=2889 width=16)
                            (actual time=112.101..135.932 rows=11287 loops=1)
                           Filter: ((annee)::double precision = $0)
                     ->  Hash  (cost=97.59..97.59 rows=6059 width=8)
                               (actual time=14.794..14.794 rows=6059 loops=1)
                           Buckets: 1024  Batches: 1  Memory Usage: 237kB
                           ->  Seq Scan on vin v
                                (cost=0.00..97.59 rows=6059 width=8)
                                (actual time=0.010..7.321 rows=6059 loops=1)
               ->  Hash  (cost=1.02..1.02 rows=2 width=8)
                         (actual time=0.013..0.013 rows=2 loops=1)
                     Buckets: 1024  Batches: 1  Memory Usage: 1kB
                     ->  Seq Scan on contenant c
                            (cost=0.00..1.02 rows=2 width=8)
                            (actual time=0.004..0.006 rows=2 loops=1)
         ->  Hash  (cost=6.19..6.19 rows=319 width=20)
                   (actual time=0.815..0.815 rows=319 loops=1)
               Buckets: 1024  Batches: 1  Memory Usage: 17kB
               ->  Seq Scan on appellation a
                    (cost=0.00..6.19 rows=319 width=20)
                    (actual time=0.004..0.387 rows=319 loops=1)
 Planning time: 1.385 ms
 Execution time: 232.251 ms
(21 lignes)
```

Nous sommes ainsi passés de 300 ms à 250 ms : la requête est donc
environ 20% plus rapide.

Que peut on conclure de cet exercice ?

  * que la création d'un index est une bonne idée ; cependant l'optimiseur peut
  ne pas l'utiliser, pour de bonnes raisons ;

  * qu'interdire les *seq scan* est toujours une mauvaise idée (ne présumez pas
  de votre supériorité sur l'optimiseur !)

-----


**Optimisation 2**

Voici la requête 2 telle que nous l'avons trouvé dans l'exercice précédent :


```sql
EXPLAIN ANALYZE
SELECT
      s.annee||' - '||a.libelle AS millesime_region,
      sum(s.nombre) AS contenants,
      sum(s.nombre*c.contenance) AS litres
FROM
     contenant c
     JOIN stock s
       ON s.contenant_id = c.id
     JOIN vin v
       ON s.vin_id = v.id
     LEFT JOIN appellation a
       ON v.appellation_id = a.id
WHERE s.annee = (SELECT ROUND(RANDOM()*50)+1970 AS annee)
GROUP BY s.annee||' - '||a.libelle;
```

On peut se demander si la jointure externe (LEFT JOIN) est fondée... On va donc
vérifier l'utilité de la ligne suivante :

```sql
vin v LEFT JOIN appellation a ON v.appellation_id = a.id
```

Cela se traduit par « récupérer tous les tuples de la table vin, et pour chaque
correspondance dans appellation, la récupérer, si elle existe ».

En regardant la description de la table `vin` (`\d vin` dans `psql`), on
remarque la contrainte de clé étrangère suivante :

```
« vin_appellation_id_fkey »
  FOREIGN KEY (appellation_id)
  REFERENCES appellation(id)
```

Cela veut dire qu'on a la certitude que pour chaque vin, si une référence à la
table appellation est présente, elle est nécessairement vérifiable.

De plus, on remarque :

```
appellation_id | integer | not null
```

Ce qui veut dire que la valeur de ce champ ne peut être nulle. Elle contient
donc obligatoirement une valeur qui est présente dans la table
`appellation`.

On peut vérifier au niveau des tuples en faisant un `COUNT(*)` du résultat,
une fois en `INNER JOIN` et une fois en `LEFT JOIN`. Si le résultat est
identique, la jointure externe ne sert à rien :

```sql
select count(*)
from vin v
  inner join appellation a on (v.appellation_id = a.id);

 count
-------
  6071
```



```sql
select count(*)
from vin v
  left join appellation a on (v.appellation_id = a.id);

 count
-------
  6071
```

On peut donc réécrire la requête 2 sans la jointure externe inutile,
comme on vient de le démontrer :

```sql
EXPLAIN ANALYZE
SELECT
      s.annee||' - '||a.libelle AS millesime_region,
      sum(s.nombre) AS contenants,
      sum(s.nombre*c.contenance) AS litres
FROM
     contenant c
     JOIN stock s
       ON s.contenant_id = c.id
     JOIN vin v
       ON s.vin_id = v.id
     JOIN appellation a
       ON v.appellation_id = a.id
WHERE s.annee = (SELECT ROUND(RANDOM()*50)+1970 AS annee)
GROUP BY s.annee||' - '||a.libelle;
```

Voici le résultat :

```
 HashAggregate  (cost=12734.64..12737.10 rows=82 width=28)
                (actual time=266.916..267.343 rows=319 loops=1)
   InitPlan 1 (returns $0)
     ->  Result  (cost=0.00..0.02 rows=1 width=0)
                 (actual time=0.005..0.006 rows=1 loops=1)
   ->  Hash Join  (cost=184.55..12712.96 rows=2889 width=28)
                  (actual time=118.759..246.391 rows=11299 loops=1)
         Hash Cond: (v.appellation_id = a.id)
         ->  Hash Join  (cost=174.37..12634.17 rows=2889 width=16)
                        (actual time=117.933..208.503 rows=11299 loops=1)
               Hash Cond: (s.contenant_id = c.id)
               ->  Hash Join  (cost=173.33..12593.40 rows=2889 width=16)
                              (actual time=117.914..182.501 rows=11299 loops=1)
                     Hash Cond: (s.vin_id = v.id)
                     ->  Seq Scan on stock s
                            (cost=0.00..12362.29 rows=2889 width=16)
                            (actual time=102.903..135.451 rows=11299 loops=1)
                           Filter: ((annee)::double precision = $0)
                     ->  Hash  (cost=97.59..97.59 rows=6059 width=8)
                               (actual time=14.979..14.979 rows=6059 loops=1)
                           Buckets: 1024  Batches: 1  Memory Usage: 237kB
                           ->  Seq Scan on vin v
                                (cost=0.00..97.59 rows=6059 width=8)
                                (actual time=0.010..7.387 rows=6059 loops=1)
               ->  Hash  (cost=1.02..1.02 rows=2 width=8)
                         (actual time=0.009..0.009 rows=2 loops=1)
                     Buckets: 1024  Batches: 1  Memory Usage: 1kB
                     ->  Seq Scan on contenant c
                            (cost=0.00..1.02 rows=2 width=8)
                            (actual time=0.002..0.004 rows=2 loops=1)
         ->  Hash  (cost=6.19..6.19 rows=319 width=20)
                   (actual time=0.802..0.802 rows=319 loops=1)
               Buckets: 1024  Batches: 1  Memory Usage: 17kB
               ->  Seq Scan on appellation a
                    (cost=0.00..6.19 rows=319 width=20)
                    (actual time=0.004..0.397 rows=319 loops=1)
 Planning time: 1.369 ms
 Execution time: 227.893 ms
(21 lignes)
```

Cette réécriture n'a pas d'effet sur le temps d'exécution de la requête
dans notre cas. Mais il est probable qu'avec des cardinalités différentes dans
la base, cette réécriture aurait eu un impact. Remplacer un `LEFT JOIN` par un
`JOIN` est le plus souvent intéressant, car il laisse davantage de liberté au
moteur sur le sens de planification des requêtes.

-----


**Optimisation 3**

Si on observe attentivement le plan, on constate qu'on a toujours le parcours
séquentiel de la table `stock`, qui est notre plus grosse table. Pourquoi
a-t-il lieu ?

Si on regarde le filtre (ligne `Filter`) du parcours de la table `stock`, on
constate qu'il est écrit :

```
  Filter: ((annee)::double precision = $0)
```

Ceci signifie que pour tous les enregistrements de la table, l'année est
convertie en nombre en double précision (un nombre à virgule flottante), afin
d'être comparé à $0, une valeur filtre appliquée à la table. Cette valeur est
le résultat du calcul :

```sql
select round(random()*50)+1970 as annee
```

comme indiquée par le début du plan (les lignes de l'initplan 1).

Pourquoi compare-t-il l'année, déclarée comme un entier (`integer`), en la
convertissant en un nombre à virgule flottante ?

Parce que la fonction `round()` retourne un nombre à virgule flottante. La
somme d'un nombre à virgule flottante et d'un entier est évidemment un nombre à
virgule flottante. Si on veut que la fonction `round()` retourne un entier, il
faut forcer explicitement sa conversion, via  `CAST(xxx as int)` ou `::int`.

Réécrivons encore une fois cette requête :

```sql
EXPLAIN ANALYZE
SELECT
      s.annee||' - '||a.libelle AS millesime_region,
      sum(s.nombre) AS contenants,
      sum(s.nombre*c.contenance) AS litres
FROM
     contenant c
     JOIN stock s
       ON s.contenant_id = c.id
     JOIN vin v
       ON s.vin_id = v.id
     JOIN appellation a
       ON v.appellation_id = a.id
WHERE s.annee = (SELECT (ROUND(RANDOM()*50)+1970)::int AS annee)
GROUP BY s.annee||' - '||a.libelle;
```

Voici son plan :

```
 HashAggregate  (cost=1251.12..1260.69 rows=319 width=28)
                (actual time=138.418..138.825 rows=319 loops=1)
   InitPlan 1 (returns $0)
     ->  Result  (cost=0.00..0.02 rows=1 width=0)
                 (actual time=0.005..0.006 rows=1 loops=1)
   ->  Hash Join  (cost=267.86..1166.13 rows=11329 width=28)
                  (actual time=31.108..118.193 rows=11389 loops=1)
         Hash Cond: (s.contenant_id = c.id)
         ->  Hash Join  (cost=266.82..896.02 rows=11329 width=28)
                        (actual time=31.071..80.980 rows=11389 loops=1)
               Hash Cond: (s.vin_id = v.id)
               ->  Index Scan using stock_annee on stock s
                    (cost=0.00..402.61 rows=11331 width=16)
                    (actual time=0.049..17.191 rows=11389 loops=1)
                     Index Cond: (annee = $0)
               ->  Hash  (cost=191.08..191.08 rows=6059 width=20)
                         (actual time=31.006..31.006 rows=6059 loops=1)
                     Buckets: 1024  Batches: 1  Memory Usage: 313kB
                     ->  Hash Join  (cost=10.18..191.08 rows=6059 width=20)
                                (actual time=0.814..22.856 rows=6059 loops=1)
                           Hash Cond: (v.appellation_id = a.id)
                           ->  Seq Scan on vin v
                                (cost=0.00..97.59 rows=6059 width=8)
                                (actual time=0.005..7.197 rows=6059 loops=1)
                           ->  Hash  (cost=6.19..6.19 rows=319 width=20)
                                     (actual time=0.800..0.800 rows=319 loops=1)
                                 Buckets: 1024  Batches: 1  Memory Usage: 17kB
                                 ->  Seq Scan on appellation a
                                    (cost=0.00..6.19 rows=319 width=20)
                                    (actual time=0.002..0.363 rows=319 loops=1)
         ->  Hash  (cost=1.02..1.02 rows=2 width=8)
                   (actual time=0.013..0.013 rows=2 loops=1)
               Buckets: 1024  Batches: 1  Memory Usage: 1kB
               ->  Seq Scan on contenant c  (cost=0.00..1.02 rows=2 width=8)
                                    (actual time=0.003..0.006 rows=2 loops=1)
 Planning time: 0.490 ms
 Execution time: 31.118 ms
(21 rows)
```

On constate qu'on utilise enfin l'index de `stock`. Le temps d'exécution a
encore été divisé par sept.

__NB :__ ce problème d'incohérence de type était la cause du plus gros
ralentissement de la requête. En reprenant la requête initiale, et en ajoutant
directement le cast, la requête s'exécute déjà en 80 millisecondes.

</div>

------
