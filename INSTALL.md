# INSTALL


## Debian / Ubuntu

1- Installer les outils de production (Attention: le meta-paquet texlive est monstrueux)

```
apt install pandoc texlive texlive-lang-french texlive-fonts-extra texlive-xetex
```

2- Récupérer les thèmes :

```
mkdir ../themes
cd ../themes
git clone https://github.com/hakimel/reveal.js.git
git clone https://github.com/Wandmalfarbe/pandoc-latex-template.git
```

**Attention** : texlive-fonts-extra va déclencher un déluge de paquets. Prévoir
2 Go d'espace libre. Si vous souhaitez simplement corriger une typo, il est
probablement plus simple et plus rapide d'éditer le projet via l'interface
Gitlab.

## Compilation

```
make
```
