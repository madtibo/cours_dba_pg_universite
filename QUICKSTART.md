
  * Quick Start
================================================================================


Étape 0 - Installer Docker
--------------------------------------------------------------------------------

https://docs.docker.com/engine/installation/

Étape 1 - Déployer les thèmes
--------------------------------------------------------------------------------

```
mkdir ../themes
cd ../themes
git clone https://github.com/hakimel/reveal.js.git
git clone https://github.com/Wandmalfarbe/pandoc-latex-template.git
```

Étape 2 - Compilation avec pandocker
--------------------------------------------------------------------------------

Pour produire les fichiers pdf, html et reveal  du module cours, faites :

```
make complet
```

Étape 3 - Enjoy !
--------------------------------------------------------------------------------

Votre fichier compilé se trouve dans le dossier `_build`


----

Comment ça marche ?
--------------------------------------------------------------------------------

### Reconstruire l'image en local

L'image `pandocker` est préchargée sur hub.docker.com. Mais
vous pouvez la compiler localement avec

```
git clone git@github.com:daamien/pandocker.git
cd pandocker
docker build .
```

Suivant votre machine et votre liaison internet cela peut prendre entre
10 et 15 minutes.

### Mettre à jour l'image

```
docker pull daamien/pandocker
```

### Nettoyer après usage

Lorsque vous n'avez plus besoin de l'image vous pouvez simplement la supprimer
avec `docker rmi daamien/dalidoc`


