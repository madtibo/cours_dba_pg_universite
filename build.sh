#!/bin/bash

REMOTE_PATH="/var/www/private/exports/formation/manuels"
REMOTE_STABLE_PATH="/var/www/static/exports/formation/manuels"

echo "Build ${BRANCH_NAME}"

make all

rc=$?

if [ $rc -ne 0 ] ; then

    echo "build failed!"  
    exit $rc

fi

echo "Build CONTRIBUTING.pdf"

pandoc CONTRIBUTING.md --latex-engine=xelatex -o CONTRIBUTING.pdf

rc=$?

if [ $rc -ne 0 ] ; then

    echo "Build CONTRIBUTING.pdf failed!"  
    exit $rc
fi

if [ $rc -ne 0 ] ; then

    echo "Error publish to owncloud.dalibo.info"
    exit $rc

fi


